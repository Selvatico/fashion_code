source 'https://rubygems.org'

gem 'pg'

gem 'rails', ['>= 3.2.12', '< 3.3']
gem 'haml-rails', '~> 0.3.5'
gem 'redis', '~> 3.0.2'
gem 'rabl', '0.7.9'
gem 'pg_search'

gem 'agent_orange'
# uploaders related gems
gem 'carrierwave', "~> 0.7.1"
gem "fog"

gem 'rmagick', '~> 2.13.1'
gem 'kaminari', '~> 0.14.1'
gem 'resque', '~> 1.23.0'
gem 'resque_mailer', '~> 2.2.2'
gem 'resque-scheduler', '~> 2.0.0'
gem 'acts-as-taggable-on'
gem 'twitter-text', '~> 1.5.0'
gem 'simple_form', '~> 2.0.4'
gem 'rails_config'
gem 'remotipart', '~> 1.0'

gem 'whenever', :require => false

gem 'requirejs-rails', git: 'git@github.com:jwhitley/requirejs-rails.git'
#, path: 'vendor/gems/requirejs-rails-0.9.1/'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails', '~> 3.2'
  gem "coffee-rails", "~> 3.2.2"

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

gem 'jquery-rails'
gem 'cocoon'

gem 'cancan'
gem 'devise'
gem 'omniauth', '~> 1.1.1'
gem 'omniauth-facebook', '~> 1.4.1'
gem 'omniauth-twitter',  '~> 0.0.14'
gem 'omniauth-instagram', '~> 1.0.0'
gem 'omniauth-flickr', '~> 0.0.11'
gem 'omniauth-tumblr', '~> 1.1'




#------- notification
gem 'private_pub'
gem 'thin'
gem 'rapns', '3.1.0'
#---------

#-------- social_networks
gem 'koala'
gem 'twitter'
gem 'tumblr_client'
gem 'flickraw'
gem 'google_plus'
gem 'instagram'
gem 'actionmailer'
#------------

gem 'unicorn'

#gem 'enumerated_attribute'
gem "enumerated_attribute", 
  :git =>"git@github.com:edave/enumerated_attribute.git", 
  :ref => "ec1021d11874"

gem 'rails_config'
gem 'seedbank'

group :development, :test do
  gem 'rspec-rails'
  gem 'mail_view', '~> 1.0.3'
end

group :development do
  gem 'capistrano'
  gem 'capistrano-rbenv'
  gem 'capistrano-resque'
end

group :test do
  gem 'spork-rails'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'simplecov', require: false
  gem "fakeredis", require: "fakeredis/rspec"
  gem 'resque_spec'
end

group :staging, :production do
  gem 'newrelic_rpm'	
end

gem 'pry-rails'