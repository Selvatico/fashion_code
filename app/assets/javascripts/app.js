define(function ( require ) {

	'use strict';

	// jQuery and plugins
	var $				= require( 'jquery' ),
		arcticmodal		= require( 'arcticmodal' ),
		moment			= require( 'moment' ),
        jpretext        = require( 'jpretext' );

	// Backbone and plugins
	var Backbone		= require( 'backbone' ),
		layoutmanager	= require( 'layoutmanager' ),
		routemanager	= require( 'routemanager' ),
		validation		= require( 'validation' ),
		backcookie		= require( 'backcookie' );

	// Other libs
	var uploader		= require( 'uploader' ),
		json2			= require( 'faye' );

	// Application
	var app = {
		root: '/',
		fileApiSupport: !!( window.File && window.FileReader && window.FileList )
	};

	// Mix Backbone.Events, modules, and layout management into the app object.
	return _.extend(app, {

		model : new Backbone.Model(),

		configure: function () {

			// Configure LayoutManager with Backbone Boilerplate defaults.
			Backbone.Layout.configure({
				// Allow LayoutManager to augment Backbone.View.prototype.
				manage: true,
				// Underscore templating
				fetch: function ( html ) {
					return _.template( html );
				}
			});

			Backbone.ajax = function( request ) {
				request.method   = request.type;
				request.callback = request.success || false;
				app.sendRequest( request.url, request );
			};


			app.$container = $('<div id="main-render-container"></div>');
			$('body').append(app.$container);

			/**
			 * Set up output format for moments.js function moment.fromNow();
			 * h - Hours
			 * min - minutes
			 * s - seconds
			 * y -years
			 * just now - <1 min
			 * m - month
			 */
			moment.lang('en', {
				relativeTime : {
					future: "in %s",
					past:   "%s ago",
					s:  "just now",
					m:  "1 min",
					mm: "%d min",
					h:  "1 h",
					hh: "%d h",
					d:  "1 d",
					dd: "%d d",
					M:  "1 m",
					MM: "%d m",
					y:  "1 y",
					yy: "%d y"
				}
			});

			// Global notifications
			window.notifications = _.clone( Backbone.Events );

		},
		/**
		 * Service method for receiving path for pics
		 */
		getURL: function () {
			var URL;
			switch ( app.config.get( 'ajaxTarget' ) ) {
				case 'local':
					URL = app.config.get( 'basePath' );
					break;
				case 'live':
					URL = '';
					break;
				default:
					URL = '';
			}
			return URL;
		},
		isMe: function (username) {
			return app.models.userModel.get( 'username' ) == username;
		},
		
		navigate: function ( destination, reload ) {
			( reload ) ? location.assign( destination ) : this.router.navigate( destination, { 'trigger' : true } );
		},

		/**
		 * Check if request with a blocking now
		 * @param key
		 * @param value
		 * @returns {boolean}
		 */
		checkBlockExist: function ( key, value ) {
			return app.model.get( key ) == value;
		},
		renderMenu: function (menu) {
			app.header = menu;
			$( 'body' ).prepend( menu.el );
			menu.render();
			$( 'body' ).on( 'click', function( e ){
				app.trigger( 'close:dropdown', e);
			} );
		},
		/**
		 * Service method to get Header element
		 * @returns {Backbone.View}
		 */
		getHeader : function () {
			return app.header;
		},
		renderPage: function ( page, target ) {

			var container = target ? $( target ) : app.$container;

			$( '.overlay.spinner' ).remove();

			if ( app.currentPage ) { app.removeViews( app.currentPage ); }
			app.trigger( 'show:submenu' );
			container.html( page.el );
			page.render();
			app.currentPage = page;
													 
			if ( page.linkClass ) {
				app.header.$el.find( '.active' ).removeClass( 'active' );
				app.header.$el.find( '.' + page.linkClass + '-link' ).addClass( 'active' );
			}
			
		},
		removeViews: function ( container ) {

			var subViews = container.getViews().value();

			if ( subViews ) {
				container.getViews().each(function ( view ) {
					app.removeViews( view );
				});
			} else {
				app.off( null, null, container );
				container.remove();
			}

		},
		/**
		 * One point for all ajax requests
		 * @param {String} path Request url
		 * @param {Object|jqXHR} options Settings of request
		 * @param {String} options.sendType In most cases for POST request API require JSON
		 * @param {String} options.contentType 'Content-Type' header
		 * @param {String} options.method Request type 'POST', 'GET', 'PUT', 'DELETE'
		 * @param {Object|String} options.data Original data object or stringifyed object
		 * @param {Function} options.callback Callback for success
		 * @param {Function} options.error Callback for error end of request
		 * @param {Object} options.context Content for success execution
		 * @param {Object} options.single If we can't run two identical requests at one time
		 */
		sendRequest: function ( path, options ) {

			var config    = this.config,
				data    = options.data || {},
				httpType  = options.method || 'GET',
				requestType = options.sendType || 'form',
				contentType = options.contentType || 'application/x-www-form-urlencoded; charset=UTF-8',
                no_redirect = options.no_redirect || false,
				url,
				blockKey,
				blockValue;

			//we on dev or on live
			if ( config.get('ajaxTarget') !== 'local' ) {
				url = path;
				//if server require json body of post request set right content type and stringify data
				if ( httpType == 'POST' && requestType == 'json' && _.isObject(data) ) {
					data = JSON.stringify( data );
					contentType = 'application/json';
				}
			} else {
				//construct request for proxy
				url  = "/ajax_request?realPath=" + path;
				data = {
					proxyParams : ( 'object' === typeof data ) ? JSON.stringify( data ) : data,
					formType  : ( 'object' === typeof options.data) ? 'form' : 'json',
					cookie    : Backbone.Cookie.getCookie( '_modera_web_new_session' )
				};
				contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
			}

			if ( options.single ) {
				blockKey = url;
				blockValue = ( 'string' == typeof data ) ? data : JSON.stringify( data );
				if ( app.checkBlockExist( blockKey, blockValue ) ) {
					return;
				} else {
					app.model.set( blockKey, blockValue );
				}
			}


			$.ajax({

				url  : url,

				data  : data,

				dataType: 'json',

				type  : httpType,

				contentType: contentType,

				beforeSend: function ( xhr ) {

					xhr.setRequestHeader( 'X-CSRF-Token', $( 'meta[name="csrf-token"]' ).attr( 'content' ) );

				},
				statusCode: {
					/*
					TODO:
					100 - error (for example, password is wrong - it should use functionality of error callback)
					401 - sign up (redirect with refresh page)
					200 - success (just FYI, don't use here, it is success callback)
					500 - server error (popup - not implemented yet, so just use alert or console.log) */
					100: function ( jqXHR ,  textStatus, errorThrown ) {
						if ( options.error ) {
							options.error( jqXHR, textStatus, errorThrown );
						}
					},
					302: function ( jqXHR ) {
						var data = JSON.parse( jqXHR.responseText );
						if ( data.setCookie ) {
							Backbone.Cookie.setCookieList( data.setCookie );
						}

						if ( data.csrf_token ) {
							$( 'meta[name="csrf-token"]' ).attr( 'content', data.csrf_token );
						}

						if ( data.redirect_uri && !no_redirect ) {
							Backbone.history.navigate( data.redirect_uri, { trigger: true } );
						}
					},
					401: function ( jqXHR ) {
						var data = JSON.parse( jqXHR.responseText );
						app.navigate( '/#signin', true );
					}
				},
				/**
				 *
				 * @param data Server or proxy data
				 * @param data.setCookie Cookie from remote server via proxy
				 * @param data.csrf_token new token which was sent by remore server
				 */
				success: function ( data ) {

					if ( data.setCookie ) {
						Backbone.Cookie.setCookieList( data.setCookie );
					}

					if ( data.csrf_token ) {
						$( 'meta[name="csrf-token"]' ).attr( 'content', data.csrf_token );
					}

					if ( options.callback ) {
						var context = options.context || window;
						options.callback.call( context, data );
					}

				},
				error: function( jqXHR ,  textStatus, errorThrown ) {
					// TODO: move options.error to statusCode 100, here just make console.log for now (unknown error - with unknown error code)
					if ( options.error ) {
						options.error( jqXHR, textStatus, errorThrown );
					}
				},
				complete: function ( jqXHR, textStatus ) {
					if ( options.complete) {
						var context = options.context || window;
						options.complete.call( context, jqXHR, textStatus );
					}

					if ( options.single && app.checkBlockExist( blockKey, blockValue ) ) {
						app.model.unset( blockKey );
					}
				}

			});

		}

	}, Backbone.Events);

});
