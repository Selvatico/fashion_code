/**
	* @exports ContestsCollection
	* @namespace app.collections
*/
define(function ( require ) {

	var app		= require( 'app' ),
		Contest	= require( 'models/ContestModel' );

	return Backbone.Collection.extend({

		model: Contest,

		initialize: function ( models, options ) {

			this.contestType = options.type;

			this.urlRoot = '/contests/list/' + this.contestType;

		},

		/**
			* Load from API list of contests and save it to collection
		*/
		fetch: function () {
			var self = this;
			app.sendRequest( this.urlRoot,
				{
					/**
					* Saves to colelction
					* @param {Array}data
					* @param {Array} data.contests List of contests
					*/
					callback: function ( data ) {
						if ( data.contests ) {
							self.add( data.contests );
							switch ( self.contestType ) {
								case 'joined':
									app.trigger( 'contests:loaded', data );
									break;
								case 'finished':
									app.trigger( 'finishedContests:loaded', data );
									break;
								case 'new':
									app.trigger( 'newContests:loaded', data );
									break;
								case 'started':
									app.trigger( 'startedContests:loaded', data );
									break;
							}
						} else {
							throw Error( 'Contests can be loaded' );
						}
					}
				}
			);
		}
	});
});