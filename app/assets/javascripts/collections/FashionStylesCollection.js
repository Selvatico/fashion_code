/**
	* @exports FashionStylesCollection
	* @namespace app.collections
*/
define([
	'app',
	'backbone',
	'../models/FashionStyle'
], function (
	app,
	Backbone,
	FashionStyle
	) {
	return Backbone.Collection.extend ( {

		url		: '/fashion_styles',

		model	: FashionStyle,

		/**
		* Load from API list of styels and save it to collection
		*/
		fashionList : function ( key ) {
			var _self = this,
				dataKey = key || 'fashion_styles';
			app.sendRequest ( this.url, {
				/**
				* Saves to colelction
				* @param {Array}data
				* @param {Array} data.fashion_styles List of styles
				*/
				callback : function ( data ) {
					if ( data [ dataKey ] ) {
						_self.add ( data[dataKey] );
						app.trigger ( 'fashion:loaded', data, _self );
					} else {
						throw Error ( 'Fashion styles can be loaded' );
					}
				}
			} );
		}
	});
});