/**
 * @exports LeaderBoardCollection
 * @namespace app.collections
 */
define([
	'app',
	'backbone',
	'../models/UserModel'
], function (
	app,
	Backbone,
	UserModel
	) {
	return Backbone.Collection.extend ( {
		model		: UserModel,
		initialize	: function ( models, options ) {
			if ( options && options.section ) {
				if ( options.section == 'contest' ) {
					this.url = '/contests/hall_of_fame.json';
				} else if ( options.section == 'hashtag' ) {
					this.url = '/hashtag_contests/' + options['hashTag'] +'/hall_of_fame.json';
				}
			}
		},
		load : function (params, callback) {
			var _self = this;
			app.sendRequest ( this.url, {
				callback : function ( data ) {
					if ( data ) {
						_self.add( data  );
					}
					_self.trigger ( 'leaders:loaded', data );
				},
				error : function () {
					_self.trigger ( 'leaders:loaded', [] );
				},
				data : params || {},
                single: true
			} );
		}
	});
});