/**
 * @exports NotificationsCollection
 * @namespace app.collections
 */
define( [
	'app', 'backbone'
], function ( app, Backbone ) {
	return Backbone.Collection.extend( {
		url: '/notifications.json',
		initialize: function ( models, options ) {

		},
		load: function ( params, callback ) {
			var _self = this;            
			app.sendRequest( this.url, {
				single:true,
				callback: function ( data ) {
					if ( data ) {
						_self.add( data );
					}
                    //YOU SHOULD CHECK IF WE LOADED SOMETHID BEFORE
                    if ( !data.length && _self.length == 0 ) {
                        _self.trigger( 'notify:empty' );
                    } else {
                        _self.trigger( 'notify:loaded', data );    
                    }
                      
					
				},
				error: function () {
					_self.trigger( 'notify:loaded', [] );
				},
				data: params || {}
			} );
		}
	} );
} );