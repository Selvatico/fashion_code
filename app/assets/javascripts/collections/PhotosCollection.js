/**
	* @exports PhotosCollection
	* @namespace app.collections
	*/
define([
	'app',
	'backbone',
	'models/PhotoModel'
], function (
		app,
		Backbone,
		Photo
) {
	return Backbone.Collection.extend ( {

		urlRoot : '/photos',
		method : 'GET',

		model : Photo,
		initialize: function (models, options) {
			if (options) {
				this.urlRoot = options.urlRoot || '/photos';
			}
		},
		/**
		* Load from API list of contests and save it to collection
		*/
		photoList : function ( map ) {//request: {  "page": номер страницы с фоторграфиями }
			var that = this;
			app.sendRequest ( this.urlRoot, {
				/**
				 * Saves to colelction
				 * @param {Array}data
				 * @param {Array} data.photos List of photos
				 */
				data		: map,
				method		: this.method,
				single		: true,
				callback	: function ( data ) {
					if ( data.photos ) {                        
						that.add ( data.photos );
						app.trigger ( 'photos:loaded', data, that );
						that.trigger ( 'photo:loaded', data, that );
					} else {
						throw Error ( 'Photos can be loaded' );
					}
				},
				error: function () {
					app.trigger( 'photos:loaded', {photos: []}, that );
				}
			} );
		}
	});
});