/**
	* @exports TrendingsCollection
	* @namespace app.collections
*/
define([
	'app',
	'backbone',
	'models/TrendingModel'
], function (
	app,
	Backbone,
	Trending
	) {
	return Backbone.Collection.extend ( {

		urlRoot	: '/hashtag_contests/trending.json',

		model	: Trending,

		TrendingsList : function () {
			var that = this;
			app.sendRequest ( this.urlRoot, {
				
				callback : function ( response ) {
					//console.log('response',response);
					if ( response.tags ) {
						that.add ( JSON.parse(response.tags) );
					} else {
						throw Error ( 'Trendings can be loaded' );
					}
				}
			} );
			
			return this;
		}
	});
});