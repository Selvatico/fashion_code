/**
	* @exports UsersCollection
	* @namespace app.collections
*/
define([
	'app',
	'backbone',
	'../models/UserModel'
], function (
	app,
	Backbone,
	UserModel
	) {
	return Backbone.Collection.extend ( {
		model		: UserModel,
		key			: '',
		initialize	: function ( models, options ) {
			if ( options && options.userUrl ) {
				this.url = options.userUrl;
			}
			//save key in response from where to retrieve data
			if ( options && options.key ) {
				this.key = options.key;
			}
		},
		load : function (params, callback) {
			var _self = this;
			app.sendRequest ( this.url, {
				callback : function ( data ) {
					if ( data [ _self.key ] ) {
						_self.add( data [ _self.key ] );
						if ( callback ) {
							callback ( _self, data );
						}
					}
					_self.trigger ( 'users:loaded', _self, data );
				},
				single:true,
				error : function () {
					_self.trigger ( 'users:loaded', _self, {} );
				},
				data : params || {}
			} );
		}
	});
});