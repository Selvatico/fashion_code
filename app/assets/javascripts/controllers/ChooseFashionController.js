/**
 * Define workflow for choose style page
 */
define ([
    'app'
], function (app) {

    return (function(){
        var controller = {
            saveStyle  : function ( id ) {
                app.sendRequest( '/choose_style', {
                    method      : 'POST',
                    sendType    : 'json',
                    data        : {
                        fashion_style_id : id
                    },
                    callback: function (data) {
                        app.models.userModel.set( 'fashion_id', id );
                        app.trigger( 'fashion:style:set', id );
						location.href = data.redirect_uri;
                    }
                });
            }
        };

        //subscribe to join button click
        app.on( 'join:style', controller.saveStyle, controller );

        return controller;
    })();
});