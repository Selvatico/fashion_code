/**
	* When we get javascript sdk from facebook and FB object is ready to use 
	* fire facebookLoaded on app object 
*/
define([
	'app',
], function(
	app
) {
	var _userData, _configData,
	FacebookController = function() {
		Priv.initialize();
		_userData = app.models.userModel;
		_configData = app.config;
	},
	Priv = {};

	Priv.initialize = function() {
		window.fbAsyncInit = function() {
			FB.init({
				appId		: _configData.get('fbAppId'), // app id
				status		: true, // true to fetch fresh status.
				cookie		: true, // true to enable cookie support.
				xfbml		: true, // true to parse XFBML tags.
				logging		: true, // false to disable logging.
				oauth		: true
			});

			FB.getLoginStatus(function( response ) {
				if ( response.status === 'connected' ) {

					_userData.set( 'facebookLoggedIn', true );

				} else if ( response.status === 'not_authorized' ) {

					_userData.set( 'facebookLoggedIn', false );

				} else {

					_userData.set( 'facebookLoggedIn', false );

				}
			});

			FB.XFBML.parse();
			app.trigger('facebookLoaded');
		};

		var js, id = 'facebook-jssdk', ref = document.getElementsByTagName('script')[0];
		if (document.getElementById(id)) {return;}
		js = document.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	};

	Priv.login = function( success, error ) {

		FB.login(function( response ) {
			if ( response.authResponse ) {
				// User is ingelogd
				_userData.set({"accessToken": FB.getAuthResponse()['accessToken'] });

				FB.api("/me", function( result ) {
					if( success ){ success( result ); }
				});
			} else {
				if( error ) { error(); }
			}
		}, { scope: _configData.get('facebookPermissions').join(', ') } );

	};
	/**
	 * Size can be small|normal|large|square
	 */
	Priv.getProfilePicture = function( size, callback ) {
		var size = ( size == null ) ? 'large' : size;
		FB.api('/me?fields=picture.type(' + size + ')', function( result ) {
			if ( callback ) callback( result );
		});
	};

	FacebookController.prototype.execute = function( name ) {
		return Priv[name] && Priv[name].apply( Priv, [].slice.call( arguments, 1 ) );
	}

	return FacebookController;
});