define([
	'app',
	'ig'
], function (
	app
	) {
	var _userData, _configData,
	InstagramController = function() {
		_userData = app.models.userModel;
		_configData = app.config;
		Priv.initialize();
	},
	Priv = {};

	Priv.initialize = function() {
		this.IG = new window.Instagram();
	}

	Priv.login = function() {
		this.IG.auth({
			client_id: _configData.get('igAppId'),
			redirect_uri: _configData.get('domain'),
			scope: _configData.get('igPermissions').join('+'),
			response_type: 'token'
		})
	}

	Priv.setAccessToken = function( accessToken ) {
		this.IG.setOptions({
			token: accessToken 
		});
	}

	Priv.getUserInfo = function( success ) {
		this.IG.currentUser( success );
	} 

	InstagramController.prototype.execute = function( name ) {
		return Priv[name] && Priv[name].apply( Priv, [].slice.call( arguments, 1 ) );
	}

	return InstagramController;
});