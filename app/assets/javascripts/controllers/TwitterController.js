define([
	'app'
], function(
	app 
) {
	var _userData, _configData,

	TwitterController = function() {
		Priv.initialize();
		_userData = app.models.userModel;
		_configData = app.config;
	},
	Priv = {};

	Priv.initialize = function() {

	}
	return TwitterController;
});