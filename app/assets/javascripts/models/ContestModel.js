/**
 * @exports Contest model
 * @namespace app.models
 */
define(function ( require ) {

	var app = require( 'app' );

	return Backbone.Model.extend({

		idAttribute: 'id',

		//urlRoot : '/contests/{contest_id}/join',
		method : 'POST',
		/**
		 * Load from API list of contests and save it to collection
		 */
		joinContest : function ( map, contestId ) {//request: { "photo_id": ID выбранной фотографии	}
			var self = this;
			app.sendRequest( '/contests/' + contestId + '/join', //this.urlRoot, 
				{
					/**
					 * Saves to colelction
					 * @param {Array}data
					 * @param {Array} data List of data
					 */
					//data : map,
					method: self.method,

					callback: function ( data ) {
						if ( data ) {
							app.trigger( 'contestJoined:loaded', data, self );
						} else {
							throw Error( 'Contest can be joined' );
						}
					}
				}
			);
		}

	});
});