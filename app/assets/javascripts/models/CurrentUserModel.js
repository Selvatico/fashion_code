define([
	'app',
	'backbone' 
], function (
	app,
	Backbone
) {
	return Backbone.Model.extend({
		defaults: {
			username: '',
			email: '',
			avatar: ''			
		},
		initialize: function () {},
		
		setSettings : function ( options ) {
			app.sendRequest( '/users', {
				method: 'POST',
				data : options.map,
				complete: options.success,
				single: true,
				context: this
			} );
		},

		checkToken: function ( options ) {
			app.sendRequest( '/password/edit', {
				//method: 'POST',
				data : options.data,
				complete: options.success,
				context: this
			} );
		}
	}); 
});