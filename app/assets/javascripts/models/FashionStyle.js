/**
	* @exports FashionStyle model
	* @namespace app.models
*/
define([
	'backbone'
], function (
	Backbone
) {
	return Backbone.Model.extend({
		idAttribute: 'id'
	});
});