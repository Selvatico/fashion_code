/**
 * @exports Contest model
 * @namespace app.models
 */
define([
    'app',
	'backbone'
], function (
	app,
	Backbone
	) {
	return Backbone.Model.extend({
		idAttribute: 'id',
		constructor: function ( rawData, options ) {
			if (rawData) {
				//rawData.rate = Math.abs(Math.floor(Math.random() * (1 - 100) + 1));
			}
			Backbone.Model.apply( this, [rawData, options] );
		},
		
		uploadPhoto : function ( options ) {
			app.sendRequest( '/photos', {
				method: 'POST',
				data : options.map,
				complete: options.success,
				single: true,
				context: this
			} );
		},
		
		deletePhoto : function ( options ) {
			app.sendRequest( '/photos/' + options.map.id, {
				method: 'delete',
				//data : options.map,
				complete: options.success,
				single: true,
				context: this
			} );
		}
	});
});