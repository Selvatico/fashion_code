/**
	* This models holds all data which we receive from server in tag <body> in data attribute.
	* Current known keys:
	* 		currentuser - Data about current user
	* 		powerhour	- Data  if member can create power hour and how many time left, if no
	* @export ServerDataModel
	* @namespace app.models
*/
define( [
	'backbone'
], function ( Backbone ) {
	return Backbone.Model.extend( {
		initialize: function () {
		},
		getCurrentUser: function () {
			return (this.get( 'currentuser' ) != 'undefined') ? this.get( 'currentuser' ) : {};
		}
	} );
} );