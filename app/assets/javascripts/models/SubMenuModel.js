/**
 * @exports NotificationsCollection
 * @namespace app.collections
 */
define( [
	'app', 'backbone'
], function ( app, Backbone ) {
	return Backbone.Model.extend( {
		url: '/notifications.json',
        defaults: {            
            following: 0,
            action: 0,
            comment: 0
        },	
		load: function () {
		    var self = this;
            app.sendRequest( '/notifications/stats', {
               method: 'GET',               
               callback: function( data ) {                                    
                  if ( !data.following && !data.action && !data.comment ) 
                     self.trigger('empty');
                                                          
                  self.set( data );      
               }
            });
		}
	} );
} );