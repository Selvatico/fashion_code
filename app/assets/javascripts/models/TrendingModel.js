/**
 * @exports Trending model
 * @namespace app.models
 */
define([
	'app',
	'backbone'
], function (
	app,
	Backbone
) {
	return Backbone.Model.extend({
		idAttribute: 'id',

		method : 'POST'
		
		
	});
});