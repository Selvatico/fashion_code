define( [
	'app',
	'backbone'
], function ( app, Backbone ) {
	return Backbone.Model.extend( {
		idAttribute: 'username',
		initialize: function () {
		},
		constructor: function ( rawData, options ) {
			if ( rawData.hasOwnProperty( 'is_following' ) || rawData.hasOwnProperty( 'followed_by_you' ) ) {
				rawData.followText = (rawData.is_following || rawData['followed_by_you']) ? 'Unfollow' : 'Follow';
				rawData.active = (rawData.is_following || rawData['followed_by_you']) ? 'active' : '';
			} else {
				rawData.followText = 'Follow';
				rawData.active = '';

			}
			Backbone.Model.apply( this, [rawData, options] );
		},
		initialize: function () {
			this.on( 'change:is_following', this.changeActive, this );
			this.on( 'change:followed_by_you', this.changeActive, this );
		},
		/**
		 * Construct follow url from current username
		 * @returns {string}
		 */
		getFollowUrl: function () {
			return '/profiles/' + this.get( 'username' ) + '/' + ((this.get( 'is_following' ) || this.get( 'followed_by_you' )) ? 'unfollow' : 'follow');
		},
		/**
		 * Return if member followed now
		 * @returns {string}
		 */
		getCurrentFollowStatus: function () {
			return ((this.get( 'is_following' ) || this.get( 'followed_by_you' )) ? 'follow' : 'unfollow');
		},
		/**
		 * Change text for button
		 */
		changeActive: function () {
			this.set( 'followText', (this.get( 'is_following' ) || this.get( 'followed_by_you' )) ? 'Unfollow' : 'Follow' );
			this.set( 'active', (this.get( 'is_following' ) || this.get( 'followed_by_you' )) ? 'active' : '' );
		},
		followMember: function ( app ) {
			app.sendRequest( this.getFollowUrl(), {
				method: 'POST',
				complete: function ( jqXHR ) {
					if ( jqXHR.status == 200 ) {
						this.set( 'is_following', !this.get( 'is_following' ) );
						var currentStatus = this.getCurrentFollowStatus();
						app.trigger( 'profile:' + currentStatus + ':' + app.models.userModel.get( 'username' ), currentStatus );
					}
				},
				single: true,
				context: this
			} );
		}
	} );
} );