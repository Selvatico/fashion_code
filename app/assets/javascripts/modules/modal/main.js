require([
	'app',
	'layoutmanager'
], function (
	app    
) {

	'use strict';

	var modalWindow = Backbone.Layout.extend({

		className: 'popup',

		initialize: function () {
			jQuery.arcticmodal('close');
			_.bindAll( this, 'onClose' );
		},

		beforeRender: function () {
			this.insertView( this.options.content );
			this.$el.prepend('<div class="close right arcticmodal-close">×</div>');
		},

		afterRender: function () {
			this.$el.arcticmodal({
				afterClose: this.onClose,
				overlay: {
				tpl: '<div class="overlay"></div>',
					css: {
						backgroundColor: '#f5f5f5',
						opacity: .85
					}
				}
			});
		},

		onClose: function () {
			this.options.content.remove();
			this.remove();
		}

	});

	app.on({

		'openModal': function ( view ) {
			var modal = new modalWindow( { content: view } );
			modal.$el.appendTo( 'body' );
			modal.render();
		}
	

	}); 

});