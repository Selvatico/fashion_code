define( [
    'app',
	'jquery'
], function ( app) {

	'use strict';
	return function ( event, selector, activeClass, context ) {
		var scroller = function scrollElements ( activeIndex, baseWidth, down ) {
  			var increment = (down) ? 1 : -1,
  				_self = context;
  			//animate all battles
			context.$( '.vs.abs.c' ).fadeOut(100);

  			_.each( context.$versuse, function stylesIterate ( $battle, index ) {
  				var currentMargin = parseInt( context.$( $battle ).css( 'margin-top' ).replace( 'px', '' ), 10 ),
  					marginDiff = ((down) ? -1 : 1) * context.$( $battle ).data( 'margin-offset' ),
  					dataMargin = context.$( $battle ).data( 'target-margin' ),
  					targetMargin = marginDiff + ((!!dataMargin) ? dataMargin : currentMargin),
  					animateParams = {
  						'margin-top': targetMargin + 'px'
  					};


  				//save in case new animation will start when current not finished
  				context.$( $battle ).data( 'target-margin', targetMargin );

  				//set to current active element more width and margin
  				if ( index == activeIndex ) {
  					animateParams.width = context.$( $battle ).data( 'width' ) + 'px';
  					animateParams['margin-left'] = context.$( $battle ).data( 'margin-left' ) + 'px';
  					_self.$( $battle ).find( '.info-bottom' ).fadeOut( 'fast' );
  				} else if ( index == (activeIndex + increment) ) {
  					//set to new active element base width and margin
  					animateParams.width = context.$( $battle ).data( 'base-width' ) + 'px';
  					animateParams['margin-left'] = context.$( $battle ).data( 'base-margin-left' ) + 'px';
  				}

				//if we go down and this current active element set special margin
  				if (down && index == activeIndex) {
  					animateParams['margin-top'] = context.$( $battle ).data( 'margit-top-hide' ) + 'px';
  					context.$( $battle ).data( 'first-top', true );
  				} else if (!down && index == (activeIndex - 2)) {
  					animateParams['margin-top'] = context.$( $battle ).data( 'margit-top-hide' ) + 'px';
  				}

				//animate one battle
  				context.$( $battle ).animate( animateParams, 500, function () {
					//if animation for last element finsihed. we say OK one animation step finsihed
  					if ( index == (activeIndex + increment) ) {
  						//show bottom panel for current active element
  						_self.$( $battle ).find( '.info-bottom' ).fadeIn( 'slow' );
						context.$( '.vs.abs.c' ).fadeIn( 'slow' );
						context.scrolling = false;
  					}
  				} );

  			}, context );
		};


		var activeIndex,
			diff,
			direction,
			$eventTarget = context.$( event.target ),
			$parents = $eventTarget.parents( '.list-comments, .voters-list' ),
            length;

		//if scroll on comments and voters list
		if ( $parents.length > 0 ) {
			var $content = $parents.find( '.content' );
			if ( $content.length > 0 ) {
				if ( $content.get( 0 ).clientHeight < $content.get( 0 ).scrollHeight ) {
					context.scrolling = false;
					return;
				}
			}

		}

		//if we scrolled in comments and coters child elements
		if ( context.$( event.target ).hasClass( 'list-comments' ) || context.$( event.target ).hasClass( 'voters-list' ) ) {
			if ( event.target.clientHeight < event.target.scrollHeight ) {
				context.scrolling = false;
				return;
			}
		}

		//if we didn't cached battle list
		if ( !context.$versuse ) {
			context.$versuse = context.$( selector );
		}

		var delta = event.originalEvent.wheelDeltaY || event.originalEvent.deltaY;

		//for IE and FF multypli delta
		if (jQuery.browser.msie || jQuery.browser.mozilla) {
			delta *= -1;
		}

		activeIndex = context.$versuse.index( context.$( '.' + activeClass ) ),
			length = context.$versuse.length;

		//if didn't find current active element
		if ( activeIndex == -1 ) {
			context.scrolling = false;
			return;
		}

		if ( activeIndex === 0 ) {
		    app.trigger( 'versusScroll:start' );
		} else if ( activeIndex === length - 1 ) {
		    app.trigger( 'versusScroll:stop' );
		} else if ( activeIndex === ( Math.floor ( length / 4 ) * 3 ) ) {
			app.trigger( 'versusScroll:nextPage' );
		}

		//we go donw
		if ( delta < 0 ) {
			//reached the end
			if ( activeIndex == (context.$versuse.length - 1) ) {
				context.scrolling = false;
				context.$versuse = false;
				return;
			}
			diff = 1;
			direction = true;
		} else if ( delta > 0 ) {
			if ( activeIndex === 0 ) {
				context.scrolling = false;
				return;
			}
			diff = -1;
			direction = false;
		}
		context.$versuse.removeClass( activeClass );
		context.$versuse.eq( activeIndex + diff ).addClass( activeClass );
	    scroller.call( context, activeIndex, context.$( '.' + activeClass ).data( 'width' ), direction );
	}
} );