define([
	'app',
	'widgets/menu/main',
	'text!./templates/layout.tpl',
	'./views/StylesList'
], function (
	app,
	HeaderView,
	chooseTemplate,
	StylesListView
) {

	'use strict';

	return Backbone.Layout.extend({

		$battles : false,
		className : 'page-ChooseStyle',
		template : chooseTemplate,
		scrolling: false,

		events : {
			'mousewheel .wrapper': 'scrollCatch',
			'wheel .wrapper': 'scrollCatch'
		},
		scrollCatch: function (event) {
			if ( !this.scrolling ) {
				this.scrolling = true;
				var baseWidth = 680,
					active = null,
					activeIndex = 0;

				if ( !this.$battles ) {
					this.$battles = this.$( '.style' );
				}

				var delta = event.originalEvent.wheelDeltaY || event.originalEvent.deltaY;
				activeIndex = this.$battles.index( this.$( '.activeStyle' ) );
				if ( activeIndex == -1 ) return;


				if ( delta < 0 ) {
					if ( activeIndex == (this.$battles.length - 1) ) {
						this.scrolling = false;
						return;
					}
					this.$battles.removeClass( 'activeStyle' );
					this.$battles.eq( activeIndex + 1 ).addClass( 'activeStyle' );
					this.scrollElements( activeIndex, baseWidth, true );
				} else if ( delta > 0 ) {
					if ( activeIndex === 0 ) {
						this.scrolling = false;
						return;
					}
					this.$battles.removeClass( 'activeStyle' );
					this.$battles.eq( activeIndex - 1 ).addClass( 'activeStyle' );
					this.scrollElements( activeIndex, baseWidth, false );
				}
			}
		},
		scrollElements: function (activeIndex, baseWidth, down) {
			var increment  = (down) ? 1 : -1,
					marginDiff = (down) ? -460 : 460,
				_self = this;
			_.each(this.$battles, function stylesIterate($style, index) {
				var currentMargin = parseInt(this.$($style).css('margin-top').replace('px', ''),10),
				dataMargin    = this.$($style).data('target-margin'),
				targetMargin  = marginDiff + ((!!dataMargin) ? dataMargin : currentMargin),
				animateParams = {
					'margin-top': targetMargin + 'px'
				};

				this.$($style).data('target-margin', targetMargin)

				if (index == activeIndex) {
					animateParams.width = '1200px';
					animateParams['margin-left'] = '-558px';
				} else if (index == (activeIndex + increment)) {
					animateParams.width = baseWidth + 'px';
					animateParams['margin-left'] = '-340px';
				}
				this.$($style).animate(animateParams, 500, function () {
					if (index == _self.$battles.length - 1) {
						_self.scrolling = false;
					}
				});

			}, this);
		},
		initialize: function () {

		},

		beforeRender: function () {
			$('body' ).css({overflow:'hidden'});
			this.insertView( '.wrapper', new StylesListView( { parent: this } ));
			//this.insertView( 'footer', new FooterView ( { parent : this } ) );
		}

	});

});