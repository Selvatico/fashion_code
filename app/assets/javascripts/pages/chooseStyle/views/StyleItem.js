/**
	* Defines holder for choose style slider
	* @exports StylesListView
*/
define([
	'app',
	'text!../templates/oneStyle.tpl'
], function (
	app,
	styleListTpl
) {

	'use strict';

	return Backbone.View.extend({

		className : 'style',
		template : styleListTpl,

		events : {
			'click .joinBtn' : 'joinBtn'
		},

		serialize : function () {
			this.model.set('tagsList', this.model.get('tags').split(','));
			var data = this.model.toJSON();
			return _.defaults(data,{'url' : app.getURL() });
		},
		joinBtn: function() {
			//say to controller to join style
			app.trigger( 'join:style', this.model.get('id') );
		},
		afterRender: function () {
			var _self  = this,
			index  = _self.model.get('index'),
			margin = ( (-240) + (index * 460));

			this.$el.
			data('original', margin)
			.css({
				'margin-top' : margin  + 'px',
				'width' : ((index == 0 ) ? 680  : 1200) + 'px',
				'margin-left': ((index == 0 ) ? -340 : -558) + 'px'
			})
			.addClass(((index == 0) ? 'activeStyle' : ''));

		}
	});

});