/**
	* Defines holder for choose style slider
	* @exports StylesListView
*/
define([
	'app',
	'text!../templates/stylesList.tpl',
	'../views/StyleItem',
	'scrollpath'
], function (
	app,
	styleListTpl,
	StyleItem,
	scrollpath
) {

	'use strict';

	return Backbone.View.extend({

		className : 'choose-style-list',
		template : styleListTpl,
		listCount : 0,


		/**
		 * Initiate load of colletion and subscribe to finish load event
		 */
		initialize : function () {
			app.on ( 'fashion:loaded', this.renderFashions, this );
			app.collections.fashionStyles.fashionList();
		},
		/**
		 * Render all fashions one by one
		 */
		renderFashions: function () {
			var _self = this,
			iterator = 0;

			app.collections.fashionStyles.each( function ( fashion ) {
				fashion.set( 'index', iterator );
				_self.setView( '.one-fashion', new StyleItem( {model: fashion} ), true).render();
				iterator++;
			});
			this.listCount = app.collections.fashionStyles.length;
			this.$( '.style:eq(0)').show();
		}
	});

});