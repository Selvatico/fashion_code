define( [
	'app',
	'backbone',
	'widgets/flameHall/main',
	'layoutmanager'
], function (app, Backbone, FlameHallWidget ) {

	'use strict';

	return Backbone.Layout.extend( {

		className : 'wrapper page-flameHall',
		linkClass : 'leader',
		events: {

		},
		initialize: function () {
			if (this.options.section == 'hashtag' && _.isEmpty(this.options.hashTag)) {
				//TODO: redirect to 404 page
			} else {
				this.mainLayout = new FlameHallWidget( { parent: this } );
			}
		},
		beforeRender: function () {
			this.insertViews( [this.mainLayout ] );
			//app.getHeader().renderHallMenu( this );
			app.getHeader().renderPhotoSubmenu();
			app.trigger( 'hide:submenu' );
			$( 'body' ).css( 'overflow', 'auto' );
		}
	});
} );