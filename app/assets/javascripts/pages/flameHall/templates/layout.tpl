<div class="item-row">
    <div class="npp h1 item-col">2</div>
    <div class="up-down up item-col">
        <i class="moderaIcons">)</i>
    </div>
    <div class="user item-col fs14">
        <a class="disblock rel" href="#" title="Marry Poppins">
            <img alt="Marry Poppins" class="userpic left" height="42" src="/assets/samples/username_pic.png" width="42">
            <span class="nowrap textOverhide">Marry Poppins</span>
        </a>
        <span class="nowrap thin">#Casual</span>
    </div>
    <div class="likes-count item-col">
        <i class="moderaIcons m">C</i>
        <span class="thin fs14">1500</span>
    </div>
    <div class="followers-count item-col fs14">
        <b>987</b>
        <span class="thin">Followers</span>
    </div>
    <div class="verified item-col">
        <i class="moderaIcons rel m">C</i>
        <span>verified</span>
    </div>
    <div class="prize item-col">
        <p class="thin nomrg left">
            <img alt="Contest name" class="left" height="46" src="/assets/samples/contest_photo.png" width="46">
            For
            <br>
            1
            <sup>st</sup>
            place
        </p>
        <p class="thin nomrg left">
            <img alt="Contest name" class="left" height="46" src="/assets/samples/contest_photo.png" width="46">
            For
            <br>
            2
            <sup>nd</sup>
            place
        </p>
    </div>
</div>