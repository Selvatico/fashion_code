define([
	'app',
	'backbone',
	'../views/ContestList',
	'../views/DetailedContestList',
	'../views/StartedContests',
	'../views/NewContests',
	'../views/FinishedContests',
	'text!../templates/layout.tpl',
	'layoutmanager'
	], function (
	app,
	Backbone,
	ContestListView,
	DetailedContestListView,
	StartedContestsView,
	NewContestsView,
	FinishedContests,
	layoutTemplate
) {

	'use strict';

	return Backbone.View.extend ( {
		className: 'wrapper',
		template : layoutTemplate,
		pageViews : [],
		data : null,

		initialize : function () {

			this.contestList			= new ContestListView( { parent: this, data: this.data } );
			this.detailedContestList	= new DetailedContestListView( { parent: this } );
			this.startedContests		= new StartedContestsView( { parent: this } );
			this.newContests			= new NewContestsView( { parent: this } );
			this.finishedContests		= new FinishedContests( { parent: this } );

			app.on( 'contests:loaded', this.renderContests, this );
			app.collections.contests.joined.fetch();
		},
		beforeRender : function () {

			//this.insertViews ('.container', [ this.contestList, this.detailedContestList, this.startedContests, this.newContests ] );

			this.insertView ( '.container', this.contestList );
			this.insertView ( '.container', this.detailedContestList );
			this.insertView ( '.container', this.startedContests );
			this.insertView ( '.container', this.newContests );
			this.insertView ( '.container', this.finishedContests );
		},

		renderContests : function ( data ) {
			this.data = data;
			this.contestList.setData( data );
			this.detailedContestList.setData( data );
		}
	});
});