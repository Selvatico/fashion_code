define([
	'app',
	'backbone',
	'./layouts/main',
	'text!./templates/layout.tpl',
	'nanoscroller',
	'layoutmanager'
], function (
	app,
	Backbone,
	LayoutMain,
	layoutTemplate
) {

	'use strict';

	return Backbone.Layout.extend ( {

		className : 'page-GetPrizes',
		pageViews : [],
		linkClass: 'prize',
		initialize : function () {
			app.on( 'contestJoined:loaded', this.refresh, this );
			app.on( 'newContests:loaded', this.setScroll, this );
			this.pageViews = [ new LayoutMain ( { parent : this } )];
		},

		beforeRender : function () {
			$( 'body' ).css ( { overflow : 'auto' } );
			this.insertViews ( this.pageViews );
			app.getHeader().renderHomeSubmenu();
		},
		
		setScroll : function (){
			
			this.$( '.wrapper' ).addClass ( 'nano' );
			var padding = parseInt ( this.$( '.container' ).css ( 'padding-top' ).replace ( 'px', '' ) );
			var height = $( 'body' ).height();
			
			this.$( '.nano' ).css ( { 'height' : height + 'px' } );
			this.$( '.nano' ).nanoScroller ( { scroll: 'top' } );
		},

		refresh : function ( data ) {
			$( '.arcticmodal-close' ).click();
			this.render();
		}
	} );
} );