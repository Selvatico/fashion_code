<section>
	<%if(data && data.length){%><h2 class='c'>Contests that will start</h2><%}%>
	<div class='contest-list view-list fs14'>
		<%for(var i in data){%>
		<div class='item no-joined-contest'>
			<div class='right'>
				<div class='r'>
					in <span class='h2'><%=data[i].difference%></span> days
				</div>
				<a href="#" class="btn active waiting-list" contestId="<%=data[i].id%>">Waiting List</a>
			</div>
			<img alt="name contest" class="left" src="<%=url%><%=data[i].prize%>" style="width: 180px; height: 180px;"/>
			<div class='h2'>#<%=data[i].name%></div>
			<p><%=data[i].description%></p>
		</div>
		<%}%>
	</div>
</section>
