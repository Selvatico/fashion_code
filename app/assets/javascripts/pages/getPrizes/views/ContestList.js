define([
	'app',
	'backbone',
	'text!../templates/contestList.tpl',
	'layoutmanager'
], function (
	app,
	Backbone, 
	template
) {

	'use strict';

	return Backbone.View.extend ( {
		className: 'list-joined-contest fs14',
		template : template,
		contests : {},
		
		events : {
			'click span' : 'selectContest'
		},

		setData : function (options) {
			this.contests = options;
			this.render();
		},
		
		serialize : function () {
			return { data : this.contests };
		},
		
		afterRender : function () {
			if(!this.contests || !this.contests.contests || !this.contests.contests.length) $('.joined').addClass('disnone');
			else $('.joined').removeClass('disnone');
		},
		
		selectContest : function (e) {
			var contestId = $( e.currentTarget ).attr ( 'contestId' );
			app.router.navigate ( '/contests/' + contestId + '/hall_of_fame', {trigger: true} );
		}
		
	});

});