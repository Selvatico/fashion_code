define([
	'app',
	'backbone',
	'text!../templates/detailedContestList.tpl',
	'layoutmanager'
], function (
	app,
	Backbone, 
	template
) {

	'use strict';

	return Backbone.View.extend ( {
		tagName : 'section',
		className: 'contest-list view-list fs14',
		template : template,
		contests : {},
		
		events : {
			//'click .see-hall-oflfame' : 'selectContest',
			//'click .jump-to-contest' : 'jumpToContest'
		},
		
		setData : function (options) {
			this.contests = options;
			
			this.render();
		},
		
		afterRender : function () {
			if(!this.contests || !this.contests.contests || !this.contests.contests.length) $('.joined').addClass('disnone');
		},
		
		serialize : function () {
			return { 'data' : this.contests, 'url' : app.getURL() };
		}//,
		
		/*selectContest : function (e) {
			var contestId = $( e.currentTarget ).attr ( 'contestId' );
			app.router.navigate ( '/contests/' + contestId + '/hall_of_fame', {trigger: true} );
			return false;
		},*/
		
		/*jumpToContest : function ( e ) {
			var contestId = $( e.currentTarget ).attr ( 'contestId' );
			//location.assign( '/contests/' + contestId );
			app.navigate( '/contests/' + contestId );
			return false;
		}*/
	});
});