define([
	'app',
	'backbone',
	'moment',
	'widgets/popUpPhotos/main',
	'models/ContestModel',
	'text!../templates/newContests.tpl',
	'layoutmanager'
], function (
	app,
	Backbone,
	moment,
	PopUpPhotos,
	ContestModel,
	template
) {

	'use strict';

	return Backbone.View.extend ( {
		tagName: 'section',
		template : template,
		
		events : {
			'click .waiting-list' : 'waitingList'
		},

		initialize : function () {
			app.on( 'newContests:loaded', this.setData, this );
		},
		
		setData : function (options) {

			for(var i in options.contests){
				var start = moment( options.contests[i].started_at, 'YYYY-MM-DD HH:mm:ssZ');
				var end = moment();
				
				var diff = end.diff(start, 'days');
				//console.log('options',start.format('YYYY-MM-DD HH:mm:ss'), ' - ', end.format('YYYY-MM-DD HH:mm:ss'), '-', diff);
				options.contests[i].difference = diff;
			}
			
			this.contests = options.contests;
			this.render();
		},
		
		serialize : function () {
			return { 'data' : this.contests, 'url' : app.getURL() };
		},
		
		waitingList : function (e) {
			var contestId = $( e.currentTarget ).attr ( 'contestId' );
			//location.assign( '/contests/' + contestId );*/
			var model = new ContestModel();
			model.joinContest( null, contestId );
			//app.trigger ( 'openModal', new PopUpPhotos ( { page: this, contestId : contestId } ) );
			
			return false;
		}
	});
});