define([
	'app',
	'backbone',
	'moment',
	'widgets/popUpPhotos/main',
	'models/ContestModel',
	'text!../templates/startedContests.tpl',
	'layoutmanager'
], function (
		app,
	Backbone,
	moment,
	PopUpPhotos,
	ContestModel,
	template
) {

	'use strict';

	return Backbone.View.extend ( {
		tagName: 'section',
		template : template,
		
		events : {
			'click .see-hall-oflfame' : 'selectContest',
			'click .jump-to-contest' : 'jumpToContest'
		},

		initialize : function () {
			app.on( 'startedContests:loaded', this.setData, this );
		},
		
		setData : function (options) {
			
			for(var i in options.contests){
				var formatString = 'YYYY-MM-DD HH:mm:ssZ';
				var start = moment( options.contests[i].started_at, formatString);
				var end = moment( options.contests[i].expired_at, formatString);
				
				var diff = end.diff(start, 'days');
				options.contests[i].difference = diff;
			}
			
			this.contests = options.contests;
			this.render();
		},
		
		serialize : function () {
			return { 'data' : this.contests, 'url' : app.getURL() };
		},
		
		selectContest : function (e) {
			var contestId = $( e.currentTarget ).attr ( 'contestId' );
			app.router.navigate ( '/contests/' + contestId + '/hall_of_fame', {trigger: true} );
			return false;
		},
		
		jumpToContest : function (e) {
			var contestId = $( e.currentTarget ).attr ( 'contestId' );
			/*location.assign( '/contests/' + contestId );*/
			var model = new ContestModel();
			model.joinContest( null, contestId );
			//app.trigger ( 'openModal', new PopUpPhotos ( { page: this, contestId : contestId } ) );
			
			return false;
		}
		
	});

});