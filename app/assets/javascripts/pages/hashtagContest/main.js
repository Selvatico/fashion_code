/**
 * Main layout for Hashtag contest page.
 * Renders to itself hash tag photos view and contests which belongs to particular hash tag.
 * Call menu widget to render specific submenu for this page.
 * @author dseredenko
 * @since 05.05.2013
 * @extends Backbone.Layout
 */
define( [
	'app',
	'widgets/menu/main',
	'widgets/hashtagPhotos/main',
	'widgets/hashtagContests/main',
	'text!./templates/layout.tpl'
], function ( app, HeaderView, HashTagPhotosView, HashTagContestsView, pageLayout ) {

	'use strict';

	return Backbone.Layout.extend( {
		className: 'page-hashtagContest',
		template : pageLayout,
		model	 : new Backbone.Model({page:1}),
		events   : {},
		initialize: function () {
			if (this.options.tagCName) {
				this.model.set( 'tagCName', this.options.tagCName );
				this.model.set( 'tab', this.options.tab );
			} else {
				//@todo throw 404 error
			}
			var urlPart = (this.options.tab == 'contests') ? 'versuses' : 'photos';
			app.sendRequest( '/hashtag_contests/' + this.options.tagCName +'/' + urlPart, {
				context: this,
				callback: function ( data ) {
					this.model.set( 'response', data );
					if ( data['owner'] != null ) {
						app.getHeader().renderHashTagMenu( this );
					} else {
						app.trigger( 'hide:submenu' );
					}
					if (this.options.tab == 'photos') {
						this.insertView( '.wrapper', new HashTagPhotosView( {parent: this, data: data[urlPart], tagCName: this.options.tagCName} ) ).render();
					} else {
						this.insertView( '.wrapper', new HashTagContestsView( {parent: this, data: data[urlPart], tagCName: this.options.tagCName} ) ).render();
					}
				},
				error: function () {
					//@todo throw 404 error
				}
			} );
			$( window ).off( 'scroll');
   
		},
		beforeRender: function () {

		},
		afterRender: function () {
			if (this.options.tab == 'photos') {
				$( 'body' ).css( {overflow: 'auto'} )
			} else {
				$( 'body' ).css( {overflow: 'hidden'} )
			}

		}
	} );

} );