define( [
	'app',
	'backbone',
	'widgets/versus/main',
	'text!../templates/layout.tpl',
	'modules/versusScroller/main',
	'layoutmanager'
], function (app, Backbone, VersusWidget, layoutTemplate, versusScroller ) {

	'use strict';

	return Backbone.Layout.extend( {
		className: 'wrapper',
		template: layoutTemplate,
		scrolling: false,

		events: {
			'mousewheel .battle-list': 'scrollCatch',
			'wheel .battle-list': 'scrollCatch'
		},
		$battles: false,

		initialize: function () {
			this.versusWidget = new VersusWidget( { parent: this } );
			app.on( 'move:next:versus', this.scrollCatch, this );
			app.on( 'versus:list:loaded', this.clearCacheList, this );
		},
		beforeRender: function () {
			this.insertView( '.battle-list', this.versusWidget );
		},
		scrollCatch: function ( event ) {
			if(!event.originalEvent) return;
			var delta = event.originalEvent.wheelDeltaY || event.originalEvent.deltaY;
			if( delta < -120 || delta > 120 ) {
				return;
			}
			if ( delta )
				if (!this.scrolling) {
					//this.$( '.vs.abs.c' ).fadeOut(100);
					this.scrolling = true;
					versusScroller( event, '.battle', 'activeBattle', this );
				}
		},
		clearCacheList: function () {
			this.$versuse = false;
		}
	} );

} );
