define( [
	'app',
	'backbone',
	'widgets/menu/main',
	'./views/MojoView',
	'./views/TrendingTagsView',
	'./layouts/main',
	'layoutmanager'
], function ( app, Backbone, HeaderView, MojoView, TrendingTagsView, MainLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'page-Home',
		linkClass: 'home',
		
		initialize: function () {
			this.mainLayout = new MainLayout( { parent: this } );
			this.mojoView = new MojoView( { parent: this } );            
		},
		beforeRender: function () {
			$('body' ).css({overflow:'hidden'});
			this.insertViews( [ /*this.headerView,*/ this.mainLayout, this.mojoView ] );
			this.insertView('.trending-tags', new TrendingTagsView( { parent : this} ) );
			app.getHeader().renderHomeSubmenu();
		},
		
		msg : function (){
			app.trigger ( 'show:message', { 'text' : 'Success!', 'type' : 'success' } );
		},
        afterRender: function ( event ) {
        }
	} );
} );