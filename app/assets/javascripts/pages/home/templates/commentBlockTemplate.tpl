        <div class='disnone'>
          <span class='asLink right'>See previous</span>
          <h3>
            Comments
            <span>1022</span>
          </h3>
          <div class='list-comments'>
            <div class='comment-item'>
              <div class='time right'>2h</div>
              <a href='#'>
                <img alt="Mary Poppins" class="userpic left" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                <b>Mary Poppins</b>
                <br>
                <span>Lorem Ipsum set amet comment</span>
              </a>
            </div>
            <div class='comment-item'>
              <div class='time right'>2h</div>
              <a href='#'>
                <img alt="Mary Poppins" class="userpic left" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                <b>Mary Poppins</b>
                <br>
                <span>Lorem Ipsum set amet comment</span>
              </a>
            </div>
            <div class='comment-item'>
              <div class='time right'>2h</div>
              <a href='#'>
                <img alt="Mary Poppins" class="userpic left" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                <b>Mary Poppins</b>
                <br>
                <span>Lorem Ipsum set amet comment</span>
              </a>
            </div>
            <div class='comment-item'>
              <div class='time right'>2h</div>
              <a href='#'>
                <img alt="Mary Poppins" class="userpic left" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                <b>Mary Poppins</b>
                <br>
                <span>Lorem Ipsum set amet comment</span>
              </a>
            </div>
            <div class='comment-item'>
              <div class='time right'>2h</div>
              <a href='#'>
                <img alt="Mary Poppins" class="userpic left" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                <b>Mary Poppins</b>
                <br>
                <span>Lorem Ipsum set amet comment</span>
              </a>
            </div>
            <div class='comment-item'>
              <div class='time right'>2h</div>
              <a href='#'>
                <img alt="Mary Poppins" class="userpic left" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                <b>Mary Poppins</b>
                <br>
                <span>Lorem Ipsum set amet comment</span>
              </a>
            </div>
          </div>
        </div>
        <!-- / only if hasn't comments -->
        <div class='faceplate c'>
          <i class='moderaIcons'>H</i>
          <span class='thin'>Be the first to comment</span>
        </div>
        <form action='#' class='form-comment rel'>
          <h3>Leave Comment</h3>
          <img alt="Mary Poppins" class="userpic left m" height="42" src="<%=url%>assets/samples/username_pic.png" width="42" />
          <input class="left" id="comment" name="comment" placeholder="@Katy Franklin" type="text" />
          <input class="btn left" name="commit" type="submit" value="Post" />
          <div class='comment-mention abs disnone'>
            <ul class='search-list-mention'>
              <li>
                <a class='textOverhide' href='#' title='Katy Franklin'>
                  <img alt="Katy Franklin" class="userpic" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                  <span>Katy Franklin</span>
                </a>
              </li>
              <li>
                <a class='textOverhide' href='#' title='Katy Franklin'>
                  <img alt="Katy Franklin" class="userpic" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                  <span>Katy Franklin</span>
                </a>
              </li>
              <li>
                <a class='textOverhide' href='#' title='Katy Franklin'>
                  <img alt="Katy Franklin" class="userpic" height="30" src="<%=url%>assets/samples/username_pic.png" width="30" />
                  <span>Katy Franklin</span>
                </a>
              </li>
            </ul>
          </div>
        </form>  