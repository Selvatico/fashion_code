<!-- / open -->
<div class='mojo disnone'>
  <div class='width'>
    <div class='mojo-tongue abs c'>
      <i class='moderaIcons b'>W</i>
    </div>
    <div class='mojo-load left'>
      <div class='left'>
        <i class='moderaIcons'>C</i>
        <br>
        <b>Mojo</b>
      </div>
      <span class='disinbl asLink textUnderline'>Mojo List</span>
      <div class='bar left'>
        <div class='progress' style='width: 20%'></div>
      </div>
    </div>
    <div class='mojo-message left c'>
      <i class='moderaIcons'>D</i>
      You got 540 votes on
      <b>Red Bull Lifestyle Contest</b>
    </div>
    <div class='mojo-action left'>
      <div class='go-mojo-list left asLink'>
        <i class='moderaIcons m'>C</i>
        <span>Mojo List</span>
      </div>
      <div class='mojo-share left asLink'>
        <i class='moderaIcons m'>I</i>
        <span>Share</span>
      </div>
      <div class='mojo-skip left asLink'>
        <i class='moderaIcons b'>K</i>
        <span>Skip</span>
      </div>
    </div>
  </div>
</div>
<!-- / closed -->
<div class='mojo closed disnone'>
  <div class='mojo-tongue abs c'>
    <i class='moderaIcons b'>V</i>
  </div>
  <div class='mojo-load'>
    <div class='left'>
      <i class='moderaIcons'>C</i>
      <br>
      <b>Mojo</b>
    </div>
    <div class='bar left'>
      <div class='progress' style='width: 20%'></div>
    </div>
  </div>
</div>
<!-- / mojo-list -->
<div class='mojo above disnone'>
  <div class='width'>
    <div class='mojo-tongue abs c'>&times;</div>
    <div class='mojo-load'>
      <div class='h1 right r'>35%</div>
      <div class='left'>
        <i class='moderaIcons'>C</i>
        <br>
        <b>Mojo</b>
      </div>
      <div class='bar left'>
        <div class='progress' style='width: 35%'></div>
      </div>
      <div class='fs14 clear r'>You can do better</div>
    </div>
    <div class='mojo-list-container rel'>
      <ul>
        <li class='item'>
          <div class='mojo-close right curpoint'>Later</div>
          <div class='btn right'>Start</div>
          <i class='moderaIcons m right'>D</i>
          <div class='mojo-vote left'>
            <b class='disinbl'>10</b>
            vote
          </div>
          <p>
            Upload your first photo -
            <b>10</b>
            votes
          </p>
        </li>
        <li class='item'>
          <div class='mojo-close right curpoint'>Later</div>
          <div class='btn right'>Start</div>
          <i class='moderaIcons m right'>B</i>
          <div class='mojo-vote left'>
            <b class='disinbl'>10</b>
            vote
          </div>
          <p>
            Connect you social networks -
            <b>5</b>
            votes for each
          </p>
        </li>
        <li class='item'>
          <div class='mojo-close right curpoint'>Later</div>
          <div class='btn right'>Start</div>
          <i class='moderaIcons invite right'>H</i>
          <div class='mojo-vote left'>
            <b class='disinbl'>10</b>
            vote
          </div>
          <p>
            Invite your Facebook Friends -
            <b>5</b>
            votes per 50 invites
          </p>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- / mojo-popup -->
<div class='overlay disnone'>
  <div class='mojo-popup'>
    <h1 class='c'>Congratulations</h1>
    <div class='mojo-card c'>
      <div class='mojo-vote'>
        <b class='disblock'>20</b>
        <span class='textTransUpper'>votes</span>
      </div>
      <div class='h1'>Mojo Card</div>
      <p class='c'>
        Great Start! Don't stop now!
        <br>
        Let everyone know who you are!
      </p>
      <div class='btn active'>Spend</div>
    </div>
  </div>
</div>
<!-- / mojo-notifications -->
<div class='mojo disnone'>
  <div class='mojo-notification c'>
    <div class='mojo-message disinbl'>
      <div class='mojo-vote right'>
        <b class='disinbl rel fs14'>10</b>
        points
      </div>
      <i class='moderaIcons'>D</i>
      You got 540 votes on
      <b>Red Bull Lifestyle Contest</b>
    </div>
    <div class='mojo-action disinbl'>
      <div class='mojo-share asLink left'>
        <i class='moderaIcons m'>I</i>
        <span>Share</span>
      </div>
    </div>
  </div>
</div>