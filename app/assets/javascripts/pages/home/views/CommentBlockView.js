define([
	'app',
	'text!../templates/commentBlockTemplate.tpl'
], function (
	app,
	commentBlockTemplate
) {

	'use strict';

	return Backbone.View.extend({

		template: commentBlockTemplate,

		className: 'comment-block right',

		initialize: function() {
			this.parentView = this.options.parent;
		},
		
		serialize : function () {
			return { 'url' : app.getURL() };
		}
	});

});