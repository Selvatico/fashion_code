define([
	'app',
	'text!../templates/mojoTemplate.tpl'    
], function (
	app,
	mojoTemplate
) {

	'use strict';

	return Backbone.View.extend({
		template: mojoTemplate,
		initialize: function() {
			this.parentView = this.options.parent;
		}
	});

});