define([
	'app',
	'text!../templates/participantLeftTemplate.tpl'
], function (
	app,
	participantLeftTemplate
) {

	'use strict';

	return Backbone.View.extend({

		template: participantLeftTemplate,

		className: 'participant left',

		initialize: function() {
			this.parentView = this.options.parent;
		}
	});

});