define([
	'app',
	'text!../templates/participantRightTemplate.tpl'    
], function (
	app,
	participantRightTemplate
) {

	'use strict';

	return Backbone.View.extend({
		template: participantRightTemplate,
		className: 'participant right',
		initialize: function() {
			this.parentView = this.options.parent;
		}
	});

});