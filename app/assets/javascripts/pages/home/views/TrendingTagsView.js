define([
	'app',
	'collections/TrendingsCollection',
	'text!../templates/trendingTags.tpl'
], function (
	app,
	TrendingsCollection,
	trendingTags
) {

	'use strict';

	return Backbone.View.extend({

		template: trendingTags,

		initialize: function() {
			this.parentView = this.options.parent;
			this.collection = new TrendingsCollection().TrendingsList().on('add',this.render,this);
		},
		
		serialize : function () {
			return { 'url' : app.getURL(), 'tags' : this.collection.toJSON() };
		}
	});

});