define([
	'backbone',
	'widgets/menu/main',
	'widgets/inviteFriends/main',
	'./views/InviteFriends',
], function (
	Backbone,
	HeaderView,
	inviteFriendsWidget,
	InviteFriends
) {

	'use strict';

	return Backbone.Layout.extend({
		className: 'page-inviteFriends',
		facebookInviteFriends: false,
		initialize: function () {
			//this.headerView = new HeaderView( { parent: this } );
			this.inviteFriends = new InviteFriends( { parent: this } );
			this.inviteFriendsWidget = new inviteFriendsWidget( { page: this } );

			this.inviteFriends.on('inviteFacebookFriends', this.inviteFacebookFriends, this);
		},

		beforeRender: function () {
			//this.insertView( this.headerView );
			if ( this.facebookInviteFriends )
				this.insertView( this.inviteFriendsWidget );
			else
				this.insertView( this.inviteFriends );
		},

		inviteFacebookFriends: function() {
			this.facebookInviteFriends = true;
			this.render();
		}
	});

});