    <div class='wrapper'>
      <!-- / step 1 -->
      <div class='invite fixed c'>
        <div class='container'>
          <h2>Invite Friends</h2>
          <div class='invite-form'>
            <div class='inviteFacebook c'>
              <i class='moderaIcons left'>f</i>
              <span>Invite friends from Facebook</span>
            </div>
          </div>
          <div class='btn skip'>Skip</div>
        </div>
      </div>
    </div>