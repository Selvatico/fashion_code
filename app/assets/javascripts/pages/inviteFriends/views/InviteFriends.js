define([
	'app',
	'text!../templates/inviteFriendsTemplate.tpl',    
], function (
	app,
	inviteFriendsTemplate
) {

	'use strict';

	return Backbone.View.extend({
		template: inviteFriendsTemplate,
		events: {
			'click .inviteFacebook': 'inviteFacebookFriedns',
			'click .skip': 'skip'
		},
		initialize: function() {
			this.parentView = this.options.parent;  
		},
		inviteFacebookFriedns: function( e ) {
			this.trigger('inviteFacebookFriends');
		},
		skip: function( e ) {
			e.preventDefault();
			Backbone.history.navigate('home', { trigger: true });
		}
	});

});