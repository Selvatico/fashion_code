/**
 * Main Notifications page layout
 * @exports NotificationsPage
 */
define( [
	'backbone', 'widgets/notifications/main', 'layoutmanager', 'app'
], function ( Backbone, NotificationsWidget, layoutTemplate, app ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'wrapper page-Notifications',
		linkClass : 'notify',
		initialize: function () {			
            this.mainLayout = new NotificationsWidget( {parent: this, type: this.options.type} );
            app.getHeader().notificationModel.on('change', this.notificationReady, this );
		},
		beforeRender: function () {
			this.insertViews( [ this.mainLayout ] );
			app.getHeader().renderNotifySubmenu( this );
			$( 'body' ).css( 'overflow', 'auto' );
		},
        notificationReady: function() {
            app.getHeader().renderNotifySubmenu( this );
        }
	} );

} );