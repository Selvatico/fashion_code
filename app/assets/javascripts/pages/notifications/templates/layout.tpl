    <!-- / open -->
    <div class='mojo disnone'>
      <div class='width'>
        <div class='mojo-tongue abs c'>
          <i class='moderaIcons b'>W</i>
        </div>
        <div class='mojo-load left'>
          <div class='left'>
            <i class='moderaIcons'>C</i>
            <br>
            <b>Mojo</b>
          </div>
          <div class='bar left'>
            <div class='progress' style='width: 20%'></div>
          </div>
        </div>
        <div class='mojo-message left c'>
          <i class='moderaIcons'>D</i>
          You got 540 votes on
          <b>Red Bull Lifestyle Contest</b>
        </div>
        <div class='mojo-action left'>
          <div class='mojo-share left asLink'>
            <i class='moderaIcons m'>I</i>
            <span>Share</span>
          </div>
          <div class='mojo-skip left asLink'>
            <i class='moderaIcons b'>K</i>
            <span>Skip</span>
          </div>
        </div>
      </div>
    </div>
    <!-- / closed -->
    <div class='mojo closed'>
      <div class='mojo-tongue abs c'>
        <i class='moderaIcons b'>V</i>
      </div>
      <div class='mojo-load'>
        <div class='left'>
          <i class='moderaIcons'>C</i>
          <br>
          <b>Mojo</b>
        </div>
        <div class='bar left'>
          <div class='progress' style='width: 20%'></div>
        </div>
      </div>
    </div>