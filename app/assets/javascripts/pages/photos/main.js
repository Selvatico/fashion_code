define([
	'backbone',
	'app',
	'jcrop',
	'widgets/menu/main',
	'./views/ChooseFile',
	'./views/Crop',
	'./views/Spread',
	'models/PhotoModel',
	'./models/CommentModel',
	'text!./templates/layout.tpl',
	'layoutmanager'
	], function (
	Backbone,
	app,
	Jcrop,
	HeaderView,
	ChooseFileView,
	CropView,
	SpreadView,
	PhotoModel,
	CommentModel,
	layoutTemplate,
	layoutmanager
) {

	'use strict';

	return Backbone.Layout.extend ( {

		className : 'page-Photo',
		template : layoutTemplate,
		pageViews : [],
		prevView : 0,
		currentView : 1,
		image : null,
		linkClass: 'photo',
		croppedCoords: null,
		picData: new Backbone.Model(),

		events : {
			'click .upload-photo .btn.active' : 'click',
			'click .crop-photo .btn.active' : 'crop',
			'click .crop-photo .btn.cancel' : 'cropCancel',
			'click .share-photo-page .btn.active' : 'crop',
			'click .share-photo-page .btn.cancel' : 'cropCancel',
			'click .done' : 'upload',
			'change #uploadPhoto' : 'fileChoosed'
		},

		initialize : function () {
			this.cropView = new CropView( { parent: this, img: this.img } );
			this.spreadView = new SpreadView( { parent: this } );
			this.pageViews = [ new ChooseFileView( { parent: this } ), this.cropView, this.spreadView ];
			app.on( 'photo:uploaded', this.fileChoosed, this );
			app.on( 'photo:croped', this.croped, this );
		},

		beforeRender : function () {
			this.insertViews ( this.pageViews );
			app.getHeader().renderPhotoSubmenu();
			app.trigger( 'hide:submenu' );
		},
		
		afterRender : function () {
			// $('body').css( { 'overflow' : 'hidden' } );
			// this.$('.upload-photo').css({ 'position' : 'absolute', 'top' : '110px', 'height' : window.innerHeight + 'px' });
			
			/*padding-top: 110px;
			position: absolute;
			top: 110px;
			height: 778px;
			left: 0px;*/
		},

		crop : function () {
			if (this.croppedCoords != null) {
				this.nextStep ();
			} else {
				alert('Crop the image first!');
			}
			return false;
		},

		click : function () {
			this.$('#uploadPhoto').click();
		},

		fileChoosed : function ( options ) {
			options.parent.cropView.setImg({ 'image_url' : options.data.image_url/*'uploads/tmp/20130529-1754-27375-9743/__________2.png'*/ || '/assets/samples/contest_photo.png' },options.parent.cropView);
			this.picData.set( options.data );
			this.nextStep ();
		},

		cropCancel : function () {
			if( this.currentView == 3 ){
				var that = this;
				var photo = new PhotoModel();
				var map = { 'id' : this.cropedData.id };
				photo.deletePhoto( { 'map' : map, 'success' : function( response ){
					document.location.reload(true)
				}});
			}
			else
				this.backStep();
			return false;
		},

		nextStep : function () {
			this.$('#uploadPhoto').val("");
			
			if ( this.currentView == 3 ) return;

			var screenWidth = $( document ).width ();
			var screenHeight = window.innerHeight;
			var leftC = 0;
			var leftN = screenWidth;
			var speed = 100;
			var that = this;

			var current = this.pageViews [ this.currentView - 1 ].$el;
			var next = this.pageViews [ this.currentView ].$el;

			next.removeClass ( 'disnone' );

			var interval = setInterval ( function () {
				leftC -= speed;

				if( leftN > speed )
					leftN -= speed;
				if ( leftC < ( screenWidth * -1.4 ) ) {
					current.addClass ( 'disnone' );
					// current.css ( { 'left' : '0px' } );
					// next.css ( { 'left' : '0px' } );
					that.prevView = that.currentView;
					that.currentView++;
					if (that.currentView == 3) that.spread();
					clearInterval ( interval );
				}
			}, 10 );
		},

		backStep : function () {
			if ( this.currentView == 1 ) return;

			var screenWidth = $( document ).width ();
			var leftC = 0;
			var leftN = screenWidth * -1.4;
			var speed = 100;
			var that = this;

			var current = this.pageViews [ this.currentView - 1 ].$el;
			var next = this.pageViews [ this.currentView - 2 ].$el;

			next.removeClass ( 'disnone' );

			var interval = setInterval ( function () {
				leftC += speed;

				if( leftN < speed )
					leftN += speed;

				if ( leftC > ( screenWidth * 1.4 ) ) {
					current.addClass ( 'disnone' );
					that.prevView = that.currentView;
					that.currentView--;
					if(that.currentView == 1) that.pageViews [ 2 ].render ();
					clearInterval ( interval );
				}
			}, 10 );
		},

		croped : function (data) {
			this.croppedCoords = data.data;
		},

		spread : function () {
			var rounding = function ( x ) {	x *= 1;	return x - (x % 1);	};
			var model = new PhotoModel();
			var that = this;
			var map = {
				'photo_crop_w': rounding(this.croppedCoords.w), //600, //this.croppedCoords.w,
				'photo_crop_h': rounding(this.croppedCoords.h), //600, //this.croppedCoords.h,
				'photo_crop_x': rounding(this.croppedCoords.x), //0, //this.croppedCoords.x2,
				'photo_crop_y': rounding(this.croppedCoords.y),//0, //this.croppedCoords.y2,
				'photo_min_size': this.croppedCoords.min,
				'photo_crop_default': this.croppedCoords.def,
				'photo_image_cache': this.picData.get( 'image_cache' )
			};
			model.uploadPhoto( { 'map': map,
				success: function ( response ) {
					if(response.responseText){
						var data = JSON.parse(response.responseText) || null;
						that.spreadView.setImg({ 'data' : data});
						that.cropedData = data;
					}
				}
			} );
		},
		
		upload : function () {
			var commentModel = new CommentModel();
			var comment = this.$('input[name="hash-Comment"]').val() || null;
			if(comment){
				var map = { 'id' : this.cropedData.id, 'data' : { 'comment' : comment } };
				commentModel.postComment( { 'map' : map, 'success' : function( response ){
					//console.log('response',response);
					app.navigate( '/profiles/' + app.models.userModel.get( 'username' ), true );
				}} );
			}
			else{
				app.navigate( '/profiles/' + app.models.userModel.get( 'username' ), true );
			}
		}
	} );

} );