define([
    'app',
	'backbone'
	], function (
	app,
	Backbone
	) {
	return Backbone.Model.extend({
		idAttribute:'id',
		
		postComment : function ( options ) {
			app.sendRequest( '/photos/' + options.map.id + '/comments', {
				method: 'POST',
				data : options.map.data,
				complete: options.success,
				single: true,
				context: this
			} );
		}
	}
	);
});