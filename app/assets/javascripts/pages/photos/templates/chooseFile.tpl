<div class="width">
	<div class="container">
		<% if( fileApiSupport ) { %>
			<input class="vishid disnone" id="uploadPhoto" name="file" type="file" multiple="multiple" />
			<div class='btn active thin'>Choose File to Upload</div>
		<% } else { %>
			<div id="file-uploader">
				<noscript>
					<p>Please enable JavaScript to use file uploader.</p>		
				</noscript>
				<div class='btn active thin choose-file'>Choose File to Upload</div>         
			</div>
		<% } %>
	</div>
</div>