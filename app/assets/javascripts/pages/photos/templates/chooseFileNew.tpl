<div class="width">
	<div class="container">
		<form action="/photos/upload_photo" method="post" encType="multipart/form-data" target="hiddenframe">
			<div id="file-uploader" class='btn active thin'>Choose File to Upload</div>
		</form>
		<iframe id="hiddenframe" name="hiddenframe" style="width:0px; height:0px; border:0px"></iframe>
    
    <div class="spinner disnone">
      <img src="<%=url%>/assets/loading.gif" class="loading" />
    </div>
	</div>
</div>