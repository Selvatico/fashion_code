<div class="width">
  <div class="container">
    <!-- / step-2 -->
    <h2>Crop your Photo</h2>
    <div><img id="uploadedPhoto" alt="upload photo" src="<%=url%><%=imgUrl%>" /></div>
    <form>
      <span class="btn cancel">Cancel</span>
    	<input class="btn active" name="commit" type="submit" value="Crop" />
    </form>
  </div>
</div>