<div class="width">
  <div class="container">
    <!-- / step-3 -->
    <h2>Spread the world</h2>
    <img alt="name photo" id="croppedImage" class="m" <%if(data && data.data.image){%> src="<%=url%><%=data.data.image%>" <%}else{console.log('sample');%> src="<%=url%>assets/samples/contest_photo.png" <%}%> />

    <form action='#' class='rel'>
    	<input id="hash-Comment" name="hash-Comment"
    		placeholder="Enter your comment..." type="text" />
    </form>
    <div class='share-panel'>
    	<!-- /facebook -->
        <a <%if(data && data.data.facebook_url){%> href="<%=data.data.facebook_url%>"<%}else{%> href="https://facebook.com/share?url=" <%}%> class='share-icons facebook' target="_blank" title="Share on Facebook"><i class="moderaIcons" >f</i>
    	</a>
        <!-- /pintest -->
        <a <%if(data && data.data.pinterest_url){%> href="<%=data.data.pinterest_url%>"<%}else{%> href="https://facebook.com/pinterest?url=" <%}%> class='share-icons pinterest' target="_blank" title="Share on Pinterest"><i class="moderaIcons" >p</i>
    	</a>
    	<!-- /twitter -->
    	<a <%if(data && data.data.twitter_url){%> href="<%=data.data.twitter_url%>"<%}else{%> href="https://twitter.com/share?url=" <%}%> class='share-icons twitter' target="_blank" title="Share on Twitter"><i class="moderaIcons" >t</i>
    	</a>    	
    	<!-- /tumblr -->
    	<a <%if(data && data.data.tumblr_url){%> href="<%=data.data.tumblr_url%>"<%}else{%> href="http://www.tumblr.com/share/link?url=&name=&description=" <%}%> title="Share on Tumblr" class='share-icons tumblr' target="_blank"><i class="moderaIcons" >u</i>
    	</a>	
    </div>
    <span class="btn cancel">Cancel</span>
    <span class="btn active done">Done</span>
  </div>
</div>
