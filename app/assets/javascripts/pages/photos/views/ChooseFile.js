define([
	'app',
	'text!../templates/chooseFileNew.tpl',
	'uploader'
], function (
	app,
	template
) {

	'use strict';

	return Backbone.View.extend ({

		className : 'upload-photo fixed c',
		template : template,
		events: {},
		initialize: function() {
		},
		afterRender: function () {
			var that = this;
			new qq.FineUploaderBasic({
				button: document.getElementById( 'file-uploader' ),
				request: {
					endpoint: '/photos/upload_photo'
				},
				callbacks: {
					onUpload: function(id, fileName) { 
						that.$('.spinner').removeClass('disnone');
						//console.log('upload'); 
					},
					onComplete: function(id, fileName, data) {						
						//console.log('complite');
						that.$('.spinner').addClass('disnone');
						if ( !data.success ) {
							app.trigger ( 'show:message', { 'text' : 'Wrong file format!', 'type' : 'error' } );
						} else {
							app.trigger( 'photo:uploaded', { 'data': data, 'parent': that.options.parent}, that.options.parent );
						}
					}
				}
			} );
		},
		serialize: function() {
			return { fileApiSupport: app.fileApiSupport, 'url' : app.getURL() };
		}
	});
});