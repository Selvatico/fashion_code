define( [
	'app', 'text!../templates/crop.tpl'
], function ( app, template ) {

	'use strict';

	return Backbone.View.extend( {

		className: 'crop-photo fixed c disnone',
		template: template,
		imgUrl: '',
		photo_crop_default: null,
		photo_min_size: null,

		serialize: function () {
			return { 'url': app.getURL(), 'imgUrl': this.imgUrl };
		},

		afterRender: function () {
			this.$( '#uploadedPhoto' ).load( this.setCrop );
		},

		setImg: function ( options, that ) {
			this.imgUrl = options.image_url;
			this.render();
		},

		setCrop: function () {

			var that = this;
			var cropHandler = setCropCoords( this );
			//console.log(, ' * ', );
			var imgHeight = $( '#uploadedPhoto' ).height();
			var imgWidth = $( '#uploadedPhoto' ).width();

			getImageRealSize( $( '#uploadedPhoto' ), cropInitialize );

			function cropInitialize ( size ) {

				
				
				var ratio = 600 / (size[1] > size[0] ? size[1] : size[0]);
				var min_size = 200;
				that.photo_min_size = min_size;

				var width = Math.round( size[0] * ratio );

				var height = Math.round( size[1] * ratio );

				//var x2 = imgHeight > imgWidth ? imgWidth : imgHeight;
				var x2 = size[1] > size[1] ? size[1] : size[0];
				that.photo_crop_default = x2;

				$( '#uploadedPhoto' ).Jcrop( {
					//trueSize: size,
					aspectRatio: 1,
					onSelect: cropHandler,
					onChange: cropHandler,
					//onRelease: cropHandler,
					minSize: [min_size, min_size],
					trueSize: [size[0], size[1]],
					setSelect: [0, 0, x2, x2]
					
				} );

				var margin = (($( '.crop-photo' ).width() / 2) - ($( '.jcrop-holder' ).width() / 2));
				// $( '.crop-photo>div' ).css( { 'margin-left': margin + 'px' } );
			}

			function setCropCoords ( that ) {
				return function ( coords ) {
					app.trigger( 'photo:croped', { 'data': _.defaults( coords, { def: that.photo_crop_default, min: that.photo_min_size } ) } );
				};
			}

			function getImageRealSize ( img, callback ) {
				var im = new Image();
				im.onload = function () {
					callback( [ im.width, im.height ] );
				}
				im.src = img.attr( 'src' );
			}
		}
	} );

} );