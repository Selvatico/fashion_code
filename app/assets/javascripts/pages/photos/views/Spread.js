define([
	'app',
	'text!../templates/spread.tpl'
	], function (
	app,
	template
) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'share-photo-page c disnone',
		template : template,
		data : null,
		
		serialize: function () {
			return { 'url': app.getURL(), 'data': this.data };
		},
		
		setImg: function ( options, that ) {
		    //console.log( options );
			this.data = options;
			this.render();
		}

	} );

} );