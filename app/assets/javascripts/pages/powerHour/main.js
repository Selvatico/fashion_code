define( [
	'app',
	'backbone',
	'widgets/menu/main',
	'widgets/powerHour/main',
	'layoutmanager'
], function (app, Backbone, HeaderView, MainListLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'page-PowerHour',

		events: {
			'click .createBtn': 'goToCreate'
		},
		goToCreate: function () {
			Backbone.history.navigate( '/power_hour/new', { trigger: true } );
		},
		initialize: function () {
			this.mainLayout = new MainListLayout( { parent: this } );
		},
		beforeRender: function () {
			this.insertViews( [this.mainLayout ] );

		}
	});
} );
