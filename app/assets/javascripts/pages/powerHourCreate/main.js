define( [
	'backbone',
	'widgets/menu/main',
	'widgets/powerHourCreate/main',
	'layoutmanager'
], function ( Backbone, HeaderView, CreateStepsLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'page-PowerHourCreate',
		events: {},
		initialize: function () {
			this.mainLayout = new CreateStepsLayout( { parent: this } );
		},
		beforeRender: function () {
			this.insertViews( [this.mainLayout ] );
		}
	});
} );
