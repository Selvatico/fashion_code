define( [
         'app',
         'backbone',
         'widgets/menu/main',
         'widgets/profileHeader/main',
         'widgets/profileFollowers/main',
         'widgets/profileBrowseLegacy/main',
         'widgets/profileCompares/main',
         'widgets/profileHallFlame/main',
         'widgets/profileLegacy/main',
         'widgets/profilePhotos/main',
         'widgets/profileSettings/main',
         'models/UserModel',
         'text!./templates/layout.tpl',
         './views/Navigation',
         'nanoscroller',	
         'layoutmanager'
         ], function (
        		 app,
        		 Backbone,
        		 HeaderView,
        		 ProfileHeader,
        		 ProfileFollowers,
        		 ProfileBrowseLegacy,
        		 ProfileCompares,
        		 ProfileHallFlame,
        		 ProfileLegacy,
        		 ProfilePhotos,
        		 ProfileSettings,
        		 UserModel,
        		 template,
        		 Navigation) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'wrapper page-Profile nano',
		linkClass : 'profile',

		template : template,

		//model : new (Backbone.Model.extend({})),
		submenu : false,

		scrollPages: ['browse_legacy', 'photos', 'compares'],

		constructors: {
			//'legacy'			: ProfileLegacy,
			'browse_legacy'		: ProfileBrowseLegacy,
			'photos'			: ProfilePhotos,
			//'hall_of_fame'		: ProfileHallFlame,
			'compares'			: ProfileCompares,
			'followers'			: ProfileFollowers
		},
		events: {
			'click .step-btn'		: 'navigateSteps',
			'click .settings-btn'	: 'settings',
			'click .toTop': 'goToTop'
		},

		/**
		 * Navigate controls handler
		 * @param event
		 */
		navigateSteps: function ( step, id ) {
			//this.$('.step-btn' ).removeClass('active');
			//this.$(event.currentTarget ).addClass('active');
			this.setActiveElement( step );
			//this.setStep( this.$( event.currentTarget ).data( 'step' ) );
			if ( step != 'settings' ) {
				this.setStep( step, id );
			} else {
				this.settings();
			}
			app.trigger( 'scroll:refresh', null);
		},
		/**
		 * Scroll page to top
		 */
		goToTop: function () {
			$(".page-Profile").nanoScroller({ scroll: 'top' });
		},
		/**
		 * Set active menu link
		 * @param step
		 */
		setActiveElement: function (step) {
			app.getHeader().profileSubMenu.$el.find( '.step-switcher' ).removeClass( 'active' );
			app.getHeader().profileSubMenu.$el.find( '.' +  step ).addClass( 'active' );
		},
		initialize: function () {
			app.on( 'scroll:refresh', this.setNanoscroll, this)
			this.model = new (Backbone.Model.extend({}));

			this.pages = {
					//'legacy'		: false,
					'browse_legacy': false,
					'photos'     : false,
					//'hall_of_fame'	: false,
					'compares'   : false,
					'followers'  : false
			};
			//var _self = this;
			/*this.navigation = new Navigation( {
				parent: this,
				insert: function ( $root, $el ) {
					$root.prepend( $el );
				}} );*/

			this.getInfoData();
			this.model.set( 'photoId', this.options.photoId );
		},

		/**
		 * Send request to get main user data
		 */
		getInfoData : function(){
			app.sendRequest( '/profiles/' + this.options.username +'.json', {
				context : this,
				callback: this.infoLoaded
			});
		},
		/**
		 * Render profile menu
		 */
		beforeRender: function () {
			//set header
			//show profile page specific submenu
			app.getHeader().renderProfileSubmenu( this );

			//temp code to allow scroll page
			//$( 'body' ).css( {overflow: 'auto'} );
		},
		afterRender : function(){
			this.setNanoscroll(this);
		},
		/**
		 * Apply nanoscroll to page
		 */
		setNanoscroll : function (that) {
			var that = this;
			this.$( '.content' ).on( 'scroll', function ( event ) {
				that.catchScrollEvent( event );
			} );
			$( '.page-Profile' ).nanoScroller();
			$( '.pane' ).css( {'z-index': '3'} );
		},
		/**
		 * Get length of scrolled from the top
		 * @param $content
		 * @returns {number}
		 */
		getScrollTop : function ( $content ) {
			var scrOfY = 0;
			if ( $content.get( 0 ).scrollLeft || $content.get( 0 ).scrollTop ) {
				scrOfY = $content.get( 0 ).scrollTop;
			}
			return scrOfY;
		},
		/**
		 * Fixate header panel while scroll
		 * @param $content
		 */
		fixPaneRefresh : function ( $content ){
			if ( jQuery( "header" ).length ) {
				var top  = this.getScrollTop( $content ),
				$topPanel = app.getHeader().$el.find('.top-panel' ),
				maxMargin = $topPanel.height();
				if ( top < maxMargin ) {
					jQuery( "header" ).css( "margin-top", -top + "px" );
					this.$( '.toTop' ).addClass( 'disnone' );
					$topPanel.removeClass( 'hide-menu' );
				} else {
					jQuery( "header" ).css( "margin-top", -maxMargin + "px" );
					this.$( '.toTop' ).removeClass( 'disnone' );
					$topPanel.addClass( 'hide-menu' );
				}
			}
		},
		/**
		 * Scroll calback
		 * @param event
		 */
		catchScrollEvent: function (event) {
			var $content = $( '.content' ),
			pathLength = $content.get( 0 ).scrollHeight - $content.height(),
			percent    = ( $content.scrollTop() / pathLength) * 100;
			this.fixPaneRefresh( $content );
			//we scrolled 75% of scroll
			if ( percent > 75 ) {
				//if nested view has method 'paginate' - lets call it
				if (this.scrollPages.indexOf(this.model.get('current')) != -1 && !this.model.get('settingsPage')) {
					if (this.pages[this.model.get('current')].paginate) {
						this.pages[this.model.get('current')].paginate();
					}
				}
			}
		},
		/**
		 * Set current visible step
		 * @param {String} step Step to set
		 * @param {String} [id] Step to set
		 */
		setStep  : function (step, id) {
			var first		= typeof this.model.get('current') == 'undefined',
			currentStep	= this.model.get('current' ),
			self		= this;


			if ( this.model.get('settingsPage')) {
				this.$( '.profile, .profile-content' ).removeClass( 'disnone' );
				this.settingsView.$el.addClass( 'disnone' );
				this.model.set('settingsPage', false);
			}

			if (step == this.model.get('current')) return;

			if ( step == 'browse_legacy' && id != null && this.pages['browse_legacy'] ) {
				this.pages['browse_legacy'].remove();
				this.pages['browse_legacy'] = false;
				this.model.set('photoId', id);
			}

			//init step object
			if ( !this.pages[step]) {
				this.pages[step] = new this.constructors[step]( {parent: this, username: this.model.get( 'info' ).username} );
				this.insertView( '.profile-scroll-container', this.pages[step] ).render();
			}

			//show animation if we appended not first step
			if ( !first ) {
				this.pages[step].$el.detach().insertAfter(this.pages[currentStep].$el);
				this.pages[step].$el.css( {'margin-left': '930px', opacity: 0} );
				this.pages[currentStep].$el.css( {'margin-left': '0px'} ).animate( { 'margin-left': '-930px', opacity: 0 }, function () {
					self.pages[currentStep].$el.addClass( 'disnone' );
				});
				this.pages[step].$el.removeClass( 'disnone' ).animate( { 'margin-left': '0px', opacity: 1 });
			}
			//this.navigation.setScrollPosition(step);
			//save current step in the model
			this.model.set( 'current', step );
			//
			//Backbone.history.navigate( step, {trigger: false} );
		},
		/**
		 * Hide all main profile steps and insert/show settings html
		 * @returns {boolean}
		 */
		settings : function () {
			$('.buttonsBlock').show();
			if ( !$( '.widget-DynamicSubMenu' ).is( ':visible' ) ) {
				$('#subMenuLink').click();
			}
			if (!this.model.get('info' ).owner) return false;

			this.$( '.profile, .profile-content' ).addClass( 'disnone' );
			if ( !this.settingsView ) {
				this.settingsView = new ProfileSettings( {parent: this, model: new UserModel( this.model.get( 'info' ) ) } );
				this.insertView( '.mainContainer', this.settingsView ).render();
			} else {
				this.settingsView.$el.removeClass( 'disnone' );
			}
			this.model.set( 'settingsPage', 'true' );
			var navigateUrl = (this.options.notifications) ? 'settings/notifications' : 'settings';
			Backbone.history.navigate( navigateUrl, {trigger: false} );
			return true;
		},
		/**
		 * Callback for user info load
		 * @param data.info Main user info
		 * @param data.legacy Data for legacy tab
		 * @param data
		 */
		infoLoaded: function ( data ) {
			//save response to current model
			this.model.set(data);

			//load
			this.profileHeader = new ProfileHeader( {parent: this, model: new UserModel( this.model.get('info') )} );
			if(this.$('.widget-ProfileSection').length) this.$('.widget-ProfileSection').remove();
			this.insertView( '.profile', this.profileHeader ).render();
			this.setActiveElement( this.options.step );

			//this.insertView( '.profile-content', this.navigation ).render();

			if ( app.isMe( this.model.get( 'info' ).username ) ) {
				this.$( '.profile' ).addClass( 'my-profile ' )
			}

			if ( this.options.step != 'settings' ) {
				//show bottom part of the page
				this.setStep( this.options.step || 'followers' );
			} else {
				this.settings();
			}
			this.setNanoscroll(this);
		}
	} );
} );
