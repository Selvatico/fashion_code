<div class="content">
  <section class="profile overhide">

  </section>

  <div class="mainContainer">
    <section class="profile-content rel">
      <div class="profile-scroll-container"></div>
        
      <div class="toTop curpoint disnone">
        <i class="moderaIcons clear">V</i>
        <span class="disinbl textTransUpper">to Top</span>
      </div>
    </section>
  </div>
</div>

<div  class="overlay disnone">
  <div class="popup c mini">
    <p class="c">You are assured that wish to remove?</p>
    <div class="btn">Yes</div>&nbsp;
    <div class="btn">No</div>
  </div>
</div>