<div class="popup disnone">
    <div class="close right">×</div>
    <h1>Share with</h1>
    <div class="share-panel c">
                  <!-- /facebook -->
                    <span class="share-icons facebook l">
                      <a href="https://www.facebook.com/sharer/sharer.php?u=<%= url %>" class="moderaIcons">f</a>
                    </span>
            <!-- /pinterest -->
                    <span class="share-icons pinterest l">
                      <a href="http://pinterest.com/pin/create/button/?url=<%= url %>&media=<%= url %>" class="moderaIcons">p</a>
                    </span>
            <!-- /twitter -->
                    <span class="share-icons twitter l">
                      <a class="moderaIcons" href="https://twitter.com/share?url=<%= url %>">t</a>
                    </span>
            <!-- /gplus -->
                    <span  class="moderaIcons" class="share-icons gplus">
                      <a href="https://plus.google.com/share?url=<%= url %>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">g</a>
                    </span>
            <!-- /tumblr -->
                    <span class="share-icons tumblr l">
                      <a class="moderaIcons" href="http://www.tumblr.com/share/link?url=<%= url %>&name=&description=" title="Share on Tumblr">u</a>
                    </span>
            <!-- /flickr -->
                    <span class="share-icons flickr l">
                      <i class="moderaIcons">
                          <small>0</small>
                          <small>0</small>
                      </i>
                    </span>
            <!-- /mail -->
                    <span class="share-icons mail l">
                      <i class="moderaIcons">m</i>
                    </span>

</div>
