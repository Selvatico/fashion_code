define( [
	'app', 'text!../templates/navigation.tpl', 'layoutmanager'
], function ( app, widhetLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template: widhetLayout,
		events: {
			'click .custom-scroll': 'switchSteps'
		},
		tagName: 'div',
		index: 0,
		model: new Backbone.Model(),
		navSteps: [/*'legacy',*/ 'browse_legacy', 'compares', 'photos', 'hall_of_fame', 'followers'],
		initialize: function () {

		},
		switchSteps: function ( event ) {
			if ( this.$( event.target ).hasClass( 'polzunok' ) ) return false;
			var newIndex = Math.floor( event.offsetX / 186 );
			if ( newIndex != this.index ) {
				this.options.parent.setStep( this.navSteps[newIndex] );
			}
		},
		setScrollPosition: function ( step ) {
			this.index = this.navSteps.indexOf( step );
			this.$( '.polzunok' ).animate( {left: this.index * 186 + 'px'} );
		}

	} );

} );
