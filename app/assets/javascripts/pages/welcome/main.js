define([
	'app',
	'backbone',
	'moment',
	'./views/Slider',
	'widgets/welcomeFooter/main',
	'widgets/welcomeHeader/main',
	'widgets/join/main',
	'widgets/signin/main',
	'widgets/signup/main',
	'text!./templates/layout.tpl'
], function (
	app,
	Backbone,
	moment,
	SliderView,
	FooterView,
	HeaderView,
	JoinWidget,
	SigninWidget,
	SignupWidget,
	layoutTemplate
) {

	'use strict';

	return Backbone.Layout.extend({

		className	: 'page-Welcome',
		template	: layoutTemplate,
		// slideId		: 1,
		// slides		: [ '', 'page2', 'page3', 'page4', 'page5', 'page6', 'brands' ],
		// sliding		: false,
		// slider		: null,
		// mouseDown	: false,
		// startDrag	: 0,
		// direction	: null,
		// current		: null,
		// next		: null,

		events: {
			'click .signin-link'		: 'signin',
			'click .join-link'			: 'join'
			/*
            'click .changePage'			: 'changePage',
			'click .toTop'				: 'toTop',
			'click .style-box div span'	: 'selectStyle',
			'mousedown'					: 'down',
			'mouseup'					: 'up',
			'mousemove'					: 'move',
			'mousewheel'				: 'slide',
			'wheel'						: 'slide'
            */
		},

		serialize: function () {
			return { 'url': app.getURL() };
		},

		afterRender: function () {
			$( 'body' ).css( { 'overflow': 'hidden' } );            
		},        
        /*
		down: function ( e ) {
			this.mouseDown = true;
			this.startDrag = e.screenY;
			return false;
		},

		up: function ( e ) {
			this.startDrag = 0;
			this.mouseDown = false;
			if ( this.direction ) { this[ this.direction ](); }
		},

		slideUp: function () {
			var that = this;
			
			if ( ( !this.next || !this.current ) || ( !this.next.length || !this.current.length ) ) return;

			var topC = this.current.css( 'top' ).replace( 'px', '' ) * 1;
			var topN = this.next.css( 'top' ).replace( 'px', '' ) * 1;
			var speed = 100;

			var interval = setInterval( function () {

				topC -= speed;
				topN -= speed;
				that.current.css( { 'top' : topC + 'px' } );
				that.next.css( { 'top' : topN + 'px' } );

				if ( topC < ( window.innerHeight * -1 ) ) {
					that.next.css( { 'top' : '0px' } );
					that.sliding = false;
					that.current.addClass( 'disnone' );
					that.current.css( { 'top' : '0px' } );
					that.slideId++;
					that.slider.renderTextView( { 'slide' : that.slideId } );
					app.router.navigate( that.slides [ that.slideId - 1 ] );
					that.current = null;
					that.next = null;
					clearInterval( interval );
				}
			}, 10 );
		},

		slideDown: function () {
			var that = this;
			
			if ( ( !this.next || !this.current ) || ( !this.next.length || !this.current.length ) ) return;

			var topC = this.current.css( 'top' ).replace( 'px', '' ) * 1;
			var topN = this.next.css( 'top' ).replace( 'px', '' ) * 1;
			var speed = 100;

			var interval = setInterval( function () {

				topC += speed;
				topN += speed;
				that.current.css( { 'top': topC + 'px' } );
				that.next.css( { 'top': topN + 'px' } );

				if ( topC > window.innerHeight ) {
					that.next.css( { 'top': '0px' } );
					that.sliding = false;
					that.current.addClass( 'disnone' );
					that.current.css( { 'top': '0px' } );
					that.slideId--;
					that.slider.renderTextView( { 'slide': that.slideId } );
					app.router.navigate( that.slides [ that.slideId - 1 ] );
					clearInterval( interval );
					that.current= null;
					that.next = null;
				}
			}, 10 );
		},

		move: function ( e ) {
			if ( !this.mouseDown ) return;
			
			var top = window.innerHeight;
			if ( ( e.screenY - this.startDrag ) < 0 ) {
				var num = parseInt( this.slideId ) + parseInt ( 1 );
				this.next = this.$( '.page-slide-' + ( num ) );
				this.$( '.page-slide-' + ( this.slideId - 1 ) ).addClass( 'disnone' ).css( { 'top' : '0px' } );
				this.direction = 'slideUp';
			}
			else {
				this.next = this.$( '.page-slide-' + ( this.slideId - 1 ) );
				this.$( '.page-slide-' + ( this.slideId + 1 ) ).addClass( 'disnone' ).css( { 'top' : '0px' } );
				top*=-1;
				this.direction = 'slideDown';
			}
			
			if(!this.next.length) return;
			this.sliding = true;
			
			this.next.removeClass ( 'disnone' );
			this.next.css ( { 'position' : 'absolute' } );
			this.next.css ( { 'width' : $( document ).width () + 'px' } );
			this.next.find ( 'img' ).css ( { 'position' : 'relative' } );
			this.next.css ( { 'top' : top + 'px' } );
			this.next.css ( { 'top' : top + ( e.screenY - this.startDrag ) + 'px' } );

			this.current = this.$( '.page-slide-' + this.slideId );
			this.current.css ( { 'position' : 'absolute' } );
			this.current.css ( { 'width' : $( document ).width () + 'px' } );
			this.current.css ( { 'top' : ( e.screenY - this.startDrag ) + 'px' } );
		},

		changePage : function() {
			this.slideId++;
			if ( this.slideId > this.$( '.item' ).length ) this.slideId = 1;
			app.router.navigate( this.slides [ this.slideId - 1 ] );
			this.moveUp ( this.$( '.page-slide-' + ( this.slideId - 1 ) ), this.$( '.page-slide-' + ( this.slideId ) ) );
		},

		slide : function ( event ) {

			var delta = event.originalEvent.wheelDeltaY || event.originalEvent.deltaY;
			 if ( delta == 3 || delta == -3 || delta == 107 || delta == -107 || delta == 100 || delta == -100 ) {
			 	delta *= -1;
			 }
			console.log('delta - ', delta);
			var X = event.screenX || event.originalEvent.screenX; 

			if ( 
					X > ( $( document ).width () * 0.255 ) && 
					X < $( document ).width() * 0.75 && this.slideId == 6 
			) return;
			if ( this.sliding ) return;
			
			//console.log(this.slideId);

			var scroll = delta;
			var current = this.$( '.page-slide-' + this.slideId );
			if ( scroll < 0  && this.slideId < this.$( '.item' ).length ) {
				this.slideId++;
				this.sliding = true;
				var next = this.$( '.page-slide-' + this.slideId );
				this.moveUp ( current, next );
				app.router.navigate ( this.slides [ this.slideId - 1 ]);
			}
			if ( scroll > 0 && this.slideId > 1) {
				this.slideId--;
				this.sliding = true;
				var next = this.$( '.page-slide-' + this.slideId );
				this.moveDown ( current, next );
				app.router.navigate ( this.slides [ this.slideId - 1 ] );
			}
		},

		moveUp : function ( current, next ) {
			current.css ( { 'position' : 'absolute' } );
			current.css ( { 'width' : $( document ).width () + 'px' } );
			current.find ( 'img' ).css ( { 'position' : 'relative' } );
			next.removeClass ( 'disnone' );
			next.css ( { 'position' : 'absolute' } );
			next.css ( { 'width' : $( document ).width () + 'px' } );
			next.find ( 'img' ).css ( { 'position' : 'relative' } );
			next.css ( { 'top' : '1080px' } );

			var topC = 0;
			var topN = 1080;
			var speed = 100;
			var that = this;

			var interval = setInterval ( function () {

				topC -= speed;
				topN -= speed;
				current.css ( { 'top' : topC + 'px' } );
				next.css ( { 'top' : topN + 'px' } );

				if ( topC < ( -1080 ) ) {
					next.css ( { 'top' : '0px' } );
					that.sliding = false;
					current.addClass ( 'disnone' );
					current.css ( { 'top' : '0px' } );
					that.slider.renderTextView ( { 'slide' : that.slideId } );
					clearInterval ( interval );
				}
			}, 10 );
		},

		moveDown : function ( current, next ) {
			current.css ( { 'position' : 'absolute' } );
			current.css ( { 'width' : $( document ).width () + 'px' } );
			current.find ( 'img' ).css ( { 'position' : 'relative' } );
			next.removeClass ( 'disnone' );
			next.css ( { 'position' : 'absolute' } );
			next.css ( { 'width' : $( document ).width () + 'px' } );
			next.find ( 'img' ).css ( { 'position' : 'relative' } );
			next.css ( { 'top' : '-1080px' } );

			var topC = 0;
			var topN = -1080;
			var speed = 100;
			var that = this;

			var interval = setInterval ( function ()  {

				topC += speed;
				topN += speed;
				current.css ( { 'top' : topC + 'px' } );
				next.css ( { 'top' : topN + 'px' } );

				if ( topC > 1080 ) {
					next.css ( { 'top' : '0px' } );
					that.sliding = false;
					current.addClass( 'disnone' );
					current.css( { 'top' : '0px' } );
					that.slider.renderTextView ( { 'slide' : that.slideId } );
					clearInterval ( interval );
				}
			}, 10 );
		},

		toTop : function (options) {
			if ( this.sliding ) return;
			var current = options.current || 6;
			this.slideId = 1;
			this.$( '.page-slide-1' ).removeClass ( 'disnone' );
			this.$( '.page-slide-' + current ).addClass( 'disnone' );
			app.router.navigate( this.slides[0] );
			if(options.callback) options.callback();
		},
        */

		signin : function () {
			app.trigger ( 'openModal', new SigninWidget ( { page: this } ) );
		},

		join : function () {
			app.trigger ( 'openModal', new JoinWidget ( { page: this } ) );
		},

		selectStyle : function (e) {
			//console.log($(e.currentTarget).text());
			this.toTop({ 'current' : 3, callback : function (){app.trigger( 'openModal', new SignupWidget( { page: this } ) );} });
		},

		beforeRender : function () {
			//console.log(this.slideId, this.options.slide);
			this.slideId = this.options.slide || 1;
			this.slider = new SliderView ( { 'slide' : this.options.slide || 1 } );
			this.insertView( 'header', new HeaderView( { parent : this } ) );
			this.insertView( '.page-slider-container', this.slider );
			this.insertView( 'footer', new FooterView ( { parent : this } ) );
		}

	} );

} );