<div id="slider">
<div class="item page-slide-1 slide">
	<!-- <img alt="Modera" class="background" src="<%=url%>assets/lp/slide_1.jpg"> -->
	<img alt="Welcome to the Brand New Modera" src="<%=url%>assets/welcome.png" />
	<iframe width="445" height="250" src="http://www.youtube.com/embed/DFTHLbuHzxA?rel=0" frameborder="0" allowfullscreen></iframe>
	<div class="infoPlate">
		<div class="width">

			<div class="fs14">
				<p>
					When we started Modera, we were overwhelmed by the amount of clutter on all of our other social networks. Our mission was to create a platform to share photos and have them ranked so that only the very best content from our friends and things we're interested in came up to the top of our news feed.
				</p>
				<p>
					We want to thank you for all of feedback you've given us as we implement this idea of a more relevant social network. We believe we've perfected it with the brand new Modera.  The new Modera was lovingly hand-crafted both here in Sacramento, CA and overseas in Tashkent, UZ. We're very proud to share this update with you and can't wait to hear your feedback.
				</p>
				<div class="c">
					<div class="btn active join-link">Try it Now!</div>
				</div>
			</div>

			<!-- <ul class="moderaStep">
				<li><i class="moderaIcons">E</i>
					<p>
						<em>Upload your photos to get votes and quickly see how your
							lifestyle stacks up against the best.</em>
					</p></li>
				<li><i class="moderaIcons">D</i>
					<p>
						<em>Complete with others from within your selected style
							category.</em>
					</p></li>
				<li><i class="moderaIcons">C</i>
					<p>
						<em>Defeat your opponent's photos and one by one climb your
							way up the Hall of Fame.</em>
					</p></li>
				<li><i class="moderaIcons">R</i>
					<p class="nobrd">
						<em>Be the best and win prizes from some of the most amazing
							brands.</em>
					</p></li>
			</ul> -->
		</div>
		<!-- <div class="changePage curpoint"></div> -->
	</div>
</div>
<div class="item page-slide-2 disnone slide">
	<!-- <img alt="Upload photo" class="background" src="<%=url%>assets/lp/slide_2.jpg"> -->
	<div class="infoPlate">
		<div class="width">
			
		</div>
		<div class="changePage curpoint"></div>
	</div>
</div>
<!-- 
<div class="item page-slide-3 disnone slide">
	<div class="style-box">
		<div>
			<span>Vintage</span> <img alt="Vintage" src="<%=url%>assets/lp/vintage.jpg">
		</div>
		<div>
			<span>Casual</span> <img alt="Casual" src="<%=url%>assets/lp/casual.jpg">
		</div>
		<div>
			<span>Urban</span> <img alt="Urban" src="<%=url%>assets/lp/urban.jpg">
		</div>
		<div>
			<span>Hipster</span> <img alt="Hipster" src="<%=url%>assets/lp/hipster.jpg">
		</div>
		<div>
			<span>Glamour</span> <img alt="Glamour" src="<%=url%>assets/lp/glamour.jpg">
		</div>
	</div>
	<div class="infoPlate">
		<div class="width">
			<div class="text-slider-container rel">
				<span class="prev disnone"></span> <span class="next disnone"></span>
				<div class="text-slider-item">Five unique style categories to compete in and win prizes.</div>
			</div>
		</div>
		<div class="changePage curpoint"></div>
	</div>
</div>
-->
<div class="item page-slide-3 disnone slide">
	<!-- <img alt="Upload photo" class="background" src="<%=url%>assets/lp/slide_4.jpg"> -->
	<div class="infoPlate">
		<div class="width">
			<div class="text-slider-container rel">
				<span class="prev disnone"></span> <span class="next disnone"></span>
				<div class="text-slider-item">Upload your most stylish photos.
					And start taking the votes from your opponents.</div>
			</div>
		</div>
		<div class="changePage curpoint"></div>
	</div>
</div>
<div class="item page-slide-4 disnone slide">
	<!-- <img alt="Upload photo" class="background" src="<%=url%>assets/lp/slide_5.jpg" /> -->
	<div class="c white-block rel">
		<span>Compete with others</span> <br> <span class="disk-border">
			<small class="m">● ● ● ● ● ● ● ●</small> in the <small class="m">●
				● ● ● ● ● ● ●</small>
		</span> <br> <b>same style category as you.</b>
	</div>
	<div class="infoPlate">
		<div class="width">
			
		</div>
		<div class="changePage curpoint"></div>
	</div>
</div>
<div class="item page-slide-5 disnone slide">
	<!-- <img alt="Upload photo" class="background" src="<%=url%>assets/lp/slide_6.jpg" /> -->
	<div class="infoPlate">
		<div class="width">
			
		</div>
		<div class="changePage curpoint"></div>
	</div>
</div>
<div class="item page-slide-6 disnone slide">
	<!-- <img alt="Upload photo" class="background" src="<%=url%>assets/lp/slide_7.jpg" /> -->
	<div class="static-page">
		
	</div>
</div>
</div>
<div class="nav nav-left"></div>
<div class="nav nav-right"></div>