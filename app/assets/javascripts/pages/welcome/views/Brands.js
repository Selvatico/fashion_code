define([
	'app',
	'mansonry',
	'text!../templates/brands.tpl' 
	], function (
	app,
	mansonry,
	brandsTemplate
	) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'width rel',
		template : brandsTemplate,
		listOfBrands : null,
		

		initialize : function () {
			app.collections.brandCollection.brandList ( {
				success : function ( data, that ) {
					that.listOfBrands = data;
					that.render ();
				}
			}, this );
		},

		afterRender : function () {
			this.$( '.box' ).css ( { 'float' : 'left' } );

			var $container = this.$( '.brands' );

			

			var height = $( window ).height () - 200;

			//$container.css ( { 'clear' : 'both' } );
			$( '.page-slide-6 .width' ).css ( { 'overflow' : 'hidden' } );
			$( ".page-slide-6 .static-page" ).css ( {
				
				"position" : "absolute",
				"top" : "0px",
				"width": "100%"
			} );

			this.$( '.static-page-container' ).css ( { 'width' : '995px' } );

			this.$( '.wrap-brands' ).css ( {
				'height' : height + 'px',
				'overflow': 'auto'
			} );
			
			if(this.listOfBrands){
				$container.imagesLoaded ( function () {
					$container.masonry ( {
						//itemSelector : '.box'
						columnWidth: 20
					} );
				} );
			}
				
		},

		serialize : function () {
			return { 'data' : this.listOfBrands, 'url' : app.getURL() };
		}

	} );

} );