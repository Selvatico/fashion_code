define([
	'app',
	'./TextSliderView2'
	], function (
	app,
	TextSliderView
	) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'text-slider-container rel',

		number : 1,
		slideTime : null,

		initialize : function () {
			var that = this;
			var interval = setInterval ( function () {
				if ( that.slideTime && ( new Date().valueOf () - that.slideTime ) > 3000 ) {
					that.render ();
					that.slideTime = 0;
				}
			}, 100 );
		},

		beforeRender : function () {
			this.setView ( new TextSliderView ( { 'parent' : this, 'number' : this.number } ) );
		}
	} );

} );