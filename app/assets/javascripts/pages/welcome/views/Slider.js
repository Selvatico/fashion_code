define([
	'app',
	'slidesjs',
	'./Brands',
	'./Page2TextSlider',
	'./Page5TextSlider',
	'./Page6TextSlider',
	'text!../templates/slider.tpl'
	], function (
	app,
	slidesjs,
	BrandsView,
	TextSliderView2,
	TextSliderView5,
	TextSliderView6,
	sliderTemplate
	) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'slidorion',
		template : sliderTemplate,
		textSliders : [],
		viewsTextSliders : { "2" : TextSliderView2, "4" : TextSliderView5, "5" : TextSliderView6 },
		barands : null,
		
		serialize : function () {
			return { 'url' : app.getURL() };
		},

		beforeRender : function () {
			this.textSliders [ 2 ] = new this.viewsTextSliders [ 2 ]( { parent : this } );
			this.textSliders [ 4 ] = new this.viewsTextSliders [ 4 ]( { parent : this } );
			this.textSliders [ 5 ] = new this.viewsTextSliders [ 5 ]( { parent : this } );
			this.brands = new BrandsView ( { parent : this } );
			
			// this.insertView ( ".page-slide-2 .width", this.textSliders [ 2 ] );
			// this.insertView ( ".page-slide-5 .width", this.textSliders [ 5 ] );
			// this.insertView ( ".page-slide-6 .width", this.textSliders [ 6 ] );
			this.insertView ( ".page-slide-6 .static-page", this.brands );
		},

		renderTextView : function ( options ) {
		    //console.log( options );
			if( options.slide == 2 || options.slide == 4 || options.slide == 5 ) {
				if ( this.textSliders [ options.slide ] )
					this.textSliders [ options.slide ].render ();
				else {
					this.textSliders [ options.slide ] = new this.viewsTextSliders [ options.slide ] ( { parent : this } );
					this.insertView ( ".page-slide-" + options.slide + " .width", this.textSliders [ options.slide ] );
				}
			}
			if ( options.slide == 6 ) {
			}
		},

		afterRender : function ( options ) {
			this.$( '.item' ).addClass( "disnone" );
			this.$( ".page-slide-" + this.options.slide ).removeClass ( "disnone" );
		}

	} );

} );