define([
	'app',
	'slidesjs',
	'text!../templates/page5TextSlider.tpl'
	], function (
	app,
	slidesjs,
	template
	) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'slides',
		template : template,


		afterRender : function () {
			var that = this;
			$( '.slides' ).css ( 'padding', '0 125px' );

			var options = {
					navigation : { active : false },
					width : 700,
					height : 250,
					start : that.options.number || 1,
					pagination : {
						active : false,
						effect : 'slide'
					},
					play : {
						active : false,
						effect : 'slide',
						interval : 3000,
						auto : true,
						swap : false,
						restartDelay : 3
					},
					callback : {
						start : function ( number ) {
							that.options.parent.number = number;
							that.options.parent.slideTime = new Date ().valueOf ();
						}
					}
			};

			$( '.page-slide-5 .slides' ).slidesjs ( options );
		}
	} );

} );