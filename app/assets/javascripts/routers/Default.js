define(function ( require ) {

	'use strict';

	// Application
	var app						= require( 'app' );

	// Routers
	var HallOfFameRouter		= require( 'routers/HallOfFame' ),
		HashtagContestRouter	= require( 'routers/HashtagContest' ),
		NotificationsRouter		= require( 'routers/Notifications' );

	// Pages
	var ChallengesPage			= require( 'pages/powerHour/main' ),
		PowerHourCreate			= require( 'pages/powerHourCreate/main' ),
		PhotosPage				= require( 'pages/photos/main' ),
		NotificationsPage		= require( 'pages/notifications/main' ),
		ProfilesPage			= require( 'pages/profiles/main' ),
		InviteFriends			= require( 'pages/inviteFriends/main' ),
		ChooseStylePage			= require( 'pages/chooseStyle/main' ),
		GetPrizesPage			= require( 'pages/getPrizes/main' );

	// Widgets
	var JoinFillEmailWidget		= require( 'widgets/joinFillEmail/main' );

	return Backbone.RouteManager.extend({

		routes: {
			''						: 'index',
			'challenges'			: 'challenges',
			'get_prizes'			: 'getPrizes',
			'photos/:query'			: 'photos',
			'power_hour/new'		: 'powerHour',
			'choose_style'			: 'chooseStyle',
			'invitation'			: 'inviteFriends',
			'signup'				: 'signup',
			'about'					: 'about',
			'hashtag_contests/'		: HashtagContestRouter,
			'hall_of_fame/'			: HallOfFameRouter,
			'notifications/'		: NotificationsRouter,
			'_=_'					: 'index'
		},

		// Callbacks
		index: function ( query ) {
			var page = new HomePage;
			app.renderPage( page );
		},
		challenges: function () {
			var page = new ChallengesPage();
			app.renderPage( page );
		},
		photos: function ( query ) {
			var page = new PhotosPage();
			app.renderPage( page );
		},
		getPrizes: function () {
			var page = new GetPrizesPage();
			app.renderPage( page );
		},
		notifications: function ( type ) {
			var page = new NotificationsPage( { type: type || 'actions' } );
			app.renderPage( page );
		},
		/**
		* Invite friends route handler
		*/
		inviteFriends: function () {
			var page = new InviteFriends();
			app.renderPage( page );
		},
		/**
		* Choose style route handler
		*/
		chooseStyle: function () {
			var page = new ChooseStylePage();
			app.renderPage( page );
		},
		signup: function () {
			this.welcome();
			app.trigger( 'openModal', new JoinFillEmailWidget() );
		},
		powerHour: function () {
			app.renderPage( new PowerHourCreate() );
		}

	});

});