define(function ( require ) {

	'use strict';
	
	var app					= require( 'app' ),
		HallOfFamePage		= require( 'pages/flameHall/main' );

	return Backbone.Router.extend({

		routes: {
			''				: 'hall_of_fame',
			'tag/:tag'		: 'hall_of_fame_tag',
			':contest_id'	: 'hall_of_fame'
		},

		// Callbacks

		hall_of_fame: function ( contestId ) {
			var page = new HallOfFamePage({
				section: 'contest',
				contestId: contestId
			});
			app.renderPage( page );
		},

		hall_of_fame_tag: function ( tag ) {
			var page = new HallOfFamePage({
				section: 'hashtag',
				hashTag: tag
			});
			app.renderPage( page );
		}

	});

});