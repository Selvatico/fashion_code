define(function ( require ) {

	'use strict';

	var app						= require( 'app' ),
		HashtagContestPage		= require( 'pages/hashtagContest/main' );

	return Backbone.Router.extend({

		routes: {
			':tag'			: 'index',
			':tag/:tab'		: 'index'
		},

		// Callbacks

		index: function ( tagName, tab ) {
			var tabName = tab || 'contests';
			if ( !{ contests: 1, photos: 1 }[ tabName ] ) {
				//@todo throw 404 error
				return false;
			}
			var page = new HashtagContestPage({
				tagCName: tagName,
				tab: tabName
			});
			app.renderPage( page );
		}

	});

});