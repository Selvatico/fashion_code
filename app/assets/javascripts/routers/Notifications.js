define(function ( require ) {

	'use strict';

	var app					= require( 'app' ),
		NotificationsPage	= require( 'pages/notifications/main' );

	return Backbone.Router.extend({

		routes: {
			''		: 'index',
			':type'	: 'index'
		},

		// Callbacks

		index: function( type ) {            		
			var page = new NotificationsPage( { type: type } );
			app.renderPage( page );
		}

	});

});