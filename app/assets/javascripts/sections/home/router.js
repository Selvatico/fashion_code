define(function ( require ) {

	'use strict';

	// Application
	var app				= require( 'app' );

	// Routers
	var DefaultRouter	= require( 'routers/Default' );

	// Pages
	var HomePage		= require( 'pages/home/main' );

	// Widgets
	var HeaderWidget	= require( 'widgets/menu/main' );

	return DefaultRouter.extend({

		initialize: function () {
			this.routes[ 'home' ] = 'index';
			app.currentSection = 'home';
			app.renderMenu( new HeaderWidget () );
		},

		index: function ( query ) {
			var page = new HomePage;
			app.renderPage( page );
		}

	});

});