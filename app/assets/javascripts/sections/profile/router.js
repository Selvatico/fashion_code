define(function ( require ) {

	'use strict';

	// Application
	var app				= require( 'app' );

	// Routers
	var DefaultRouter	= require( 'routers/Default' );

	// Pages
	var ProfilePage		= require( 'pages/profiles/main' );

	// Widgets
	var HeaderWidget	= require( 'widgets/menu/main' );

	return DefaultRouter.extend({

		initialize: function () {
			this.routes[ 'settings' ]			= 'index';
			this.routes[ 'settings/:section' ]	= 'settingSection';
			this.routes[ 'photos' ]				= 'index';
			this.routes[ 'browse_legacy' ]		= 'index';
			this.routes[ 'browse_legacy/:id' ]	= 'index';
			this.routes[ 'compares'	]			= 'index';
			this.routes[ 'followers' ]			= 'index';
			app.currentSection = 'profile';
			app.renderMenu( new HeaderWidget () );
		},

		index: function ( id ) {
			var path	= location.pathname.split('/' ),
				photoId = id || null,
				step	= (photoId != null) ? 'browse_legacy' : Backbone.history.getHash();

			if (app.currentPage && app.currentPage.navigateSteps) {
				app.currentPage.navigateSteps(step, photoId);
			} else {
				app.renderPage( new ProfilePage( { username: path[ 2 ], step: step || 'photos', photoId: photoId } ) );
			}
		},
		settingSection: function (section) {
			var path = location.pathname.split( '/' );
			section = section || 'profile';
			if ( !{profile: 1, account: 1, share: 1, password: 1, notifications: 1}[section] ) {
				section = 'profile';
			}
			app.renderPage( new ProfilePage( { username: path[ 2 ], step: 'settings', settingSection: section } ) );
		}

	});

});