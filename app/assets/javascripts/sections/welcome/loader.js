require([ 
	'app',

	// Modules
	'allModules',

	// Routers
	'sections/welcome/router',

	// Models
	'models/CurrentUserModel',
	'models/ConfigModel',
	'models/ContestModel',
	'models/PhotoModel',
	'models/ServerDataModel',

	// Collections
	'collections/BrandsCollection',
	'collections/FashionStylesCollection',
	'collections/Contests',
	'collections/PhotosCollection',

	// Сontrollers
	'controllers/FacebookController',
	'controllers/InstagramController',
	'controllers/TwitterController',
	'controllers/ChooseFashionController'
], function (
	app,

	// Modules
	allModules,

	// Routers
	Router,

	// Models
	UserModel,
	ConfigModel,
	ContestModel,
	PhotoModel,
	ServerDataModel,

	// Collections
	BrandsCollection,
	FashionStylesCollection,
	ContestsCollection,
	PhotosCollection,

	// Сontrollers
	FacebookController,
	InstagramController,
	TwitterController,
	ChooseFashionController
) {

	'use strict';

	app.configure();

	app.router = new Router();
	
	app.models = {};
	app.controllers = {};
	app.views = {};
	app.collections = {};

	// Config
	app.config = new ConfigModel();
	
	// Initialize models
	app.models.ServerDataModel = new ServerDataModel( $('body' ).data());
	app.models.userModel	= new UserModel(app.models.ServerDataModel.getCurrentUser());
	app.models.ContestModel = new ContestModel();
	app.models.PhotoModel   = new PhotoModel();

	//Initialize collections
	app.collections.brandCollection		= new BrandsCollection();
	app.collections.fashionStyles		= new FashionStylesCollection();
	app.collections.PhotosCollection	= new PhotosCollection();
	app.collections.contests = {
		'joined'	: new ContestsCollection( [], { type: 'joined' } ),
		'started'	: new ContestsCollection( [], { type: 'started' } ),
		'new'		: new ContestsCollection( [], { type: 'new' } ),
		'finished'	: new ContestsCollection( [], { type: 'finished' } )
	};

	// Initialize controllers
	/*app.controllers.facebookController  = new FacebookController();
	app.controllers.instagramController = new InstagramController();
	app.controllers.twitterController   = new TwitterController();
	app.controllers.chooseFashion	  = ChooseFashionController;*/

	Backbone.history.start();

});