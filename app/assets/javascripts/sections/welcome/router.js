define([
	'app',
	'pages/welcome/main',
	'pages/chooseStyle/main',
	'widgets/signin/main',
	'widgets/joinFillEmail/main',
	'widgets/newPassword/main',
	'models/CurrentUserModel',
	'allModules'
], function (
	app,
	WelcomePage,
	ChooseStylePage,
	SigninWidget,
	JoinFillEmailWidget,
	NewPassword,
	UserModel
	
) {

	'use strict';

	return Backbone.RouteManager.extend({

		routes: {
			''					: 'welcome',
			/*
            'page:id'			: 'welcome',
			'brands'			: 'welcome',
            */
			'signup'			: 'signup',
			'signin'			: 'signin',
			'style'				: 'chooseStyle',
			'new_password'		: 'newPassword',
			'*defaults'			: 'welcome'
		},

		initialize: function () {
			// Check if user logged in
			this.on(
				[
					'route:welcome'
				].join( ' ' ), function () {
					if ( app.models.userModel.get( 'userSignIn' ) ) Backbone.history.navigate( 'home', { trigger: true } );
				}
			);
			// Check if user not logged in
			/*
			this.on(
				[
					'route:choose_style',
					'route:invite_friends',
					'route:index',
				].join( ' ' ), function() {
				if( !app.models.userModel.get( 'userSignIn' ) ) Backbone.history.navigate( '', { trigger: true } );    
				}
			);
			*/
			app.currentSection = 'welcome';
		},
		/**
		* Welcome page route handler
		* @param {Number} id Number of slide
		*/
		welcome: function ( id ) {	
			/*
            var urlKey = _.isNumber( parseInt(id) ) ? id : 1;
			if ( Backbone.history.fragment == 'brands' ) urlKey = 6;			
			var page = new WelcomePage( { 'slide' : urlKey } );
            */
            var page = new WelcomePage();
			this.page = page;
			app.renderPage( page );
		},
		signup: function () {
			this.welcome();
			app.trigger( 'openModal', new JoinFillEmailWidget( { page: this.page } ) );
			
		},
		signin: function() {
			this.welcome();
			app.trigger( 'openModal', new SigninWidget( { page: this.page } ) );
		},
		newPassword : function () {
			this.welcome();
			app.trigger( 'openModal', new NewPassword( { page: this.page } ) );
		},
		chooseStyle: function () {
			var page = new ChooseStylePage();
			app.renderPage( page );
		}/*,
		checkToken : function ( token ) {
			var that = this;
			var options = { 'data' : { 'reset_password_token' : token }, 'success' : function (response) {
						console.log('response',response);
						app.navigate('#new_password');
				} }
			var model = new UserModel();
			console.log('check token', token,model );
			var that = this;
			app.sendRequest( '/password/edit', {
				//method: 'POST',
				data : options.data,
				complete : options.success,
				context : this
			} );
			model.checkToken( 
					{ 'data' : { 'reset_password_token' : token },
						'success' : function (response) {
								var data = JSON.parse(response.responseText);
								console.log('response',data);
								
								//that.welcome();
								if( response.status == '200') {
									
									if(data.csrf){
										$('meta[name="csrf-token"]').attr( { 'content' : data.csrf } );
										app.navigate('#new_password', true);
										
										//app.navigate('/profiles/user1#settings', true);
									}
								}
								else {that.welcome();}
									//app.navigate('#new_password');
						} }  );
		}*/
	});

});