#= require jquery

$(document).ready ->
  # Set active link(s) in left menu
  path = window.location.pathname
  left_menu_item = $("aside.left > nav a[href='#{path}']")
  top_menu_item = $("div.w-nav.left > a[href='#{path}']")
  left_menu_item.addClass('active')
  top_menu_item.addClass('active')
  if left_menu_item.length > 0 and left_menu_item.parent().get(0).tagName is 'DIV'
    left_menu_item.parent().prev().addClass('active')