/**
	* @exports CommentsCollection
	* @namespace app.collections
*/
define([
	'app',
	'backbone',
	'../models/CommentModel'
	], function (
	app,
	Backbone,
	CommentModel
	) {
	return Backbone.Collection.extend ( {
		model		: CommentModel,
		initialize	: function (models, options) {
			_.extend(this.urlConfig, options);
		},
		/**
		 * Settings for retrieving data for response.
		 * Because data for collections lien not in the root, but in some key ex. "comments"
		 */
		urlConfig: {},
		url: function () {
			return '/photos/' + this.urlConfig.photo_id + '/comments';
		},
		/**
		 * Load data using one ajax point and add to collection
		 * @param {Object} params Additional params such as page etc.
		 */
		load: function (params) {
			var _self = this,
				beforeLength = this.length;
			app.sendRequest ( this.url(), {
				callback : function ( data ) {
					if ( data[_self.urlConfig.key] ) {
						_self.add( data[_self.urlConfig.key] );
						_self.trigger('comments:loaded', _self, data, data[_self.urlConfig.key].length, beforeLength);
					}
				},
				data: params || {}
			} );
		}
	});
});