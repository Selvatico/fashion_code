define([
	'app',
	'text!./templates/commentsTemplate.tpl',
	'./collections/CommentsCollection',
	'../../collections/UsersCollection',
	'./views/CommentItem',
	'./views/UserView',
	'./models/PruposeModel',
	'jcaret'
	], function (
	app,
	commentsTemplate,
	CommentsCollection,
	UsersCollection,
	CommentItem,
	SearchUserView,
	PruposeConfigModel
	) {

	'use strict';

	return Backbone.View.extend({

		template		: commentsTemplate,
		collection		: null,
		userCollection	: null,
		model			: new PruposeConfigModel(),
		/**
		* If we track input to prupose usernames
		* @var {Boolean}
		*/
		trackingInput: false,

		events : {
			'keyup #comment' : 'searchMember',
			'focus #comment' : 'focusComment',
			'submit .form-comment' : 'submitForm',
			'click .asLink' : 'loadMoreComments',
			'click .close' : 'closeComments',
            'mousewheel .nano': 'scrollCatch',
			'wheel .nano': 'scrollCatch'
		},

		initialize: function() {
			this.parentView = this.options.parent;

			this.collection = new CommentsCollection( [], { photo_id: this.options.photo_id, key: 'comments'} );

			//event when we loaded list of comments
			this.collection.on( 'comments:loaded', this.commentsLoaded, this );

			this.collection.on( 'add', this.appendComment, this );

			this.model.set( {page: 1} );

			//load comments
			this.collection.load( {page: this.model.get( 'page' )} );
		},
		/**
		* Proxy method to close comments element
		* @param event
		*/
		closeComments: function (event) {
			event.preventDefault();
			this.options.parent.closeComments( this.options.holderClass );
		},
		/**
		* Service method for lazy user collection init and data load
		* @returns {null}
		*/
		getUserCollection: function () {
			if ( this.userCollection == null ) {
				this.userCollection = new UsersCollection ( [], { userUrl: '/search/following', key: 'users'} );//'/search/users'
				this.userCollection.on( 'users:loaded', this.renderPrupose, this );
			}
			return this.userCollection;
		},
		afterRender: function () {
			//save reference to input
			this.$input = this.$( '#comment' );            
		},
		/**
		* Catch click on 'See previsious' and load more comments
		* @param event
		*/
		loadMoreComments: function (event) {
			this.model.set( 'page', this.model.get ( 'page' ) + 1 );
			this.collection.load( {page: this.model.get ( 'page' )} );
			event.preventDefault();
		},
		/**
		* Catch input keyup event and start searching members if member entered sign '@'
		* @param event
		*/
		searchMember: function (event) {
			            
            var $input = this.$(event.currentTarget),
			caretPosition = $input.caret(),
			searchValue;
			//code for @ sign or if white space stop
			if ( event.keyCode == 50 ) {
				//if inserted
				this.model.set( { 'trackingInput': true, trackIndex: caretPosition - 1} );
			} else if ( event.keyCode == 32 ) {
				this._stopTracking();
			} else if ( this.model.get( 'trackingInput' ) ) {
				//if we deleted current @sign
				if ( caretPosition == this.model.get( 'trackIndex' ) ) {
					this._stopTracking();
				} else if ( caretPosition > this.model.get( 'trackIndex' ) ) {
					//get subsctring after sign @
					searchValue = $input.val().substring( ( this.model.get( 'trackIndex' ) + 1 ) );
					//if value not empty and not the same
					if ( searchValue != '' && searchValue != this.model.get( 'search' ) ) {
						this.model.set( { lastCarret: caretPosition, search: searchValue } );
						if ( searchValue.length >= 2 ) {
							this.getUserCollection().reset().load( { q: searchValue} );
						}
					}
				}
			}
            this.$(".nano").nanoScroller({ alwaysVisible: true });                                  
		},
		/**
		* Service method to stop tracking and remove prupose panel
		* @private
		*/
		_stopTracking: function () {
			this.model.set( 'trackingInput', false );
			var userViews = this.getViews( '.search-list-mention' );
			if ( userViews.value().length > 0 ) {
				_.each( userViews.value(), function ( view ) {
					if ( view !== undefined ) {
						view.remove();
					}
				}, this );
			}
			this.$( '.comment-mention' ).addClass( 'disnone' );
		},
		/**
		* Method to catch focis after we start searching.
		* If member set caret to position before search stop tracking
		*/
		focusComment: function () {
			var $input = $('#comment'),
			caretPosition = $input.caret();
			//if after focus we set caret to aerly position stop stracking
			if ( this.trackingInput && caretPosition < this.model.get( 'trackIndex' ) ) {
				//this._stopTracking();
				//@TODO: remake this and check
			}
		},
		/**
		* Render list of pruposed useres after sign @
		* @param {Backbone.Collection} collection
		* @param {Function} collection.each Iterator funtion
		* @param {Object} data Raw response from the server
		*/
		renderPrupose: function ( collection, data ) {
			if ( collection.length > 0 ) {
				this.$('.search-list-mention').empty();
				collection.each ( this.renderPruposeUser, this );
				this.$( '.comment-mention' ).removeClass ( 'disnone' );
			}
		},
		/**
		* Render one prupose user
		* @param {Backbone.Model} model
		*/
		renderPruposeUser: function ( model ) {			
			this.insertView ( '.search-list-mention', new SearchUserView ( { model: model, parent: this} ) ).render ();
		},
		/**
		* Insert username from the list of prupose
		* @param {String} username
		*/
		insertUsername: function (username) {
			var currentValue = this.$('#comment').val(),
			index = this.model.get( 'trackIndex' ),
			search = this.model.get( 'search' ),
			beforeReplace = currentValue.slice( 0, index ),
			afterReplace = currentValue.slice( (index + search.length + 1), currentValue.length ),
			finalName = beforeReplace + '@' + username + afterReplace;

			this.$( '#comment' ).val ( finalName );
			this._stopTracking ();
		},
		/**
		* Submit send comment form
		* @param event
		* @returns {boolean}
		*/
		submitForm: function(event) {
			event.preventDefault();
			if (this.$input.val().replace(/ /g,'').length  == 0) {
				return false;
			}
			app.sendRequest('/photos/' + this.options.photo_id +'/comments', {
				method   : 'POST',
				callback : this.sendCommentResult,
				context  : this,
				data     : { comment: this.$input.val()}

			});
			this.$input.val('');

			return false;
		},
		/**
		* Callback for send comemnt request
		* @param data
		*/
		sendCommentResult: function (data) {
			data.comment.justNew = 1;
			if ( this.collection.length == 0 ) {
				this.$( '.faceplate' ).addClass( 'disnone' );
				this.$( '.list-comments' ).removeClass( 'disnone' );
			}
			this.collection.add ( data.comment );
			this.$( '.list-comments' ).scrollTop( 99999 )
		},
		/**
		* Get data for template
		* @returns {*}
		*/
		serialize: function () {
			var data = app.models.userModel.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		/**
		* Render comments one by one
		* @param {Backbone.Model} model
		* @param {String} insertType append or prepend
		*/
		appendComment: function (model, arg2, parent, insertType) {
			if ( !insertType ) {
				if ( model.get( 'justNew' ) ) {
					insertType = 'append';
				} else {
					insertType = (this.model.get( 'page' ) == 1) ? 'append' : 'prepend';
				}
			}

			if ( model.get( 'user' ) == null ) {
				return false;
			}

			this.insertView( '.list-comments>.content', new CommentItem( {
				model: model,
				insert: function ( $root, $el ) {
					var method = insertType || 'append';
					$root[method]( $el );
				}
			} ) ).render();
			this.$( '.list-comments').addClass('nano').nanoScroller({ scroll: 'bottom' });
			this.$( '.content').width(this.$( '.comments-holder').width());
		},
		/**
		* Fires after phpotp comments loaded
		* @param {Object} collection
		* @param {Object} data Raw response from server
		* @param {Array} data.comments List of all comments
		* @param {Number} data.comments_count Total count of comments for photo
		*/
		commentsLoaded: function (collection, data) {
			if ( collection.length > 0 ) {
				this.$( '.list-comments' ).removeClass( 'disnone' );
				this.$( 'h3 span' ).html( data.comments_count );
			} else {
				this.$( '.faceplate' ).removeClass( 'disnone' );

			}
			this.$( '.form-comment' ).removeClass( 'disnone' );
			if ( collection.length < data.comments_count ) {
				this.$( '.asLink' ).removeClass( 'disnone' );
			} else {
				this.$( '.asLink' ).addClass( 'disnone' );
			}
		},
        scrollCatch: function( event ) {
            event.stopPropagation();
        }        
	});

});