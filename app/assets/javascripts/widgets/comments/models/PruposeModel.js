define([
	'backbone'
], function (
	Backbone
	) {
	return Backbone.Model.extend({
		defaults: {
			search			: '',
			trackIndex		: 0,
			lastCarret		: 0,
			page			: 1,
			trackingInput	: false
		},
		initialize: function () {}
	});
});