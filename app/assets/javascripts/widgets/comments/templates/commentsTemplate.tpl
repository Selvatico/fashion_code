    <div class="comments-holder" style="">
        <div class="close curpoint right">×</div>
        <span class="asLink right disnone">See previous</span>
        <h3>
            Comments
            <span>0</span>
        </h3>

        <div class="list-comments disnone">
			<div class="content">
				
			</div>
        </div>
    </div>
    <!-- / only if hasn't comments -->
    <div class="faceplate c disnone">
        <i class="moderaIcons">H</i>
        <span class="thin">Be the first to comment</span>
    </div>
    <form action="#" class="form-comment rel disnone">
        <h3>Leave Comment</h3>
        <img alt="<%= username%>" class="userpic left m" height="42" src="<%=url%><%= avatar%>" width="42">
        <input class="left" id="comment" name="comment" placeholder="@<%= username%>" type="text">
        <input class="btn left" name="commit" type="submit" value="Post">
        <div class="comment-mention abs disnone">
            <div class="nano">
                <div class="content">
                    <ul class="search-list-mention">

                    </ul>
                </div>
            </div>
        </div>
    </form>
