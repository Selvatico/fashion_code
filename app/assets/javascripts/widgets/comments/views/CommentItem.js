define([
	'backbone',
	'text!../templates/commentItem.tpl',
	'app'
	], function (
	Backbone,
	commentItemTemplate,
	app
	) {
	return Backbone.View.extend({

		template: commentItemTemplate,
		events: { },
		className: 'comment-item',
		initialize: function() {},
		serialize : function() {
			var data = this.model.toJSON();
			data.ago = moment(this.model.get('created_at'),  'YYYY-MM-DD HH:mm:ssZ').fromNow();
			return _.defaults ( data, { 'url' : app.getURL () } );
		}
	});
});