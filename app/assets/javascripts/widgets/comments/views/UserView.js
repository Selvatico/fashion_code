define([
	'backbone',
	'text!../templates/userItem.tpl',
	'app'
], function (
	Backbone,
	commentItemTemplate,
	app
) {
	return Backbone.View.extend({

		template: commentItemTemplate,
		events: {
			'click .prupose-user': 'pruposeUser'
		},
		tagName: 'li',
		className: 'user-view',
		pruposeUser: function (event) {
			event.preventDefault();
			this.options.parent.insertUsername(this.model.get('username'));
		},
		serialize: function () {
			var data = this.model.toJSON();
			return _.defaults ( data, { 'url' : app.getURL() } );
		}
	});
});