/**
 * Windo for sharing compare from user profile
 */
define( [
	'backbone',
	'text!./templates/widgetTemplate.tpl',
	'app'
], function ( Backbone, shareWindow, app ) {
	return Backbone.View.extend( {
		template  : shareWindow,
		events    : {
			'click .okBtn'     : 'okBtn',
			'click .cancelBtn' : 'cancelBtn'
		},
		tagName    : 'div',
		afterRender: function () {
			if ( this.options.popupWidth ) {
				this.$el.parent( '.popup' ).width( this.options.popupWidth );
			}
			//check if we need custom text for ok btn
			if ( this.options['okText'] ) {
				this.$( '.okBtn' ).html( this.options['okText'] );
			}

			this.$( 'h2' ).html( this.options.titleText || 'Are you sure?' );
		},
		/**
		 * Ok btn click handler
		 */
		okBtn: function () {
			if ( this.options['okAction'] && typeof this.options['okAction'] ) {
				var context = this.options.context || window;
				this.options['okAction'].call( context, 'ok' );
			}
			//check if we should close confirm window after OK click
			if ( this.options.okClose ) {
				jQuery.arcticmodal('close');
			}
		},
		/**
		 * Cancel btn click btn. Close window by default
		 */
		cancelBtn: function () {
			if ( this.options['closeAction'] && typeof this.options['closeAction'] ) {
				var context = this.options.context || window;
				this.options['closeAction'].call( context, 'cancel' );
			}
		}
	} );
} );