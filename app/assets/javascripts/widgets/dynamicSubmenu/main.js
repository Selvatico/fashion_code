define( [
	'app',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function ( app, subMenu ) {

	'use strict';

	return Backbone.Layout.extend({

		className: 'width widget-DynamicSubMenu',
		pages : [ 'settings', 'invitation', 'about', 'brands', 'help'],

		template: subMenu,

		events: {
			'click a.settings' : 'onClickSettings'
		},

		initialize: function () {
			var href = location.href; 
			for(var i in this.pages){
				if(href.indexOf(this.pages[i])+1){
					this.curPage = this.pages[i];
					break;
				}
			}
		},
		afterRender: function () {
			this.$('a').removeClass('active');
			this.$('.' + this.curPage).addClass('active');
		},
		
		serialize: function () {
			return {
				'user' : app.models.userModel.get('username') || '',
				'url' : app.getURL()
			};
		},

		onClickSettings: function () {
			/*if ( app.currentSection === 'profile' ) {
				app.navigate( 'settings' );
			} else {
				app.navigate( '/profiles/' + app.models.userModel.get( 'username' ) + '/#settings', true );   
			}
			return false;*/
		}

	});

});
