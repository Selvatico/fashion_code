<div class="w-nav profile-menu right">
	<a href="/profiles/<%=user%>#settings" class="settings"> <i class="moderaIcons">@</i> Settings
	</a> <a href="/home#invitation" class="invitation disnone"> <i class="moderaIcons">+</i> Find Friends
	</a> <a href="/logout"> <i class="moderaIcons">[</i> Log Out
	</a>
</div>
<div class="w-nav left">
<div class="message-block disnone">
		<div class="success fs14 thin disnone">
			<i class="moderaIcons">/</i> <span>Photo successfully uploaded</span>
		</div>
		<div class="error fs14 thin disnone">
			<i class="m">&times;</i> <span>Photo unsuccessfully uploaded</span>
		</div>
	</div>

	<div class="buttonsBlock">
	<a href="/about" class="about"> <i class="moderaIcons">C</i> About
	</a> <a href="/pages/brands" class="brands"> <i class="moderaIcons">Z</i> Brands
	</a> 
	<a href="/pages/help" class="help"> <i class="moderaIcons">?</i> Help	</a>
	</div>
</div>