/**
 * Layout for render leaders rows
 * @exports FlameHallWidget
 */
define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'collections/LeaderBoardCollection',
	'./views/HallRowView',
	'nanoscroller',
	'layoutmanager'
], function ( app, widhetLayout, LeaderBoardCollection, HallRowView ) {

	'use strict';

	return Backbone.View.extend( {

		template  : widhetLayout,

		model     : new Backbone.Model( {page: 1, endReached: false} ),
		className : 'width widget-flameHall',
		tagName   : 'div',
		initialize: function () {
			this.collection = new LeaderBoardCollection( [], { section: this.options.parent.options.section, hashTag: this.options.parent.options.hashTag} );
			this.collection.on( 'add', this.renderRow, this );
			this.collection.on( 'leaders:loaded', this.dataLoaded, this );

			this.model.set( {'page': 1, endReached: false } );

			this.loadData();
		},
		/**
		 * Proxy method to paginate results
		 */
		loadData: function () {
			var data = {page: this.model.get( 'page' )};
			if (this.options.parent.options.contestId) {
				data.contest_id = this.options.parent.options.contestId;
			}
			if ( !this.model.get( 'endReached' ) ) {				
				this.collection.load( data );
			}
		},
		/**
		 * Insert one leader row
		 * @param {Backbone.Model} model
		 */
		renderRow: function ( model ) {
			this.insertView( '#mainList', new HallRowView( {parent: this, model: model} ) ).render();
		},
		/**
		 * Callback for finish request
		 * @param {Array} data
		 */
		dataLoaded: function (data) {
			var that = this;
                        
            
			if (data.length == 0) {
				this.model.set('endReached', true);
			}
			
			if( !$( '.content' ).length ){
				this.$el.wrap( '<div class="content"></div>' );
				$( '.content' ).on ( 'scroll', function ( event ) {
					that.catchScrollEvent ( event );
				});
			}
			
			$( '.wrapper' ).addClass ( 'nano' );
			$( '.nano' ).nanoScroller ();
			
            
			//if member not in first 10 show fixed block in the bot of the page.
			if ( this.model.get( 'page' ) == 1 && this.collection.where( {username: app.models.userModel.get('username')} ).length == 0) {			    
				var params = {username: app.models.userModel.get('username')};
				//check if contest of particular contest of global
				if ( this.options.parent.options.contestId ) {
					params.contest_id = this.options.parent.options.contestId;
				}
				app.sendRequest( '/contests/user_place', {
					data: params,
					context: this,
					method: 'GET',
					callback: function ( data ) {
						if ( !_.isEmpty( data ) ) {
							this.myView = new HallRowView( {parent: this, model: new Backbone.Model( data )} );
							this.insertView( '#ownerHolder', this.myView ).render();
						}
					}
				} );
			}
            
            this.model.attributes.page++;
		},
		catchScrollEvent: function () {
			var $content = $( '.content' );
			var pathLength = $content.get( 0 ).scrollHeight - $content.height();
			var percent    = ( $content.scrollTop() / pathLength ) * 100;
			
			//var $body = $( 'body' );//,
				//pathLength = $body.get( 0 ).scrollHeight - $body.height(),
				//percent    = ( $body.scrollTop() / pathLength) * 100;

			//we scrolled 75% of scroll
			if ( percent > 75 ) {
				this.loadData();
			}

			if ( this.myView ) {
				if ( this._rowIsVisible( this.$( '#mainList .me' ).get( 0 ) ) ) {
					this.myView.$el.addClass( 'disnone' );
				} else {
					this.myView.$el.removeClass( 'disnone' );
				}
			}
		},
		/**
		 * Check if row visible now in the list
		 * @param elem
		 * @returns {boolean}
		 * @private
		 */
		_rowIsVisible: function ( elem ) {
			if (!elem) return false;
			var docViewTop = $( window ).scrollTop();
			var docViewBottom = docViewTop + $( window ).height();

			var elemTop = this.$( elem ).offset().top;
			var elemBottom = elemTop + this.$( elem ).height();

			return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		}
	} );

} );
