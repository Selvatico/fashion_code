<div class="npp h1 item-col"><%= place %></div>
<div class="up-down <%= arrowCls %> item-col">
    <i class="moderaIcons"><%= arrow%></i>
</div>
<div class="user item-col fs14">
    <a class="disblock rel" href="/profiles/<%= username %>" title="<%= name %>">
        <img alt="Marry Poppins" class="userpic left" height="42" src="<%=url%><%= avatar %>" width="42">
        <span class="nowrap textOverhide"><%= name %></span>
    </a>
    <span class="nowrap thin">#<%= fashion_style %></span>
</div>
<div class="likes-count item-col">
    <i class="moderaIcons b">C</i>
    <span class="thin fs14"><%= votes_score %></span>
</div>
<div class="followers-count item-col fs14">
    <b><%= followers_count %></b>
    <span class="thin">Followers</span>
</div>

<div class="verified item-col" >
    <% if (verified) { %>
    <i class="moderaIcons rel m">C</i>
    <span>verified</span>
    <% } %>
</div>

<div class="prize item-col">
    <% if (wons.length > 0) { %>
    <% for (var i = 0; i < wons.length; i++) { %>
    <p class="thin nomrg left">
        <img alt="Contest name" class="left" height="46" src="<%=url%><%= wons[i].image %>" width="46" title="<%= wons[i].prize %> won in <%= wons[i].name %>" />
    </p>
    <% } %>
    <% } %>
</div>