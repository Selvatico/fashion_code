<div class="hall-of-fame fix-top nopad">
	<div class="width">
		<h2 class="c">Leaderboard</h2>
		<div class="content-table">
			<div class="item-row th thin">
				<div class="npp item-col">&nbsp;</div>
				<div class="up-down item-col">&nbsp;</div>
				<div class="user item-col">Name</div>
				<div class="likes-count item-col">Votes</div>
				<div class="followers-count item-col">Followers</div>
				<div class="verified item-col">&nbsp;</div>
				<div class="prize item-col">Prizes</div>
			</div>
		</div>
	</div>
</div>

<div class="container rel">
	<div class="hall-of-fame">

		<!-- <h1 class="c">Leaderboard</h1> -->
		<div class="content-table scrollHandler" id="mainList"></div>
		<div class="fix-item">
			<div class="content-table" id="ownerHolder"></div>
		</div>
	</div>
</div>
