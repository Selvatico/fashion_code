define( [
	'backbone',
	'text!../templates/hallRowTemplate.tpl',
	'app'
], function ( Backbone, hallRowTemplate, app ) {
	return Backbone.View.extend( {

		events: {

		},
		template   : hallRowTemplate,
		tagName    : 'div',
		className  : 'item-row',
		initialize : function () {},
		serialize  : function () {
			var data = this.model.toJSON();
			if (data['place'] > data['previous_place']) {
				data.arrow = '(';
				data.arrowCls = 'down';
			} else if (data['place'] < data['previous_place']) {
				data.arrow = ')';
				data.arrowCls = 'up';
			} else {
				data.arrow = '';
				data.arrowCls = '';
			}
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		/**
		 * Set class me if row in leader board bout me
		 */
		afterRender: function () {
			if ( this.model.get( 'username' ) == app.models.userModel.get( 'username' ) ) {
				this.$el.addClass( 'me' );
			}
		}
	} );
} );