define([
	'backbone',
	'text!./templates/layout.tpl',
	'../modalform/main',
	'./models/ForgotPasswordModel'
], function (
	Backbone,
	layoutTemplate,
	ModalForm,
	ForgotPasswordModel
) {
	'use strict';

	return ModalForm.extend({
		
		template: layoutTemplate,

		model: new ForgotPasswordModel(),
		
		submit: function(e){
			e.preventDefault();
			var that = this;
			var data = this.$('form').serializeObject();
			if(this.model.set(data) && this.model.isValid(true)){
				this.model.forgotPassword( { 'user' : data }, function (response) {
					that.$('.message-success').fadeIn();
					that.$('.message-success').parent().addClass('success');
				});
			}
			else {
				this.$('.alert-error').fadeIn();
			}
		}
		
	});
});