define([
    'app',
	'backbone',
], function( app, Backbone ) {
	return Backbone.Model.extend({
		
		method : 'POST',
		
		validation: {
			email: [{
				required: true,
				msg: 'Email can\'t be blank'
			},{
				pattern: 'email',
				msg: 'Email is invalid'
			}]
		},
		
		forgotPassword : function ( map, callback ) {//request: { "photo_id": ID выбранной фотографии	}
			//var that = this;
			app.sendRequest ( 
					'/password', //this.urlRoot, 

					{
						data : map,
						method : this.method,

						callback : callback
					} );
		}
	});
});