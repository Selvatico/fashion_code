<h2>Forgot your password?</h2>
<form action="#" class="forgotPass-form">
	<p class="fs14">To reset your password, enter your email address and we'll send you instructions on how to create a new password.</p>
	<label class="disblock rel">
		<span class="close-error abs">×</span>
		<span class="message-error abs alert-error"></span>
    	<span class="message-success abs">Instructions sent to your email.</span>
		<input placeholder="Email" type="email" name="email">
	</label>
	<div class="divider clear"></div>
	<input class="btn active right" type="submit" value="Submit">
</form>