/**
 * Navigation panel for hall of flame page
 * @author d.seredenko
 * @exports HallOfFlameSubmenu
 */
define( [
	'app',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function ( app, subMenu ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'width widget-HallSubMenu',

		template: subMenu,

		events: { },
		initialize: function () {

		},
		beforeRender: function () {

		},
		serialize: function () {
			return app.models.userModel.toJSON();
		}
	} );

} );
