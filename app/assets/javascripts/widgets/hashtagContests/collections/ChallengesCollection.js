/**
 * @exports ChallengesCollection
 */
define( [
	'app',
	'backbone'
], function ( app, Backbone ) {
	return Backbone.Collection.extend( {
		initialize: function ( models, options ) {
			if ( options ) {
				this.url = options.url || '/photos';
			}
		},
		load: function ( params ) {
			var _self = this;
			app.sendRequest( this.url, {
				callback: function ( data ) {
					if ( data ) {
						_self.trigger( 'hashtag:contests:loaded', _self, data );
						_self.add( data['versuses'] );
					}
				},
				error: function () {
					_self.trigger( 'hashtag:contests:loaded', _self, [] );
				},
				data: params || {},
				single: true
			} );
		}
	} );
} );