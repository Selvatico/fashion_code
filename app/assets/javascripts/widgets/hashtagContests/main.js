/**
 * Layout for view tag contests. Almost the same as home
 * @exports HashTagContestView
 */
define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'../versus/views/BattleView',
	'./collections/ChallengesCollection',
	'modules/versusScroller/main',
	'layoutmanager'
], function ( app, widhetLayout, BattleView, ContestsCollection, versusScroller ) {

	'use strict';

	return Backbone.Layout.extend( {

		template	: widhetLayout,
		events		: {
			'mousewheel .battle-list': 'scrollCatch',
			'wheel .battle-list': 'scrollCatch'
		},
		className   : 'widget-hashtagContest',
		scrolling: false,
		tagName     : 'div',
		initialize: function () {
			app.on( 'move:next:versus', this.scrollCatch, this );

			this.collection = new ContestsCollection( [], {url: '/versuses/by_hashtag/' + this.options.tagCName} );

			this.collection.on( 'add', this.renderBattle, this );

			this.collection.on( 'hashtag:contests:loaded', this.loadFinish, this );

			app.on( 'versusScroll:nextPage', this.loadContest, this );

			this.loadContest();

		},
		loadContest: function () {
			this.collection.load();
		},
		serialize: function () {
			return {tagName: this.options.tagCName};
		},
		afterRender: function () {
			var _self = this;
			$( window ).on( 'scroll', function ( event ) {
				_self.catchScrollEvent( event );
			} );
		},
		loadFinish: function (collection, data) {
			if ( !data['versuses'] || data['versuses'].length == 0 ) {
				this.$( '.battle-list' ).addClass( 'disnone' );
				this.$( '.faceplate' ).removeClass( 'disnone' );
			} else {
				this.$( '.battle-list' ).removeClass( 'disnone' );
			}
		},
		/**
		 * Insert one battle to the list
		 * @param {Backbone.Model} model Contest Model
		 */
		renderBattle: function ( model ) {
			this.insertView( '.battle-list', new BattleView( {parent: this, model: model, index: this.collection.indexOf( model )} ) ).render();
			this.$versuse = false;
		},
		scrollCatch: function ( event ) {
			if ( !event.originalEvent ) return;
			var delta = event.originalEvent.wheelDeltaY || event.originalEvent.deltaY;
			if ( delta < -120 || delta > 120 ) {
				return;
			}
			if ( delta ) {
				if ( !this.scrolling ) {
					this.scrolling = true;
					this.$( '.vs.abs.c' ).fadeOut( 100 );
					versusScroller( event, '.battle', 'activeBattle', this );
				}
			}
		}
	} );

} );

