<div class="battle-list disnone">
    <div class="trending-tags c">
        <h2>
            # <%= tagName%>
            <!-- <i class="moderaIcons m">-</i> -->
        </h2>
    </div>
    <div class="vs abs c">
        <i class="moderaIcons">A</i>
    </div>
</div>
<div class="faceplate c disnone">
    <i class="moderaIcons">
        D
        <sub class="disinbl">#</sub>
    </i>
    <span class="thin disblock">You need at least 3 people to start the contest</span>
    <div class="btn disnone">Invite Friends</div>
</div>
