/**
 * Layout for view tag photos
 * @exports HashTagPhotoView
 */
define( [
	'app',
	'collections/PhotosCollection',
	'./views/OnePhotoView',
	'text!./templates/widgetLayout.tpl',
	'nanoscroller',
	'layoutmanager'
], function ( app, PhotosCollection, OnePhotoView, widhetLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template	: widhetLayout,
		events		: {},
		className   : 'widget-ProfilePhotos width',
		tagName     : 'div',
		model		: new Backbone.Model( {page: 1, endReached: false} ),
		initialize: function () {
			this.collection = new PhotosCollection( [], { urlRoot: '/photos/by_hashtag/' + this.options.tagCName} );
			this.collection.on( 'add', this.renderPhoto, this );
			this.collection.on( 'photo:loaded', this.photoLoaded, this );
			if ( this.options.data.length > 0 ) {
				this.collection.add( this.options.data );
			}

			this.model.set( {'page': 1, endReached: false } );
		},
		serialize: function () {
			return {tagName: this.options.tagCName};
		},
		/**
		 * Proxy method for photo load
		 */
		loadPhotos  : function () {
			this.collection.photoList( {page: this.model.get( 'page' )} );
		},
		/**
		 * Atach event handler to body scroll after render
		 */
		afterRender: function () {
			var _self = this;
			/*$( window ).on('scroll', function (event) {
				_self.catchScrollEvent(event);
			});*/
			
			if( !$( '.content' ).length ){
				this.$el.wrap( '<div class="content"></div' );
				$( '.content' ).on ( 'scroll', function ( event ) {
					_self.catchScrollEvent ( event );
				});
			}
			
			$( '.wrapper' ).addClass ( 'nano' );
			$( '.nano' ).nanoScroller ();
		},
		/**
		 * End AJAX request catcher, to check count results
		 * and prevent next requests
		 * @param {Object} data Server raw data
		 * @param {Object} data.photos Photo list
		 */
		photoLoaded : function (data) {
			if ( data.photos.length == 0 ) {
				this.model.set( 'endReached', true );
			}
			$( '.nano' ).nanoScroller ();
		},
		/**
		 * Some kind of pagination - infinity scroll
		 */
		loadMore: function () {
			if ( !this.model.get( 'endReached' ) ) {
				this.model.attributes.page++;
				this.loadPhotos();
			}
			
		},
		/**
		 * Insert one photo to the list
		 * @param {Backbone.Model} model Photo Model
		 */
		renderPhoto: function (model) {
			this.insertView( '.view-photo', new OnePhotoView( {parent: this, model: model} ) ).render();
		},
		catchScrollEvent: function () {
			var $content = $( '.content' ),
				pathLength = $content.get( 0 ).scrollHeight - $content.height(),
				percent    = ( $content.scrollTop() / pathLength) * 100;

			//we scrolled 75% of scroll
			if ( percent > 75 ) {
				this.loadMore();
			}
		}
	} );

} );
