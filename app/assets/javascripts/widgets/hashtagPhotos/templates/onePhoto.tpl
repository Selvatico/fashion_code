<div class="info-top">
    <div class="timeline right">
        <i class="moderaIcons">G</i>
        <%= moment(created_at,  'YYYY-MM-DD HH:mm:ssZ').fromNow()%>
    </div>
    <a class="textOverhide rel" href="/profiles/<%= user.username %>" title="<%= user.name %>">
        <img alt="<%= user.name %>" class="userpic" height="30" src="<%=url%><%= user.avatar %>" width="30">
        <span><%= user.name %></span>
    </a>
</div>
<div class="photoframe">
    <a href="/profiles/<%= user.username %>/#browse_legacy/<%=id%>"><img alt="name photo" height="302" src="<%=url%><%= image %>" width="302"></a>
</div>
