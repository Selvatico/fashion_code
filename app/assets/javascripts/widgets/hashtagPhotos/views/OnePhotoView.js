define( [
	'backbone',
	'text!../templates/onePhoto.tpl',
	'app'
], function ( Backbone, photoTpl, app ) {
	return Backbone.View.extend( {

		events: {

		},
		template  : photoTpl,
		tagName   : 'div',
		className : 'item',
		initialize: function () {

		},
		serialize: function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() });
		}
	} );
} );