define( [
	'app',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function ( app, subMenu ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'width widget-hashtagSubMenu',

		template: subMenu,

		initialize: function () {

		},
		afterRender: function () {
			var activetab = this.options.parent.model.get( 'tab' );
			this.$( '.' + activetab + 'Link' ).addClass( 'active' );
		},
		serialize: function () {
			var parentModel  = this.options.parent.model;
			return {
				users_count: parentModel.get( 'response' ).users_count,
				activetab  : parentModel.get( 'tab' ),
				tagName    : parentModel.get( 'tagCName' ),
				owner	   : parentModel.get( 'response' ).owner,
				url			: app.getURL()
			};
		}
	} );

} );
