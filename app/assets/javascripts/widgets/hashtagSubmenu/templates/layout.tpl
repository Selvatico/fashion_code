<div class="width">
    <!-- / only if has hash contest -->
    <% if (owner != null) { %>
    <div class="owner right">
        <a class="rel disblock" href="/profiles/<%= owner.username %>" title="<%= owner.name %>">
            <img alt="<%= owner.name %>" class="userpic left" height="30" src="<%= url %><%= owner.avatar %>" width="30">
            <span class="nowrap asLink"><%= owner.name %></span>
        </a>
        <span class="nowrap thin">Contest Owner</span>
    </div>
    <div class="hash-members right thin">
        <span><%= users_count%> members</span>
        <a href="#hall_of_fame/tag/<%=tagName %>">go to leaderboard</a>
    </div>
    <% } %>
    <div class="w-nav left">
        <a class="contestsLink" href="#/hashtag_contests/<%=tagName %>/contests">
            <i class="moderaIcons sub">
                D
                <sub>#</sub>
            </i>
            Style Contest
        </a>
        <a class="photosLink" href="#/hashtag_contests/<%=tagName %>/photos">
            <i class="moderaIcons">U</i>
            Photos view
        </a>
    </div>
</div>
