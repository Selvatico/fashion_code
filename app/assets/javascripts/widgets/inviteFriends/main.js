define([
	'app',
	'backbone',
	'./models/inviteFriendsModel',
	'./views/FriendItemView',
	'text!./templates/layout.tpl',
	'layoutmanager'
	], function (
	app,
	Backbone,
	inviteFriendsModel,
	FriendItemView, 
	layoutTemplate
	) {
	'use strict';

	return Backbone.Layout.extend({

		className: 'invite fixed',

		model: new inviteFriendsModel(),

		template: layoutTemplate,
		events: {
			'click .skip': 'skip',
			'click .invite-all': 'inviteAllFriends'
		},
		initialize: function() {
			this.model.fetch();
			_.bindAll(this, 'inviteAllActive');
		},
		beforeRender: function() {
			_.each( this.model.get('friends'), function( el, index, list ) {
				this.addView( el, false );
			}, this);
		},
		serialize: function() {
			return { friends_count: this.model.get('friends_count'), inviteAllActive: this.inviteAllActive };
		},
		addView: function(model, render) {
			// Insert a nested View into this View.
			var view = this.insertView( '.friend-list', new FriendItemView({ model: model, parent: this }) );
		},
		skip: function( e ) {
			e.preventDefault();
			Backbone.history.navigate( 'home', { trigger: true });
		},
		inviteAllActive: function() {
			return ( this.model.get('inviteAllActive') ) ? 'active' : '';
		},
		inviteAllFriends: function( e ) {
			e.preventDefault();
			this.model.set('inviteAllActive', ( this.model.get('inviteAllActive') == undefined ) ? true : !this.model.get('inviteAllActive') );
			this.trigger('inviteAllFriends');
			this.render();
		}
	});
});