define([
	'app',
	'backbone'
	], function(
	app,
	Backbone
	) {
	return Backbone.Model.extend({
		defaults: {
			friends_count: '',
			friends: ''
		},
		urlRoot: '/friends'
	});
});