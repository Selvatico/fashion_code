<div class="btn right <%= checkActive() %> follow"><%= followText() %></div>
<a class="textOverhide" href="<%= profile_url %>" title="<%= name %>" target="_blank">
	<img alt="<%= name %>" class="userpic" height="40" src="<%= userPicture() %>" width="40" />
	<span><%= name %></span>
</a>