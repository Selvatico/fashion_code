  
        <div class='invite-tape container'>
          <h2 class='c'>Invite Friends</h2>
          <div class='overhide'>
            <div class='right'>
              <a class='btn skip' href='#'>Skip</a>
              <div class='btn active'>Done</div>
            </div>
            <div class='left l'>
              <h2 class='fs14'>Facebook invite</h2>
              <span class='fs14'>Invite friends to Modera</span>
            </div>
          </div>
          <div class='result-panel'>
            <form action='#'></form>
            <input class="right btn invite-all <%= inviteAllActive() %>" name="commit" type="submit" value="Invite all" />
            <div class='h2'>
              <span><%= friends_count %></a></span>
              friends found
            </div>
          </div>
          <ul class="friend-list view-list"></ul>
        </div>