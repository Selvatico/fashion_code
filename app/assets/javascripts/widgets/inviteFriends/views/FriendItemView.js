define([
	'app',
	'backbone',
	'text!../templates/friendItemTemplate.tpl'
	], function(
	app,
	Backbone,
	friendItemTemplate
	) {
	return Backbone.View.extend({
		template: friendItemTemplate,
		tagName: 'li',
		className: 'item',
		events: {
			'click .follow': 'follow'
		},
		initialize: function() {
			this.$el.attr( 'id', this.model.uid );
			this.parentView = this.options.parent;
			this.parentView.on('inviteAllFriends', this.inviteAllFriends, this);
		},
		serialize: function() {
			return _.extend( _.clone( this.model ), { 
				userPicture: this.userPicture,
				checkActive: this.checkActive,
				followText:  this.followText 
			}
			);
		},
		userPicture: function() {
			return ( this.avatar ) ? this.avatar : '/assets/samples/username_pic.png';
		},
		checkActive: function() {
			return ( this.is_follow ) ? 'active' : '';
		},
		followText: function() {
			return ( this.is_follow ) ? 'Unfollow' : 'Follow'
		},
		inviteAllFriends: function() {
			this.model.is_follow = this.parentView.model.get('inviteAllActive');
		},
		follow: function( e ) {
			this.model.is_follow = !this.model.is_follow 
			this.render( this.serialize() );
		}
	});
});