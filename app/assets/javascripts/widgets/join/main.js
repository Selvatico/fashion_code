define([
	'app',
	'backbone',
	'widgets/signup/main',
	'text!./templates/layout.tpl'
], function (
	app,
	Backbone,
	SignupWidget,
	layoutTemplate
) {
	'use strict';
	
	return Backbone.Layout.extend({

		events : {
			'click .signup-link' : 'signup'
		},

		template: layoutTemplate,
		
		initialize: function() {},

		signup : function() {
			app.trigger( 'openModal', new SignupWidget( { page: this } ) );
		}
		
	});
});