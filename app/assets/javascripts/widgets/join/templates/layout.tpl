<h2>Join to Modera</h2>
<div class="joinModera-form">
	<a href="/auth/facebook" class="joinFacebook c disblock">
		<i class="moderaIcons left">f</i>
		<span>Join with Facebook</span>
	</a>
	<a href="/auth/twitter" class="joinTwitter c disblock">
		<i class="moderaIcons left">t</i>
		<span>Join with Twitter</span>
	</a>
	<a href="/auth/instagram" class="joinInstagram c disblock">
		<i class="moderaIcons left">i</i>
		<span>Join with Instagram</span>
	</a>
	<div class="divider c clear">
		<i class="moderaIcons rel">C</i>
	</div>
	<p class="fs14 c">
		or
		<a class="textUnderline signup-link" href="#">sign up</a>
		with your email
	</p>
	<p class="fs14 c">
		By creating an account, you agree to the
		<a class="textUnderline" href="/tos">Terms of service</a>
		and
		<a class="textUnderline" href="/privacy">Privacy Policy.</a>
	</p>
</div>