define([
	'app',
	'backbone',
	'text!./templates/layout.tpl',
	'./models/UserFillEmailModel'
	], function (
	app,
	Backbone,
	layoutTemplate,
	UserFillEmailModel
	) {
	'use strict';

	return Backbone.View.extend({

		template: layoutTemplate,

		model: new UserFillEmailModel,
        emailValue: null,
		events: {
			'submit .joinFillEmail-form': 'submit',
			'click label *': 'closeError',
			'focus label input': 'closeError'
		},
		placeholders: {
			'signup-email': 'Email'
		},
		initialize: function() {
			Backbone.Validation.bind( this );
            this.on( 'afterRender', this.afterRender, this );
		},
		serialize: function() {
			return { provider: $('.socialProviderName').data('provider') || 'Twitter' };
		},
		submit: function( e ) {
			e.preventDefault();
            var _self = this,
			    data = this.$('.joinFillEmail-form').serializeObject(), self = this;
			if ( this.model.set(data) && this.model.isValid(true) ) {
				// Bad way to save data.
				this.model.unset('user');
				var data = _.clone(this.model.attributes);
				this.model.save({ 'user': data }, {
					success: function( model, response ) {},
					error: function( model, xhr ) {
                        var response = JSON.parse( xhr.responseText );
                        if ( 'email' in response ) {
                            _self.$( '.email-wrapper' ).addClass( 'error' );
                            _self.$( '.message-error' ).text( response[ 'email' ][ 0 ] ).css( 'display', 'block' );
                            _self.emailValue = _self.$( '#signup-email' ).val();
                            _self.$( '#signup-email' ).val('');
                        } else if ( 'redirect_uri' in response ) {
                            location.href = response.redirect_uri;
                        }
					}
				});
			}
			else {
				this.$('[placeholder]').attr('placeholder', ''); 
				this.$('.alert-error').fadeIn();
			}
		},
		closeError: function( e ){
			var $el = $( e.currentTarget ).parent( 'label' ), $input = $el.find( 'input' );
			$el.removeClass( 'error' );
            this.$( '.message-error' ).css( 'display', 'none' );
            if ( this.emailValue ) {
                $input.val( this.emailValue );
            } else {
                $input.attr( 'placeholder', this.placeholders[ $input.attr('id') ] );    
            }			
		},
        afterRender: function( event ) {
            this.$( '#signup-email' ).placeholder();
        }
	});
});