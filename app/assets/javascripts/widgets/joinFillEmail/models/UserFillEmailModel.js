define([
	'backbone',
	], function( Backbone ) {
	return Backbone.Model.extend({
		defaults: {
			username: '',
			email: '',
			avatar_cache: '',
			password: ''
		},
		validation: {
			email: [{
				required: true,
				msg: 'Email can\'t be blank'
			},{
				pattern: 'email',
				msg: 'Email is invalid'
			}]
		},
		urlRoot: '/signup'
	});
});