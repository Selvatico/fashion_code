  <h2>Join with <%= provider %></h2>
  <form class="joinFillEmail-form" action="#">
    <p class="fs14">Please, enter your email</p>
    <label class="disblock rel c email-wrapper">
      <span class="close-error abs">×</span>
      <span class="message-error abs">Error message</span>
      <input id="signup-email" type="email" name="email" placeholder="Email"/>
    </label>
    <div class="divider clear"></div>
    <input type="submit" value="Join" class="btn active right">
  </form>