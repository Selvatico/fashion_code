define(function ( require ) {

	'use strict';

	var app					= require( 'app' ),
		SubMenuView			= require( 'widgets/submenu/main' ),
		SocialSubMenu		= require( 'widgets/socialsubmenu/main' ),
		ProfileSubMenu		= require( 'widgets/profileSubmenu/main' ),
		HashtagSubmenu		= require( 'widgets/hashtagSubmenu/main' ),
		HallSubMenu			= require( 'widgets/hallSubmenu/main' ),
		NotifySubmenu		= require( 'widgets/notificationSubmenu/main' ),
		PhotoSubmenu		= require( 'widgets/photoSubmenu/main' ),
		DynamicSubmenu		= require( 'widgets/dynamicSubmenu/main' ),
		SubMenuModel		= require( 'models/SubMenuModel' ),
		NotifyDropDown		= require( 'widgets/notifyDropDownmenu/main' ),
		headerTemplate		= require( 'text!./templates/layout.tpl' );

	return Backbone.Layout.extend({

		tagName				: 'header',
		className			: 'widget-Menu',
		template			: headerTemplate,
		dynamicSubmenu		: null,
		dynamicSubMenuTop	: false,
		page				: null,
		
		events: {			
			'click .notify-link'				: 'notification',
			'click nav.left ul li:last-child'	: 'showSecondarySubmenu',
            'click .profile-link'               : 'profilePage'
		},

		notificationModel: new SubMenuModel(),

		initialize: function() {
			
			app.on( 'show:message', this.showMessage, this );
			//app.on( 'header:update', this.setAvatar, this );
			app.on( 'hide:submenu', this.hideSubmenu, this );
			app.on( 'show:submenu', this.showSubmenu, this );

			this.notifySubmenu				= new NotifySubmenu( { parent: this, model: this.notificationModel } );
			this.notificationDropDownView	= new NotifyDropDown( { parent: this, model: this.notificationModel } );
						
			this.notificationModel.on( 'change', this.notificationReady, this );
			this.notificationModel.on( 'empty', this.notificationReady, this );
            this.notificationModel.on( 'faye:show', this.notificationReady, this );
						
			// app.router.on('all', this.changeRouter, this);
			Backbone.history.on( 'all', this.changeRouter, this );

			app.sendRequest('/subscription', {
				callback: this.fayeSubscribe,
				context: this
			});

		},
		
		/*setAvatar : function (data) {
			console.log('data',data);
		},*/
		/**
		 * Hide submenu for pages where no links there
		 */
		hideSubmenu: function () {
			this.$( '.w-nav-panel' ).addClass( 'disnone' );
			if ( this.fakeSubMenu ) {
				this.fakeSubMenu.remove();
			}
		},
		showSubmenu: function () {
			this.$( '.w-nav-panel' ).removeClass( 'disnone' );
		},
		/**
		 * render home page specific submenu
		 */
		renderHomeSubmenu: function () {
			this.setView( '.w-nav-panel', new SubMenuView( { parent: this } ) ).render();
		},
		/**
		 * Callback for receive connection data from Faye server
		 * @param {Object} data Connection data
		 * @param {Object} data.channel Channel subscribe to
		 */
		fayeSubscribe: function ( data ) {
			PrivatePub.sign( data );
			PrivatePub.subscribe( data.channel, this.newNotificatiosData, this );
		},
		/**
		 * Callback for every time when new faye package arrive.
		 * @param {Object} data Notifications counters
		 * @param {Object} data.stats Notifications counters
		 * @param {Object} data.stats.actions Count of unread activities
		 * @param {Object} data.stats.following Count of unread followers notifications
		 * @param {Object} data.stats.comments Count of unread comments notifications
		 */
		newNotificatiosData: function ( data ) {
			if ( typeof data != 'undefined' ) {
				if ( data.stats ) {
					this.notificationModel.set( _.extend( data.stats, { fayeEvent: true } ), { silent: true } );                    
                    this.notificationModel.trigger( 'faye:show' );                                        
                    this.$( '.notification-dd' ).removeClass( 'disnone' );  
				} else {
					this.notificationModel.load();
				}
			}
		},
		/**
		 * Show social submenu if we have data fot it.
		 * For example for power hour page
		 */
		renderSocialSubmenu: function () {
			this.setView( '.w-nav-panel', new SocialSubMenu( { parent: this } ) ).render();
		},
		/**
		 * Show profile navigation between tabs on profile page
		 */
		renderProfileSubmenu: function (parent) {

			this.profileSubMenu = new ProfileSubMenu( { parent: parent } );
			this.setView( '.w-nav-panel', this.profileSubMenu ).render();
		},
		/**
		 * Render menu for hashtag photo and versuses page
		 * @param parent
		 */
		renderHashTagMenu: function ( parent ) {
			this.setView( '.w-nav-panel', new HashtagSubmenu( { parent: parent } ) ).render();               
		},
		renderHallMenu: function (parent) {
			this.dynamicSubMenuTop = true;
			this.dynamicSubmenu = null;
			this.setView( '.w-nav-panel', new HallSubMenu( { parent: parent } ) ).render();
		},
		renderNotifySubmenu: function () {
			this.dynamicSubMenuTop = null;
			this.dynamicSubmenu = null;
			this.setView( '.w-nav-panel', this.notifySubmenu ).render();
			this.secondarySubmenu();
		},
		renderPhotoSubmenu: function () {
			this.fakeSubMenu = new PhotoSubmenu( { parent: this } );
			this.setView( '.w-nav-panel', this.fakeSubMenu ).render();
			this.dynamicSubmenu = null;
			this.dynamicSubMenuTop = true;
			this.$( 'nav.left ul li:last-child' ).find( '.moderaIcons' ).text( '>' );
		},
		serialize: function () {
			var data = app.models.userModel.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		/**
		 * Action when member click notifications link in header. Show dropdown with counters
		 * @param event
		 */
		notification: function ( event ) {
			event.preventDefault();
			if ( this.$( '.notification-dd' ).hasClass( 'disnone' ) ) {
				this.notificationModel.load();
				this.$( '.notification-dd' ).removeClass( 'disnone' );
			} else {
				this.$( '.notification-dd' ).addClass( 'disnone' );
			}
		},
		/**
		 * Callback when notifications counter data loaded
		 */
		notificationReady: function () {
			this.setView( '.notification-dd', this.notificationDropDownView ).render();
		},
		/**
		 * Wtf?
		 * @param route
		 * @param router
		 * @param callback
		 */
		changeRouter: function ( route, router, callback ) {            
			var callbacks = ['hall_of_fame', 'photos', 'chooseStyle', 'inviteFriends', 'powerHour'];
                        
			if ( callbacks.indexOf( callback ) != -1 ) {
				this.$( '.w-nav-panel' ).addClass( 'disnone' );
			} else {
				this.$( '.w-nav-panel' ).removeClass( 'disnone' );
			}

			if ( Backbone.history.getFragment() == 'settings' ) {
				this.$( '.profile-menu .buttonsBlock' ).hide();
			} else {
				this.$( '.profile-menu .buttonsBlock' ).show();
			}
		},
		/**
		 * Show messages to user in submenu
		 * @param options
		 */
		showMessage: function ( options ) {
			this.$('.w-nav-panel').css({
				// 'height'	: '41px',
				'overflow'	: 'hidden'
			});
			
			var margin = -47,
				hideSubmenu = this.$( '.w-nav-panel' ).hasClass( 'disnone' );

			if ( hideSubmenu ) {
				this.renderPhotoSubmenu();
				app.trigger( 'show:submenu' );
			}

			var $message = this.$('.message-block');

			$message
				.removeClass('disnone')
				.css( { 'margin-top' : margin + 'px'} )
				.find( '.' + options.type )
				.removeClass( 'disnone' )
				.find( 'span' )
				.html( options.text );

			$message.animate( {
				'margin-top': '-4px'
			}, 800, function () {
				setTimeout( function () {
					$message.animate( {
						'margin-top': margin + 'px'
					}, 1000, function () {
						$message.addClass( 'disnone' ).find( '.' + options.type ).addClass( 'disnone' );
						if ( hideSubmenu ) {
							app.trigger( 'hide:submenu' );
						}
					} );
				}, 1800 )
			} );
		},
		/**
		 * Show menu with About, Logout links
		 * @param eventSender
		 */
		showSecondarySubmenu : function ( eventSender ) {
			if ( this.$( '.w-nav-panel' ).hasClass( 'disnone' ) ) {
				$( '.w-nav-panel' ).removeClass( 'disnone' )
			}
			if ( this.dynamicSubMenuTop ) {
				this.subMenuFromTop( eventSender );
				return;
			}
			var status = $( eventSender.currentTarget ).find( '.moderaIcons' ).text();

			if ( !this.dynamicSubmenu ) {
				this.dynamicSubmenu = new DynamicSubmenu( { parent: this } );
				this.$( '.w-nav-panel' ).wrapInner( '<div class="submenuWraper"><div class="submenuHolder"></div></div>' );

			}
			this.insertView( '.submenuHolder', this.dynamicSubmenu ).render();
			this.$( '.submenuWraper' ).css( { 'width': '1002px', 'margin': 'auto', 'overflow': 'hidden' } );
			this.$( '.submenuHolder' ).css( { 'width': '2004px' } );
			this.$( '.submenuHolder>div' ).css( { 'display': 'inline-block' } );

			var that = this;
			var speed = 50;
			if ( status == '>' ) {
				var margin = 0;
				$( eventSender.currentTarget ).find( '.moderaIcons' ).text( 'W' );

				var interval = setInterval( function () {
					margin -= speed;
					that.$( '.submenuHolder' ).css( { 'margin-left': margin + 'px'} );
					if ( margin < -1002 ) {
						margin = -1002;
						that.$( '.submenuHolder' ).css( { 'margin-left': margin + 'px'} );
						clearInterval( interval );
					}
				}, 20 );
			} else {
				$( eventSender.currentTarget ).find( '.moderaIcons' ).text( '>' );
				var margin = -1002;
				var interval = setInterval( function () {
					margin += speed;
					that.$( '.submenuHolder' ).css( { 'margin-left': margin + 'px'} );
					if ( margin > 0 ) {
						margin = 0;
						that.$( '.submenuHolder' ).css( { 'margin-left': margin + 'px'} );

						//that.$('.' + options.type).addClass('disnone');
						clearInterval( interval );
					}
				}, 20 );
			}
		},
		
		subMenuFromTop : function ( eventSender ) {

			var status = $( eventSender.currentTarget ).find( '.moderaIcons' ).text();
			if ( !this.dynamicSubmenu ) {
				this.dynamicSubmenu = new DynamicSubmenu( { parent: this } );
				this.$( '.w-nav-panel' ).wrapInner( '<div class="submenuWraper"><div class="submenuHolder"></div></div>' );
			}
			this.insertView( '.submenuHolder', this.dynamicSubmenu ).render();
			this.$( '.submenuWraper' ).css( { 'width': '1002px', 'margin': 'auto', 'overflow': 'hidden' } );

			if ( status == 'W' ) {
				this.$( '.w-nav-panel' ).addClass( 'disnone' );
			}

			if ( status == '>' ) {
				$( eventSender.currentTarget ).find( '.moderaIcons' ).text( 'W' );
			} else {
				$( eventSender.currentTarget ).find( '.moderaIcons' ).text( '>' );
			}
		},
		
		secondarySubmenu : function(){
			if ( !this.dynamicSubmenu ) {
				this.dynamicSubmenu = new DynamicSubmenu( { parent: this } );
				this.$( '.w-nav-panel' ).wrapInner( '<div class="submenuWraper"><div class="submenuHolder"></div></div>' );

			}
			this.insertView( '.submenuHolder', this.dynamicSubmenu ).render();
			this.$( '.submenuWraper' ).css( { 'width': '1002px', 'margin': 'auto', 'overflow': 'hidden' } );
			this.$( '.submenuHolder' ).css( { 'width': '2004px' } );
			this.$( '.submenuHolder>div' ).css( { 'display': 'inline-block' } );
		},
        
        profilePage: function( event ) {
            var paths = location.pathname.split( '/' ), link;
            if ( paths[ 1 ] == 'profiles' && paths[ 2 ] == app.models.userModel.get( 'username' ) ) {
                app.navigate( 'photos' );
            } else {
                location.href = '/profiles/' + app.models.userModel.get( 'username' );
            }
            return false;
        }
	});

});
