<div class="top-panel">
	<div class="width">
		<a class="logo left" href="/home/"><img src="<%= url %>/assets/logo.png" alt="Modera.co"/></a>
		<nav class="left textTransUpper">
			<ul>
				<li>
					<!-- <a href="#challenges"> -->
					<a href="#hall_of_fame" class="leader-link">
						<i class="moderaIcons disblock">5</i>
						LeaderBoards
					</a>
				</li>
				<li>
					<a href="#photos/new" class="photo-link">
						<i class="moderaIcons disblock">E</i>
						Upload Photo
					</a>
				</li>
				<li>
					<a href="#notifications" class="notify-link">
						<i class="moderaIcons disblock">F</i>
						Notifications
					</a>
					<div class="notification-dd rel disnone fs14">
					</div>
				</li>
				<li class="textTransNone">
					<a title="<%= username %>" href="/profiles/<%= username %>/" class="textOverhide rel profile-link">
						<img width="30" height="30" src="<%= url %><%= avatar %>" class="userpic" alt="<%= username %>"/>
						<span><%= username %></span>
					</a>
				</li>
				<li class="rel" id="subMenuLink">
					<span class="curpoint">
						<i class="moderaIcons m">&gt;</i>
					</span>
					<!-- <div class="more-dd abs thin">
						<i class="moderaIcons m abs">W</i>
						<a href="/about">About</a>
						<a href="#">Brands</a>
						<a href="#">Help</a>
					</div> -->
				</li>
			</ul>
		</nav>
	</div>
</div>
<div class="w-nav-panel"></div>
