define([
	'underscore',
	'backbone',
	'validation'
], function(){
	_.extend(Backbone.Validation.callbacks, {
		valid: function(view, attr, selector){
			var control = view.$('[' + selector + '=' + attr + ']');
			var group = control.parents(".disblock");
			group.removeClass("error");
			group.find(".message-error").hide();
		},
		invalid: function(view, attr, error, selector) {
			var control = view.$('[' + selector + '=' + attr + ']');
			var group = control.parents(".disblock");
			group.addClass("error");
			var target = group.find(".message-error");
			target.text(error);
		}
	});
});