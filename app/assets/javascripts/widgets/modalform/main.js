define([
	'app',
	'backbone',
	'./helpers/serializeObject',
	'./helpers/validationHandler'
], function (
	app,
	Backbone
	) {
	'use strict';

	return Backbone.Layout.extend({

		initialize: function(){
			Backbone.Validation.bind(this);
			return this;
		},

		events: {
			'submit': 'submit',
			'click label *': 'closeError',
			'focus label input': 'closeError'
		},

		submit: function(e){
			e.preventDefault();
			var data = this.$('form').serializeObject();
			if(this.model.set(data) && this.model.isValid(true)){
				alert('Validated successfully!');
			}
			else {
				this.$('.alert-error').fadeIn();
			}
		},

		closeError: function(e){
			jQuery(e.currentTarget).parent('label').removeClass('error');
		}

	});
});
