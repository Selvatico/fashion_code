define([
	'backbone',
	'text!./templates/layout.tpl',
	'../modalform/main',
	'./models/NewPasswordModel'
], function (
	Backbone,
	layoutTemplate,
	ModalForm,
	NewPasswordModel
) {
	'use strict';

	return ModalForm.extend({
		
		template: layoutTemplate,

		model: new NewPasswordModel(),
		
		initialize : function () {
			Backbone.Validation.bind(this);
		},
		
		submit: function(e){
			e.preventDefault();
			var data = this.$( 'form' ).serializeObject();
			data = _.defaults( data, { 'reset_password_token': $( '.resetPasswordToken' ).data( 'token' ) } );
			this.model.set( data )
			if ( this.model.isValid( 'password2' ) ) {
				this.model.newPassword( { 'user': data }, function ( response ) {
				} );
			}
			else {
				this.$( '.alert-error' ).fadeIn();
			}
		}
		
	});
});