define([
    'app',
	'backbone',
], function( app, Backbone ) {
	return Backbone.Model.extend({
		
		method : 'POST',
		
		validation: {
			password1 : {
			      required: true
		    },
		    
			password2: function( data ){
				//console.log('validation',this.get('password1'), this.password2);
				if(this.get('password1') && this.get('password2')){
					if(this.get('password1') == this.get('password2')){
						return true;
					}
				}
				return false;
			}
		},
		
		forgotPassword : function ( map, callback ) {//request: { "photo_id": ID выбранной фотографии	}
			//var that = this;
			app.sendRequest ( 
					'/password', //this.urlRoot, 

					{
						data : map,
						method : 'PUT',

						callback : callback
					} );
		},
		
		newPassword : function ( map, callback ) {//request: { "photo_id": ID выбранной фотографии	}
			//var that = this;
			app.sendRequest ( 
					'/users', //this.urlRoot, 

					{
						data : map,
						method : this.method,

						callback : callback
					} );
		}
	});
});