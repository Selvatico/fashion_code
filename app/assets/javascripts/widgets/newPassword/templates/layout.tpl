<h1>New Password</h1>
<form action="#" class="newPass-form">
	<p class="fs14">Enter your password bellow</p>
	<label class="disblock rel c"> <span class="close-error abs">×</span>
		<span class="message-error abs">Error message</span>
		<input placeholder="New Password" type="password" name="password">
	</label> <label class="disblock rel c">
		<div class="close-error abs">×</div>
		<input placeholder="New Password Confirmation" type="password" name="password_confirmation">
	</label>
	<div class="divider clear"></div>
	<input class="btn active right" type="submit" value="Ok">
</form>
