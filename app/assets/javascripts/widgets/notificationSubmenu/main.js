define( [
	'app', 'text!./templates/layout.tpl', 'layoutmanager'
], function ( app, subMenuTemplate ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'widget-hashtagSubMenu',
		events: {
			'click a': 'clickToMenu'
		},
		template: subMenuTemplate,
		initialize: function () {
			app.on( 'set:active:notify', this.setActiveNotify, this);
		},

		serialize: function () {
			return this.model.toJSON();
		},
		clickToMenu: function ( event ) {
			this.$( 'a' ).removeClass( 'active' );
			$( event.target ).addClass( 'active' );
		},
		setActiveNotify: function (section) {
			this.$( 'a' ).removeClass( 'active' );
			this.$( 'a.' + section + '-link' ).addClass( 'active' );
		}
	} );

} );
