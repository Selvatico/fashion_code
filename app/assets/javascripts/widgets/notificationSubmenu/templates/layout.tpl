<div class="width">
    <div class="w-nav left">
        <a class="followings-link" href="#notifications/followings">
            <i class="moderaIcons disinbl">S</i>
            Followers
            <span><%= following %></span>
        </a>
        <a class="comments-link" href="#notifications/comments">
            <i class="moderaIcons disinbl">H</i>
            Comments
            <span><%= comment %></span>
        </a>
        <a class="actions-link" href="#notifications/actions">
            <i class="moderaIcons disinbl">C</i>
            Activity
            <span><%= action %></span>
        </a>
    </div>
</div>