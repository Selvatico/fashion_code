/**
 * Layout for render leaders rows
 * @exports FlameHallWidget
 */
define( [
	'app',
	'text!../templates/contestFinishedItem.tpl',
	'text!../templates/contestStartedItem.tpl',
	'text!../templates/contestTenItem.tpl',
	'text!../templates/levelDownItem.tpl',
	'text!../templates/levelUpItem.tpl',
	'text!../templates/photoReportItem.tpl',
	'text!../templates/votedItem.tpl',
	'text!../templates/wonContestItem.tpl'
], function (
	app,
	contestFinishedItem,
	contestStartedItem,
	contestTenItem,
	levelDownItem,
	levelUpItem,
	photoReportItem,
	votedItem,
	wonContestItem
	) {

	'use strict';

	return {
		contest_started : contestStartedItem,
		voted : votedItem,
		won_contest : wonContestItem,
		contest_finished: contestFinishedItem,
		photo_inappropriated: photoReportItem,
		level_up: levelUpItem,
		level_down: levelDownItem,
		contest_ten: contestTenItem
	};

} );
