/**
 * Layout for render leaders rows
 * @exports FlameHallWidget
 */
define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'collections/NotificationsCollection',
	'./views/FollowItem',
	'./views/CommentItem',
	'./views/ActivityItem',
	'./helper/templates',
	'layoutmanager'
], function ( app, widhetLayout, NotificationsCollection, FollowItem, CommentItem, ActivityItem, templatesHolder ) {

	'use strict';

	return Backbone.View.extend( {

		template: widhetLayout,
		model    : new Backbone.Model( {page: 1, endReached: false} ),
		className: 'width widget-Notifications',
		tagName  : 'div',
		
		events : {
			'click .poster-block-text>div.h1' : 'hallOfFane'
		},
		initialize: function () {		    
			var _self = this;
			this.collection = new NotificationsCollection( [], { a:1} );

			this.collection.on( 'add', this.renderRow, this );
            this.collection.on( 'notify:empty', this.notifyEmpty, this );
            this.collection.on( 'notify:loaded', this.notifyLoaded, this );

			this.model.set( { 'page': 1, 'endReached': false } );

			this.loadData();
			$( window ).on('scroll', function (event) {
				_self.catchScrollEvent(event);
			});
            this.model.set( 'type', this.options.type );
		},
		/**
		 * Proxy method to load data. Check if reached the end and increment page
		 */
        titles: { actions: 'ACTIVITY', followings: 'FOLLOWERS', comments: 'COMMENTS' }, 
		loadData: function () {
			var data = {page: this.model.get( 'page' )};
			//data.following = false;
			data.per_page  = 50;
			data.type      = this.options.type || 'actions';

			this.model.set( {currentSection: data.type} );
                        
			//  THIS CODE IS REQUIRED BECAUSE IN OTHER CASE WHEN NO DATA THE REQUESTS WIL BE SEND AGAIN AND AGAIN
			if ( !this.model.get( 'endReached' ) ) {
				this.model.attributes.page++;
				this.collection.load( data );
			}
		},
		/**
		 * Get row constructor regarding type
		 * @param {String} type Notification type
		 * @returns {Array|Backbone.View}
		 * @private
		 */
		_getNotifyRow: function ( type ) {
			if ( {new_follower: 1, friend_won: 1, facebook_friend: 1}[type] ) {
				return [FollowItem, 'follow'];
			} else if ( {contest_started: 1, voted: 1, won_contest: 1, contest_finished: 1, photo_inappropriated: 1, level_up: 1, level_down: 1, contest_ten: 1}[type] ) {
				return [ActivityItem, 'activity'];
			} else if ( {commented: 1, mentioned: 1}[type] ) {
				return [CommentItem, 'comment'];
			}
		},
		/**
		 * Insert one leader row
		 * @param {Backbone.Model} model
		 */
		renderRow: function ( model ) {


			if ( model.get( 'kind' ) ) {
				model.set( 'type', model.get( 'kind' ) );
			}

			var sectionData = this._getNotifyRow( model.get( 'type' ) ),
				viewParams = {
					parent: this, model: model
				};

			if ( sectionData[1] ) {
				viewParams.template = templatesHolder[model.get( 'type' )];
			}

			if (!this.model.get('read')) {
				this.$( '.new-h2' ).removeClass( 'disnone' );
			} else {
				this.$( '.old-h2' ).removeClass( 'disnone' );
			}

			this.insertView( ((model.get( 'read' )) ? '.old-list' : '.new-list'), new sectionData[0]( viewParams ) ).render();
		},
		/**
		 * Catch scroll event and if scrolled 75% of page load more notifications
		 */
		catchScrollEvent: function () {
			var $body = $( 'body' ),
				pathLength = $body.get( 0 ).scrollHeight - $body.height(),
				percent    = ( $body.scrollTop() / pathLength) * 100;

			//we scrolled 75% of scroll
			if ( percent > 75 ) {
				this.loadData();
			}
		}, 
        serialize: function() {
            return { title: this.titles[ this.options.type ], 
                     titleLower: this.titles[ this.options.type ].toLowerCase() };
        },
        notifyEmpty: function() {
			var sectionsSigns = {
				actions : 'F',
				comments: 'H',
				followings : 'S'
			};
            this.$( '#notification-wrapper, .notification-title' ).addClass( 'disnone' );
            this.$( '.faceplate' ).removeClass( 'disnone' );
			this.$( '.faceplate' ).find( '.moderaIcons' ).html( sectionsSigns[this.model.get( 'currentSection' )] );

		},
        notifyLoaded: function(data) {
			this.$( '.notification-title' ).removeClass( 'disnone' );
			app.trigger( 'set:active:notify', this.model.get( 'currentSection' ) );
			if (data.length == 0 ) {
				this.model.set('endReached', true);
			}
		},
		
		hallOfFane : function ( eventSender ) {
			//console.log($(eventSender.currentTarget).attr('contestId'));
			app.navigate('/home#hall_of_fame/' + $(eventSender.currentTarget).attr('contestId'),true );
		}
	} );

} );
