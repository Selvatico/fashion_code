<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <a class="right" href="#" title="<%= notification.user.name %>">
        <img alt="<%= notification.user.name %>" class="m" height="44" src="<%= url %><%= notification.photo.image %>" width="44" />
    </a>

    <div class="not-icon left">
        <i class="moderaIcons happy m">H</i>
    </div>
    <p class="fs14 nomrg thin">
        <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>"><b><%= notification.user.name %></b></a>
        <% if (type == 'commented') { %>
        left a comment on your <a href="/profiles/<%= username %>/#photos/<%= notification.photo.id %>">photo:</a>

        <% } else if (type == 'mentioned') { %>
        mentioned you in a <a href="/profiles/<%= username %>/#photos/<%= notification.photo.id %>">comment:</a>
        <% } %>
        <i><%= notification.comment.text %> </i>
    </p>
</div>
