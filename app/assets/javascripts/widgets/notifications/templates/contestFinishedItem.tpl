<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <div class="not-poster left">
        <img alt="Contest" class="m" src="<%= url %><%= notification.contest.prize %>" width="271" />
    </div>
    <div class="poster-block-text">
        <div class="h1 c" contestId="<%=notification.contest.id%>"><%= notification.contest.name %></div>
        <p class="c"> has just finished</p>
    </div>
</div>
