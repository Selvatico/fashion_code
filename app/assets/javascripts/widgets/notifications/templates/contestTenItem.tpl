<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <div class="not-poster left">
        <img alt="Contest" class="m" src="<%= url %><%= notification.contest.prize %>" width="271" />
    </div>
    <div class="poster-block-text">
        <p class="c"> You are in the top 10 users in  <br> <a href="/home#hall_of_fame/<%=notification.contest.id%>"><%= notification.contest.name %> contest</a>
        </p>
    </div>
</div>
