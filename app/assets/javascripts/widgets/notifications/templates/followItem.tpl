<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <div class="not-icon left">
        <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>">
            <img alt="<%= notification.user.name %>" class="userpic" height="42" src="<%= url %><%= notification.user.avatar %>" width="42" />
        </a>
    </div>
    <p class="fs14 nomrg thin">
        <% if (type == 'new_follower') { %>
        <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>"><b><%= notification.user.name %></b></a> started following you
        <% } else if (type == 'friend_won') { %>
        <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>"><b><%= notification.user.name %></b></a> you follow just won a contest
        <% } else if (type == 'facebook_friend') { %>
        Your friend <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>"><b><%= notification.user.name %></b></a> from Facebook just joined Modera
        <% } %>

    </p>
</div>
