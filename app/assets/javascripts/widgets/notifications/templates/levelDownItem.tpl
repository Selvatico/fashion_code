<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <div class="c fs14 place">
        <i class="moderaIcons m">(</i>
        You just moved to <a class="textUnderline" href="#hall_of_fame"> <%= notification.place %> <sub>th</sub> </a> place
    </div>
</div>