<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <a class="right" href="/profiles/<%=usernameOwner %>#browse_legacy/<%= notification.photo.id %>" title="#">
        <img alt="#" class="m" height="44" src="<%= url %><%= notification.photo.image %>" width="44" />
    </a>

    <div class="not-icon left">
        <i class="moderaIcons happy">D</i>
    </div>
    <p class="fs14 nomrg thin"> Your <a class="textUnderline" href="/profiles/<%=usernameOwner %>#browse_legacy/<%= notification.photo.id %>">photo</a> has been reported
    </p>
</div>
