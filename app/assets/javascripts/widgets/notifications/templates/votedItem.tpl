<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <% if (notification.photo != null) { %>
    <a class="right" href="/profiles/<%=usernameOwner %>#browse_legacy/<%= notification.photo.id %>" title="">
        <img alt="<%= notification.user.name %>" height="44" src="<%= url %><%= notification.photo.image %>" width="44" />
    </a>
    <% } %>
    <div class="not-icon left">
        <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>">
            <img alt="<%= notification.user.name %>" class="userpic" height="42" src="<%= url %><%= notification.user.avatar %>" width="42" />
        </a>
    </div>
    <p class="fs14 nomrg thin">
        <a href="/profiles/<%= notification.user.username %>" title="<%= notification.user.name %>">
            <b><%= notification.user.name %></b></a> voted for your <a class="textUnderline" href="/profiles/<%=usernameOwner %>#browse_legacy/<%= notification.photo.id %>">photo</a>
    </p>
</div>
