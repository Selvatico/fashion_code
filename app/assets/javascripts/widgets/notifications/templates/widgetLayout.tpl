<div class="container rel">
    <h2 class="c notification-title disnone">Notifications</h2>
    <section id="notification-wrapper">
        <h2 class="textTransNone rel disnone new-h2">New <%= title %> <span></span></h2>

        <div class="notification-list view-list new-list">

        </div>

        <h2 class="disnone old-h2">Older</h2>

        <div class="notification-list view-list old-list">

        </div>
    </section>
    <div class="faceplate c disnone">
        <i class="moderaIcons">F</i>
        <span class="thin disblock">There is no <%= titleLower %> to show at the moment</span>
    </div>    
</div>