<div class="right">
    <div class="timeline right"><%= ago %></div>
</div>
<div class="left">
    <div class="not-poster left">
        <img alt="Congratulations!" class="m" src="<%= url %><%= notification.contest.prize %>" width="271" />
    </div>
    <div class="poster-block-text">
        <div class="h1 c">Congratulations</div>
        <p class="c"> you won
            <br>
            <a href="/home#hall_of_fame/<%=notification.contest.id%>"><%= notification.contest.name %> Contest</a>
        </p>
    </div>
</div>
