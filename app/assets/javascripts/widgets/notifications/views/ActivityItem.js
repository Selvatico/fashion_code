define( [
	'backbone',
	'app'
], function ( Backbone, app ) {
	return Backbone.View.extend( {
		tagName    : 'div',
		className  : 'item',
		initialize : function () {},
		serialize  : function () {
			var data = this.model.toJSON();
			data.ago = moment( this.model.get( 'created_at' ), 'YYYY-MM-DD HH:mm:ssZ' ).fromNow();
			data.usernameOwner = app.models.userModel.get( 'username' );
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		/**
		 * Set class me if row in leader board bout me
		 */
		afterRender: function () {
			if ( !this.model.get( 'read' ) ) {
				this.$el.addClass( 'new' );
			}
		}
	} );
} );