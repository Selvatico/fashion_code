define( [
	'backbone',
	'text!../templates/commentItem.tpl',
	'app'
], function ( Backbone, hallRowTemplate, app ) {
	return Backbone.View.extend( {

		events: {

		},
		template   : hallRowTemplate,
		tagName    : 'div',
		className  : 'item',
		initialize : function () {},
		serialize  : function () {
			var data = this.model.toJSON();
			return _.defaults( data, {
				'url'		: app.getURL(),
				'ago'		: moment( this.model.get( 'created_at' ), 'YYYY-MM-DD HH:mm:ssZ' ).fromNow(),
				'username'	: app.models.userModel.get( 'username' )
			});
		},
		/**
		 * Set class me if row in leader board bout me
		 */
		afterRender: function () {
			if ( !this.model.get( 'read' ) ) {
				this.$el.addClass( 'new' );
			}
		}
	} );
} );