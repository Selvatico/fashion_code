define( [
	'app',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function ( app, subMenu ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'widget-notifyDropDown',

		template: subMenu,
        events: {
            'click a': 'closeMenu',
            'mouseout': 'fayeHideDropDown',
            'mouseover': 'fayeShowDropDown' 
        },
        initialize: function() {
            _.bindAll( this, 'checkNew', 'fayeTimeoutEnd' );
            app.on( 'close:dropdown', this.close, this );
            this.parentView = this.options.parent;
            this.options.parent.notificationModel.on( 'faye:show', this.fayeHideDropDown, this );
        },
        serialize: function() {
            return _.extend( this.model.toJSON(), {
                checkNew: this.checkNew
            });
        },
        checkNew: function( param ) {
            return ( this.model.get( param ) > 0 ) ? 'new' : '';
        },
        closeMenu: function( event ) {
            $( event.target ).addClass( 'active' );
			this.options.parent.$( '.notification-dd' ).addClass( 'disnone' );
		},
		close : function( e ){
			if ( !$( '.notification-dd' ).is(':hidden') && !$(e.target).parent('.notify-link').length )
				$( '.notification-dd' ).addClass( 'disnone' );
		},
        fayeHideDropDown: function( event ) {            
            if ( this.options.parent.notificationModel.get( 'fayeEvent' ) ) {
                this.fayeTimeout = setTimeout( this.fayeTimeoutEnd, 3000 );
            }
        },
        fayeShowDropDown: function ( event ) {
            if ( this.options.parent.notificationModel.get( 'fayeEvent' ) ) {
                clearTimeout( this.fayeTimeout );
            }
        },
        fayeTimeoutEnd: function() {
            this.options.parent.$('.notification-dd').addClass( 'disnone' );
            this.options.parent.notificationModel.set( 'fayeEvent', false );
        }
	} );

} );
