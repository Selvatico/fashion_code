<a href="#notifications/followings">
    <i class="moderaIcons">S</i>
    <b><%= following %></b>
    <span class="thin"><%= checkNew('following') %> followers</span>
</a>
<a href="#notifications/comments">
    <i class="moderaIcons">H</i>
    <b><%= comment %></b>
    <span class="thin"><%= checkNew('comment') %> @comments</span>
</a>
<a href="#notifications/actions">
    <i class="moderaIcons">C</i>
    <b><%= action %></b>
    <span class="thin"><%= checkNew('action') %> Activity</span>
</a>

