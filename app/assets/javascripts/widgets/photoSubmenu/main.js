define( [
	'app',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function ( app, subMenu ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'width widget-PhotoSubMenu',

		template: subMenu,

		initialize: function () {
			
		}
	} );

} );
