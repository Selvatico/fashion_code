define([
	'app',
	'backbone',
	'text!./templates/layout.tpl'
	], function (
		app,
		Backbone,
		layoutTemplate
		) {
	'use strict';
	return Backbone.Layout.extend({

		template: layoutTemplate,
		className : '',
		photos : [],
		page : 1,
		photosCount : null,
		contestId : null,

		events: {
			'click .upload-photo' 		: 'uploadPhoto',
			'click .terms' 				: 'terms',
			'click .photoframe' 		: 'checkPhoto',
			'mousewheel .photo-list'	: 'scroll',
			'wheel .photo-list'			: 'scroll'
		},

		initialize: function() {
			app.on( 'photos:loaded', this.setData, this );
			this.contestId = this.options.contestId;
			//app.collections.PhotosCollection.photoList({  'page' : this.page });
			this.getPhotos();
		},

		getPhotos : function(){
			app.collections.PhotosCollection.photoList({ 'page' : this.page });
		},

		setData : function ( options ) {
			if( options.photos_count - this.photos.length <= 0 ) return;
			//console.log(' photos', options.photos);
			//console.log(this.page,options.photos_count,this.photos.length);
			this.page++;
			this.photosCount = options.photos_count;
			this.photos = this.photos.concat ( options.photos );
			//console.log('this.photos', this.photos);
			if( this.page == 2 )
				this.render ();
			else
				this.insertPhotos( options.photos );
		},

		insertPhotos : function ( photos ) {
			var $list = this.$( '.photo-list' );
			for( var i in photos ) {
				var item = '<div class="item"><div class="photoframe" photoId="' + photos [ i ].id + '">'+
				'<input src="' + app.getURL()  + 'assets/checked.png" type="image" />'+
				'<img alt="name photo" height="172" src="' + app.getURL() + photos [ i ].image + '" width="172" /></div></div>';
				$list.append ( item );
			}
		},

		serialize : function () {
			return { 'data' : this.photos, 'url' : app.getURL() };
		},

		uploadPhoto : function () {
			//console.log ( 'upload photos' );
			//console.log(this.$('.marcked'));
			var selectedImg = this.$( '.marcked' ).attr( 'photoId' );
			app.models.ContestModel.joinContest ( { "photo_id" : selectedImg }, this.contestId );
			//console.log(selectedImg);
			return false;
		},

		afterRender : function () {
			this.$( '.view-photo' ).css ( { 'height' : '338px' } );

			this.$( '.photo-list' ).css( {
				'height' : '338px',
				'overflow' : 'auto',
				'width' : '588px'
			} );
		},

		terms : function () {
			location.assign( '/tos' );
			return false;
		},

		scroll : function (e) {
			//console.log(e.currentTarget.scrollTop, (e.currentTarget.scrollHeight - 338),(e.currentTarget.scrollHeight - 338)-e.currentTarget.scrollTop);
			if( (e.currentTarget.scrollHeight - 338) - e.currentTarget.scrollTop < 300 ) this.getPhotos();
		}, 

		checkPhoto : function (e) {
			this.$('.photoframe').removeClass('marcked');
			this.$('.photoframe input').hide();
			$(e.currentTarget).addClass('marcked');
			$(e.currentTarget).find('input').css( {'display' : 'block' } );
			//el.show();
			//el.css( {'display' : 'block' } );
			//$(e.currentTarget).toggleClass('marcked');
		}
	});
});