<!-- /choose photo -->
<!-- <div class="close right">&times;</div> -->
<h1>Choose photo for contest</h1>
<div class="choose-photo-contest c">
	<div class="view-photo">
		<div class="photo-list">
			<%for(var i in data){%>
			<div class="item">
				<div class="photoframe" photoId="<%=data[i].id%>">
					<input src="<%= url %>assets/checked.png" type="image" /> <img alt="name photo" height="172"
						src="<%= url %><%=data[i].image%>"
						width="172" />
				</div>
			</div>

			<%}%>
		</div>

	</div>
	<a href="#" class="btn upload-photo">Upload Photo</a>
</div>

<div class="divider clear"></div>
<p class="fs14 c thin">
	By uploading a picture to the sponsorship contest you are <br>
	agreeing to our terms and conditions
</p>
<p class="fs14 c thin nomrg">
	<a href="#" class="terms">Terms and conditions</a>
</p>