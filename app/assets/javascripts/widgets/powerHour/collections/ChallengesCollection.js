/**
 * @exports ChallengesCollection
 */
define( [
	'app',
	'backbone'
], function ( app, Backbone ) {
	return Backbone.Collection.extend( {
		//model: new Backbone.Model(),
		url  : '/power_hour',
		load : function ( params ) {
			var _self = this;
			app.sendRequest( this.url, {
				callback: function ( data ) {
					if ( data ) {
						_self.add( data );
						app.trigger( 'power:hours:loaded', _self, data );
					}
				},
				data: params || {}
			} );
		}
	} );
} );