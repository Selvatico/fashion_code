/**
 * @uthor D. Seredenko
 * @export VotersMainLayout
 * @namespace home
 */
define( [
	'app',
	'text!./templates/widgetTemplate.tpl',
	'../versus/views/BattleView',
	'./collections/ChallengesCollection',
	'modules/versusScroller/main'
], function ( app, widgetTpl, BattleView, ChallengesCollection, versusScroller) {

	'use strict';

	return Backbone.View.extend( {

		template : widgetTpl,

		className: 'wrapper widget-powerHour',
		/**
		 * @var Number
		 */
		page : 1,

		events: {
			'mousewheel .battle-list': 'scrollCatch'
		},

		initialize: function () {

			this.collection = new ChallengesCollection();

			//subscribe to load challenges event
			app.on( 'power:hours:loaded', this.renderChallenges, this );

			this.loadChallenges();
		},


		scrollCatch: function ( event ) {
			versusScroller( event, '.battle', 'activeBattle', this );
		},
		/**
		 * Render all challenges
		 * @param collection
		 */
		renderChallenges: function ( collection ) {
			if ( collection.length > 0 ) {
				collection.each( this.renderChallenge, this );
				this.$('.battle-list' ).removeClass('disnone');
				this.options.parent.headerView.renderSocialSubmenu();
			}
			this._showHolders();
		},
		/**
		 * Service method to show right info holder for
		 * current challenge status and estimated time for status
		 * @private
		 */
		_showHolders: function () {
			var powerHourData;

			if ( app.models.ServerDataModel.get( 'powerhour' ) ) {
				powerHourData = app.models.ServerDataModel.get( 'powerhour' );
				switch ( powerHourData.status ) {
					case 'new':
						if ( this.collection.length > 0 ) {
							this.$( '.start-new' ).removeClass( 'disnone' );
						} else {
							this.$( '.no-challenges' ).removeClass( 'disnone' );
						}
						break;

					case 'started' :
					case 'finished':
						var holderClass = ( this.collection.length > 0 ) ? '.' + powerHourData.status + '-time' : '.no-challenges-' + powerHourData.status;
						this.$( holderClass )
							.removeClass( 'disnone' )
							.find( '.time-ph' )
							.html( this._getTimeString( powerHourData.time ) );
						break;
				}
			}
		},
		/**
		 * Construct estimate time for holder
		 * @param {Object} timeObj Challenge time data
		 * @param {Number} [timeObj.hours] Hours
		 * @param {Number} timeObj.minutes Minutes
		 * @param {Number} timeObj.seconds Minutes
		 * @returns {string}
		 * @private
		 */
		_getTimeString: function (timeObj) {
			return ((timeObj.hours) ? timeObj.hours + ':' : '') + timeObj.minutes + ':' + timeObj.seconds;
		},
		/**
		 * Proxy method for load challenges
		 */
		loadChallenges: function () {
			this.collection.load( {page: this.page} );
		},
		/**
		 * Insert one challenge to page
		 * @param {Backbone.Model} model Challenge model
		 * @param {Number} index Index in the colletion
		 */
		renderChallenge: function ( model, index ) {
			this.insertView( '.battle-list', new BattleView( {parent:this, model: model, index: index } ) ).render();
		}
	} );

} );