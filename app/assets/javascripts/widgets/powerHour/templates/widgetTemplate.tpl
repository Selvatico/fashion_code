<div class="battle-list disnone">
    <!-- / for on -->
    <div class="trending-tags disnone c started-time">
        <h1>
            Power Hour is on
            <span class="time-ph">59:56</span>
            left
        </h1>
        Lorem ipsum dolore set amet.
    </div>
    <!-- / for off -->
    <div class="trending-tags c disnone finished-time">
        <h1>
            Power Hour will start in
            <span class="time-ph">-23:32</span>
            hours
        </h1>
        Lorem ipsum dolore set amet.
    </div>

    <div class="trending-tags c disnone start-new">
            <div class="btn createBtn">Start new Power Hour</div>
    </div>

    <div class="vs abs c">
        <i class="moderaIcons">A</i>
    </div>

</div>


<!-- / only if hasn't history of power hour -->
<div class="no-challenges faceplate c disnone">
    <i class="moderaIcons">B</i>
    <span class="thin disblock">There's no Power Hour history now.</span>

    <div class="btn createBtn">Start new Power Hour</div>
</div>

<div class="no-challenges-started faceplate c disnone">
    <i class="moderaIcons">B</i>
    <span class="thin disblock">There's no Power Hour history now.</span>
    <h1>
        Power Hour is on
        <span class="time-ph">59:56</span>
        left
    </h1>
</div>

<div class="no-challenges-finished faceplate c disnone">
    <i class="moderaIcons">B</i>
    <span class="thin disblock">There's no Power Hour history now.</span>
    <h1>
        Power Hour will start in
        <span class="time-ph">-23:32</span>
        hours
    </h1>
</div>