define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'./views/ChallengeStep1',
	'./views/ChallengeStep2',
	'./views/ChallengeStep3',
	'./views/ErrorWindow',
	'./views/BlockCreate',
	'layoutmanager'
], function ( app, widgetLayoutTemplate, Step1, Step2, Step3, ErrorWindow, BlockCreateOverlay ) {

	'use strict';

	return Backbone.View.extend( {

		template : widgetLayoutTemplate,

		className : 'wrapper widget-powerHourCreate',

		steps : {},

		currentStep : 1,

		model : new Backbone.Model(),

		events : {
			'click .nextBtn': 'navigateBtn',
			'click .prevBtn': 'navigateBtn'
		},

		initialize: function () {
			this.parentView = this.options.parent;

			//become ready for set oponent style
			app.on( 'challenge:style:selected', this.setSelectedStyle, this);

			//become ready for set photo event
			app.on( 'selected:photo', this.setSelectedPhoto, this);
		},
		/**
		 * For lazy init of views
		 * @param {Number} step 1 or 2 or 3
		 * @returns {Backbone.View}
		 */
		getStep: function (step) {
			var stepObj;
			if ( !this.steps[step] ) {
				if ( step == 1 ) {
					stepObj = new Step1( {parent: this} );
				} else if ( step == 2 ) {
					stepObj = new Step2( {parent: this, model: app.collections.fashionStyles.get(this.model.get('challenge_type_id'))} );
				} else if ( step == 3 ) {
					stepObj = new Step3( {parent: this} );
				} else {
					throw Error( 'Step ID out of range!' );
				}
				this.steps[step] = stepObj;
			} else {
				stepObj = this.steps[step];
			}
			return stepObj;
		},
		/**
		 * Set current active step
		 * @param {Number} step
		 */
		setStep: function (step) {
			var foundValue = this.$('.challenge-step' + step),
				stepObj;

			this.$( '.steps' ).addClass( 'disnone' );

			if (foundValue.length == 0) {
				stepObj = this.getStep( step );
				this.insertView( '.ph_create', stepObj ).render();
			} else {
				foundValue.removeClass( 'disnone' );
			}
			this.currentStep = step;
		},
		/**
		 * We check if we have in body required params and start proccess or
		 * show overlay with message how much time left before next posibility
		 */
		afterRender: function () {
			var powerHourData;
			if ( app.models.ServerDataModel.get( 'powerhour' ) ) {
				powerHourData = app.models.ServerDataModel.get( 'powerhour' );
				if (powerHourData.status == 'started' || powerHourData.status == 'finished') {
					this.insertView( '.notAvalaible', new BlockCreateOverlay( {started: this._getTimeString(powerHourData.time) } ) ).render();
				} else if (powerHourData.status == 'new') {
					this.setStep( this.options.step || 1 );
					return true;
				}
			} else {
				Backbone.history.navigate( '/challenges', { trigger: true } );
			}

		},
		_getTimeString: function (timeObj) {
			return ((timeObj.hours) ? timeObj.hours + ':' : '') + timeObj.minutes + ':' + timeObj.seconds;
		},
		/**
		 * Send request to start challenge
		 */
		startChallenge: function () {
			app.sendRequest('/power_hour', {
				method	 : 'POST',
				data	 : this.model.toJSON(),
				context  : this,
				complete : function (jqXHR) {
					if (jqXHR.status == '200') {
						//remove events from the app object
						app.off('selected:photo');
						app.off('challenge:style:selected');
						//go to challenge history page
						Backbone.history.navigate( '/challenges', { trigger: true } );
					}
				}
			})
		},
		/**
		 * Method for navigate between step
		 * @param {Boolean} direction TRUE - next, FALSE - back
		 */
		moveSteps: function (direction) {
			if ( (!direction && this.currentStep == 1) ) {
				throw Error( ' Step ID out of range' );
			}
			//if go next
			if ( direction ) {
				if ( this.checkStep( this.currentStep ) ) {
					if (this.currentStep == 3) {
						this.startChallenge();
					} else {
						this.setStep( this.currentStep + 1 );
					}
				}
			} else {
				this.setStep( this.currentStep - 1 );
			}
		},
		/**
		 * Perform check conditions which depends on step. Should be saved in the model
		 * @param {Number} step
		 * @return {Boolean}
		 */
		checkStep: function (step) {
			var result = true,
				section;
			if ( step == 1 ) {
				if ( 'undefined' == typeof this.model.get( 'challenge_type_id' ) ) {
					section = 'style';
					result  = false;
				}
			} else if ( step == 2 ) {
				if ( 'undefined' == typeof this.model.get( 'level' ) ) {
					section = 'level';
					result  = false;
				}
			} else if ( step == 3 ) {
				if ( 'undefined' == typeof this.model.get( 'photo_id' ) ) {
					section = 'photo';
					result  = false;
				}
			}
			if (!result) {
				app.trigger ( 'openModal', new ErrorWindow ( { page: this, section: section } ) );
			}
			return result;
		},
		/**
		 * Catch selected style for create challenge
		 * @param name
		 * @param id
		 */
		setSelectedStyle: function (name, id) {
			this.model.set('challenge_type_id', id);
			this.moveSteps(true);
			this.$( '.oneCreateStyle' ).removeClass( 'active' );
			this.$( '.oneCreateStyle.' + name ).addClass( 'active' );
		},
		/**
		 * Catch select photo event
		 * @param {Number} id Id of selected photo for challenge
		 */
		setSelectedPhoto: function (id) {
			this.model.set( 'photo_id', id );
		},
		/**
		 * Next And back button functionality
		 * @param event
		 */
		navigateBtn: function (event) {
			this.moveSteps(!!this.$(event.currentTarget ).hasClass('nextBtn'));
		}
	} );

} );