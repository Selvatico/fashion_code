<div class="width">
    <div class="container rel">
        <div class="btn right abs nextBtn">Next</div>
        <div class="btn left abs prevBtn">Back</div>
        <h1 class="c">Choose the one below</h1>

        <p class="fs14 c">
            <b>Description</b>
        </p>

        <p class="fs14 c">
            Now is the time to accelerate your vores against your challegers
            <br>
            Your votes can be worth +<%=easy_score%>, +<%=medium_score%> or +<%=hard_score%> time
            <br>
            Every vote converts to + or -
        </p>

        <div class="h1 c"> <%=fashion_style%> vs.  <%=opponent_fashion_style%></div>
    </div>
    <div class="level-list">
        <div class="item" data-level="easy">
            <div class="clickable-box rel">
                <span>25%</span>
                <img alt="25%" src="<%= url %>assets/ph25.png">
            </div>
            <div class="h1 c">
                to start
                <div class="vote disinbl c">
                    <b class="disblock"><%=easy_score%></b>
                    vote
                </div>
            </div>
            <p>
                Now every vote: +<%=easy_score%> votes
                <br>
                But if you lose every vote: -<%=easy_score%> votes
            </p>

            <p>
                You are challenging with the newbie.
                <br>
                Good to start
            </p>
        </div>
        <div class="item" data-level="medium">
            <div class="clickable-box rel">
                <span>50%</span>
                <img alt="50%" src="<%= url %>assets/ph50.png">
            </div>
            <div class="h1 c">
                Try your luck
                <div class="vote disinbl c">
                    <b class="disblock"><%=medium_score%></b>
                    vote
                </div>
            </div>
            <p>
                Now every vote: +<%=medium_score%> votes
                <br>
                But if you lose every vote: -<%=medium_score%> votes
            </p>

            <p>
                That's one for experlenced users.
                <br>
                You will be shown to a big amounth of users for an hour
            </p>
        </div>
        <div class="item" data-level="hard">
            <div class="clickable-box rel">
                <span>75%</span>
                <img alt="75%" src="<%= url %>assets/ph75.png">
            </div>
            <div class="h1 c">
                Challenge with the best
                <div class="vote disinbl c">
                    <b class="disblock"><%=hard_score%></b>
                    vote
                </div>
            </div>
            <p>
                Now every vote: +<%=hard_score%> votes
                <br>
                But if you lose every vote: -<%=hard_score%> votes
            </p>

            <p>
                You will be shown to extremely big amount
                <br>
                of users against Top Stars and Famous users
            </p>
        </div>
    </div>
</div>