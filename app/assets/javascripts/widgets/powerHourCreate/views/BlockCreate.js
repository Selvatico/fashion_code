define([
	'backbone',
	'text!../templates/blockCreate.tpl',
	'app'
], function (
	Backbone,
	blockCreate,
	app
	) {
	return Backbone.View.extend({

		template : blockCreate,
		tagName  : 'div',
		className: 'overlay',
		events   : {
			'click .challenges': 'goToChallenges'
		},
		serialize: function () {
			return {started: this.options.started};
		},
		goToChallenges: function () {
			Backbone.history.navigate( '/challenges', { trigger: true } );
		}
	});
});

