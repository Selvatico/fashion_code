define( [
	'backbone',
	'text!../templates/challengeStep1.tpl',
	'./StyleColumn',
	'app'
], function ( Backbone, challengeStep1, StyleColumn, app ) {
	return Backbone.View.extend( {

		template: challengeStep1,
		className: 'challenge-step1 steps',
		initialize: function () {
			app.on( 'fashion:loaded', this.renderStyles, this );
		},
		beforeRender: function () {
			app.collections.fashionStyles.url = '/challenge_types';
			app.collections.fashionStyles.fashionList( 'challenge_types' );
		},
		/**
		 * Callback for fashion loaded event
		 * @param {Object} data
		 * @param {Object} collection
		 */
		renderStyles: function (data, collection) {
			collection.each(this.renderStyle, this);
		},
		/**
		 * Insert one style to step
		 * @param model
		 */
		renderStyle: function ( model ) {
			this.insertView( '.style-box', new StyleColumn( {model: model, parent: this} ) ).render();
		}
	} );
} );
