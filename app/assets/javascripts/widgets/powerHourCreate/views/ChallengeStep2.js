define([
	'backbone',
	'text!../templates/challengeStep2.tpl',
	'app'
], function (
	Backbone,
	challengeStep2,
	app
	) {
	return Backbone.View.extend({

		template: challengeStep2,
		events: {
			'click .item': 'setLevel'
		},
		className: 'challenge-step2  steps',
		/**
		 * Set current challenge value base on data attribute of .item
		 * @param event
		 */
		setLevel : function (event) {
			var level = this.$( event.currentTarget ).data( 'level' );
			//check allowed values
			if ( {easy: 1, medium: 1, hard: 1}[level] ) {
				this.$( '.item' ).removeClass( 'active' );
				this.$( event.currentTarget ).addClass( 'active' );
				this.options.parent.model.set( 'level', level );
				this.options.parent.moveSteps( true );
				this.$('.nextBtn' ).addClass('active');
			}
		},
		serialize: function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() });
		}
	});
});

