define( [
	'backbone',
	'text!../templates/challengeStep3.tpl',
	'./PhotoItem',
	'app'
], function ( Backbone, challengeStep3Tpl, PhotoItem, app ) {
	return Backbone.View.extend( {

		template   : challengeStep3Tpl,
		events     : {
		},
		className  : 'challenge-step3 steps width',
		initialize : function () {
			//subscribe to photo loaded event
			app.on( 'photos:loaded', this.renderPhotos, this );

			//start loading photos
			app.collections.PhotosCollection.reset().photoList( {page: 1} );
		},
		afterRender: function () {
			//@TODO remove this in the future. I know that this code is sucks
			$( '.lp' ).css( {overflow: 'scroll'} );
		},
		/**
		 * Event catcher for photo:laoded event
		 * @param {Object} data Raw data from server
		 * @param {Backbone.Collection} collection Collections with photos
		 * @param {Function} collection.each Iterator from underscore.js
		 */
		renderPhotos: function ( data, collection ) {
			if ( collection.length > 0 ) {
				collection.each( this.renderPhoto, this );
			}
		},
		/**
		 * Append one photo to list and render
		 * @param {Backbone.Model} model One photo model
		 */
		renderPhoto: function ( model ) {
			this.insertView( '.view-photo', new PhotoItem( {model: model, parent: this} ) ).render();
		},
		hideAllMarks: function () {
			this.$( '.selected-image' ).removeClass( 'selectedPhoto' );
			this.$('.nextBtn' ).addClass('active');
		}
	} );
} );

