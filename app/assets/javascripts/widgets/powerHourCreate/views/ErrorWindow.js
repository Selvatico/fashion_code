/**
 * Error window when member didn't missed some params in create challenge proccess
 */
define( [
	'backbone',
	'text!../templates/errorWindow.tpl',
	'app'
], function ( Backbone, errorWindow ) {
	return Backbone.View.extend( {

		template: errorWindow,
		errorMsgs: {
			style: 'Please choose oposite style first!',
			level: 'Please select level for your challenge!',
			photo: 'Please select photo from the list!'
		},
		tagName  : 'div',
		className: '',
		serialize: function () {
			return {msg: this.errorMsgs[this.options.section]};
		}
	} );
} );

