define([
	'backbone',
	'text!../templates/photoItem.tpl',
	'app'
], function (
	Backbone,
	photoItem,
	app
	) {
	return Backbone.View.extend({

		template: photoItem,
		events: {
			'click .selected-image' : 'selectImage'
		},
		tagName  : 'div',
		className: 'item',
		serialize: function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		selectImage: function () {
			this.options.parent.hideAllMarks();
			this.$( '.selected-image' ).addClass( 'selectedPhoto' );
			app.trigger( 'selected:photo', this.model.get( 'id' ) );
		}
	});
});
