define([
	'backbone',
	'text!../templates/styleColumn.tpl',
	'app'
], function (
	Backbone,
	styleColumnTpl,
	app
	) {
	return Backbone.View.extend({

		template: styleColumnTpl,
		events: {
			'click span' : 'styleSelected'
		},
		tagName: 'div',
		className: 'oneCreateStyle',
		serialize: function () {
			var data = _.extend( this.model.toJSON(), {lowerName: this.model.get( 'opponent_fashion_style' ).toLowerCase()} )
			return _.defaults ( data, { 'url' : app.getURL() } );
		},
		styleSelected: function () {
			//this.$( 'img' ).addClass( 'active' );
			app.trigger( 'challenge:style:selected', this.model.get( 'opponent_fashion_style' ).toLowerCase(), this.model.get( 'id' ) );
			this.options.parent.$el.find('.nextBtn' ).addClass('active');
		},
		afterRender: function () {
			this.$el.addClass( this.model.get( 'opponent_fashion_style' ).toLowerCase() );
		}
	});
});
