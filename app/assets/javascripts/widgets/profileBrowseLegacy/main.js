define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'collections/PhotosCollection',
	'./views/PhotoItem',
	'layoutmanager'
], function ( app, widgetLayout, PhotosCollection, PhotoItem ) {

	'use strict';

	return Backbone.Layout.extend( {

		template  : widgetLayout,
		events    : { },
		className : 'widget-ProfileBrowseLegacy profile-steps profile-item',
		tagName	  : 'div',
		model	  : new Backbone.Model( {page: 1} ),
		initialize: function () {
			var collectionUrl = ' /profiles/' + this.options.parent.model.get('info' ).username + '/photos';
			if ( this.options.parent.model.get( 'photoId' ) != null ) {
				collectionUrl += '/' + this.options.parent.model.get( 'photoId' );
			}
            
			this.photoCollection = new PhotosCollection( [], {urlRoot: collectionUrl} );
			this.photoCollection.on( 'add', this.renderPhoto, this );
			this.photoCollection.on( 'photo:loaded', this.photoLoaded, this);
			this.loadPhotos();
            this.on( 'afterRender', this.afterRenderHandler, this );

			this.model.set({ 'page': 1, username: this.options.username});
		},
		loadPhotos	: function () {
			this.photoCollection.photoList( { page: this.model.get( 'page' ), view: 'list' } );
		},
		/**
		 *
		 * @param {Object} data
		 * @param {Object} data.photos_count Overall count of photos
		 */
		photoLoaded : function (data) {
			this.model.set('count_photos', data.photos_count);
			if (data.photos_count == 0) {
				this.$( '.faceplate' ).removeClass( 'disnone' );
			}
			app.trigger( 'scroll:refresh', null);
			
		},

		renderPhoto: function ( model ) {
			var item = new PhotoItem( {parent: this, model: model, username: this.model.get('username')} );
			this.insertView( '.profile-list', item ).render();
			if ( this.options.parent.model.get( 'info' ).username == app.models.userModel.get( 'username' ) ) {
				item.$el.find( '.reportLink' ).addClass( 'disnone' );
			}
		},
		/**
		 * Load more photos on scroll
		 */
		paginate: function () {
			var countPhotos  = this.model.get('count_photos' ),
				limit  		 = 10,
				currentCount = this.photoCollection.length,
				currentPage  = this.model.get('page');

			if ( currentCount < countPhotos && (currentPage) * limit < countPhotos ) {
				this.model.set( 'page', currentPage + 1 );
				this.loadPhotos()
			}
		},
        afterRenderHandler: function() {
			//ПРОВЕРЯЙТЕ ЧТО ПРИМЕНЯЕТЕ. ЭТО ЛОМАЕТ ЖИЗИНИ И СТРАНИЦУ
            //this.$('.nano').nanoScroller({ scroll: 'top' });
        }
	} );

} );
