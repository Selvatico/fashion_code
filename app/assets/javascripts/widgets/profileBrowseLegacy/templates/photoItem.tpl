    <div class="info-side right rel">
        <div class="like-count h1 curpoint">
            <i class="moderaIcons">C</i>
            <%= likeCount %>
        </div>
        <div class="comment-count h1 curpoint">
            <i class="moderaIcons">H</i>
            <%= commentCount %>
        </div>
        <div class="timeline">
            <i class="moderaIcons">G</i>
            <%= timeAgo %>
        </div>
        <div class="asLink share-switcher">Share</div>
        <div class="share-panel left abs disnone">
            <!-- /facebook -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<%= url %><%= image %>"  class="share-icons facebook" target="_blank">
                      <i class="moderaIcons">f</i>
                    </a>
            <!-- /pinterest -->
                    <a href="http://pinterest.com/pin/create/button/?url=<%= url %><%= image %>&media=<%= url %><%= image %>" class="share-icons pinterest" target="_blank">
                      <i class="moderaIcons">p</i>
                    </a>
            <!-- /twitter -->
                    <a href="https://twitter.com/share?url=<%= url %><%= image %>" class="share-icons twitter" target="_blank">
                      <i class="moderaIcons" >t</i>
                    </a>
            <!-- /gplus -->
                    <!-- <a href="https://plus.google.com/share?url=<%= url %><%= image %>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="share-icons gplus" target="_blank">
                      <i class="moderaIcons">g</i>
                    </a> -->
            <!-- /tumblr -->
                    <a href="http://www.tumblr.com/share/link?url=<%= url %><%= image %>&name=&description=" title="Share on Tumblr" class="share-icons tumblr" target="_blank">
                      <i class="moderaIcons" >u</i>
                    </a>
            <!-- /flickr -->
                    <!-- <a href="#" class="share-icons flickr" target="_blank">
                      <i class="moderaIcons">
                          <small>0</small>
                          <small>0</small>
                      </i>
                    </a> -->
            <!-- /mail -->
                    <!-- <span class="share-icons mail">
                      <i class="moderaIcons">m</i>
                    </span> -->
        </div>
    </div>
    
    <div class="photoframe voteStar">
      <% if (!isMe && !voted) { %>
      <input src="<%= url %>/assets/like.png" type="image" class="like" />
      <% } %>
      <img alt="" height="600" src="<%= url %><%= image %>" width="600" />
    </div>
    <div class="info-bottom">
        <div class="right r">
            <span class="disnone"> <span><%=rate %>%</span> win rate in <span><%=compares_count %></span> compares </span>
            <span class="asLink reportLink">Report</span>
            <% if (!isMe && !voted) { %>
                <b class="asLink voteLink textTransUpper"><i class="moderaIcons m textUndernone">C</i>vote</b>
            <% } %> 
            <% if (isMe ) { %>
                <span class="asLink deleteLink">Delete</span>
            <% } %>
        </div>
        <% if (comment != null) { %>
        <div class='last-comment'>
            <a class='left' href='/profiles/<%= comment.user.username %>' title='<%= comment.user.username %>'>
                <img alt="<%= comment.user.username %>" class="userpic" height="42" src="<%= url %><%= comment.user.avatar %>" width="42" />
            </a>
            <div class='overhide'>
                <a class='disblock textOverhide' href='/profiles/<%= comment.user.username %>' title='Mary Poppins'>
                <b><%= comment.user.username %></b>
                </a>
                <span class='text-comment disblock textOverhide'><%= comment.comment %></span>
            </div>
        </div>
        <% } %>
    </div>
