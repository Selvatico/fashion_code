<div class="profile-control c rel overhide disnone">
    <div class="btn right step-btn" data-step="compares">
        Compares
        <i class="moderaIcons m">&gt;</i>
    </div>
    <h1 class="left">Browse legacy</h1>
    <div class="abs disnone">
        <div class="btn">Share with your Media</div>
    </div>
</div>
<!--<canvas height="292" id="browse_legacy" width="930"></canvas>-->
<!-- /test script -->
<script>
    /*var example = document.getElementById("browse_legacy");
     var ctx = example.getContext('2d');
     var pic = new Image();
     pic.src = 'http://localhost:3000/assets/browse_legacy_profile.png';
     pic.onload = function() {
     ctx.drawImage(pic, 0, 0);
     }*/
</script>
<div class="profile-sort c disnone">
    <span>Sort photo's by quality</span>
    <span class="asLink">More than 80%</span>
    <span class="asLink active">More than 50%</span>
    <span class="asLink">Less than 20%</span>
    <span class="asLink">Recently posted</span>
</div>

<div id="about" >
 <div class="profile-list">
     <!-- /test script -->
 </div> 
</div>


<div class="faceplate c disnone">
    <i class="moderaIcons">E</i>    
    <span class="thin disblock">Let's put your first photo on Modera</span>
    <p class="c">
        <a href="#photos/new" class="btn">Upload Photo</a>
    </p>
</div>