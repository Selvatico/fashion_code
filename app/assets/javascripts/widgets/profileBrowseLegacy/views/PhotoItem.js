define( [
	'backbone',
	'text!../templates/photoItem.tpl',
	'app',
    'widgets/comments/main',
    'widgets/voters/main',
    'widgets/confirmWindow/main'
], function ( Backbone, photoTpl, app, CommentsWidget, VotersWidget, ConfirmWindow ) {
	return Backbone.View.extend( {

		events: {
			'click .share-switcher' : 'shareSwitcher',
            'click .like-count'     : 'likeCountEventHandler',
            'click .reportLink'     : 'reportPhoto',
            'click .comment-count'  : 'commentCountEventHandler',
			'click .voteLink, .voteStar' : 'photoVote',
			'click .deleteLink'		     : 'deletePhoto'
		},
		template  : photoTpl,
		tagName   : 'div',
		className : 'participant rel',
		initialize: function () {
			this.model.on('change:voted', this.render, this);
		},
		serialize : function () {
			return _.extend( this.model.toJSON(), {
				isMe 			  : app.isMe( this.options.username ),
				likeCount         : this.likeCount(),
				commentCount      : this.commentCount(),
				timeAgo			  : moment(this.model.get('created_at'),  'YYYY-MM-DD HH:mm:ssZ').fromNow(),
				url				  : app.getURL()
			});
		},
		/**
		 * Click on vote button or on star in the bottom of photo
		 */
		photoVote: function () {
			app.sendRequest( '/photos/' + this.model.get( 'id' ) + '/vote', {
				complete: function (xhr) {
					if (xhr.status && xhr.status == '200') {
						this.model.set( 'voted', true );
					}
				},
				method: 'POST',
				context: this
			} );
		},
		/**
		 * Delete own photo from list
		 */
		deletePhoto: function () {
			app.trigger ( 'openModal', new ConfirmWindow (
				{
					page: this, popupWidth: 437, context: this,
					okAction: this.runDeletePhoto, okClose: true, okText: 'Delete',
					titleText: 'Are you sure want to delete photo ?'
				}
			) );
		},
		runDeletePhoto: function () {
			app.sendRequest( '/photos/' + this.model.get( 'id' ), {
				complete: function (xhr) {
					if (xhr.status && xhr.status == '200') {
						this.remove();
					}
				},
				method: 'DELETE',
				context: this
			} );
		},
		commentCount: function() {
			return this.countFunction( 'comments_count' );
		},
		likeCount: function() {
			var score =  this.countFunction( 'score' );
			if (!score) {
				score = this.countFunction('votes_score');
			}
			return score;
		},
		/**
		 * Count and format some photo properties
		 * @param prop
		 * @returns {string}
		 */
		countFunction: function( prop ) {
			return ( this.model.get( prop ) > 1000 ) ? ( this.model.get( prop ) / 1000 ) + 'k' : this.model.get( prop );
		},
		/**
		 * Show/hide share panel on the left side of the photo
		 */
		shareSwitcher: function () {
			this.$( '.share-panel' ).toggleClass( 'disnone' );
		},
        likeCountEventHandler: function( ) {
            var self = this,
                votersWidget = new VotersWidget ( { parent: this, photo_id: self.model.get('id'), className: 'voters-block' } );            
            this.insertView( votersWidget ).render();
            this.$('.info-side, .photoframe, .info-bottom').css('display', 'none');
        },
        commentCountEventHandler: function( ) {
            var self = this,
                commentsWidget = new CommentsWidget( { parent: this, photo_id: self.model.get('id'), className: 'comment-block' } );                         
            this.insertView( commentsWidget ).render();
            this.$('.info-side, .photoframe, .info-bottom').css('display', 'none');
        },
		/**
		 * Close photo comments block
		 */
        closeComments: function() {
            this.getView( { className: 'comment-block' } ).remove();
            this.$('.info-side, .photoframe, .info-bottom').css('display', 'block');
        },
        closeVoters: function() {
            this.getView( { className: 'voters-block' } ).remove();
            this.$('.info-side, .photoframe, .info-bottom').css('display', 'block');
        },
		/**
		 * Report photo inappropriate
		 */
		reportPhoto: function () {
			app.sendRequest( '/photos/' + this.model.get( 'id' ) + '/inappropriate', {
				method  : 'POST',
				complete: function () {
					app.trigger ( 'show:message', { 'text' : 'The report has been sent successfully!', 'type' : 'success' } );
				},
				context: this
			});
		}
	} );
} );