
define( [
	'app',
	'backbone'
], function ( app, Backbone ) {
	return Backbone.Collection.extend( {
		initialize: function (models, options) {
		   	this.url = options.url;
		},
		load : function ( params ) {
			var _self = this;
			app.sendRequest( this.url, {
				single: true,
				callback: function ( data ) {
                    if ( data ) {
						_self.add( data );
						app.trigger( 'compares:loaded', _self, data );
					}
				},
				data: params || {}
			} );
		}
	} );
} );
