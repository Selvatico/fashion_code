define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'./collections/ComparesCollection',
    'collections/UsersCollection',
	'../versus/views/BattleView',
    'widgets/profileFollowers/views/TopVoterView',
	'modules/versusScroller/main',
	'layoutmanager'
], function ( app, widgetLayout, ComparesCollection, UsersCollection, BattleView, TopVoterView, versusScroller ) {

	'use strict';

	return Backbone.Layout.extend( {

		template  : widgetLayout,
        page: 1,
		events: {
			//'mousewheel .battle-list': 'scrollCatch'
		},
		className : 'widget-ProfileCompares profile-steps profile-item',
		tagName	  : 'div',
		model     : new Backbone.Model({page:1, endReached: false}),
		initialize: function () {
			this.collection = new ComparesCollection( [], {url: '/profiles/' + this.options.parent.model.get( 'info' ).username + '/versuses.json'} );
            
			//subscribe to load challenges event
			app.on( 'compares:loaded', this.comapresLoaded, this );

			this.collection.on( 'add', this.renderCompared, this );

			this.model.set( {'page': 1, endReached: false } );

			this.loadCompares();
		},
		scrollCatch: function ( event ) {
			versusScroller( event, '.battle', 'activeBattle', this );
		},
		/**
		 * Render all challenges
		 * @param collection
		 * @param data
		 */
		comapresLoaded: function ( collection, data ) {
			if ( collection.length == 0 ) {
				this.$( '.faceplate' ).removeClass( 'disnone' );
                this.renderRecomendedUsers();                
			}
			if (data.length == 0) {
				this.model.set('endReached', true);
			}
		},
		/**
		 * Method to load more compares
		 */
		paginate: function () {
			this.model.attributes.page++;
			this.loadCompares();
		},
		/**
		 * Proxy method for load challenges
		 */
		loadCompares: function () {
			if (this.model.get('endReached')) return false;
			this.collection.load( {page: this.model.get('page')} );
			return true;
		},
		/**
		 * Insert one challenge to page, show voter and VS view
		 * @param {Backbone.Model} model Challenge model
		 */
		renderCompared: function ( model ) {
			var index = this.collection.indexOf( model );
			var battleView = new BattleView( {parent:this, model: model, index: index, marginLeft: 0, marginTop: ((index == 0) ? -0: 0), skipWidth: true } );
			battleView.renderVs( model );
			battleView.leftVersus.renderTopVoter( new Backbone.Model( {voter: model.get( 'voter' ), pos: model.get( 'photo' )['vote_weight'] > 0} ));
			this.insertView( '.battle-list', battleView ).render();
			battleView.$el.find( '.info-bottom .share' ).addClass( 'disnone' );
			battleView.rightVersus.$('.info-top').removeClass('disnone');
		},
        renderRecomendedUsers: function () {
			this.collectionTop = new UsersCollection( [] );
			this.collectionTop.on( 'add', this.renderTopVoter, this );
			app.sendRequest( ' /suggestions', {
				context: this,
                data: { page: this.page },
				callback: function ( data ) {
				    if ( 'users' in data ) {
				       this.collectionTop.add( data.users ); 
				    }					   
				}
			});            
        },
		renderTopVoter: function (model) {
			this.insertView( '#top-recomend', new TopVoterView( {model: model, parent: this} ) ).render();
		}        
	} );

} );
