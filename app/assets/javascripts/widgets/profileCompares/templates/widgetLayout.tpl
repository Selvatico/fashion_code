<div class="profile-control c rel overhide disnone">
    <div class="btn right step-btn" data-step="photos">
        Photos Info
        <i class="moderaIcons m">&gt;</i>
    </div>
    <h1 class="left">Compares</h1>
    <div class="abs disnone">
        <div class="btn">Share with your Media</div>
    </div>
</div>
<div class="profile-most-wins disnone">
    <ul class="c">
        <li>
            <div class="h1">Britany's most popular tags</div>
            <p>
                <span>#boujoi</span>
                60% win rate &amp; 3k votes
            </p>
        </li>
        <li>
            <div class="h1">Britany's most successful contests</div>
            <p>
                <span>#boujoi</span>
                60% win rate &amp; 3k votes
            </p>
        </li>
    </ul>
</div>
<div class="profile-sort c disnone">
    <span>Sort contest</span>
    <span class="asLink">All contest</span>
    <span class="asLink active">Sponsored contest</span>
    <span class="asLink">User contest</span>
    <span class="asLink">Brittany's contest</span>
</div>
<div class="battle-list">

</div>
<div class="faceplate c disnone no-contest">
    <i class="moderaIcons">
        D
        <sub>G</sub>
    </i>
    <span class="thin disblock nomrg">No contest history to show</span>
    <p class="c thin fs14 disnone">
        Give your photos Lucky Chance
        <br>
        push the button
    </p>
    <a class="btn disnone" href="#/power_hour/new">Start Power Hour</a>
    <div class="or c h1 disnone">
        <span class="rel">or</span>
    </div>
    <p class="c thin fs14">
        Challenge with your friends push the button have some fun
    </p>
    <a class="btn disnone" href="/#invite_friends">Invite Friends</a>
    <div class="profile-top-vote">    
        <div class="h1">Recommended users</div>
        <ul class="c thin" id="top-recomend">

        </ul>
    </div>
</div>
