define( [
	'app',
	'./views/TopVoterView',
	'./views/FollowerListView',
	'collections/UsersCollection',
	'text!./templates/widgetLayout.tpl',
	'layoutmanager'
], function ( app, TopVoterView, FollowerListView, UsersCollection, widhetLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template: widhetLayout,
		events: { },
		className: 'widget-ProfileFollowers profile-steps profile-item',
		tagName: 'div',
		initialize: function () {
			this.collectionTop = new UsersCollection( [] );
			this.collectionTop.on( 'add', this.renderTopVoter, this );
			app.sendRequest( ' /profiles/' + this.options.parent.model.get( 'info' ).username + '/top_voters', {
				context: this,
				callback: function ( data ) {
					this.collectionTop.add( data );
				}
			} );
		},
		beforeRender: function () {
			var userName = this.options.parent.model.get( 'info' ).username;

			this.insertView( '.followers-columns', new FollowerListView(
				{parent: this, blockName: 'Following', collection: new UsersCollection( [], {userUrl: ' /profiles/' + userName +'/following', key : 'users'} ) } ) );

			this.insertView( '.followers-columns', new FollowerListView(
				{parent: this, blockName: 'Followers', collection: new UsersCollection( [], {userUrl: ' /profiles/' + userName +'/followers', key : 'users'} ) } ) );

			this.insertView( '.followers-columns', new FollowerListView(
				{parent: this, blockName: 'Recommend', collection: new UsersCollection( [], {userUrl: '/suggestions', key : 'users'} )} ) );
		},
		serialize: function () {
			return this.options.parent.model.toJSON();
		},
		
		renderTopVoter: function (model) {
			this.insertView( '#top-voters', new TopVoterView( {model: model, parent: this} ) ).render();
		}
	} );

} );
