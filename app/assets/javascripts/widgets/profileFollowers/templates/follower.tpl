<div class="btn right follow <%= active %>"><%= followText %></div>
<a class="textOverhide" href="/profiles/<%= username %>">
    <img alt="<%= username %>" class="userpic left" height="42" src="<%= url %><%= avatar %>" width="42">
    <b><%= name %></b>
    <br>
    <span>#<%=fashion_style%></span>
</a>
