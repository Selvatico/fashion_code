<b class="fs14"><%=blockName %></b>

<div class="view-list">
    <!-- / one Following item -->
    <div class="content"></div>

</div>
<!-- / only if no following -->
<div class="faceplate c disnone">
    <i class="moderaIcons">S</i>
    <span class="thin disblock">No <%=blockName %> to show</span>
</div>