<div class="profile-control c rel overhide disnone">
    <div class="btn right step-btn" data-step="browse_legacy">
        Browse legacy
        <i class="moderaIcons m">&gt;</i>
    </div>
    <h1 class="left">Followers</h1>
    <div class="abs disnone">
        <div class="btn">Share with your Media</div>
    </div>
</div>
<div class="profile-top-vote">
    <div class="h1 c"><%=info.username%>'s Top Voters</div>
    <ul class="c thin" id="top-voters">

    </ul>
</div>
<div class="profile-followers-list overhide">
    <ul class="followers-columns">

    </ul>
</div>
