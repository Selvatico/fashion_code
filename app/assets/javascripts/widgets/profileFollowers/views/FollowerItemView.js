define( [
	'backbone',
	'text!../templates/follower.tpl',
	'app'
], function ( Backbone, topVoter, app ) {
	return Backbone.View.extend( {

		events: {
			'click .follow' : 'follow'
		},
		template  : topVoter,
		tagName   : 'div',
		className : 'item',
		initialize: function () {},
		serialize : function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		follow    : function () {
			var _self = this;
			app.sendRequest( this.model.getFollowUrl(), {
				method: 'POST',
				complete: function ( jqXHR ) {
					if (jqXHR.status == 200) {
						_self.model.set('is_following', !_self.model.get('is_following'));
						var currentStatus = this.model.getCurrentFollowStatus();
						app.trigger( 'profile:' + currentStatus + ':' + app.models.userModel.get( 'username' ), currentStatus );
						this.render();
					}
				},
				single : true,
				context: this
			});
		}
	} );
} );