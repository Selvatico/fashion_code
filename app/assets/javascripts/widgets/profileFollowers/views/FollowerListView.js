define( [
	'backbone',
	'./FollowerItemView',
	'text!../templates/followerBlock.tpl',
	'app',
	'nanoscroller'
], function ( Backbone, FollowerItemView, followerBlock, app ) {
	return Backbone.View.extend( {
		events: {

		},
		tagName: 'li',
		template: followerBlock,
		collection: null,
		page: 1,
		endReached: false,
		count: 0,
		initialize: function () {
			this.collection = this.options.collection;
			if ( this.collection ) {
				this.collection.on( 'users:loaded', this.followersLoaded, this );
				this.collection.on( 'add', this.renderFollower, this );
				this.loadFollowers();
			}
		},
		serialize: function () {
			return { blockName: this.options.blockName};
		},
		/**
		 * Proxy method for not dublicate
		 */
		loadFollowers: function () {
			if ( this.endReached ) {
				return;
			}
			this.collection.load( {page: this.page} );
		},
		/**
		 * Callback for finish load request
		 * @returns {boolean}
		 */
		followersLoaded: function ( collection, data ) {
			if ( this.collection.length == 0 ) {
				this.$( '.faceplate' ).removeClass( 'disnone' );
				this.$( '.content' ).remove();
				return false;
			}
			this.page++;
			if ( data.count && collection.length >= data.count ) {
				this.endReached = true;
			}
			return true;
		},

		/**
		 * Append one follower to list
		 * @param model
		 */
		renderFollower: function ( model ) {
			this.count++;
			this.insertView( '.view-list>.content', new FollowerItemView( {model: model, parent: this} ) ).render();
			if ( this.count == this.collection.length ) {
				this.addScroll( this );
			}
			this.$( '.content .item' ).width( this.$( '.nano' ).width() );
		},
		/**
		 * Init scroll on the list and bind event
		 * @param that
		 */
		addScroll: function ( that ) {
			this.$( '.content' ).on( 'scroll', function ( event ) {
				that.catchScrollEvent( event );
			} );

			that.$( '.view-list' ).css( {'overflow': 'hidden', 'height': '500px'} ).addClass( 'nano' );
			that.$( '.view-list' ).nanoScroller();
			that.$( '.content .item' ).width( that.$( '.nano' ).width() );
		},
		/**
		 * Catch nanoscroll scroll event
		 */
		catchScrollEvent: function () {
			var $slider = this.$( '.pane .slider' );
			var height  = 2 * $slider.height();
			var top     = parseInt( $slider.css( 'top' ), 10 );
			var percent = (top / height) * 100;

			//if we scrolled more that 75% of the scroll
			if ( percent > 75 ) {
				this.loadFollowers();
			}
		}
	} );
} );
