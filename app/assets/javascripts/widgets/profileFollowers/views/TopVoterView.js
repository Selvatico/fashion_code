define( [
	'backbone',
	'text!../templates/topVoter.tpl',
	'app'
], function ( Backbone, topVoter, app ) {
	return Backbone.View.extend( {
		template  : topVoter,
		events: {
			'click .follow' : 'follow'
		},
		tagName : 'li',
		initialize: function () {
			this.model.on('change:is_following', this.followChanged, this);
		},
		serialize : function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		followChanged: function () {
			this.render();
		},
		follow: function () {
			this.model.followMember(app);
		}
	} );
} );