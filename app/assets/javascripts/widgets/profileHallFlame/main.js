define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'layoutmanager'
], function ( app, widhetLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template : widhetLayout,
		events : { },
		className : 'widget-ProfileHallFlame profile-steps profile-item hall_of_fame',
		tagName : 'div',
		initialize: function () {},
		
		serialize : function () {
			return { 'url' : app.getURL() };
		}
	} );

} );
