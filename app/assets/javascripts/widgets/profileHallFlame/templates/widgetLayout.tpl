<div class="profile-control c rel overhide">
    <div class="btn right step-btn" data-step="followers">
        Followers
        <i class="moderaIcons m">&gt;</i>
    </div>
    <h1 class="left">Hall of Fame</h1>
    <div class="abs">
        <div class="btn">Share with your Media</div>
    </div>
</div>
<div class="profile-hall-top-third disnone">
    <div class="h1 c">
        <span>Top 3</span>
        users
    </div>
    <ul class="fs14 thin">
        <li>
            <a class="left" href="#">
                <img alt="username" class="m" height="120" src="<%= url %>assets/samples/username_pic.png" width="120">
            </a>
            <a class="disblock" href="#">
                <b>Adrian Hans</b>
            </a>
            <span>#hipster</span>

            <div class="like-counter">
                <i class="moderaIcons">C</i>
                <span>15000</span>
            </div>
            <div class="followers-counter">
                <b>9857</b>
                Followers
            </div>
            <div class="prize clear">
                <img alt="name of contest" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">

                <p>Gift card from BOUJOI won in BOUJOI contest</p>
            </div>
        </li>
        <li>
            <a class="left" href="#">
                <img alt="username" class="m" height="120" src="<%= url %>assets/samples/username_pic.png" width="120">
            </a>
            <a class="disblock" href="#">
                <b>Adrian Hans</b>
            </a>
            <span>#hipster</span>

            <div class="like-counter">
                <i class="moderaIcons">C</i>
                <span>15000</span>
            </div>
            <div class="followers-counter">
                <b>9857</b>
                Followers
            </div>
            <div class="prize clear">
                <img alt="name of contest" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">

                <p>Gift card from BOUJOI won in BOUJOI contest</p>
            </div>
        </li>
        <li>
            <a class="left" href="#">
                <img alt="username" class="m" height="120" src="<%= url %>assets/samples/username_pic.png" width="120">
            </a>
            <a class="disblock" href="#">
                <b>Adrian Hans</b>
            </a>
            <span>#hipster</span>

            <div class="like-counter">
                <i class="moderaIcons">C</i>
                <span>15000</span>
            </div>
            <div class="followers-counter">
                <b>9857</b>
                Followers
            </div>
            <div class="prize clear">
                <img alt="name of contest" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">

                <p>Gift card from BOUJOI won in BOUJOI contest</p>
            </div>
        </li>
    </ul>
</div>
<div class="hall-of-fame">
    <div class="h1 c textTransUpper">Top 1%</div>
    <a href="#" class="right rel">See all Hall of Fame</a>

    <div class="content-table percent">
        <div class="item-row th thin">
            <div class="up-down item-col">&nbsp;</div>
            <div class="user item-col">Name</div>
            <div class="likes-count item-col">Votes</div>
            <div class="followers-count item-col">Followers</div>
            <div class="verified item-col">&nbsp;</div>
            <div class="prize item-col">Prizes</div>
        </div>
        <div class="item-row">
            <div class="up-down down item-col">
                <i class="moderaIcons">(</i>
                <b class="disblock">6</b>
            </div>
            <div class="user item-col fs14">
                <a class="disblock rel" href="#" title="Marry Poppins">
                    <img alt="Marry Poppins" class="userpic left" height="42" src="<%= url %>assets/samples/username_pic.png"
                         width="42">
                    <span class="nowrap textOverhide">Marry Poppins</span>
                </a>
                <span class="nowrap thin">#Casual</span>
            </div>
            <div class="likes-count item-col">
                <i class="moderaIcons m">C</i>
                <span class="thin fs14">1500</span>
            </div>
            <div class="followers-count item-col fs14">
                <b>987</b>
                <span class="thin">Followers</span>
            </div>
            <div class="verified item-col">
                <i class="moderaIcons rel m">C</i>
                <span>verified</span>
            </div>
            <div class="prize item-col">
                <p class="fs14 thin nomrg left">
                    <img alt="Contest name" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">
                    Gift card from BOUJOI won in BOUJOI contest
                </p>
            </div>
        </div>
        <div class="item-row me">
            <div class="up-down up item-col">
                <i class="moderaIcons">)</i>
                <b class="disblock">6</b>
            </div>
            <div class="user item-col fs14">
                <a class="disblock rel" href="#" title="Brittany Beckman">
                    <img alt="Brittany Beckman" class="userpic left" height="42" src="<%= url %>assets/samples/username_pic.png"
                         width="42">
                    <span class="nowrap textOverhide">Brittany Beckman</span>
                </a>
                <span class="nowrap thin">#Casual</span>
            </div>
            <div class="likes-count item-col">
                <i class="moderaIcons m">C</i>
                <span class="thin fs14">1500</span>
            </div>
            <div class="followers-count item-col fs14">
                <b>987</b>
                <span class="thin">Followers</span>
            </div>
            <div class="verified item-col">
                <i class="moderaIcons rel m">C</i>
                <span>verified</span>
            </div>
            <div class="prize item-col">
                <p class="fs14 thin nomrg left">
                    <img alt="Contest name" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">
                    For
                    <br>
                    1
                    <sup>st</sup>
                    place
                </p>

                <p class="fs14 thin nomrg left">
                    <img alt="Contest name" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">
                    For
                    <br>
                    2
                    <sup>nd</sup>
                    place
                </p>
            </div>
        </div>
        <div class="item-row">
            <div class="up-down down item-col">&nbsp;</div>
            <div class="user item-col fs14">
                <a class="disblock rel" href="#" title="Marry Poppins">
                    <img alt="Marry Poppins" class="userpic left" height="42" src="<%= url %>assets/samples/username_pic.png"
                         width="42">
                    <span class="nowrap textOverhide">Marry Poppins</span>
                </a>
                <span class="nowrap thin">#Casual</span>
            </div>
            <div class="likes-count item-col">
                <i class="moderaIcons m">C</i>
                <span class="thin fs14">1500</span>
            </div>
            <div class="followers-count item-col fs14">
                <b>987</b>
                <span class="thin">Followers</span>
            </div>
            <div class="verified item-col">&nbsp;</div>
            <div class="prize item-col">
                <p class="fs14 thin nomrg left">
                    <img alt="Contest name" class="left" height="46" src="<%= url %>assets/samples/contest_photo.png" width="46">
                    Gift card from BOUJOI won in BOUJOI contest
                </p>
            </div>
        </div>
    </div>
</div>
