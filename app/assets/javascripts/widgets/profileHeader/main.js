define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'./models/UserSignupModel',
	'layoutmanager',
	'uploader'
], function ( app, widhetLayout, UserSignupModel ) {

	'use strict';

	return Backbone.View.extend( {

		template : widhetLayout,
		events : {
			'click .follow-btn' : 'followUser',
			'blur #description': 'saveDescription'
		},
		className : 'width widget-ProfileSection profile-item',
		tagName : 'div',
		initialize: function () {

			this.model.on('change', this.changeAttributes, this);

			app.on( 'profile:follow:' + this.model.get( 'username' ), this.followerCountChange, this );

			app.on( 'profile:unfollow:' + this.model.get( 'username' ), this.followerCountChange, this );

		},
		serialize : function () {
			var data = this.model.toJSON();
			data.isMe = app.isMe( this.model.get( 'username' ) );
			return _.defaults( data, { 'url' : app.getURL(), 'image': data.avatar } );
		},
		/**
		 * Re-render header if main info changed
		 */
		changeAttributes: function () {
			this.render();
		},
		/**
		 * When some one clicked foolow/unfollow on profile page. Change counters in this case
		 * @param {String} action follow or unfollow
		 */
		followerCountChange: function (action) {
			var numberF = (action == 'follow') ? 1 : -1;
			if ( app.isMe( this.model.get( 'username' ) ) ) {
				this.model.set( 'following', this.model.get( 'following') + numberF );
			} else {
				this.model.set( 'followers', this.model.get( 'followers') + numberF );
			}
		},
		afterRender: function () {
			/**
			 * If we on our own profile - show upload button on avatar
			 */
			if ( app.isMe( this.model.get( 'username' ) ) ) {
				this.initUploader();
			}
		},
		/**
		 * Save description on blur event
		 */
		saveDescription: function () {
			var infoText = this.$( '#description' ).val();

			if ( _.isEmpty( infoText ) ) {
				return false;
			}

			app.sendRequest( '/users', {
				method: 'POST',
				data: { user: {about: infoText.substr( 0, 250 ) }},
				complete: function () {
					app.trigger( 'show:message', { 'text': 'Your info succesfully saved', 'type': 'success' } );
				}
			} );

		},
		/**
		 * Follow profile owner
		 */
		followUser: function () {
			app.sendRequest( this.model.getFollowUrl(), {
				method: 'POST',
				complete: function ( jqXHR ) {
					if (jqXHR.status == 200) {
						this.model.set('followed_by_you', !this.model.get('followed_by_you'));
						var currentStatus = this.model.getCurrentFollowStatus();
						app.trigger( 'profile:' + currentStatus + ':' + this.model.get( 'username' ), currentStatus );
						this.render();
					}
				},
				single: true,
				context: this
			});
		},
		/**
		 * Init avatar uploader, when we click on avatar pic
		 */
		initUploader: function () {
			var _self = this;
			new qq.FineUploaderBasic( {
				button: $( '.avatar ' )[0],
				request: {
					endpoint: '/profiles/upload_avatar'
				},
				callbacks: {
					onUpload: function ( ) {
						_self.$('.spinner').removeClass('disnone');
					},
					onComplete: function ( id, fileName, data ) {
						_self.$('.spinner').addClass('disnone');
						if ( data && data.avatar ) {
							app.sendRequest( '/users', {
								data   : { user: {avatar_cache: data.avatar_cache}},
								context: _self,
								method : 'POST',
								complete: function () {
									this.$( '#mainAvatar' ).attr( 'src', app.getURL() + data.avatar );
									$('.userpic').attr( 'src', app.getURL() + data.avatar );
									app.trigger( 'show:message', { 'text': 'Avatar successfully changed!', 'type': 'success' } );
								}
							} )
						} else {
							app.trigger( 'show:message', { 'text': 'Wrong file format!', 'type': 'error' } );
						}
					}
				}
			} );
		}
	} );

} );
