define([
    'app'
], function( app ) {

	'use strict';

	return Backbone.Model.extend({
		
		validation: {
			
			'phone' : {
				required: false,				
                pattern: /^[\d\s\-\+_]+$/,
				msg: 'Phone is invalid' 
			},
			'website': {
				required: false,
				pattern: /^(https?:\/\/)?[a-z0-9~_\-\.]+\.[a-z]{2,9}(\/|:|\?[!-~]*)?$/,
				msg: 'Website is invalid'
			},
			
			'about': {
				required: false,
				maxLength: 250,
				msg: 'Max size is 250 characters'
			}
            
		},

		setSettings: function ( options ) {
			var that = this;
			app.sendRequest('/users', {
				data: options.data,
				method: 'POST',
				callback: function ( response ) {
					if ( response ) {
						options.success( response );
					} else {
						options.error( response );
					}
				},
				complete: options.complete || false
			});

			return this;
		}
	});
});