<ul>
    <li>
        <div class="avatar left rel">
            <img id="mainAvatar" alt="<%= fullname%>" class="m" height="151" src="<%= url %><%= avatar%>" width="151" />
            <!-- <i class="moderaIcons">E</i> -->
            <div class="spinner disnone">
              <img src="<%=url%>/assets/loading.gif" class="loading" />
            </div>
        </div>
        <ul class="fs14 thin">
            <li class="step-btn" data-step="photos">
                <a class="step-switcher" href="#photos">
                    <b class="disblock"><%= photos%></b> Photos
                </a>
            </li>
            <li data-step="followers">
                <a class="step-switcher" href="#followers">
                    <b class="disblock"><%= followers%></b> Followers
                </a>
            </li>
            <li data-step="followers">
                <a class="step-switcher" href="#followers">
                    <b class="disblock"><%= following%></b> Following
                </a>
            </li>
        </ul>
    </li>
    <li class="c">
        <h2 class="c"><%= fullname%></h2>
        <div class="clear">
            <a href="/home/#/hashtag_contests/<%= style%>" class="fs14 thin disblock">#<%= style%></a>
        </div>
        <a href="#hall_of_fame" class="btn place">
            <!--<i class="moderaIcons left">)</i>-->
                <span class="disinbl">
                  <%= place%>
                  <!-- <br /> -->
                  <span class="thin">place</span>
                </span>
        </a>
        <% if (!owner){ %>
        <div class="btn follow-btn <%= active%>"><%= followText%></div>
        <% } %>
    </li>
    <li>
        <div class="share-panel">
            <!-- /facebook -->
                <a
            <% if (facebook != null){ %>
                href="<%= facebook %>" target="_blank"
            <% } else if (isMe) { %>
                href="/profiles/<%= username %>/#settings/profile"
            <% } else { %>
                nohref="nohref"
            <% } %> class="share-icons facebook <% if (facebook != null) { %> active <% } %>" >
                      <i class="moderaIcons">f</i>
                </a>
            <!-- /twitter -->
                <a
            <% if (twitter != null){ %>
                href="http://<%= twitter %>" target="_blank"
            <% } else if (isMe) { %>
                href="/profiles/<%= username %>/#settings/profile"
            <% } else { %>
                nohref="nohref"
            <% } %>
            class="share-icons twitter <% if (twitter != null) { %> active <% } %>" >
                      <i class="moderaIcons">t</i>
                </a>
            <!-- /tumblr -->
                <a href="http://www.tumblr.com/share/link?url=<%= url %><%= image %>&name=&description=" title="Share on Tumblr" class="share-icons tumblr disnone" target="_blank">
                      <i class="moderaIcons" >u</i>
                </a>
            <!-- /gplus -->
                <a href="https://plus.google.com/share?url=<%= url %><%= image %>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="share-icons gplus disnone" target="_blank">
                      <i class="moderaIcons">g</i>
                </a>
            <!-- /instagram -->
                <a
            <% if (instagram != null){ %>
                href="<%= instagram %>" target="_blank"
            <% } else if (isMe) { %>
                href="/profiles/<%= username %>/#settings/profile"
            <% } else { %>
                nohref="nohref"
            <% } %>  class="share-icons instagram <% if (instagram != null){ %> active <% } %>" target="_blank">
                  <i class="moderaIcons">i</i>
                </a>

            <!-- /web -->
                <a
            <% if (!_.isEmpty(website)){ %>
                href="http://<%= website %>" target="_blank"
            <% } else if (isMe) { %>
                href="/profiles/<%= username %>/#settings/profile"
            <% } else { %>
                nohref="nohref"
            <% } %> class="share-icons web <% if (website != null){ %> active <% } %>" >
                  <i class="moderaIcons">w</i>
                </a>
        </div>
        <% if (isMe) { %>
            <form action="#">
                <textarea id="description" <% if (_.isEmpty(about)) { %> placeholder="Tell your followers about you" <% } %> ><% if (!_.isEmpty(about)) { %> <%=about %> <% } %></textarea>
            </form>
        <% } else { %>
            <p class="about-me clear thin"><%= about %></p>
        <% } %>

    </li>
</ul>
