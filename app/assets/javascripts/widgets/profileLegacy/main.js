define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'layoutmanager'
], function ( app, widgetLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template : widgetLayout,
		events : { },
		className : 'widget-ProfileLegacy profile-steps profile-item legacy',
		tagName : 'div',
		initialize: function () {

		}
	} );

} );
