define( [
	'app',
	'backbone'
], function ( app, Backbone ) {
	return Backbone.Collection.extend( {

		urlRoot: '/brands',

		brandList: function ( options, that ) {
			app.sendRequest( this.urlRoot, {
				callback: function ( data ) {
					options.success( data, that );
				},
				context: this
			} );
		}
	} );
} );