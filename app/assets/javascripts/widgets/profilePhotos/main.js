define( [
	'app',
	'text!./templates/widgetLayout.tpl',
	'text!./templates/oneMostPhoto.tpl',
	'./views/PhotoGridView',
	'layoutmanager'
], function ( app, widhetLayout, oneMostPhotoTpl, PhotoGridView ) {

	'use strict';

	return Backbone.Layout.extend( {

		template: widhetLayout,
		events: {
			'click .profile-sort': 'sortPhotos'
		},
		mostPhotoTpl: _.template(oneMostPhotoTpl),
		className : 'photos-info widget-ProfilePhotos profile-steps profile-item',
		tagName : 'div',
		initialize : function () {
			this.collectionMost = new Backbone.Collection( [] );
			this.collectionMost.on( 'add', this.renderPopular, this );
			app.sendRequest( ' /profiles/' + this.options.parent.model.get( 'info' ).username + '/popular_photos', {
				context: this,
				callback: function ( data ) {
					this.collectionMost.add( data );
					app.trigger( 'scroll:refresh', null);
				}
			} );
            this.on( 'afterRender', this.afterRender, this );

			//app.on( 'profile:photos:update:scroll', this.setNanoscroller, this );
		},
		serialize: function () {
			return this.options.parent.model.toJSON();
		},
		beforeRender: function () {
			this.gridView = new PhotoGridView( {parent: this, username :  this.options.parent.model.get( 'info' ).username} );
			this.insertView( '.photos-grid', this.gridView );
		},
		/**
		 * Method for inserting most populat block items
		 */
		renderPopular: function (model) {
			model.set( 'url', app.getURL() );
			this.$( '.top-photos-h' ).removeClass( 'disnone' );
			this.$('.most-top-photo' ).append(this.mostPhotoTpl(model.toJSON()));
		},
		sortPhotos: function (event) {
			var comparator = this.$( event.target ).data( 'comparator' );
			if (comparator > 0) {
				this.gridView.photoCollection.comparator = function (model) {
					return model.get('rate');
				};
				this.gridView.photoCollection.sort();
			}
		},
		paginate: function () {
			this.gridView.paginate();
		},
        afterRender: function() {
        	setTimeout(function(){
        	app.trigger( 'scroll:refresh', null);
        	},500);
        	
              //this.setNanoscroller();
        }/*,
		setNanoscroller: function () {
			this.$('.nano').css('height', screen.availHeight * 0.6 );
			this.$('.nano').nanoScroller({ scroll: 'top' });
		}*/
	} );

} );
