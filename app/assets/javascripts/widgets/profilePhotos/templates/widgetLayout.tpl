<div class="profile-control c rel overhide disnone">
    <div class="btn right step-btn" data-step="hall_of_fame">
        Hall of Fame
        <i class="moderaIcons m">&gt;</i>
    </div>
    <h1 class="left">Photos Info</h1>
    <div class="abs disnone">
        <div class="btn">Share with your Media</div>
    </div>
</div>
        <!--Hide for now-->
<div class="profile-sort c disnone">
    <span>Sort photo's by quality</span>
    <span class="asLink" data-comparator="80">More than 80%</span>
    <span class="asLink active" data-comparator="50">More than 50%</span>
    <span class="asLink" data-comparator="20">Less than 20%</span>
    <span class="asLink" data-comparator="0">Recently posted</span>
</div>

<div class="profile-step-container ">
    <div class="h1 c disnone top-photos-h disnone" style="display:none"><%=info.username%>'s most popular photos in the last 7 days</div>
    <div class="most-top-photo c disnone">
        <!--Place for most popular photos-->
    </div>
    <div class="photos-grid">
    </div>

    <div class="faceplate c disnone">
        <i class="moderaIcons">E</i>
        <span class="thin disblock">Let's put your first photo on Modera</span>
        <p class="c">
            <a href="#photos/new" class="btn">Upload Photo</a>
        </p>
    </div>
</div>

