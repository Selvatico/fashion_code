define( [
	'backbone',
	'text!../templates/oneMostPhoto.tpl',
	'app'
], function ( Backbone, photoTpl, app ) {
	return Backbone.View.extend( {
		template : photoTpl,
		tagName : 'img',
		initialize : function () {},
		serialize : function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		}
	} );
} );
