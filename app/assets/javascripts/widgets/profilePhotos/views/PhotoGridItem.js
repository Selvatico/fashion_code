define( [
	'backbone',
	'text!../templates/oneGridPhoto.tpl',
	'app'
], function ( Backbone, photoTpl, app ) {
	return Backbone.View.extend( {

		events: {

		},
		template : photoTpl,
		tagName : 'li',
		className : 'photo-grid-item',
		initialize : function () {},
		serialize : function () {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		}
	} );
} );