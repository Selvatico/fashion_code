define( [
	'backbone',
	'./PhotoGridItem',
	'models/PhotoModel',
	'collections/PhotosCollection',
	'app'
], function ( Backbone, PhotoGridItem, PhotoModel, PhotosCollection, app ) {
	return Backbone.View.extend( {

		events: {

		},
		className : 'c',
		tagName : 'ul',
		model : new Backbone.Model( {page: 1} ),
		collection : new Backbone.Collection( {model: PhotoModel} ),
		initialize : function () {
			this.photoCollection = new PhotosCollection( [], {urlRoot: '/profiles/' + this.options.username + '/photos'} );
			this.photoCollection.on( 'add', this.renderPhoto, this );
			this.photoCollection.on( 'sort', this.renderPhotos, this);
			this.photoCollection.on( 'photo:loaded', this.photoLoaded, this);
			this.model.set( {page: 1} );
			this.loadPhotos();
		},
		loadPhotos	: function () {
			this.photoCollection.photoList( {page: this.model.get( 'page' ), view: 'grid'} );
		},
		/**
		 *
		 * @param {Object} data
		 * @param {Object} data.photos_count Overall count of photos
		 */
		photoLoaded : function (data) {
			this.model.set('count_photos', data.photos_count);
			if (data.photos_count == 0) {
				this.options.parent.$el.find( '.faceplate' ).removeClass( 'disnone' );
			} else {
				app.trigger( 'profile:photos:update:scroll' );
			}
		},
		renderPhotos: function ( ) {
			if ( this.photoCollection.length > 0 ) {
				this.getViews( {className: 'photo-grid-item'} ).each( function ( nestedView ) {
					nestedView.remove();
				} );
				this.photoCollection.each( this.renderPhoto, this );
			}
		},
		renderPhoto: function ( model ) {
			this.insertView( new PhotoGridItem( {parent: this, model: model} ) ).render();
		},
		/**
		 * Load more photos on scroll
		 */
		paginate: function () {
			var countPhotos = this.model.get('count_photos' ),
				limit = 10,
				currentCount = this.photoCollection.length,
				currentPage = this.model.get('page');

			if ( currentCount < countPhotos && (currentPage) * limit < countPhotos ) {
				this.model.set( 'page', currentPage + 1 );
				this.loadPhotos();
			}
		}
	} );
} );