define([
    'app',
    '../models/SharePreferences'
], function (
	app,
	SharePreferencesModel
) {

	'use strict';

	return Backbone.Collection.extend({

		model: SharePreferencesModel,

		getBySocialNetwork: function ( name ) {

			return this.where( { 'social_network': name } )[ 0 ];

		},

		saveSettings: function ( options ) {

			app.sendRequest('/users', {
				data: {
					user: {
						'share_preferences_attributes': this.toJSON()
					}
				},
				method: 'POST',
				callback: options.success || false,
				complete: options.success || false
			});

			return this;
		}

	});

});