define( [
	'app',
	'backbone',
	'moment',
	//'widgets/menu/main',
	'models/CurrentUserModel',
	'./models/UserSignupModel',
	'./views/Profile',
	'./views/Account',
	'./views/SharePreferences',
	'./views/Password',
    './views/Notifications',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function (
	app,
	Backbone,
	moment,
	CurrentUserModel,
	UserSignupModel,
	ProfileView,
	AccountView,
	SharePreference,
	Password,
    NotificationsView,
	template) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'widget-Settings width',

		template : template,
		userModel: new UserSignupModel(),
		
		events : {
			'click aside.left a'			: 'changePage',
			'change .date-born select'		: 'setDate',
			'change #facebookNotifications, #twitterNotifications' : 'showFacebook'
		},

		initialize: function () {
			this.model = this.options.parent.model;
		},
		
		beforeRender : function () {
			this.insertView( '.profile-settings', new ProfileView( { user : this.model } ) );
			this.insertView( '.account-settings', new AccountView( { user : this.model } ) );
			this.insertView( '.share-settings', new SharePreference( { user : this.model } ) );
			this.insertView( '.password-settings', new Password( { user : this.model } ) );
            this.insertView( '.notifications-settings', new NotificationsView( { user : this.model } ) );
		},
		afterRender: function () {
			if ( this.options.parent.options.settingSection ) {
				this.setActivePage( this.options.parent.options.settingSection + '-settings' );
			}
		},
		
		serialize : function () {
			var format = 'YYYY-MM-DD HH:mm:ssZ';
			var options = {
				'curYear'	: moment().format( 'YYYY' ),
				'year'		: moment( this.model.get( 'info' ).birth, format ).format( 'YYYY' ),
				'month'		: moment( this.model.get( 'info' ).birth, format ).format( 'MMMM' ),
				'day'		: moment( this.model.get( 'info' ).birth, format ).format( 'DD' )
			};
			return {
				'data'		: this.model.toJSON(),
				'options'	: options,
				'url'		: app.getURL()
			};
		},
		
		changePage : function ( eventSender ) {
			this.setActivePage($( eventSender.currentTarget ).attr ( 'page' ));
		},
		setActivePage: function (pageName) {
			this.$( 'aside.left a' ).removeClass( 'active' );
			this.$( '.page' ).addClass( 'disnone' );
			this.$( '.' + pageName ).removeClass( 'disnone' );
			this.$( 'aside a[page="' + pageName + '"]' ).addClass( 'active' );

			Backbone.history.navigate( 'settings/' + pageName.replace('-settings', ''), { trigger: false } );
		},
		
		save : function ( e ) {
			e.preventDefault();
			var page = this.$( 'aside.left a.active' ).attr( 'page' );

			var map = this.getData [ page ]( this );

			if ( this.userModel.set( map ) && this.userModel.isValid( true ) ) {
				
			} else {
				this.$( '[placeholder]' ).attr( 'placeholder', '' );
				this.$( '.alert-error' ).fadeIn();
			}
		},
		
		showFacebook: function ( eventSender ) {
			this.$( '.' + $( eventSender.currentTarget ).attr( 'name' ) ).toggleClass( 'disnone' );
		},
		
		setDate: function ( eventSender ) {
			this.$( 'span.' + $( eventSender.currentTarget ).attr( 'name' ) ).text ( $( eventSender.currentTarget ).val() );
		}
		
	});
});
