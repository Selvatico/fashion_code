define([
    'app'
], function( app ) {

	'use strict';

	return Backbone.Model.extend({

		descriptions: function () {

			return {
				'follow'		: 'When I follow a user',
				'join_contest'	: 'When I join a contest',
				'post_comment'	: 'When I post comments',
				'upload_photo'	: 'When I upload a picture',
				'win_prize'		: 'When I win a contest'
			};

		},

		ignore: function () {

			return [
				'id',
				'active',
				'social_network'
			];

		},

		isCheckbox: function ( key ) {
			return !( !key || this.ignore().indexOf( key ) !== -1 );
		},

		getDescription: function ( key ) {

			if ( !this.isCheckbox( key ) ) { return false; }

			return this.descriptions()[ key ];

		}

	});
});