<aside class="left">
	<nav class="thin fs14 r">
		<a class="active" page="profile-settings" nohref="nohref">Profile</a>
		<a page="account-settings" nohref="nohref">Account</a>
		<a page="share-settings" nohref="nohref">Share Preferences</a>
		<a page="password-settings" nohref="nohref">Password</a>
		<a page="notifications-settings" nohref="nohref" >Notifications</a>
	</nav>
</aside>
<div class="base-content overhide settings">
	<!-- / Profile settings -->
	<div class="page profile-settings">
		<h1>
			Profile Settings <small>Here you can edit your profile</small>
		</h1>

	</div>
	<!-- / Account -->
	<div class="disnone page account-settings">
		<h1>
			Edit Account <small>Edit your account details</small>
		</h1>

	</div>
	<!-- / Share Preferences -->
	<div class="disnone page share-settings">
		<h1>Share Preferences <small>Choose what and where you'd like to share</small></h1>

	</div>
	<!-- / Change Password -->
	<div class="disnone page password-settings">
		<h1>Change Password</h1>

	</div>
	<!-- / Notifications -->
	<div class="disnone page notifications-settings">
	</div>
</div>