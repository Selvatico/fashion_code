		<h1>Notifications</h1>
		<p>
			We send notifications whenever actions are taken that involove you.
			You can select which notifications you want to receive. <br>
			Notifications will be send to <b><%= info.email %></b>
		</p>
		<form action="#" class="notification-checkboxes">
			<fieldset>
				<div class="share-panel right">
					<div class="share-icons mail">
						<i class="moderaIcons">m</i>
					</div>
				</div>
				<div class="fs14">Notifications Type</div>
				<div class="niceCheckbox clear">
					<input class="niceCheckbox-checkbox" id="N_new_follower" name="new_follower" type="checkbox" <%= checkParameter( 'info.new_follower') %>>
					
					<label class="thin fs14" for="N_new_follower">You have a new follower</label>
					<input class="niceCheckbox-checkbox" id="N_comment_pic" name="new_comment" type="checkbox" <%= checkParameter( 'info.new_comment') %>>
					
					<label class="thin fs14" for="N_comment_pic">A new comment on your picture</label>
					<!-- <input class="niceCheckbox-checkbox" id="N_mention_ph" name="N_mention_ph" type="checkbox" <%= checkParameter( 'info.new_mention') %>>
					
					<label class="thin fs14" for="N_mention_ph">Someone mentioned you in a power hour</label> -->
					<input class="niceCheckbox-checkbox" id="N_mention_comment" name="new_mention" type="checkbox" <%= checkParameter( 'info.new_mention') %>>
					
					<label class="thin fs14" for="N_mention_comment">Someone mentioned you in a comment</label>
					<!-- <input class="niceCheckbox-checkbox" id="N_joined_contest" name="join_contest" type="checkbox" <%= checkParameter( 'info.join_contest') %>>
					
					<label class="thin fs14" for="N_joined_contest">Someone joined your contest</label> -->
					<input class="niceCheckbox-checkbox" id="N_daily" name="daily_activity" type="checkbox" <%= checkParameter( 'info.daily_activity') %>>
					
					<label class="thin fs14" for="N_daily">Daily activity on Modera</label>
					<input class="niceCheckbox-checkbox" id="N_weekly" name="weekly_activity" type="checkbox" <%= checkParameter( 'info.weekly_activity') %>>
					
					<label class="thin fs14" for="N_weekly">Weekly activity on Modera</label>
					<input class="niceCheckbox-checkbox" id="N_friend_joined" name="friend_joined" type="checkbox" <%= checkParameter( 'info.friend_joined')%>>
					
					<label class="thin fs14" for="N_friend_joined">A friend joined Modera</label>
				</div>
			</fieldset>
			<!-- <fieldset>
				<div class="niceRadio right">
					<input class="niceRadio-radio" id="frequency_immediately" name="activity" type="radio" value="immediately" <%= checkParameter( 'info.activity', 'immediately' ) %>/>
					<label for="frequency_immediately">Immediatley</label>
					<input class="niceRadio-radio" id="frequency_daily" name="activity" type="radio" value="daily" <%= checkParameter( 'info.activity', 'daily' ) %> checked="checked" />
					<label for="frequency_daily">Daily</label>
					<input class="niceRadio-radio" id="frequency_never" name="activity" type="radio" value="never" <%= checkParameter( 'info.activity', 'never' ) %>/>
					<label for="frequency_never">Never</label>
				</div>
				<div class="thin fs14">
					<b>Frequency</b> <br> How often would you like to receive <br>
					email notifications?
				</div>
			</fieldset> -->
			<div class="c">
				<input class="btn active save" name="commit" type="submit" value="Save Changes">
			</div>
		</form>