<fieldset>
	<label class="thin fs14" for="password">New Password</label>
	<div class="fieldForm rel">
		<span class="close-error abs">×</span>
		<span class="message-error abs alert-error">Password can\'t be blank</span>
		<input id="password" name="password" type="password" placeholder="">
	</div>  
  <label class="thin fs14" for="password">Confirm Password</label>
  <div class="fieldForm rel">
    <span class="close-error abs">×</span>
    <span class="message-error abs alert-error">Password doesn\'t match confirmation</span>
    <input id="confirm-password" name="confirm-password" type="password" placeholder="">
  </div>
</fieldset>
<div class="c">
	<input class="btn active" name="commit" type="submit" value="Save Changes">
</div>