<fieldset>
	<label class="thin fs14" for="realname">Real Name</label>
	<div class="fieldForm">
		<input id="realname" name="realname" type="text" placeholder="Real Name" value="<%=data.info.fullname%>">		
	</div>
</fieldset>
<fieldset>
	<h3>Private profile information</h3>
	<div class="date-born thin">
		<span class="fs14">Birthday</span>
		<div class="cuSel">
			<span class="month"><%= options.month %></span>
			<select name="month" id="profile_month_select">
				<option value="January">January</option>
				<option value="February">February</option>
				<option value="March">March</option>
				<option value="April">April</option>
				<option value="May">May</option>
				<option value="June">June</option>
				<option value="July">July</option>
				<option value="August">August</option>
				<option value="September">September</option>
				<option value="October">October</option>
				<option value="November">November</option>
				<option value="December">December</option>
			</select>
		</div>
		<div class="cuSel">
			<span class="day"><%= options.day %></span> <select name="day" id="profile_day_select"> 
				<% for (var i = 1; i <=31; i++) { %>
				<option value="<%= i %>"><%= i %></option>
				<% } %>
			</select>
		</div>
		<div class="cuSel">
			<span class="year"><%= options.year %></span>
			<select name="year" id="profile_year_select">
				<% for (var i = options.curYear - 13; i > options.curYear - 90; i--) { %>
				<option value="<%= i %>"><%= i %></option>
				<%}%>
			</select>
		</div>
	</div>
	<div class="niceRadio" id="sex">
		<span class="thin fs14">Sex</span> 
		<input class="niceRadio-radio" id="sex_true" name="sexMale" type="radio" value="<%= checkSex(true) %>"<% if (data.info.sex) { %> checked="checked"<% } %>> 
		<label for="sex_true">Male</label> 
		<input class="niceRadio-radio" id="sex_false" name="sexFemale" type="radio" value="<%= checkSex(false) %>"<% if (!data.info.sex) { %> checked="checked"<% } %>> 
		<label for="sex_false">Female</label>
	</div>
	<label class="thin fs14" for="phone">Phone Number</label>
	<div class="fieldForm rel">
		<span class="close-error abs">×</span>
		<span class="message-error abs alert-error">Phone is invalid</span>
		<input id="phone" name="phone" type="text" placeholder="Phone" value="<%= data.info.phone %>" />
		<span class="info thin">This information is totally private and absolutely confidential!</span>
	</div>
</fieldset>
<fieldset>
	<label class="thin fs14" for="website">Website</label>
	<div class="fieldForm rel">
		<span class="close-error abs">×</span>
		<span class="message-error abs alert-error">Website is invalid</span>
		<input id="website" name="website" type="text" placeholder="Website" value="<%= data.info.website %>">
	</div>
	<label class="thin fs14" for="bio">Bio</label>
	<div class="fieldForm rel">
		<span class="close-error abs">×</span>
		<span class="message-error abs alert-error">Max size is 250 characters</span>
		<textarea id="bio" name="bio" rows="5" placeholder="Bio"><%= data.info.about %></textarea>
		<span class="thin disblock r">250 symbols</span>
	</div>
	<div class="fieldForm social-connect">
		<label class="left facebook rel curpoint"> <i class="moderaIcons">f</i>
		</label> <input id="facebook" name="facebook" readonly="readonly" type="text"<% if (data.info.facebook) { %>
		value="<%= data.info.facebook %>" class="left active"<% } else { %>
		value="Click to Connect" class="left noactive curpoint" <% } %> /> <a class="info disnone"
			href="#"> Change <br /> Facebook account
		</a>
	</div>
	<div class="fieldForm social-connect clear">
		<label class="left twitter rel curpoint"> <i class="moderaIcons">t</i>
		</label> <input id="twitter" name="twitter" readonly="readonly" type="text"<% if (data.info.twitter) { %>
		value="<%= data.info.twitter %>" class="left active" <% } else { %>
		value="Click to Connect" class="left noactive curpoint" <% } %> /> <a
			class="info disnone" href="#"> Change <br /> Twitter account
		</a>
	</div>
	<div class="fieldForm social-connect clear">
		<label class="left instagram rel curpoint"> <i class="moderaIcons">i</i>
		</label> <input id="instagram" name="instagram" readonly="readonly" type="text"<% if (data.info.instagram) { %>
		value="<%= data.info.instagram %>" class="left active" <% } else { %>
		value="Click to Connect" class="left noactive curpoint" <% } %> /> <a
			class="info disnone" href="#"> Change <br /> Instagram account
		</a>
	</div>
</fieldset>
<div class="c">
	<input class="btn active" name="commit" type="submit" value="Save Changes" />
</div>