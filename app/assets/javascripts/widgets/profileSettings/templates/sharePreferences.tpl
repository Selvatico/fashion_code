<fieldset>
	<div class="onoffswitch disinbl m right">
		<input class="onoffswitch-checkbox" id="facebookNotifications" data-network="facebook"
			type="checkbox" name="facebook-info"> <label
			class="onoffswitch-label" for="facebookNotifications">
			<div class="onoffswitch-inner">
				<div class="<% if (facebook) { %>onoffswitch-active<% } else { %>onoffswitch-inactive<% } %>">Yes</div>
				<div class="<% if (!facebook) { %>onoffswitch-active<% } else { %>onoffswitch-inactive<% } %>">No</div>
			</div>
		</label>
	</div>
	<div class="share-panel left">
		<div class="share-icons facebook active">
			<i class="moderaIcons">f</i> <b class="fs14">Facebook</b>
		</div>
	</div>
	<div class="niceCheckbox clear facebook-info<% if (!facebook) { %> disnone<% } %>"></div>
</fieldset>
<fieldset>
	<div class="onoffswitch disinbl m right">
		<input class="onoffswitch-checkbox" id="twitterNotifications" data-network="twitter"
			type="checkbox" name="twitter-info"> <label
			class="onoffswitch-label" for="twitterNotifications">
			<div class="onoffswitch-inner">
				<div class="<% if (twitter) { %>onoffswitch-active<% } else { %>onoffswitch-inactive<% } %>">Yes</div>
				<div class="<% if (!twitter) { %>onoffswitch-active<% } else { %>onoffswitch-inactive<% } %>">No</div>
			</div>
		</label>
	</div>
	<div class="share-panel left">
		<div class="share-icons twitter active">
			<i class="moderaIcons">t</i> <b class="fs14">Twitter</b>
		</div>
	</div>
	<div class="niceCheckbox clear twitter-info<% if (!twitter) { %> disnone<% } %>"></div>
</fieldset>
<div class="c">
	<input class="btn active" name="commit" type="submit"
		value="Save Changes">
</div>