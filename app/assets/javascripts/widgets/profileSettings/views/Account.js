define( [
	'app',
	'../models/UserSignupModel',
	'text!../templates/account.tpl',
	'uploader',
	'layoutmanager'
], function ( app, UserSignupModel, accountLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template: accountLayout,
		tagName: 'form',
		model: new UserSignupModel(),
		fileTypes: [ 'jpg', 'jpeg', 'gif', 'png' ],
		events: {
			'click input[type="submit"]'	: 'save',
			'click div *'					: 'closeError',
			'click .avatarFacebook'			: 'loadSocialAvatar',
			'click .avatarTwitter'			: 'loadSocialAvatar'
		},
		
		placeholders: {
			'email': 'Email'
		},
		
		initialize: function () {
			_.bindAll( this, 'onSocialAvatarLoaded' );
			Backbone.Validation.bind(this);
			this.avatar = this.options.user.toJSON().info.avatar;
			window.notifications.on( 'social:avatar', this.onSocialAvatarLoaded );
		},
		
		serialize : function () {
			return {
				'avatar'	: this.avatar,
				'data'		: this.options.user.toJSON(),
				'url'		: app.getURL()
			};
		},
		
		afterRender : function(){
			var that = this; 
			this.fileUploader = new qq.FineUploaderBasic({
				button: document.getElementById( 'file-uploader' ),
				
				request: {
					endpoint: '/profiles/upload_avatar',
					params : {
						'X-CSRF-Token' : $( 'meta[name="csrf-token"]' ).attr( 'content' )
					}
				},
				callbacks: {
					onUpload: function(id, fileName) {
						that.$('.spinner').removeClass('disnone');
					},
					onComplete: function(id, fileName, data) {
						that.$('.spinner').addClass('disnone');
						that.avatar = data.avatar;
						that.avatar_cache = data.avatar_cache;
						that.render();						
					},
                    onSubmit: function( id, name ) {
                        var type = name.split('.'),
                            type = type[ type.length - 1 ];
                        if ( that.fileTypes.indexOf( type ) == -1 ) {
                            that.$( '.load-avatar' ).addClass( 'error' );
                            return false;                                
                        }    
                        
                    }
				 }
			});
            this.$( '#email' ).placeholder();
		},
		
		closeError: function( e ) {
			var $el = $(e.currentTarget).parent('div'), $input = $el.find('input');
			$el.removeClass('error');
            if ( $input.attr( 'id' ) != 'user_avatar' )
			   $input.attr('placeholder', this.placeholders[ $input.attr('id') ]);
		},

		loadSocialAvatar: function ( e ) {
			var button, provider;

			button = $( e.currentTarget );

			if ( button.hasClass( 'avatarFacebook' ) ) {
				provider = 'facebook';
			} else if ( button.hasClass( 'avatarTwitter' ) ) {
				provider = 'twitter';
			}

			var width	= 500,
				height	= 534,
				left	= $( window ).width() / 2 - ( width / 2 ),
				top		= $( window ).height() / 2 - ( height / 2 ),
				params	= 'menubar=no,location=yes,resizable=yes,scrollbars=no,status=yes,width=' + width + ',height=' + height + ',modal=true, left=' + left + ',top=' + top,
				url		= '/users/social/' + provider + '/avatar',
				title	= provider.charAt( 0 ).toUpperCase() + provider.slice( 1 );

			window.open( url, title, params );
		},

		onSocialAvatarLoaded: function ( data ) {
			this.avatar = data.avatar;
			this.render();
			app.trigger( 'show:message', { 'text': 'Avatar succesfully loaded from ' + ( data.provider.charAt( 0 ).toUpperCase() + data.provider.slice( 1 ) ), 'type': 'success' } );
		},
		
		save: function ( e ) {
			e.preventDefault();

			var self = this;
			
			var map = {
				user : {
					'email' : this.$('input[name="email"]').val(),
					'avatar_cache' : this.avatar || {}
				}
			};     

			this.model.set( map );

			if ( this.model.isValid( 'user.email' ) ){
				this.model.setSettings({
					data: map,
					success: self.saveSuccess,
					complete: self.saveSuccess
				});
			} else {
				if(!this.model.get('user').email){
					this.$('input[name="email"]').parent().find('.alert-error').html('Email can\'t be blank');
				}
				else{
					this.$('input[name="email"]').parent().find('.alert-error').html('Email is invalid');
				}
				this.$('input[name="email"]').attr('placeholder', '');
			    this.$('input[name="email"]').closest('div').addClass('error');
			    //this.$('input[name="email"]').closest('.alert-error').fadeIn();
			}
		},

		saveSuccess: function (data, response) {
      if (response == 'error') {
        this.$('input[name="email"]').parent().find('.alert-error').html('Email already taken!');
        app.trigger( 'show:message', { 'text': 'Email already taken!', 'type': 'error' } );
      } else {
			 app.trigger( 'show:message', { 'text': 'Your settings are succesfully saved', 'type': 'success' } );
      }
		}

	});

});