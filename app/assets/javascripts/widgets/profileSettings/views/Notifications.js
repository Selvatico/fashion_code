define( [
	'app',
	'../models/UserSignupModel',
	'text!../templates/notifications.tpl',
	'layoutmanager'
], function ( app, UserSignupModel, notificationsLayout ) {

	'use strict';

	return Backbone.Layout.extend( {
		
		template: notificationsLayout,
		data: {},
		events: {
			'click .save': 'save',
			'click .niceRadio label': 'changeRadioEventHandler' 
		},
		fields: {},
		initialize: function () {
			_.bindAll( this, 'checkParameter' );
			this.on('afterRender', this.afterRenderEventHandler, this);
		},
		
		serialize: function() {
			return _.extend( this.options.user.toJSON(), {
				checkParameter: this.checkParameter   
			});
		},
		
		save: function ( event ) {
			event.preventDefault();
			 
			for ( var i in this.fields ) {
				this.data[ i ] = ( this.$('[name="' + i + '"]').is(':checked') ) ? true : false ;    
			}
			this.data[ 'activity' ] = this.$('.niceRadio [checked="checked"]').val() || 'weekly';
			
			app.sendRequest( '/users', {
				method: 'POST',
				data: { user: this.data },
				context: this,
				callback: this.saveSuccess,
				complete: this.saveSuccess
			}); 
		},
		saveSuccess: function(  ) {
			app.trigger( 'show:message', { 'text': 'Your settings are succesfully saved', 'type': 'success' } );
		},
		/**
		 * View helpers
		 */
		checkParameter: function ( param, value ) {
			var params = param.split( '.' ),
				checkVal = this.options.user.get( params[ 0 ] )[ params[ 1 ] ];;
			
			if( typeof value == 'undefined' ) {
				return ( checkVal ) ? 'checked="checked"' : '';   
			} else {
				if ( value == 'never' ) {
					return ( !checkVal || checkVal == 'never' ) ? 'checked="checked"' : '';     
				} else {
					return ( checkVal == value ) ? 'checked="checked"' : '';   
				}
			}
		},
		afterRenderEventHandler: function () {
			var checkboxes = this.$('.notification-checkboxes :checkbox'), i;
			
			for( i = 0; i < checkboxes.length; i++ ) {
				this.fields[ checkboxes[ i ].name ] = checkboxes[ i ].value;
			}            
		},
		changeRadioEventHandler: function( event ) {
			this.$('.niceRadio-radio').removeAttr('checked');
			var $el = $( event.currentTarget ),
				$input = this.$('#' + $el.attr('for') ).attr('checked', 'checked');
			
		} 
	});

} );
