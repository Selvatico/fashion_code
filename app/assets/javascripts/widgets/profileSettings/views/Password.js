define( [
         'app',
         '../models/UserSignupModel',
         'text!../templates/password.tpl',
         'layoutmanager'
         ], function ( app, UserSignupModel, accountLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template: accountLayout,
		tagName: 'form',
		model: new UserSignupModel(),

		events: {
			'click input[type="submit"]' : 'save',
			'click div *': 'closeError'
		},

		initialize: function () {
			Backbone.Validation.bind(this);
		},

		serialize : function () {
			return {
				'data'		: this.options.user.toJSON(),
				'url'		: app.getURL()
			};
		},
		
		closeError: function( e ){
			var $el = $(e.currentTarget).parent('div'), $input = $el.find('input');
			$el.removeClass('error');
		},

		save: function ( e ) {
			e.preventDefault();

			var map = {
				'user': {
					'password': this.$( 'input[name="password"]' ).val(),
          'password_confirmation': this.$( 'input[name="confirm-password"]' ).val()
				}
			};

			this.model.set(map);

			if ( this.model.isValid( 'user.password' ) ) {
				this.model.setSettings( { data: map, complete: function ( model, response ) {
          // TODO - fix this monkeycode :)
          if (response == 'error'){
            app.trigger( 'show:message', { 'text': 'Error!', 'type': 'error' } );
            this.$( 'input[name="confirm-password"]' ).closest( 'div' ).addClass( 'error' );
          } else {
            app.trigger( 'show:message', { 'text': 'Success!', 'type': 'success' } );
            this.$( 'input[name="confirm-password"]' ).val('');
            this.$( 'input[name="password"]' ).val('');
          }
          // No need to redirect
					// document.location.href = '/';
				}} );
			} else {
				this.$( 'input[name="password"]' ).closest( 'div' ).addClass( 'error' );
			}
		}
	} );

} );
