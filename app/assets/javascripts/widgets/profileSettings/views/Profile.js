define( [
	'app',
	'../models/UserSignupModel',
	'text!../templates/profile.tpl',
	'layoutmanager'
], function ( app, UserSignupModel, widhetLayout ) {

	'use strict';

	return Backbone.Layout.extend( {

		template: widhetLayout,
		tagName: 'form',
		model: new UserSignupModel(),
		
		events: {
			'click input[type="submit"]'	: 'save',
			'click div *'					: 'closeError',
			'click #sex label'				: 'sexEventHandler',
			'click .social-connect input'	: 'socialConnectButtonClick'
		},
		
		placeholders: {
			'phone': 'Phone',
			'website':'Website'
		},
		
		initialize: function () {
			_.bindAll( this, 'checkSex', 'onSocialConnect' );
			Backbone.Validation.bind( this );
			window.notifications.on( 'social:done', this.onSocialConnect );
		},
		months: [ 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december' ],
		serialize: function () {
			var birth = this.options.user.get( 'info' ).birth,
				birthDefined = ( birth !== null ),
				format = 'YYYY-MM-DD HH:mm:ssZ',
				options = {
					'curYear'	: moment().format( 'YYYY' ),
					'year'		: birthDefined ? moment( birth, format ).format( 'YYYY' ) : "",
					'month'		: birthDefined ? moment( birth, format ).format( 'MMMM' ) : "",
					'day'		: birthDefined ? moment( birth, format ).format( 'DD' ) : ""
				};

			return {
				data		: this.options.user.toJSON(),
				options		: options,
				url			: app.getURL(),
				checkSex	: this.checkSex
			};
		},
		
		closeError: function( e ){
			var $el = $(e.currentTarget).parent('div'), $input = $el.find('input');
			$el.removeClass('error');
			$input.attr('placeholder', this.placeholders[ $input.attr('id') ]);
		},
		
		save : function ( e ) {
			e.preventDefault();
			
			var date = {}, i,
				fields = ['month', 'day', 'year'], $el,
				selector, element;
				for ( i = 0; i < fields.length; i++ ) {
					selector = '#profile_' + fields[ i ] + '_select';
					if ( fields[i] == 'month' ) {
						element = this.$( '.' + fields[ i ] );
						if ( !element.text() ) { date = null; break; }
						date[ fields[ i ] ] = ( this.months.indexOf( element.text().toLowerCase() ) + 1 );      
					} else {
						element = this.$( '.' + this.$( selector ).attr('name') );
						if ( !element.text() ) { date = null; break; }
						date[ fields[ i ] ] = element.text();
					}
				}   
			var map = {			             			           
				fullname : this.$('input[name="realname"]').val(),
				birth : date,
				sex : (this.$('input[name="sexMale"]').val()) ? true : false,
				phone : this.$('input[name="phone"]').val(),
				website : this.$('input[name="website"]').val(),
				about : this.$('textarea[name="bio"]').val()                      					
			};		    
			this.model.set(map);            			
						
			if(this.model.isValid( 'phone' )){
				if(this.model.isValid( 'website' )){
					if(this.model.isValid( 'about' )){
						app.sendRequest( '/users', {
							method: 'POST',
							data: { user: this.model.toJSON() },
							context: this,
							callback: this.saveSuccess,
							complete: this.saveSuccess
						 });  
					}
					else{
						this.$('textarea[name="bio"]').attr('placeholder', '');
						this.$('textarea[name="bio"]').closest('div').addClass('error');						
					}
				}
				else{
					this.$('input[name="website"]').attr('placeholder', '');
					 this.$('input[name="website"]').closest('div').addClass('error');					 
				}
			}
			else {			     
				 this.$('input[name="phone"]').attr('placeholder', '');
				 this.$('input[name="phone"]').closest('div').addClass('error');				 
			}
		},
		saveSuccess: function() {
			app.trigger( 'show:message', { 'text': 'Your settings are succesfully saved', 'type': 'success' } );
		},
		sexEventHandler: function( event ) {
			var $el = $( event.currentTarget ), i,
				$input = this.$( '#' + $el.attr('for') ),
				ids = { 'sex_true': true, 'sex_false': false };
							
			for ( i in ids ) {
				if ( $input.attr('id') == i ) {
					$input.attr( 'value', ids[ i ] ).attr( 'checked', 'checked' );
				} else {
				   this.$( '#' + i ).attr('value', '').removeAttr('checked'); 
				}
			}                
		},        
		/**
		 * View helper methods
		 */
		checkSex: function( type ) {
			return ( this.options.user.get('info').sex == type ) ? type : '';
		},

		socialConnectButtonClick: function ( eventSender ) {
			var button = $( eventSender.currentTarget );
			if ( button.hasClass( 'noactive' ) ) {
				this.openSocialWindow( button.attr( 'name' ) );
			}
		},

		openSocialWindow: function ( provider ) {
			var width	= 500,
				height	= 534,
				left	= $( window ).width() / 2 - ( width / 2 ),
				top		= $( window ).height() / 2 - ( height / 2 ),
				params	= 'menubar=no,location=yes,resizable=yes,scrollbars=no,status=yes,width=' + width + ',height=' + height + ',modal=true, left=' + left + ',top=' + top,
				url		= '/users/oauth/' + provider,
				title	= provider.charAt( 0 ).toUpperCase() + provider.slice( 1 );
			window.open( url, title, params );
		},

		onSocialConnect: function ( data, success ) {
			this.$( '.social-connect' )
				.find( 'input[name="' + data.provider + '"]' )
				.removeClass( 'noactive curpoint' )
				.addClass( 'active' )
				.val( data.url );
			app.trigger( 'show:message', { 'text': 'Connect or disconnect finished!', 'type': 'success' } );
		},
        afterRender: function() {
            this.$( '#realname, #phone, #bio, #website' ).placeholder();   
        }
	});

});
