define([
	'app',
	'../collections/SharePreferences',
	'./SharePreferencesItem',
	'text!../templates/sharePreferences.tpl'
], function (
	app,
	SharePreferencesCollection,
	ItemView,
	accountLayout
) {

	'use strict';

	return Backbone.Layout.extend({

		tagName: 'form',
		template: accountLayout,

		events: {
			'click input[type="submit"]'			: 'save',
			'change input#facebookNotifications'	: 'changeNetworkState',
			'change input#twitterNotifications'		: 'changeNetworkState'
		},

		initialize: function () {
			this.collection = new SharePreferencesCollection( this.options.user.toJSON()[ 'share_preferences_attributes' ] );
			var facebookItems = this.collection.getBySocialNetwork( 'facebook' );
			var twitterItems = this.collection.getBySocialNetwork( 'twitter' );
			this.facebookItems = [];
			this.twitterItems = [];
			_.each( facebookItems.toJSON(), function ( value, key ) {
				if ( facebookItems.isCheckbox( key ) ) {
					this.facebookItems.push(
						new ItemView({
							model: new Backbone.Model({
								id: 'F_' + key,
								name: key,
								checked: value,
								description: facebookItems.getDescription( key )
							}),
							parent: this,
							networkModel: facebookItems
						})
					);
				}
			}, this);
			_.each( twitterItems.toJSON(), function ( value, key ) {
				if ( twitterItems.isCheckbox( key ) ) {
					this.twitterItems.push(
						new ItemView({
							model: new Backbone.Model({
								id: 'T_' + key,
								name: key,
								checked: value,
								description: twitterItems.getDescription( key )
							}),
							parent: this,
							networkModel: twitterItems
						})
					);
				}
			}, this);
		},

		serialize: function () {
			return {
				'facebook': !!this.collection.getBySocialNetwork( 'facebook' ).get( 'active' ),
				'twitter': !!this.collection.getBySocialNetwork( 'twitter' ).get( 'active' )
			};
		},

		beforeRender: function () {
			_.each( this.facebookItems, function ( view ) {
				this.insertView( '.niceCheckbox.facebook-info', view );
			}, this);
			_.each( this.twitterItems, function ( view ) {
				this.insertView( '.niceCheckbox.twitter-info', view );
			}, this);
		},

		changeNetworkState: function ( event ) {
			var input	= $( event.currentTarget ),
				network	= input.data( 'network' ),
				data	= this.options.user.toJSON(),
				state	= {};

			state.facebook	= !!this.$( 'input#facebookNotifications' ).prop( 'checked' );
			state.twitter	= !!this.$( 'input#twitterNotifications' ).prop( 'checked' );

			if ( !data.info[ network ] && state[ network ] ) { this.openSocialWindow( network ); }

			this.collection.getBySocialNetwork( network ).set( 'active', state[ network ] );
		},

		save: function ( e ) {
			e.preventDefault();

			this.collection.saveSettings({
				success: this.successOnSave,
				complete: this.successOnSave
			});

			return false;
		},

		successOnSave: function () {
			app.trigger( 'show:message', { 'text': 'Your settings are succesfully saved', 'type': 'success' } );
		},

		openSocialWindow: function ( provider ) {
			var width	= 500,
				height	= 534,
				left	= $( window ).width() / 2 - ( width / 2 ),
				top		= $( window ).height() / 2 - ( height / 2 ),
				params	= 'menubar=no,location=yes,resizable=yes,scrollbars=no,status=yes,width=' + width + ',height=' + height + ',modal=true, left=' + left + ',top=' + top,
				url		= '/users/oauth/' + provider,
				title	= provider.charAt( 0 ).toUpperCase() + provider.slice( 1 );
			window.open( url, title, params );
		}

	});

} );
