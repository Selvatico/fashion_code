define([
	'app',
	'text!../templates/sharePreferencesItem.tpl'
], function (
	app,
	itemTemplate
) {

	'use strict';

	return Backbone.Layout.extend({

		template: itemTemplate,

		events: {
			'change input[type="checkbox"]' : 'changeCheckbox'
		},

		initialize: function () {
			
		},

		serialize: function () {
			return this.model.toJSON();
		},

		changeCheckbox: function () {
			this.options.networkModel.set( this.model.get( 'name' ), this.$( 'input[type="checkbox"]' ).prop( 'checked' ) );
		}

	});

});
