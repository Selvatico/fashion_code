define( [
	'app',
	'text!./templates/layout.tpl',
	'layoutmanager'
], function ( app, subMenu ) {

	'use strict';

	return Backbone.Layout.extend( {

		className: 'width widget-ProfileSubMenu',

		template: subMenu,

		events: {
			//'click .step-btn' : 'navigateSteps'
		},
		initialize: function () {
			
		},
		beforeRender: function () {

		},
		serialize : function () {
			return app.models.userModel.toJSON();
		}
	} );

} );
