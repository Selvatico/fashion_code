<div class="w-nav profile-menu right">
	<!-- <a href="#" class="settings-btn step-switcher settings">
	<i class="moderaIcons">@</i> Settings
	</a> <a href="/home#inviteFriends"> <i class="moderaIcons">+</i> Find Friends</a>
	<a href="/logout"> <i class="moderaIcons">[</i> Log Out</a> -->
</div>
<div class="w-nav profile-menu left">
	<div class="message-block disnone">
		<div class="success fs14 thin disnone">
			<i class="moderaIcons">/</i> <span>Photo successfully uploaded</span>
		</div>
		<div class="error fs14 thin disnone">
			<i class="m">×</i> <span>Photo unsuccessfully uploaded</span>
		</div>
	</div>

	<div class="buttonsBlock">
		<a class="photos step-switcher" href="#photos"> <i class="moderaIcons">U</i> Grid view</a>
		<a class="browse_legacy step-switcher" href="#browse_legacy"> <i class="moderaIcons">5</i> List view</a>
		<a class="compares step-switcher" href="#compares"> <i class="moderaIcons">\</i> # ShowDowns</a>
	</div>
</div>