define([
	'app',
	'backbone',
	'text!./templates/layout.tpl',
	'./models/UserSigninModel',
	'../forgotpassword/main'
], function (
	app,
	Backbone,
	layoutTemplate,
	UserSigninModel,
	ForgotPasswordWidget
) {
	'use strict';
	return Backbone.Layout.extend({

		events: {
			'click .forgot-password-link' : 'forgotPassword',
            'blur input': 'blurInput',
            'click input[type="submit"]': 'submit',
			'click label *': 'closeError',
			'focus label input': 'closeError'
		},
		placeholders: {
          'signin-login': 'Username or Email',
          'signin-password': 'Password'
		},
		template: layoutTemplate,
		
		model: new UserSigninModel(),
		
		initialize: function() {
		    // Backbone.Validation.bind(this);
        },

		forgotPassword: function() {
			app.trigger( 'openModal', new ForgotPasswordWidget( { page: this } ) );
		},
        blurInput: function( e ) {
            var $el = $( e.currentTarget );
            this.model.set( $el.attr('name'), $el.val() );
        },
		submit: function(e){
			e.preventDefault();
			var data = this.$('form').serializeObject(), 
                self = this, error;
			
			if(this.model.set(data) && this.model.isValid(true)){
                this.model.unset('user');
			    var data = _.clone(this.model.attributes), self = this;
                                
				this.model.save({ 'user': data }, {
				    success: function( model, data ) {
				        app.models.userModel.set('userSignIn', true);
                        app.trigger('userSignIn');
						if ( 'redirect_uri' in data ) {
						    location.href = data.redirect_uri;
						}                        
				    },
                    error: function( model, xhr ) {                        
                        var error = JSON.parse( xhr.responseText ); 
                            self.$('.main-error-block').text( error.error ).css( 'display', 'block' );
                    }
				});
			}
			else {
                this.$('[placeholder]').attr('placeholder', ''); 
				this.$('.alert-error').fadeIn();
			}
		},
        closeError: function( e ){
            var $el = $(e.currentTarget).parent('label'), $input = $el.find('input');
			$el.removeClass('error');
            $input.attr('placeholder', this.placeholders[ $input.attr('id') ]);             
		},
        afterRender: function() {
            this.$('#signin-login, #signin-password').placeholder();            
        }
	});
});