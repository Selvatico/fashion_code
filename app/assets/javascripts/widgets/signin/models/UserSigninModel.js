define([
	'backbone',
], function( Backbone ) {
	return Backbone.Model.extend({
		validation: {
			password: {
				required: true,
				msg: 'Password can\'t be blank'
			},
			login: [{
				required: true,
				msg: 'Email can\'t be blank'
			}/*,{
				pattern: 'email',
				msg: 'Email is invalid'
			}*/]
		},
		urlRoot: '/login'
	});
});