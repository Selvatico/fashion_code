<h2>Log in</h2>
<form action="/login" class="loginModera-form">
	<div class="right">
		<label class="disblock rel">
			<span class="close-error abs">×</span>
			<span class="message-error abs alert-error"></span>
			<input id="signin-login" placeholder="Username or Email" type="email" name="login"/>
		</label>
		<label class="disblock rel">
			<span class="close-error abs">×</span>
			<span class="message-error abs alert-error"></span>
			<input id="signin-password" placeholder="Password" type="password" name="password"/>
		</label>
		<span class="message-error main-error-block">Error message</span>
		<a class="right textUnderline forgot-password-link" href="#">Forgot Password?</a>
	</div>
	<div class="left">
		<a href="/auth/facebook" class="loginFacebook c disblock">
			<i class="moderaIcons left">f</i>
			<span>Login with Facebook</span>
		</a>
		<a href="/auth/twitter" class="loginTwitter c disblock">
			<i class="moderaIcons left">t</i>
			<span>Login with Twitter</span>
		</a>
		<a href="/auth/instagram" class="loginInstagram c disblock">
			<i class="moderaIcons left">i</i>
			<span>Login with Instagram</span>
		</a>
	</div>
	<div class="divider clear"></div>
	<input class="btn active right" type="submit" value="Login">
</form>