define([
	'text!./templates/layout.tpl',
	'./views/LoadAvatarFormView',
	'./views/NewUserFormView',
	'./models/UserSignupModel'
	], function (
	layoutTemplate,
	LoadAvatarFormView,
	NewUserFormView,
	UserSignupModel
	) {
	'use strict';

	return Backbone.Layout.extend({

		model: new UserSignupModel(),

		initialize: function() {
			this.model.on('change', this.render, this);
			this.newUserFormView = new NewUserFormView( { model: this.model, parentView: this } );
			this.loadAvatarFormView = new LoadAvatarFormView( { model: this.model, parentView: this } );
		},

		events: {
			'click #create-account': 'submit'
		},
		template:  layoutTemplate,
		beforeRender: function() {
			this.insertView( '#form-wrapper', this.newUserFormView );
			this.insertView( '#form-wrapper', this.loadAvatarFormView );
		},
		submit: function( e ) {
			e.preventDefault();
			this.trigger('submit');
		}
	});
});
