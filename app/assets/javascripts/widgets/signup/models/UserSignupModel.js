define([
	'backbone',
	], function( Backbone ) {
	return Backbone.Model.extend({
		defaults: {
			username: '',
			email: '',
			avatar_cache: '',
			password: ''
		},
		validation: {
			username: {
				required: true,
				msg: 'Username can\'t be blank'
			},
			password: {
				required: true,
				msg: 'Password can\'t be blank'
			},
			email: [{
				required: true,
				msg: 'Email can\'t be blank'
			},{
				pattern: 'email',
				msg: 'Email is invalid'
			}]
		},
		urlRoot: '/signup'
	});
});