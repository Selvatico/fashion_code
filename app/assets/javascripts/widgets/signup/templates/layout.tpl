<h2>Sign up with your email</h2>
<div id="form-wrapper">

</div>   
	<div class="divider c clear">
		<i class="moderaIcons rel">C</i>
	</div>
	<p class="fs14 c">
		or can always sign up using
		<a class="facebookColor" href="/auth/facebook">Facebook</a>
		or
		<a class="twitterColor" href="/auth/twitter">Twitter</a>
		account.
	</p>
	<p class="fs14 c">
		By creating an account, you agree to the
		<a class="textUnderline" href="/tos">Terms of service</a>
		and
		<a class="textUnderline" href="/privacy">Privacy Policy.</a>
	</p>
	<div class="c">
    <input type="submit" name="" value="Create Account" class="btn active" id="create-account" />
	</div>