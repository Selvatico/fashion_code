	<div class="load-avatar rel">
        <span class="close-error abs">×</span>
        <img src="" class="disnone" id="avatarPic" />
        <i class="moderaIcons">E</i>
    	<input type="hidden" value="<%= avatar_cache %>" name="avatar_cache"/>
        <div class="spinner disnone">
            <img src="<%=url%>assets/loading.gif" class="loading" />
        </div>
    </div>		
	    <div id="file-uploader">		
		<noscript>			
			<p>Please enable JavaScript to use file uploader.</p>    			
		</noscript>
        <div class="btn" id="avatar-image">Upload your Photo</div>         
	</div>