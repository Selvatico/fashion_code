	<fieldset class="right">
		<label class="disblock rel">
			<span class="close-error abs">×</span>
			<span class="message-error abs alert-error"></span>
			<input placeholder="Username" type="text" value="<%= username %>" name="username" id="signup-username"/>
		</label>
		<label class="disblock rel">
			<span class="close-error abs">×</span>
			<span class="message-error abs alert-error"></span>
			<input placeholder="Email" type="text" value="<%= email %>" name="email" id="signup-email"/>
		</label>
		<label class="disblock rel">
			<span class="close-error abs">×</span>
			<span class="message-error abs alert-error"></span>
			<input placeholder="Password" value="<%= password %>" type="password" name="password" id="signup-password"/>
		</label>
	</fieldset>