define([
	'backbone',
	'text!../templates/loadAvatarForm.tpl',
	'app',
	'uploader'
], function ( Backbone, loadAvatarFormTemplate, app ) {
	return Backbone.View.extend( {
		tagName: 'form',
		attributes: { 'accept-charset': 'UTF-8', 'action': '/upload_avatar', 'method': 'post', 'enctype': 'multipart/form-data' },
		id: 'upload_avatar',
		className: 'left c signupEmail-form',
		template: loadAvatarFormTemplate,
		avatar: 'http://dev.modera.streamcrease.com/assets/samples/default-avatar.png',
		avatar_cache: '',

		fileTypes: [ "jpeg", "png", "jpg", "gif" ],

		events: {
			'click .load-avatar img': 'clickOnAvatar',
			'click #avatar-image': 'clickOnAvatar',
			'click .close-error': 'closeError'
		},
		initialize: function () {
			_.bindAll( this, 'avatarImage' );
			this.parentView = this.options.parentView;
			this.parentView.on( 'afterRender', this.setAjaxUpload, this );
		},

		serialize: function () {
			return _.extend( {}, this.model.toJSON(), { avatarImage: this.avatar, avatar_cache: this.avatar_cache, 'url' : app.getURL() } );
		},

		clickOnAvatar: function (  ) {
			this.$( '#file-uploader input' ).trigger( 'click' );
		},
		setAjaxUpload: function () {
			var that = this;
			new qq.FineUploaderBasic( {

				button: document.getElementById( 'file-uploader' ),

				request: {
					endpoint: '/upload_avatar',
					params: {
						'X-CSRF-Token': $( 'meta[name="csrf-token"]' ).attr( 'content' )
					}
				},

				callbacks: {
					onUpload: function ( id, fileName ) {
					   that.$('.spinner').removeClass('disnone');
					},
					onComplete: function ( id, fileName, data ) {
						that.$('.spinner').addClass('disnone');
						that.avatar = data.avatar;
						that.avatar_cache = data.avatar_cache;
						that.$( '#avatarPic' ).attr( 'src', data.avatar ).removeClass( 'disnone' );
						that.$( '.load-avatar .moderaIcons' ).remove();
					},
                    onSubmit: function( id, name ) {
                        var type = name.split('.'),
                            type = type[ type.length - 1 ];
                            
                        if ( that.fileTypes.indexOf( type ) == -1 ) {
                            that.$( '.load-avatar' ).addClass( 'error' );
                            that.$( '.moderaIcons' ).addClass( 'disnone' );
                            return false;                             
                        }                            
                    }                                        
				}
			} );
		},

		closeError: function( e ) {
			$( e.currentTarget ).closest('.load-avatar').removeClass('error');
            this.$( '.moderaIcons' ).removeClass( 'disnone' );
		},
		/**
		 * Render this method with template context. See
		 */  
		avatarImage: function() {
			return ( this.model.get('avatar_cache') == '' ) ? 'http://dev.modera.streamcrease.com/assets/samples/default-avatar.png' : this.model.get('avatar_cache');
		}
	});
});