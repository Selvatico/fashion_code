define([
	'backbone',
	'text!../templates/newUserForm.tpl',
	'app'
	], function (
	Backbone,
	newUserFormTemplate,
	app
	) {
	return Backbone.View.extend({
		tagName: 'form',
		attributes: { action: '/signup', 'accept-charset': 'UTF-8', method: 'post' },

		className: 'signupEmail-form',
		events: {
			'click label *': 'closeError',
			'focus label input': 'closeError',
			'blur input': 'blurInput'
		},
		id: 'new_user',
		placeholders: {
			'signup-username': 'Username',
			'signup-email': 'Email',
			'signup-password': 'Password'
		},
		template: newUserFormTemplate,

		initialize: function() {
			Backbone.Validation.bind(this);

			this.parentView = this.options.parentView;
			this.parentView.on('submit', this.submit, this);

		},
		closeError: function( e ){
			var $el = $(e.currentTarget).parent('label'), $input = $el.find('input');
			$el.removeClass('error');
			$input.attr('placeholder', this.placeholders[ $input.attr('id') ]);
		},
		submit: function( e ) {
			var data = this.$el.serializeObject(), self = this;
			if(this.model.set(data) && this.model.isValid(true)){
				// Bad way to save data.
				this.model.unset('user');
				var data = _.clone(this.model.attributes);
				this.model.save({ 'user': data }, {
					success: function( model, response) {
						try {
							if ( 'redirect_uri' in response ) {
								location.href = response.redirect_uri;
								return;
							}
						} catch ( e ) {
							console.log( e );
						}
					},
					error: function( model, xhr ) {
						try {
							var response = JSON.parse( xhr.responseText ), i, $parent;
                            if ( 'redirect_uri' in response ) {
                                location.href = response.redirect_uri;
                                return;
                            }
							for ( i in response ) {
								$parent = self.$('[name="' + i +'"]').closest('label').addClass('error');
								$parent.find('.message-error').text( response[i][0] ).show();
							}
						} catch ( e ) {
							console.log( e );
						}
					}
				});
			}
			else {
				this.$('[placeholder]').attr('placeholder', '');
				this.$('.alert-error').fadeIn();
			}
		},
		serialize: function() {
			return this.model.toJSON();
		},
		blurInput: function( e ) {
			var $el = $( e.currentTarget );
			this.model.set( $el.attr('name'), $el.val(), { silent: true });
		},
        afterRender: function() {
            this.$( '#signup-username, #signup-email, #signup-password' ).placeholder();            
        }
	});
});