define([
	'app',
	'text!./templates/layout.tpl',
	'nanoscroller'
], function (
	app,
	subMenu
	) {

	'use strict';

	return Backbone.Layout.extend({

		className : 'width widget-SubSocialMenu',

		template : subMenu,

		events : { },

		initialize: function() {

		}
	});

} );
