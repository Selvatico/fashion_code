<div class="share-panel">
            <span class="left">
              <b class="disinbl">
                  Share your Power Hour
                  <br>
                  on social Platforms
              </b>
            </span>
            <!-- /facebook -->
                    <span class="share-icons facebook">
                      <a href="https://www.facebook.com/sharer/sharer.php?u=<%= url %><%= image %>" class="moderaIcons">f</a>
                    </span>
            <!-- /pinterest -->
                    <span class="share-icons pinterest">
                      <a href="http://pinterest.com/pin/create/button/?url=<%= url %><%= image %>&media=<%= url %><%= image %>" class="moderaIcons">p</a>
                    </span>
            <!-- /twitter -->
                    <span class="share-icons twitter">
                      <a class="moderaIcons" href="https://twitter.com/share?url=<%= url %><%= image %>">t</a>
                    </span>
            <!-- /gplus -->
                    <span  class="moderaIcons" class="share-icons gplus">
                      <a href="https://plus.google.com/share?url=<%= url %><%= image %>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">g</a>
                    </span>
            <!-- /tumblr -->
                    <span class="share-icons tumblr">
                      <a class="moderaIcons" href="http://www.tumblr.com/share/link?url=<%= url %><%= image %>&name=&description=" title="Share on Tumblr">u</a>
                    </span>
</div>
