define([
	'app',
	'text!./templates/layout.tpl',
	'./views/SearchListItemUserView',
	'./views/SearchListItemTagView',
	'nanoscroller'    
	], function (
	app,
	subMenu,
	SearchListItemUserView,
	SearchListItemTagView
	) {

	'use strict';

	return Backbone.Layout.extend({

		className: 'width widget-SubMenu',

		template: subMenu,

		collection: new Backbone.Collection(),
		model: new Backbone.Model({ page: 1 }),

		events : {
			'click .w-nav a:first-child' : 'contest',
			'click .w-nav a:last-child' : 'activeContest',
			'click .search-visible > .moderaIcons' : 'search',
			'click .get-prize': 'getPrize',
			'keyup #search': 'processSearch',
			'click .search-tabs span': 'changeType',
			'click .see-all': 'seeAllElements',
			'scrollNextPage .nano': 'scrollNextPage'
		},
		initialize: function() {
			
			_.bindAll( this, 'processSearchResponse', 'processNextPage' );

			this.collection.on('reset add', this.renderSearchBlock, this);
			this._configModel = app.config;

			app.on( 'startedContests:loaded', this.setContestCount, this );
			
			app.collections.contests.started.fetch();
			app.collections.contests.finished.fetch();

            app.on( 'close:dropdown', this.closeDropDown, this );
		},


		setContestCount : function (data) {
			var that = this;
			var count = data.contests.length;
			app.collections.contests.new.fetch({
				success : function (data) {
					count += data.contests.length;
					$('body').attr('data-newcontests',count);
					if(!count) that.$('.w-nav .abs').addClass('disnone');
					else that.$('.w-nav .abs').html(count).removeClass('disnone');
				}
			});
		},

		contest : function ( eventObject ) {
			//console.log ( "contest" );
			//return false;
			eventObject.preventDefault();
			$( eventObject.currentTarget ).addClass( 'active' );
			Backbone.history.navigate( '', { trigger: true } );
		},

		activeContest : function () {
			//console.log ( "active contest", app );
			return false;
		},

		search : function () {
			//console.log ( "value: ", this.$( 'input[type="text"]' ).val () );
		},
		getPrize: function( e ) {
			e.preventDefault();
			$( e.currentTarget ).addClass( 'active' );
			Backbone.history.navigate( 'get_prizes', { trigger: true } );
		},
		processSearch: function( e ) {
			var $el = $( e.currentTarget ), val = $el.val();
			if ( val.length >= 3 ) {
				this.page = 1;

				if      ( val.charAt( 0 ) == '@' )	this.tabActivate( val.substr(1), 'users', 1 );
				else if ( val.charAt( 0 ) == '#' )	this.tabActivate( val.substr(1), 'tags', 1 );                
				else								this.tabActivateDefault( val, [ 'users', 'tags' ] );

			} else {
				this.$('.search-dd-content').addClass('disnone');
			}
		},
		tabActivate: function( val, type, page, callback ) {
			var self = this;
			this.model.set({ type: type, val: val });

			this.$('.search-tabs span').removeClass('active');
			this.$('.' + type + '-tab').addClass('active');

			callback = callback || this.processSearchResponse;
			app.sendRequest( 
					'/search/' + type, { 
						method: 'GET',
						data: { 
							q: val,
							page: page
						},
						callback: callback,
                        single: true 
					});

		},
        tabActivateDefault: function( val, data ) {
            var returnData = [], self = this,
                successCallback = function( returnData ) {
                    var result, i, type;
                    for ( i in returnData ) {
                        if ( ( 'users' in returnData[i] ) && ( returnData[i].users.length > 0 ) ) {
                            result = returnData[i], type = 'users';                            
                        } else if ( ( 'tags' in returnData[ i ] ) && ( returnData[ i ].tags.length > 0 ) ) {
                            result = returnData[i], type = 'tags';
                        }else if ( (( 'users' in returnData[i] ) && ( returnData[ i ].users.length == 0 )) || (( 'tags' in returnData[i] ) && ( returnData[ i ].tags.length == 0 ) ) ) {
                    		self.notFound();
                        }
                        if( typeof type != 'undefined' ) {
                 			self.model.set({ type: type, val: val });

                			self.$('.search-tabs span').removeClass('active');
                			self.$('.' + type + '-tab').addClass('active');
                            
                            self.processSearchResponse( result ); 
                        }
                    }
                },
                callback = function( searchData ) {
                    returnData.push( searchData );
                    if ( returnData.length == data.length ) {
                        successCallback( returnData );
                    }        
                };
                for ( var i in data ) {
                    app.sendRequest(
                        '/search/' + data[ i ], {
                            method: 'GET',
                            data: {
                                q: val,
                                page: 1
                            },
                            callback: callback,
                            single:   true    
                        }                        
                    );
                }
        },
		renderSearchBlock: function() {
			var view;
			this.$('#search-list').empty();
			this.collection.each( function( model ) {
				if ( this.model.get('type') == 'users' ) {
					view = this.insertView( '#search-list', new SearchListItemUserView( { model: model } ) );
				} else {
					view = this.insertView( '#search-list', new SearchListItemTagView( { model: model } ) );
				}
				view.render();
			}, this);
		},
		processSearchResponse: function( data ) {
			var type;
			if ( 'users' in data ) {
				type = 'users';
				this.$( '#search-list' ).attr('class', 'search-list-user');
			} else if ( 'tags' in data ) {
				type = 'tags';
				this.$( '#search-list' ).attr('class', 'search-list-tag');
			}
			if ( data[ type ].length > 0 ) {
				this.$('.search-dd-content, .see-all').removeClass('disnone');
				this.$('.no_tags, .no_users').addClass('disnone');
				this.$('#search-result-wrapper').height( this.$('#search-list').height() );
			} else if ( data[ type ].length == 0 ) {
        		this.notFound();
            }
			
            if ( data[ type ].length < this._configModel.get( 'countItemsForSearch' ) ) {
                this.model.set( 'notNextPage', true );
                this.$('.see-all').addClass('disnone');
            }

			this.model.set('data', data[ type ]);

			data = data[ type ].slice(0, this._configModel.get( 'countItemsForSearch' ) );

			this.collection.reset( data  );
			if( this.$( '#search-list' ).height() )
				this.$( '#search-result-wrapper' ).height( this.$( '#search-list' ).height() );
		},
		changeType: function( e ) {
			this.$('.no_tags, .no_users').addClass('disnone');
			var $el = $( e.currentTarget ),
			val = this.$('#search').val();

			// destroy scroller
			this.$('.has-scrollbar').removeClass('nano');
            this.$('.nano').nanoScroller( { destroy: true } );
			this.$('.search-tabs span').removeClass( 'active' );
			$el.addClass('active');

			if ( val.charAt( 0 ) == '@' || val.charAt( 0 ) == '#' ) {
				val = val.substr(1);
			}
			this.model.set({ page: 1, notNextPage: false });
			this.tabActivate( val, $el.attr('id'), 1 );
		},
		seeAllElements: function() {
			var searchResultHeight;

			this.collection.reset( this.model.get('data') );

			searchResultHeight = this._configModel.get('heighthSearchItem')[ this.model.get('type') ] * this._configModel.get('countItemsForSearchSeeAll');
			this.$( '#search-result-wrapper' ).height( searchResultHeight );

			this.$('.has-scrollbar').addClass('nano'); 
			this.$('.nano').nanoScroller({ scroll: 'top' });

		},
		scrollNextPage: function( e ) {
			if ( this.model.get( 'notNextPage' ) ) return;

			this.model.set( { page: this.model.get('page') + 1 }, { silent: true } );

			this.tabActivate( this.model.get('val'), this.model.get('type'), this.model.get('page'), this.processNextPage );
		},
		processNextPage: function( data ) {
			if ( data[ this.model.get('type') ].length == 0 ) this.model.set('notNextPage', true, { silent: true });

			this.collection.add( data[ this.model.get('type') ] );			
		},
		
		notFound : function () {			
			var type = this.model.get('type') || null;
			
			this.$('.search-dd-content').removeClass('disnone');

			this.$( '#search-list, .see-all' ).addClass('disnone');
			
			
			this.$('.no_' + type).removeClass('disnone');
									
			var height = parseInt( this.$('.no_' + type).height() );
			
			if(this.$( '.no_' + type ).css( 'padding-top' )){
				var paddingTop = parseInt(this.$( '.no_' + type ).css( 'padding-top' ).replace( 'px', '' ));
				var paddingBottom = parseInt(this.$( '.no_' + type ).css( 'padding-bottom' ).replace( 'px', '' ));
				
				this.$('#search-result-wrapper').css( { 'height' : (height + paddingTop + paddingBottom) + 'px' } );
			}

		},
        closeDropDown: function( event ) {
            var $el = $( event.target );
            if ( $el.closest( '.search-dd-content' ).length == 0 ) {
                this.$( '.search-dd-content' ).addClass( 'disnone' )
            }
        }
	});

} );