<div class='w-nav left'>
<div class="message-block disnone">
		<div class="success fs14 thin disnone">
			<i class="moderaIcons">/</i> <span>Photo successfully uploaded</span>
		</div>
		<div class="error fs14 thin disnone">
			<i class="m">×</i> <span>Photo unsuccessfully uploaded</span>
		</div>
	</div>
	<div class="buttonsBlock">
	<a class="active home-link" href='/home'> <i class='moderaIcons'>D</i>
		Home Feed
	</a> <a class='rel get-prize prize-link' href='#'> <i
		class='moderaIcons'>R</i> <span class='abs disnone'></span> Contest
	</a>
	</div>
</div>
<div class='search-block right rel'>
	<div class='search-visible'>
		<i class='moderaIcons m'>L</i> <input id="search" name="search"
			placeholder="Enter search term..." type="text" />
	</div>
	<div class='search-dd-content abs disnone'>
		<div class='search-tabs'>
			<span class='asLink disinbl users-tab' id="users"> <i
				class='moderaIcons m'>S</i> Users
			</span> <span class='asLink disinbl tags-tab' id="tags"> <i
				class='moderaIcons m'>#</i> Tags
			</span>
		</div>
		<div id="search-result-wrapper">
			<div class="nano has-scrollbar">
				<div class="content">
					<ul class='search-list' id="search-list"></ul>
					<div class="faceplate c disnone no_users">
						<i class="moderaIcons">S</i> <span class="thin disblock">No
							users</span>
					</div>
					<div class="faceplate c no_tags">
						<i class="moderaIcons">#</i> <span class="thin disblock">No
							#tags</span>
					</div>
				</div>
			</div>
		</div>
		<div class='see-all curpoint c'>See all</div>
	</div>
</div>