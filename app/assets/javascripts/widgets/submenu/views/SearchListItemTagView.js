define([
	'backbone',
	'text!../templates/searchItemTagTemplate.tpl',
	'app'
	], function (
	Backbone,
	searchListItemTemplate,
	app
	) {
	return Backbone.View.extend({

		template: searchListItemTemplate,

		tagName: 'li',
		initialize: function() {
			_.bindAll(this, 'userCount');
		},
		serialize: function() {
			return _.extend( this.model.toJSON(), { userCount: this.userCount } );
		},
		userCount: function() {
			return ( this.model.get('users_count') > 1000 ) ? ( ( this.model.get('users_count') / 1000 ) + 'k' ).replace( '.', ',' ) : this.model.get('users_count'); 
		}
	});
});