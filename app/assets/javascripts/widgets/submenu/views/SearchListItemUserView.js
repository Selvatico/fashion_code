define([
	'backbone',
	'text!../templates/searchItemUserTemplate.tpl',
	'app'
	], function (
	Backbone,
	searchListItemTemplate,
	app
	) {
	return Backbone.View.extend({

		template: searchListItemTemplate,
		events: {
			'click .follow': 'followAction'  
		},
		tagName: 'li',
		initialize: function() {
			_.bindAll( this, 'followText', 'checkActive' );  
		},
		serialize: function() {
			return _.extend( this.model.toJSON(), { 
				followText: this.followText,
				checkActive: this.checkActive 
			});
		},
		followText: function() {
			return ( this.model.get('is_following') ) ? 'Unfollow': 'Follow';
		},
		checkActive: function() {
			return ( this.model.get('is_following') ) ? 'active' : '';  
		},
		followAction: function( e ) {
			var url;

			if ( this.model.get( 'is_following' ) ) {
				url = '/profiles/' + this.model.get( 'username' ) + '/unfollow';
			} else {
				url = '/profiles/' + this.model.get( 'username' ) + '/follow';
			}

			this.model.set('is_following', !this.model.get('is_following') );

			app.sendRequest( url, { method: 'POST' } );
			this.render();
		}
	});
});