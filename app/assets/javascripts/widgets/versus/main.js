define( [
	'backbone',
	'app',
	'./views/BattleView'
], function ( Backbone, app, BattleView ) {

	'use strict';

	return Backbone.Layout.extend( {

		collection: new Backbone.Collection(),

		className: 'battle-wrapper',
        versusCount: 0,
		initialize: function () {
		    var _self = this;            
			_.bindAll( this, 'responseHandler' );
            
			this.collection.on( 'add', this.renderBattle, this );
			this.loadVersuses();
            
			window.notifications.on( 'photo:shared', function ( message, success ) {
				app.trigger( 'show:message', { text: message, type: success ? 'success' : 'error' } );
			});
            
            app.on( 'versusScroll:nextPage', this.loadVersuses, this );
		},
		responseHandler: function ( data ) {
			if ( 'versuses' in data ) {
				app.trigger('versus:list:loaded');
				this.versusCount += data['versuses'].length;
				this.collection.add( data['versuses'] );
			}
		},
		loadVersuses: function () {
			app.sendRequest( '/versuses', {
					method: 'GET',
					callback: this.responseHandler,
					single: true,
					data: { offset: this.versusCount }
				} );
		},
		/**
		 * Insert one battle to the field
		 * @param {Backbone.Model} model
		 */
		renderBattle: function ( model ) {		    
			this.insertView( new BattleView( { model: model, parent: this } ) ).render();
		}      
	})
} );