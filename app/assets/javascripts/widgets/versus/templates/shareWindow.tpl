<h2>Share with</h2>
<div class="share-panel c">
    <!-- /facebook -->
        <span class="share-icons facebook l">
          <i class="moderaIcons">f</i>
        </span>
    <!-- /pinterest -->
        <span class="share-icons pinterest l">
          <i class="moderaIcons">p</i>
        </span>
    <!-- /twitter -->
        <span class="share-icons twitter l">
          <i class="moderaIcons">t</i>
        </span>
    <!-- /gplus -->
        <!-- <span class="share-icons gplus l">
          <i class="moderaIcons">g</i>
        </span> -->
    <!-- /tumblr -->
        <span class="share-icons tumblr l">
          <i class="moderaIcons">u</i>
        </span>
    <!-- /flickr -->
        <!-- <span class="share-icons flickr l">
          <i class="moderaIcons">
              <small>0</small>
              <small>0</small>
          </i>
        </span> -->
    <!-- /linked -->
        <!-- <span class="share-icons linked l">
          <i class="moderaIcons">l</i>
        </span> -->
    <!-- /mail -->
        <!-- <span class="share-icons mail l">
          <i class="moderaIcons">m</i>
        </span> -->
</div>
