<div class="info-top disnone"></div>
<div class='info-side rel <%= className %>'>
	<div class='like-count h1 curpoint <%= rightClass %>'>
		<i class='moderaIcons'>C</i>
		<%= likeCount %>
	</div>
	<div class='comment-count h1 curpoint <%= rightClass%>'>
		<i class='moderaIcons m'>H</i>
		<%= commentCount %>            
	</div>
	<div class='timeline <%= rightClass%>'>
		<i class='moderaIcons'>G</i>
		<%= created_at %>
	</div>
	<div class='asLink share'>Share</div>
	<div class='share-panel abs <%= infoBottomSideClass %> disnone'>
		<!-- /facebook -->
		<span class='share-icons facebook' data-provider="facebook">
			<i class="moderaIcons">f</i>
		</span>
		<!-- /pinterest -->
		<span class='share-icons pinterest' data-provider="pinterest">
			<i class="moderaIcons">p</i>
		</span>
		<!-- /twitter -->
		<span class='share-icons twitter' data-provider="twitter">
			<i class="moderaIcons" >t</i>
		</span>
		<!-- /gplus -->
		<!-- <span class='share-icons gplus' data-provider="gplus">
			<i class="moderaIcons">g</i>
		</span> -->
		<!-- /tumblr -->
		<span class='share-icons tumblr' data-provider="tumblr">
			<i class="moderaIcons" >u</i>
		</span>
		<!-- /flickr -->
		<!-- <span class='share-icons flickr' data-provider="flickr">
			<i class="moderaIcons">
				<small>0</small>
				<small>0</small>
			</i>
		</span> -->
		<!-- /mail -->
		<!-- <span class='share-icons mail' data-provider="mail">
			<i class="moderaIcons">m</i>
		</span> -->
	</div>
</div>
<div class='photoframe <%= photoFrameClass %> <%= grayScale %>  '>
	<input src="<%= url %>/assets/like.png" type="image" class="like <%= liked %>" />
	<img alt="" src="<%= url %><%= image %>" />
</div>
<div class='info-bottom  <%= infoBottomSideClass %>' style="display: none">
	<div class='right r'>
		<span class='asLink hover follow'><%= followText %></span>
		<span class='asLink report'>Report</span>
		<b class="asLink voteLink textTransUpper <%= likedClasses %>"><i class="moderaIcons m">C</i>vote</b>
	</div>
	<% if (comment) { %>
	<div class='last-comment'>
		<a class='left' href='/profiles/<%= comment.user.username %>' title='<%= comment.user.username %>'>
			<img alt="<%= comment.user.username %>" class="userpic" height="42" src="<%= url %><%= comment.user.avatar %>" width="42" />
		</a>
		<div class='overhide'>
			<a class='disblock textOverhide' href='/profiles/<%= comment.user.username %>' title='Mary Poppins'>
			<b><%= comment.user.username %></b>
			</a>
			<span class='text-comment disblock textOverhide'><%= comment.comment %></span>
		</div>
	</div>
	<% } %>
</div>
