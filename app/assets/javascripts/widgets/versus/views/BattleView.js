define( [
	'backbone',
	'app',
	'./VersusItemView',
	'./VsView',
	'./VoterTopView',
	'./ShareWindow',
	'../../voters/main',
	'../../comments/main'
], function ( Backbone, app, VersusItemView, VsView, VoterTopView, ShareWindow, Voters, Comments ) {
	return Backbone.View.extend( {
		className: 'battle disnone',
		indexValue: 0,
		events: {
		  'click .shareWindow': 'openShareWindow'
		},

		initialize: function () {
			this.rightVersus = new VersusItemView( { parent: this, model: new Backbone.Model( _.extend( this.model.get( 'opponent_photo' ), { className: 'right' } ) ), className: 'participant right' } );
			this.leftVersus = new VersusItemView( { parent: this, model: new Backbone.Model( _.extend( this.model.get( 'photo' ), { className: 'left' } ) ), className: 'participant left' } );

			this.on( 'voteVersus', this.voteVersus, this );
		},
		beforeRender: function () {
			this.insertViews( [ this.rightVersus, this.leftVersus ] );
			var $battles = this.options.parent.$el.find('.battle' );
			this.indexValue = $battles.length;
		},
		afterRender: function () {

			var marginTop = (this.options.marginTop !== undefined) ? this.options.marginTop : ((parseInt( this.$el.css( 'margin-top' ) ))) + 10,
				marginLeft = (this.options.marginLeft !== undefined) ? this.options.marginLeft : ((parseInt( this.$el.css( 'margin-left' ) ))),
				marginTopOffset  = (marginTop == -223) ? 480 : 690,
				margintTopBefore = (this.$el.width() == 1002) ? -660 : -854,
				index = this.indexValue;

			if (index != 0 && this.options.marginTop == undefined) {
				var $lastEleme = this.options.parent.$el.find( '.battle' ).eq( this.indexValue - 1 );
				var dataMargin = $lastEleme.data( 'target-margin' );
				var baseTop = ((!!dataMargin) ? dataMargin : parseInt( $lastEleme.css( 'margin-top' ), 10 ))
				marginTop = baseTop + marginTopOffset;
			} else {
				marginTop += marginTopOffset * index;
			}

			var baseParams = {
				'base-margin-top' : marginTop,
				'base-width'      : this.$el.width(),
				'base-margin-left': marginLeft,
				'margin-offset'   : marginTopOffset,
				'width'           : (this.$el.width() == 1002) ? 1932 : 2122,
				'margin-left'     : (marginLeft == -501) ? -966 : -1061,
				'left'            : (this.$el.width() == 1002) ? 0 : 50,
				'margit-top-hide' : margintTopBefore
			};

			baseParams.width = (this.options.skipWidth) ? this.$el.width() : baseParams.width;

			this.$el
				.data( baseParams )
				.css( {
					'margin-top': (this.options.marginTop !== undefined) ? this.options.marginTop : baseParams['base-margin-top'],
					'width': ((index == 0 ) ? this.$el.width() : baseParams['width']) + 'px',
					'margin-left': ((index == 0 ||  this.options.marginLeft !== undefined) ? baseParams['base-margin-left'] : baseParams['margin-left'] + 'px')
				} )
				.addClass( ((index == 0) ? 'activeBattle' : '') );

			if ( index == 0 || this.options.skipWidth) {
				this.$el.find( '.info-bottom' ).fadeIn( 'slow' );
			}

			this.$el.removeClass( 'disnone' );
		},
		/**
		 * Load voters list view and replace oposite photo with this view
		 * @param {Number} id Id of photo
		 * @param {String} className Allign of the voter list (right or left)
		 */
		showVoters: function ( id, className ) {

			var targetHolder = (className == 'left') ? 'right' : 'left';
			this.checkAnotherHolder( 'comment-block', targetHolder );

			if ( this.$( '.voters-block.' + targetHolder ).length > 0 ) {
				this.closeVoters(targetHolder);
				return;
			}
			this.$( '.like' ).addClass('disnone');

			var voterView = new Voters( { photo_id: id, className: 'voters-block ' + targetHolder, holderClass: targetHolder, parent: this } );
			this.insertView( voterView ).render();
			this.$( '.participant.' + targetHolder ).hide();
		},
		checkAnotherHolder: function ( block, className ) {
			if ( this.$( '.' + block + '.' + className ).length > 0 ) {
				this.getView( { className: block + ' ' + className} ).remove();
			}
		},
		/**
		 * Remove voters list view via manager and show back oposite photo holder
		 * @param {String} className Float of the current panel (left or right)
		 */
		closeVoters: function ( className ) {
			this[((className == 'left') ? 'right' : 'left') + 'Versus'].$el.find( '.like-count' ).removeClass( 'active' );
			this.getView( { className: 'voters-block ' + className} ).remove();
			this.$( '.like' ).removeClass('disnone');
			this.$( '.participant.' + className ).show();

		},
		/**
		 * Remove comments panel
		 * @param className
		 */
		closeComments: function ( className ) {
			this[((className == 'left') ? 'right' : 'left') + 'Versus'].$el.find( '.comment-count' ).removeClass( 'active' );
			this.getView( { className: 'comment-block ' + className} ).remove();
			this.$( '.participant.' + className ).show();
			this.$( '.like' ).removeClass('disnone');
		},
		/**
		 * Load comments data and replace oposite foto holder with comments list
		 * @param {Number} id Id of photo
		 * @param {String} className Allign of the comments list (right or left)
		 */
		showComments: function ( id, className ) {
			var targetHolder = (className == 'left') ? 'right' : 'left';

			this.checkAnotherHolder( 'voters-block', targetHolder );

			if ( this.$( '.comment-block.' + targetHolder ).length > 0 ) {
				this.closeComments( targetHolder );
				return;
			}

			var commentView = new Comments(
				{ photo_id: id, className: 'comment-block ' + targetHolder, holderClass: targetHolder, parent: this }
			);
			this.$( '.like' ).addClass('disnone');
			this.insertView( commentView ).render();
			this.$( '.participant.' + targetHolder ).hide();
		},
		voteVersus: function ( className ) {
			var unvotedVersus = ( ( className == 'left' ) ? 'right' : 'left' ) + 'Versus',
				$versus = this[ unvotedVersus ];
			$versus.$( '.photoframe' ).addClass( 'grayscale' );
			var direction = (jQuery.browser.msie || jQuery.browser.mozilla) ? 120 : -120;
			app.trigger( 'move:next:versus', {originalEvent: {wheelDeltaY: direction}} );
		},
		renderVs: function (model) {
			this.insertView( new VsView( {
				parent: this,
				model : model,
				insert: function ( $root, $el ) {
					$root.prepend( $el );
				}
			}) ).render();
		},
		openShareWindow: function () {
			app.trigger ( 'openModal', new ShareWindow ( { page: this } ) );
		}
	} )
} );