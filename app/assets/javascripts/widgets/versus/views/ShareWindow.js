/**
 * Windo for sharing compare from user profile
 */
define( [
	'backbone',
	'text!../templates/shareWindow.tpl',
	'app'
], function ( Backbone, shareWindow, app ) {
	return Backbone.View.extend( {
		template  : shareWindow,
		events: { },
		tagName : 'div'
	} );
} );