define( [
	'app',
	'backbone',
	'./VoterTopView',
	'text!../templates/versusItemTemplate.tpl'
	], function ( app, Backbone, VoterTopView, versusTemplate ) {
	return Backbone.View.extend({

		template: versusTemplate,

		initialize: function () {
			this.model.set( 'infoBottomSideClass', ( this.model.get( 'className' ) == 'left' ) ? 'right' : 'left' );
			this.parentView = this.options.parent;
		},
		events: {
			'click .follow'					: 'follow',
			'click .like-count'				: 'loadVoters',
			'click .comment-count'			: 'loadComments',
			'click .like'					: 'like',
			'click .report'					: 'report',
			'click .share'					: 'share',
			'click .share-icons'			: 'shareIconClick',
            'mouseover .voteLink'           : 'voteLinkMouseOver',
            'mouseout .voteLink'            : 'voteLinkMouseOut',
            'click .voteLink'               : 'like'
		},
		serialize: function() {
			return _.extend( this.model.toJSON(), {
				likeCount		: this.likeCount(),
				commentCount	: this.commentCount(),
				followText		: this.followText(),
				rightClass		: this.rightClass(),
				photoFrameClass	: this.photoFrameClass(),
				liked			: this.checkLiked(),
				grayScale		: this.grayScale(),
				created_at		: moment( this.model.get( 'created_at' ), 'YYYY-MM-DD HH:mm:ssZ' ).fromNow(),
				url				: app.getURL(),
                likedClasses    : this.likedClasses()
			});
		},
		afterRender: function () {
			if ( app.models.userModel.get( 'username' ) == this.model.get( 'user' ).username ) {
				this.$( '.report, .follow' ).css( 'visibility', 'hidden' );
			}
		},
		loadComments: function () {
			this.parentView.showComments( this.model.get( 'id' ), this.model.get( 'className' ) );
			this.$( '.comment-count' ).addClass( 'active' );
		},
		loadVoters: function () {
			this.parentView.showVoters( this.model.get( 'id' ), this.model.get( 'className' ) );
			this.$( '.like-count' ).addClass( 'active' );
		},
		commentCount: function () {
			return this.countFunction( 'comments_count' );
		},
		likeCount: function() {
			var score = this.countFunction( 'score' );
			if ( typeof score == 'undefined' ) {
				score = this.countFunction( 'voters_count' );
			}
			return score;
		},
		countFunction: function( prop ) {
			return ( this.model.get( prop ) > 1000 ) ? ( this.model.get( prop ) / 1000 ) + 'k' : this.model.get( prop );
		},
		followText: function() {
			return ( this.model.get( 'user' ).is_following ) ? 'Unfollow' : 'Follow';
		},
		rightClass: function() {
			return ( this.model.get('className') == 'left' ) ? 'r': '';  
		},
		photoFrameClass: function() {
			return ( this.model.get('className') == 'left' ) ? 'left': '';  
		},
		follow: function( ) {
			var url, user, text;
			if ( this.model.get( 'user' ).is_following ) {
				text = 'Follow';
				url = '/profiles/' + this.model.get( 'user' ).username + '/unfollow';
			} else {
				text = 'Unfollow';
				url = '/profiles/' + this.model.get( 'user' ).username + '/follow';
			}
			user = this.model.get( 'user' );

			user.is_following = !user.is_following;

			this.model.set( 'user', user );

			app.sendRequest( url, { method: 'POST' } );
			this.$( '.info-bottom' ).find( '.follow' ).html( text );
			this.$( '.info-bottom' ).show();
		},
		like: function( e ) {
			if ( this.parentView.model.get( 'liked' ) || 'undefined' != typeof this.model.get( 'vote_weight' ) ) {
				return false;
			}
			var self = this,
			ids = [ this.parentView.model.get( 'photo' ).id, this.parentView.model.get( 'opponent_photo' ).id ];
			e.preventDefault();

			var requestData = {
				challenge_id: self.parentView.model.get( 'challenge_id' ),
				voted_photo_id: self.model.get( 'id' ),
				unvoted_photo_id: _.without( ids, self.model.get( 'id' ) )[0]
			};
                        
			app.sendRequest( '/versuses/vote', {
				method: 'POST',
				data: requestData,
				error: function ( response ) {
					if ( response.status == 200 ) {
						self.likeCallback();
					}
				}
			});
		},
		likeCallback: function() {
		    var versuses = [ 'left', 'right' ],         	
                opposerVersus = _.without( versuses, this.model.get('className') )[0] + 'Versus';
                
            this.parentView[ opposerVersus ].$( '.voteLink' ).addClass( 'disabled' );
			this.model.set( 'voted', true );
			this.parentView.trigger( 'voteVersus', this.model.get( 'className' ) );

			var score = this.model.get( 'score' );
			this.model.set( { score: ++score, likedClasses: 'active disabled', liked: true }, { silent: true } );
            this.parentView.model.set( { liked: true } );
            
			this.render();
		},
		share: function () {
			this.$( '.share-panel' ).toggleClass( 'disnone' );
		},
		shareIconClick: function ( e ) {
			var button = $( e.target ).parents( '.share-icons' );
			this.openShareWindow( button.data( 'provider' ), this.model.get( 'id' ) );
		},
		openShareWindow: function ( provider, photo ) {
			var width	= 500,
				height	= 534,
				left	= $( window ).width() / 2 - width / 2 - 100,
				top		= $( window ).height() / 2 - height / 2,
				params = 'menubar=no,location=yes,resizable=yes,scrollbars=no,status=yes,width=' + width + ',height=' + height + ',modal=true, left=' + left + ',top=' + top;
			var url, title;
			switch ( provider ) {
				case 'pinterest':
					url = this.model.get( 'pinterest_url' );
					title = 'Pinterest';
					break;
				case 'facebook':
					url = this.model.get( 'facebook_url' );
					title = 'Facebook';
					break;
				case 'twitter':
					url = this.model.get( 'twitter_url' );
					title = 'Twitter';
					break;
				case 'tumblr':
					url = this.model.get( 'tumblr_url' );
					title = 'Tumblr';
					break;
				default:
					url = '/photos/' + photo + '/share?provider=' + provider;
					title = provider.charAt( 0 ).toUpperCase() + provider.slice( 1 );
			}
			window.open( url, title, params );
		},
		checkLiked: function () {
			return ( this.model.get( 'liked' ) || (this.model.get( 'vote_weight' ) && this.model.get( 'vote_weight' ) > 0) ) ? 'active' : '';
		},
		grayScale: function () {
			return (this.model.get( 'vote_weight' ) && this.model.get( 'vote_weight' ) < 0 ) ? 'grayscale' : '';
		},
		renderTopVoter: function ( model ) {
			this.$( '.info-top' ).remove();
			this.insertView( new VoterTopView({
				parent: this,
				model: model,
				insert: function ( $root, $el ) {
					$root.prepend( $el );
				}
			})).render();
		},
		report: function () {
			app.sendRequest( '/photos/' + this.model.get( 'id' ) + '/inappropriate', {
				method  : 'POST',
				complete: function () {
					app.trigger ( 'show:message', { 'text' : 'The report has been sent successfully!', 'type' : 'success' } );
				},
				context: this
			});
		},
        voteLinkMouseOver: function ( event ) {
            if ( !this.$( '.voteLink' ).hasClass( 'disabled' ) )
                this.$( '.photoframe input' ).css( 'display', 'block' );            
                
        },
        voteLinkMouseOut: function ( event ) {
            if ( !this.$( '.voteLink' ).hasClass( 'disabled' ) )
                this.$( '.photoframe input' ).css( 'display', 'none' );
        },
        likedClasses: function( event ) {
            return ( this.model.get( 'likedClasses' ) ) ? this.model.get( 'likedClasses' ) : null;
        }
	});
});