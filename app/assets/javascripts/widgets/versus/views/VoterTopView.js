/**
 * Show who voted for pic on compares step on profile page
 */
define( [
	'backbone',
	'text!../templates/voterTop.tpl',
	'app'
], function ( Backbone, vsView, app ) {
	return Backbone.View.extend( {

		events: {

		},
		template: vsView,
		tagName: 'div',
		className: 'info-top right',
		initialize: function () {
		},
		serialize: function () {
			var data = this.model.toJSON();
			data.text = (data.pos) ? 'for' : 'against';
			return _.defaults( data, { 'url' : app.getURL() } );
		}
	} );
} );
