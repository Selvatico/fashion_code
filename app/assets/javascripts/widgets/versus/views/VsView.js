/**
 * Service view for compares page only
 */
define( [
	'backbone',
	'text!../templates/vsView.tpl',
	'app'
], function ( Backbone, vsView ) {
	return Backbone.View.extend( {

		events: {

		},
		template: vsView,
		tagName: 'div',
		initialize: function () {
		},
		serialize: function () {
			return _.extend( this.model.toJSON(), {
				created_at: moment( this.model.get( 'created_at' ), 'YYYY-MM-DD HH:mm:ssZ' ).fromNow()
			} );
		}
	} );
} );