/**
 * @uthor D. Seredenko
 * @export VotersMainLayout
 * @namespace home
 */
define([
	'app',
	'text!./templates/votesBlockTemplate.tpl',
	'../../collections/UsersCollection',
	'./views/VoterListItem',
	'nanoscroller'
	], function (
	app,
	votesBlockTemplate,
	UsersCollection,
	VoterListItem
	) {

	'use strict';

	return Backbone.View.extend({

		template : votesBlockTemplate,
		collection : null,
		page : 1,
		countVoters : 0,
		endReached : false,

		events : {
			'click  .close' : 'closeVoters'
		},

		initialize: function() {
			this.parentView = this.options.parent;
			this.collection = new UsersCollection( [], { userUrl: '/photos/' + this.options.photo_id + '/voters', key: 'voters'} );


			//subscribe to add one voter to render it
			this.collection.on( 'add', this.renderVoter, this );

			this.collection.on( 'users:loaded', this.loadFinsihed, this );

			//load collection and show message that no voters there
			this.loadUsers();
		},
		/**
		 * Service method to load voters page by page
		 */
		loadUsers: function () {
			var _self = this;
			if (_self.endReached) {
				return;
			}
			this.collection.load( { page : _self.page});
		},
		/**
		 * Load finsih callback
		 * @param {Backbone.Collection} collection
		 * @param {Object} data
		 */
		loadFinsihed: function (collection, data) {
			//if after load we don't have voters show message about that
			if ( collection.length == 0 ) {
				this.$( '.faceplate' ).removeClass( 'disnone' );
				this.$( '.voters-list' ).remove();
			}
			//if we didn't receive any data  - stop trying to get them during scroll
			if ( data['voters'].length == 0 ) {
				this.endReached = true;
			}
			this.page++;
		},
		afterRender: function ( ) {},
		/**
		 * Render one voter
		 * @param {Backbone.Model} model
		 */
		renderVoter: function ( model ) {
			var _self = this;
			this.insertView( '.voters-list>.content', new VoterListItem( { model: model} ) ).render();

			this.$( '.content' ).on( 'scroll', function ( event ) {
				_self.catchScrollEvent( event );
			} );
			
			this.$( '.voters-list').addClass('nano').nanoScroller({ scroll: 'top' });
			this.$( '.content').width(this.$( '.voters-list').parent().width());
		},
		catchScrollEvent: function () {
			var $content   = this.$( '.content' ),
				percent    = ( $content.scrollTop() / ($content.get( 0 ).scrollHeight - $content.height())) * 100;

			//we scrolled 75% of scroll
			if ( percent > 75 ) {
				this.loadUsers();
			}
		},
		/**
		 * Close voters list event handler
		 */
		closeVoters: function () {
			this.options.parent.closeVoters(this.options.holderClass);
		}
	});

});