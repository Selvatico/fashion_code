define([
	'backbone',
	'text!../templates/voterListItem.tpl',
	'app'
	], function (
	Backbone,
	VoterListItemTmplate,
	app
	) {
	return Backbone.View.extend({

		template: VoterListItemTmplate,
		events: {
			'click .follow': 'followAction'
		},
		className: 'voter-item',
		initialize: function() {

		},
		serialize: function() {
			var data = this.model.toJSON();
			return _.defaults( data, { 'url' : app.getURL() } );
		},
		followAction: function( ) {
			app.sendRequest( this.model.getFollowUrl(), {
				method: 'POST',
				error: function () {
					//alert('Follow operation failed');
				},
				complete: function ( jqXHR ) {
					if (jqXHR.status == 200) {
						this.model.set('is_following', !this.model.get('is_following'));
						this.render();
					}
				},
				single : true,
				context: this
			});

		}
	});
});