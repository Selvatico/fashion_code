define([
	'app',
	'text!./templates/layout.tpl'
	], function (
	app,
	footerTemplate
	) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'width widget-Footer',
		template : footerTemplate

	} );

} );