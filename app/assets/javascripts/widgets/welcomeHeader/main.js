define([
	'app',
	'text!./templates/layout.tpl'    
	], function (
	app,
	headerTemplate
	) {

	'use strict';

	return Backbone.View.extend ( {

		className : 'top-panel widget-Header',
		template : headerTemplate,

		serialize : function () {
			return { 'url' : app.getURL() };
		}

	} );

} );