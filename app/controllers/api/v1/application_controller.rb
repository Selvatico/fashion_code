class Api::V1::ApplicationController < Api::ApplicationController
  #include ActionController::MimeResponds
  #include AbstractController::Callbacks
  helper ApplicationHelper

  before_filter :authenticate, :load_response_status
  respond_to :json



  protected

  def load_response_status
    @status = ResponseStatus.new
  end

  def current_user
    @current_user
  end

  def authenticate
    token = request.env["HTTP_ACCESS_TOKEN"]
    @current_user = User.where(authentication_token: token).first if token.present?
    unless token && current_user.present?
      @status = ResponseStatus.new(401, 'Authentication failed.')
      render template: 'api/api/v1/status'
    end
  end

end
