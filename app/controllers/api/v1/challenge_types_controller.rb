class Api::V1::ChallengeTypesController < Api::V1::ApplicationController
  def index
    @challenge_types = ChallengeType.by_style(current_user.fashion_style)
  end
end
