class Api::V1::ChallengesController < Api::V1::ApplicationController
  before_filter :load_saved_photos, only: :index
  def create
    if current_user.can_create_challenge?
      photo = current_user.photos.where(id: params[:photo_id]).first
      challenge_type = ChallengeType.order('RANDOM()').first
      if photo && challenge_type
        score = challenge_type.send("#{params[:level]}_score")
        @challenge = current_user.challenges.new(challenge_type: challenge_type, photo: photo, score: score, mentions: params[:mentions], level: params[:level])
        unless @challenge.save
          @status.code = 500
          @status.message = @challenge.errors.full_messages.join('. ')
          @challenge = nil
        end
      else
        @status.code = 400
        @status.message = 'Bad request.'
      end
    else
      @status.code = 500
      @status.message = 'Not allowed to create challenge at this time.'
    end
  end

  def can_create
    if current_user.can_create_challenge?
      @status.code = 200
      @status.message = "OK"
    else
      @status.code = 500
      @status.message = 'Not allowed to create challenge at this time.'
    end
    render 'api/v1/status'
  end


  def index
    @versuses = current_user.photos_votes.where('NOT votes.challenge_id IS NULL').order('challenge_id desc, created_at desc').page(params[:page]).per(params[:per_page])
  end

  protected
    def load_saved_photos
      @saved_photos = current_user.saved_photo_ids
    end
end
