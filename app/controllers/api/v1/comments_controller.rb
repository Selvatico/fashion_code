class Api::V1::CommentsController < Api::V1::ApplicationController

  def index
    photo = Photo.where(id: params[:photo_id]).first
    if photo
      @comments = photo.comments.page(params[:page]).per(params[:per_page])
    else
      @status.code = 404
      @status.message = 'Photo not found'
    end
  end

  def show
    @comment = Comment.where(id: params[:id]).first
    unless @comment
      @status.code = 404
      @status.message = 'Comment not found'
    end
  end

  def create
    photo = Photo.where(id: params[:photo_id]).first
    @comment = Comment.new(comment: params[:comment])
    @comment.user = current_user
    @comment.photo = photo
    unless @comment.save
      @status.code = 500
      @status.message = @comment.errors.full_messages.join('. ')
    end
  end

  def destroy
    comment = Comment.where(id: params[:id]).first
    unless  comment && comment.user == current_user && comment.destroy
      @status.code = 500
      @status.message = 'Unable to delete comment'
    end
    render template: 'api/v1/status'
  end

  def inappropriate
    comment = Comment.where(id: params[:id]).first
    if comment.user != current_user
      #check if already reported
      unless comment.inappropriates.where(user_id: current_user.id).any?
        comment.inappropriates.create(user: current_user)
      else
        @status.code = 403
        @status.message = "Already reported"
      end
    else
      @status.code = 403
      @status.message = "Can't report own comment"
    end
    render template: 'api/v1/status'
  end
end
