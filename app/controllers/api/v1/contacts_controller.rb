class Api::V1::ContactsController < Api::V1::ApplicationController
  def find
    params[:users]
    emails = []
    phones = []
    params[:users].each do |user|
      emails.push user[:email] if user[:email]
      phones.push user[:phone] if user[:phone]
    end
    logger.debug phones.inspect
    @following = current_user.following_ids
    @users = User.by_emails_or_phones(emails: emails, phones: phones)
  end
end
