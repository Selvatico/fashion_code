class Api::V1::ContestsController < Api::V1::ApplicationController

  def index
    @contests = Contest.active.all
    @user_id = current_user.id
    @best_photo = current_user.best_photo.image.url if current_user.best_photo
  end

  def join
    contest = Contest.find(params[:id])
    participant = Participant.where(user_id: current_user.id, contest_id: contest.id).first

    unless participant
      participant = Participant.new
      participant.user = current_user
      participant.contest = contest

      unless participant.save
        @status.code = 500
        @status.message = participant.errors.full_messages.join('. ')
      end
      @image = current_user.best_photo
    else
      @status.code = 500
      @status.message = 'Already take part in this contest.'
    end
  end

  def leave
    contest = Contest.find(params[:id])
    participant = Participant.where(user_id: current_user.id, contest_id: contest.id).first
    if participant
      participant.destroy
    else
      @status.code = 500
      @status.message = 'Not a participant.'
    end
    render template: "api/v1/status"
  end

  def inappropriate
    contest = Contest.where(id: params[:id]).first
    #check if already reported
    unless contest.inappropriates.where(user_id: current_user.id).any?
      contest.inappropriates.create(user: current_user)
    else
      @status.code = 403
      @status.message = "Already reported"
    end
    render template: 'api/v1/status'
  end

  def hall_of_fame
    contest = Contest.find(params[:id])
    @participants = contest.participants.includes(:user).order(:current_place).page(params[:page]).per(params[:per_page])
  end
end
