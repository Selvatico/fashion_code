class Api::V1::FashionStylesController < Api::V1::ApplicationController

  def index
    @fashion_styles = FashionStyle.all
  end

  def choose
    fashion_style = FashionStyle.find(params[:id])
    unless current_user.fashion_style
      current_user.fashion_style = fashion_style
      current_user.save
    else
      @status.code = 403
      @status.message = "Already have a fashion style"
    end
    render template: "api/v1/status"
  end
end
