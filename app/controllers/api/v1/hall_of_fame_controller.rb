class Api::V1::HallOfFameController < Api::V1::ApplicationController
  def hall_of_fame
    @users = User.hall_of_fame.page(params[:page]).per(params[:per_page])
  end
end
