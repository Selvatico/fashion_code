class Api::V1::NotificationsController < Api::V1::ApplicationController
  def index
    @notifications = current_user.incoming_notifications.by_following(params[:following]=='1')
    .where(kind: %w(level_up level_down ten three first challenge_started challenge_finished commented mentioned contest_started contest_finish_line contest_finished new_follower facebook_friend up_challenge_versus_voted up_hashtag_versus_voted up_friend_versus_voted up_versus_voted photo_voted voted challenge_start joined_contest left_contest photo_uploaded new_following))
    .page(params[:page]).per(params[:per_page])
  end
end
