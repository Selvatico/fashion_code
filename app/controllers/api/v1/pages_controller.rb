class Api::V1::PagesController < Api::V1::ApplicationController
  skip_before_filter :authenticate, only: [:index]

  def index
    @pages = Page.where(link: %w(about tos privacy help)).pluck(:link)
  end
end
