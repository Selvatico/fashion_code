class Api::V1::PhotosController < Api::V1::ApplicationController
  before_filter :load_saved_photos, only: [:index, :by_tag]
  def index
    @user = User.where(username: params[:profile_id]).first
    if @user
      @photos = @user.photos.active.page(params[:page]).per(params[:per_page])
      @photos_count = @photos.total_count
    else
      @status.code = 404
      @status.message = "User not found."
    end

  end

  def destroy
    photo = current_user.photos.find_by_id params[:id]
    if photo && photo.active_challenge.nil?
      photo.delete_file
    else
      @status.code = 500
      @status.message = "Unable to delete photo."
    end
    render template: 'api/v1/status'
  end

  def inappropriate
    photo = Photo.where(id: params[:id]).first
    if photo.user != current_user
      #check if already reported
      unless photo.inappropriates.where(user_id: current_user.id).any?
        photo.inappropriates.create(user: current_user)
      else
        @status.code = 403
        @status.message = "Already reported"
      end
    else
      @status.code = 403
      @status.message = "Can't report own photo"
    end
    render template: 'api/v1/status'
  end

  def by_tag
    @photos =  Photo.includes(:user, :recent_comments).tagged_with(params[:tag]).page(params[:page]).per(params[:per_page])
  end

  def voters
    @voters = User.includes(:votes).
      where('modera_votes.voted_photo_id = ? OR modera_votes.unvoted_photo_id = ?', params[:photo_id],
        params[:photo_id]).page(params[:page]).per(params[:per_page]) || []
  end

  def vote
    photo = Photo.where(id: params[:id]).first
    if photo
      unless photo.vote_for(@current_user)
        @status.code = 403
        @status.message = "Already voted"
      end
    else
      @status.code = 404
      @status.message = "Photo not found"
    end
    render template: 'api/v1/status'
  end

  def share
    #TODO if twitter than we need to update keys sometimes, even if we have auth_method in base
    passed_provider = params[:provider]
    @photo = Photo.find(params[:id])
    auth_method = current_user.user_authentications.where(provider: passed_provider.downcase).first
    unless auth_method
      @status.code = 403
      @status.message = "Not authorised."
      render template: 'api/v1/status' and return
    end
    provider = SocialNetworks::Provider.new({
      name: passed_provider.downcase,
      token: auth_method.token,
      token_secret: auth_method.secret_token,
      api_key: Settings.omniauth_keys.send(passed_provider.capitalize).app_token,
      api_key_secret: Settings.omniauth_keys.send(passed_provider.capitalize).app_token_secret
    })
    begin
      provider.share({image_path: @photo.image_url, title: 'Title', share_url: @photo.share_url, own: @photo.user_id==current_user.id})
    rescue
      @status.code = 403
      @status.message = "Not authorised."
      render template: 'api/v1/status' and return
    end
    render template: 'api/v1/status'
  end

  protected
    def load_saved_photos
      @saved_photos = current_user.saved_photo_ids
    end
end
