class Api::V1::ProfilesController < Api::V1::ApplicationController

  def show
    @profile = User.where(username: params[:id]).first
    unless @profile
      @status.code = 404
      @status.message = 'User not found.'
    end
  end

  def follow
    followed = User.where(username: params[:id]).first
    if followed.nil? # no user found
      @status.code = 404
      @status.message = 'User not found.'
    elsif current_user.following?(followed) # already following
      @status.code = 500
      @status.message = 'Already following.'
    else
      begin
        current_user.follow!(followed)
      rescue # could not follow by some reason
        @status.code = 500
        @status.message = "Can't follow."
      end
    end
    render template: 'api/v1/status'
  end

  def unfollow
    followed = User.where(username: params[:id]).first
    if followed.nil? # no user found
      @status.code = 404
      @status.message = 'User not found.'
    elsif !current_user.following?(followed) # not following
      @status.code = 500
      @status.message = 'Not following.'
    else
      begin
        current_user.unfollow!(followed)
      rescue # could not unfollow by some reason
        @status.code = 500
        @status.message = "Can't unfollow."
      end
    end
    render template: 'api/v1/status'
  end

  def followers
    user = User.where(username: params[:id]).first
    if user
      @users = user.followers.page(params[:page]).per(params[:per_page])
    else
      @status.code = 404
      @status.message = "User not found."
    end
    render template: 'api/v1/profiles/followers'
  end

  def following
    user = User.where(username: params[:id]).first
    if user
     @users = user.following.page(params[:page]).per(params[:per_page])
    else
      @status.code = 404
      @status.message = "User not found."
    end
    render template: 'api/v1/profiles/following'
  end

  def upload_avatar
    user = User.where(username: params[:id]).first
    if current_user.id == user.id
      user.avatar = params[:avatar]
      unless user.save
        @status.code = 500
        @status.message = "Unable to update avatar."
      end
    else
      @status.code = 403
      @status.message = "Unable to update avatar."
    end
    render template: 'api/v1/status'
  end


  def remove_avatar
    current_user.remove_avatar!
    unless current_user.save
      @status.code = 500
      @status.message = "Unable to remove avatar."
    end
    render template: 'api/v1/status'
  end

  def upload_photo
    user = User.where(username: params[:id]).first
    if current_user.id == user.id
      @photo = current_user.photos.create(image: params[:image])
    else
      @status.code = 403
      @status.message = "Unable to upload photo."
    end
  end

  def follow_all
    current_user.followers.each{|u| current_user.follow!(u)}
    render template: 'api/v1/status'
  end

  def unfollow_all
    if current_user.following.delete_all.blank?
      @status.code = 500
      @status.message = 'Not following anybody'
    end
    render template: 'api/v1/status'
  end

  def suggestions
    @users = @current_user.suggested_followings(10)
  end
end
