class Api::V1::SavingsController < Api::V1::ApplicationController
  def index
    @photos = current_user.saved_photos.page(params[:page]).per(params[:per_page])
    @photos_count = current_user.savings.count
  end

  def create
    unless current_user.save_photo!(params[:photo_id])
      @status.code = 500
      @status.message = 'Photo not found'
    end
  end

  def destroy
    if current_user.delete_saved_photo!(params[:photo_id])
      @status.code = 500
      @status.message = 'Photo not found'
    end
  end
end
