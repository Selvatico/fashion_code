class Api::V1::SearchController < Api::V1::ApplicationController

  def users
    @users = User.search(params[:q]).page(params[:page]).per(params[:per_page])
  end

  def tags
    tags = ActsAsTaggableOn::Tag.search(params[:q]).page(params[:page]).per(params[:per_page])
    @tags = []
    tags.each do |tag|
      photos = Photo.tagged_with(tag.name).pluck(:user_id)
      has_followers = (@current_user.follower_ids & photos).length > 0 ? true : false
      @tags << {name: tag.name, has_followers: has_followers, photos_count: photos.size}
    end
  end
end
