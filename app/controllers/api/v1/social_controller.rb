class Api::V1::SocialController < Api::V1::ApplicationController
  skip_before_filter :authenticate, only: [:info, :login]

  def info
    networks = %w{facebook twitter instagram}
    if networks.include?(params[:id])
      setup = {}
      setup[:token] = params[:oauth_token]
      setup[:token_secret] = params[:oauth_secret_token]
      setup[:api_key] = Settings.omniauth_keys.send(params[:id].camelize).app_token
      setup[:api_key_secret] = Settings.omniauth_keys.send(params[:id].camelize).app_token_secret
      setup[:name] = params[:id]
      provider = SocialNetworks::Provider.new(setup)
      @user_info = provider.user_info(params)
    else
      @status.code = 404
      @status.message = "Provider not found."
    end
  end

  def login
    @status.code = 401
    @status.message = "Authentication failed."
    begin
      @user = User.from_social(params[:id], params[:oauth_token], params[:oauth_secret_token])
    rescue
    end
    if @user
      @status.code = 200
      @status.message = "OK"
      @user.reset_authentication_token!
    end
    render 'api/v1/users/login'
  end

  def fb_friends
    @friends = @current_user.friends "facebook"
    unless @friends
      @status.code = 500
      @status.message = "OAuth account not found"
    end
  end
end
