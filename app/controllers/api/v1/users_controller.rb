class Api::V1::UsersController < Api::V1::ApplicationController
  skip_before_filter :authenticate, only: [:login, :create, :password]
  def create
    @user = User.new({
      username: params[:username],
      email: params[:email],
      password: params[:password],
      password_confirmation: params[:password],
      device_token: params[:device_token],
      avatar: params[:avatar]
    })
    unless params[:oauth_token].nil?
      provider = SocialNetworks::Provider.new({
        name: params[:provider],
        api_key: Settings.omniauth_keys.send(params[:provider].camelize).app_token,
        api_key_secret: Settings.omniauth_keys.send(params[:provider].camelize).app_token_secret,
        token: params[:oauth_token],
        token_secret: params[:oauth_token_secret]
      })
      begin
        user_info = provider.user_info({})
      rescue
        user_info = nil
        @status.code = 500
        return @status.message = "OAuth token is invalid"
      end
    end

    @user.fashion_style = FashionStyle.order('RANDOM()').first
    if @user.save
      @user.ensure_authentication_token!
      unless user_info.nil?
        @user.user_authentications.create(provider: params[:provider], uid: user_info.id, token: params[:oauth_token], secret_token: params[:oauth_token_secret])
      end
    else
      @status.code = 500
      @status.message = @user.errors.full_messages.join('. ')
    end
  end

  def login
    @user = User.where(email: params[:email]).first
    if @user && @user.valid_password?(params[:password])
      @user.update_attributes(params[:device_token])
      @user.reset_authentication_token!
    else
      @status.code = 401
      @status.message = "Authentication failed."
    end
  end

  def password
    @user = User.find_by_email params[:email]
    if @user
      @user.send_reset_password_instructions
    else
      @status.code = 500
      @status.message = 'User not found.'
    end
    render template: 'api/v1/status'
  end

  def update
    current_user.update_attributes(params[:settings])
  end

  def social
    auth = current_user.user_authentications.where(provider: params[:provider]).first_or_initialize
    auth.token = params[:token]
    auth.secret_token = params[:secret_token]

    provider = SocialNetworks::Provider.new({
      name: params[:provider],
      api_key: Settings.omniauth_keys.send(params[:provider].camelize).app_token,
      api_key_secret: Settings.omniauth_keys.send(params[:provider].camelize).app_token_secret,
      token: params[:token],
      token_secret: params[:secret_token]
    })
    user_info = provider.user_info({})
    auth.uid = user_info.id
    unless auth.save
      @status.code = 500
      @status.message = 'Invalid provider'
    end
  end

  def settings
    @auths = current_user.user_authentications.pluck(:provider)
  end

  def update_password
    @user = User.find(@current_user.id)
    if @user.update_with_password(params[:user])
      @user.reset_authentication_token!
    else
      @status.code = 500
      @status.message = 'User not found.'
    end
  end

end
