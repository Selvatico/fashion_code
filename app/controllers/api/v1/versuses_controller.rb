class Api::V1::VersusesController < Api::V1::ApplicationController
  before_filter :load_saved_photos, except: :vote

  def style
    @versuses = VsTopPhoto.simple_home((params[:count].nil? ? 10 : params[:count].to_i), @current_user.id)
  end

  def friends
    @versuses = VsTopPhoto.home_by_followings((params[:count].nil? ? 10 : params[:count].to_i), @current_user.id)
    render 'style'
  end

  def index
    @own_profile = (params[:profile_id] == @current_user.username)
    if @own_profile
      @user = @current_user
      @versuses = current_user.photos_votes.page(params[:page]).per(params[:per_page])
    else
      @user = User.where(username: params[:profile_id]).first
      if @user
        @versuses = @user.photos_votes.page(params[:page]).per(params[:per_page]) #[] #place holder
      else
        @status.code = 404
        @status.message = 'User not found.'
      end
    end
  end

  def vote
    voted_photo = Photo.where( id: params[:voted_photo_id]).first
    unvoted_photo = Photo.where( id: params[:unvoted_photo_id]).first
    challenge = Challenge.where( id: params[:challenge_id]).first

    voted = current_user.vote_photo(voted_photo, unvoted_photo, challenge)
    unless voted
      @status.code = 500
      @status.message = 'Could not vote.'
    end
    render template: 'api/v1/status'
  end

  def by_hashtag
    photos = Photo.tagged_with(params[:tag]).all
    photos_size = photos.size
    quontity = params[:count].nil? ? 10 : params[:count].to_i
    @versuses = []
    used_photos = []
    i = 0
    while i < quontity && used_photos.size < photos_size
      @versuses.push([])
      while @versuses.last.size < 1 && used_photos.size < photos_size
        random_index = rand(0..photos_size-1)
        unless used_photos.include?(random_index)
          @versuses.last.push(photos[random_index])
          used_photos.push(random_index)
        end
      end
      while @versuses.last.size < 2 && used_photos.size < photos_size
        random_index = rand(0..photos_size-1)
        unless used_photos.include?(random_index) || @versuses.last[0].user_id == photos[random_index].user_id
          @versuses.last.push(photos[random_index])
          used_photos.push(random_index)
        end
      end
      i = i+1
    end
    @versuses.delete(@versuses.last) if @versuses.last && @versuses.last.size < 2
    render 'style'
  end
  protected
    def load_saved_photos
      @saved_photos = current_user.saved_photo_ids
    end
end
