class ApplicationController < ActionController::Base

  before_filter :authenticate_user!
  protect_from_forgery
  before_filter :style_choosen?
  before_filter :decode_url!
  before_filter :browser_support, if: -> { AgentOrange::UserAgent.new(request.user_agent).is_computer? and
      session[:browser_supported].blank? }
  before_filter :validate_case

  def after_sign_in_path_for(resource)
    stored_origin_path || home_path
  end

  def after_sign_up_path_for(resource)
    stored_origin_path || home_path
  end


  def authenticate_user!
    session[:invitation_token] = params[:invitation_token] if params[:invitation_token]
    store_origin_path
    super
  end

  def stored_origin_path
    origin_path = session[:user_return_to]
    clear_origin_path
    origin_path
  end

  def store_origin_path
    unless session[:user_return_to]
      session[:user_return_to] = request.fullpath if request.fullpath =~ /\/profiles/ && !request.xhr? && request.get?
    end
  end

  def clear_origin_path
    session[:user_return_to] = nil
  end

  def style_choosen?
    redirect_to('/choose/#style') if current_user && current_user.fashion_style_id.nil?
  end

  def decode_url!
    redirect_to(Base64.urlsafe_decode64(params[:u])) if params[:u]
  end

  def browser_support
    render template: "makeup/old_browser", status: 406, layout: 'error' unless browser_supported?
  end

  private

  def browser_supported?
    return true if (Rails.env.test? && request.user_agent.blank?)
    user_agent = AgentOrange::UserAgent.new(request.user_agent)
    @user_browser = user_agent.device.engine.browser
    req_browser = Settings.browsers[@user_browser.type]
    user_browser_ver = "#{@user_browser.version.major}.#{@user_browser.version.minor}".to_f
    session[:browser_supported] = req_browser.present? && user_browser_ver >= req_browser.version
  end

  def validate_case
    redirect_to(request.url.downcase, status: :moved_permanently) if request.url != request.url.downcase
  end
end