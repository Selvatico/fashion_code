class BrandsController < ApplicationController
  respond_to :json
  skip_before_filter :authenticate_user!
  skip_before_filter :style_choosen?

  def index
    @brands = Brand.all
  end
end