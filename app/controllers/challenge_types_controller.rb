class ChallengeTypesController < ApplicationController
  def index
    @challenge_types = ChallengeType.by_style(current_user.fashion_style)
  end
end