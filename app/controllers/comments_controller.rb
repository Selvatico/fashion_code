    class CommentsController < ApplicationController
    before_filter :load_parent, only: [:index, :create]
    before_filter :load_resource, only: [:destroy, :inappropriate]
    respond_to :json, only: [:index, :create, :inappropriate]

    def index
      @comments = @photo.comments.order('created_at DESC').page(params[:page])
      @comments_count = @photo.comments.count
    end

    def new      
    end


    def create
      @comment = Comment.create(comment: params[:comment], user: current_user, photo: @photo)
    end

    def destroy
      if @comment.user == current_user or @comment.photo.user == current_user
        @comment.destroy
        render nothing: true
      else
        render nothing: true, status: 401
      end
    end

    def inappropriate
      unless @comment.user_id == current_user.id
        @comment.report_inappropriate_by current_user
        render nothing: true and return
      end
      render nothing: true, status: 401
    end

    protected

    def load_resource
      @comment = Comment.find(params[:id])
    end
      
    def load_parent
      @photo = Photo.find(params[:photo_id])
    end
  end
