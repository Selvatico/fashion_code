  class ContestsController < ApplicationController
    def index
      @contests = Contest.includes(:inappropriates).order('contests.started_at ASC').all
      @user_id = current_user.id
      @best_photo = current_user.best_photo
    end

    def list
      @contests = case params[:type]
                  when 'started' then
                    Contest.started current_user.id
                  when 'new' then
                    Contest.new_scope current_user.id
                  when 'joined' then
                    Contest.joined current_user.id
                  when 'finished' then
                    Contest.finished_scope
                  else
                    raise 'undefined type of contest list'
                  end
    end

    def hall_of_fame
      # @contest_id = params[:contest_id]
      #@users = User.hall_of_fame(@contest_id).page(params[:page]).per(20)
      if params[:contest_id]
       @contest = Contest.find(params[:contest_id])
       @users = @contest.participants.includes(:user).fame_order.page(params[:page]).per(20)
       @current_user_participant = @contest.participants.includes(:user).where(user_id: current_user.id).first
       @current_user_place = @contest.participant_place(current_user.id)
      else
       @users = Participant.global.includes(:user).order('users.votes_score DESC, users.created_at ASC').page(params[:page]).per(20)
       @current_user_participant = Participant.global.includes(:user).where(user_id: current_user.id).first
       @current_user_place = current_user.fame_place
      end
    end

    def user_place
      contest_id = params[:id]
      @user = User.find_by_username params[:username]
      @current_place = @user.current_place(contest_id)
    end


    def join
      @contest = Contest.find(params[:id])
      participant = Participant.where(user_id: current_user.id, contest_id: @contest.id).first

      if !participant && current_user.photos.count > 0
        participant = Participant.new
        participant.user = current_user
        participant.contest = @contest
        participant.save
      end
      unless participant
        head :internal_server_error
        return
      end
    end

    def leave
      contest = Contest.find(params[:id])
      participant = Participant.where(user_id: current_user.id, contest_id: contest.id).first
      if participant
        participant.destroy
      end
      head :ok
    end

    def inappropriate
      contest = Contest.find(params[:id])
      #check if already reported
      unless contest.inappropriates.where(user_id: current_user.id).exists?
        contest.inappropriates.create(user: current_user)
      end
      head :ok
    end
  end
