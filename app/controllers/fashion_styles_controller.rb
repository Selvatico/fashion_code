class FashionStylesController < ApplicationController
  skip_before_filter :style_choosen?, only: [:index]
  def index
    @fashion_styles = FashionStyle.all
  end
end
