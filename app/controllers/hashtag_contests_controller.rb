  class HashtagContestsController < ApplicationController
    def hall_of_fame
      if request.xhr?
        @contest = HashtagContest.find_by_name(params[:id])
        @users = @contest.participants.includes(:user).fame_order.page(params[:page]).per(20)
        # @photos_count = Photo.tagged_with(@contest.name).count
        @current_user_participant = @contest.participants.includes(:user).where(user_id: current_user.id).first
        @current_user_place = @contest.participant_place(current_user.id)
      end
    end

    def trending
      ActiveRecord::Base.include_root_in_json = false
      render json: { tags: HashtagContest.top(7).to_json(:only => :name) }
    end

    def versuses
      @versuses = []
      @contest = HashtagContest.find_by_name(params[:id].downcase)
      @following_ids = current_user.following_ids
      @users_count = @contest ? @contest.participants.includes(:user).count : 0
      photos = Photo.active.tagged_with(params[:id]).where('not photos.user_id = ?', current_user.id).all
      photos_size = photos.size
      quantity = params[:count].nil? ? 10 : params[:count].to_i
      
      used_photos = []
      i = 0
      while i < quantity && used_photos.size < photos_size
        @versuses.push([])
        while @versuses.last.size < 1 && used_photos.size < photos_size
          random_index = rand(0..photos_size-1)
          unless used_photos.include?(random_index)
            @versuses.last.push(photos[random_index])
            used_photos.push(random_index)
          end
        end
        while @versuses.last.size < 2 && used_photos.size < photos_size
          random_index = rand(0..photos_size-1)
          unless used_photos.include?(random_index) || @versuses.last[0].user_id == photos[random_index].user_id
            @versuses.last.push(photos[random_index])
            used_photos.push(random_index)
          end
        end
        i = i+1
      end
      @versuses.delete(@versuses.last) if @versuses.last && @versuses.last.size < 2
    end

    def photos
      @following_ids = current_user.following_ids
      @contest = HashtagContest.find_by_name(params[:id].downcase)
      @users_count = @contest.participants.includes(:user).count
      @photos = Photo.active.by_date.includes(:user, :recent_comments).tagged_with(params[:id]).page(params[:page])
    end

    def hall_of_fame
      @contest = HashtagContest.find_by_name(params[:tag].downcase)
      @users = User.hashtag_hall_of_fame(@contest.id).page(params[:page]).per(20)
    end
  end
