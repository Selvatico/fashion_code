class HomeController < ApplicationController
  skip_before_filter :authenticate_user!, only: [:welcome]
  skip_before_filter :style_choosen?, only: [:welcome, :choose, :first_login]

  def index
  end

  def welcome
    redirect_to("/home/") and return if user_signed_in?

    session[:transaction_id] ||= SecureRandom.uuid
    render layout: 'welcome'
  end

  def first_login
    render 'home/first_login', layout: false
  end

  def choose
    render layout: 'welcome'
  end
end
