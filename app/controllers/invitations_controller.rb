class InvitationsController < ApplicationController
  respond_to :json, only: [:create]

  def create
    current_user.send_invitations(params[:emails], params[:message])
    render json: {status: "OK"}
  end
end