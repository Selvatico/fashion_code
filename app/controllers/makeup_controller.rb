class MakeupController < ApplicationController
  skip_before_filter :authenticate_user!
  skip_before_filter :style_choosen?

  def index
  end

  def home
  end

  def choose_style
  end

  def upload
  end

  def invite
  end

  def get_prize
  end

  def tutorial
  end

  def power_hour_create
  end

  def power_hour
  end

  def contest_hash
  end

  def contest_hash_photo
  end

  def hash_hall_of_fame
  end

  def hall_of_fame
  end

  def hall_of_fame_25
  end

  def notification_your_news
  end

  def notification_follow
  end

  def notifications
    
  end

  def profile
  end

  def profile_settings
  end

  def about
  end

  def our_story
  end

  def partnership
  end

  def business_proposal
  end

  def videos
  end

  def contact_us
  end

  def error_401    
  end

  def error_404    
  end

  def error_500    
  end

  def alex
  end

  def anvarshakirov
  end

  def anvartaishev
  end

  def artemchekhlov
  end

  def elvina
  end

  def gunaydonmez
  end

  def jonathanlenoch
  end

  def julerizzardo
  end

  def lynnebarker
  end

  def mahmudkaraev
  end

  def maratibragimov
  end

  def michael
  end

  def nodirmirsaid
  end

  def rustamkhamraev
  end

  def sanjarbabadjanov
  end

  def shanebarker
  end

  def timothymunden
  end

  def volodyapinchuk
  end

  def yaroslav
  end

  def guidlines
  end

  def tos
  end

  def privacy
  end

  def content    
  end

  def landing    
  end

  def share    
  end

  def layout_static    
  end

  def old_browser    
  end
end
