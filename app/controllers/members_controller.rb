class MembersController < ApplicationController
  respond_to :json, only: [:index, :show]

  def index
    @members = Member.all
  end

  def show
    @member = Member.includes(:questionaries).find(params[:id])
  end
end
