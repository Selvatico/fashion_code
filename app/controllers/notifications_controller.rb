class NotificationsController < ApplicationController
  def index
    #@notifications = Notification.group_notifications(params[:following], current_user, params[:last_day])
    @notifications = current_user.incoming_notifications.by_following(params[:following].present?).send('by_'+params[:type]).order_by_new.page(params[:page]).per(params[:per_page])
  end

  def ungroup
    @notifications = current_user.incoming_notifications.by_following(params[:following]).where("to_char(created_at, 'DD-MM-YYYY') = ? and grouping_kind = ?", params[:created_at].to_date.to_s(:simple_date), params[:kind])
    render 'index'
  end

  def stats
    render json: current_user.notification_stats
  end

  def share
    #TODO if twitter than we need to update keys sometimes, even if we have auth_method in base
    passed_provider = params[:provider]
    auth_method = current_user.user_authentications.where(provider: passed_provider).first
    unless auth_method
      session[:redirect_url] = notifications_path
      redirect_to user_omniauth_authorize_path(passed_provider) and return
    end
    provider = SocialNetworks::Provider.new({
      name: passed_provider,
      token: auth_method.token,
      token_secret: auth_method.secret_token,
      api_key: Settings.omniauth_keys.send(passed_provider.capitalize).app_token,
      api_key_secret: Settings.omniauth_keys.send(passed_provider.capitalize).app_token_secret
    })
    begin
      provider.share({image_path: params[:image_url], uid: auth_method.uid})
    rescue
     redirect_to user_omniauth_authorize_path(passed_provider) and return
    end

    redirect_to notifications_path(shared_provider: passed_provider)
  end

  def clear_all
    Resque.enqueue(ClearNotificationsWorker, 'by_user', {user_id: current_user.id, owner: params[:owner] == 'true'})
    render nothing: true
  end

  def mark_read
    params[:read_notifications].split(',').each do |n|
      notification = current_user.incoming_notifications.where(id: n).first
      notification.set_read! if notification          
    end
    render nothing: true
  end
  def subscription
    @subscription = nil
    @subscription = PrivatePub.subscription(:channel => "/notifications/#{current_user.id}") if current_user
    render :json => @subscription.to_json(:root => false)
  end

end