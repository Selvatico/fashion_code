class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  skip_before_filter :style_choosen?
  
  def callback
    if request.env["omniauth.error"].present?
      redirect_to mobile_url(true) and return if mobile?
      redirect_to(root_url) and return
    end
    redirect_to(mobile_url) and return if mobile?
    session[:omniauth] = auth_hash

    if user_signed_in?
      user_auth = current_user.user_authentications.find_or_initialize_by_provider_and_uid(provider: auth_hash[:provider], uid: auth_hash[:uid])
      update_user_auth(user_auth)
      redirect_back_or_default("/home/") and return
    else
      user_auth = UserAuthentication.where(provider: auth_hash[:provider], uid: auth_hash[:uid]).first
      if user_auth.present?
        update_user_auth(user_auth)
        sign_in(:user, user_auth.user)
        redirect_back_or_default("/home/") and return
      else
        user = User.new
        user.apply_omniauth(auth_hash)
        user.password = User.generate_pass(6)
        if user.save
          session[:omniauth] = nil
          sign_in(:user, user)
          render('home/first_login', layout: false) and return
        end
        redirect_to('/#signup')
      end
    end
  end

  [:facebook, :twitter, :instagram, :flickr, :tumblr].each { |provider| alias_method provider, :callback }

  def after_omniauth_failure_path_for(scope)
    home_path
  end

  private
    def mobile?
      params[:state].nil? && auth_hash['provider'] == 'instagram'
    end

    def mobile_url(error = false)
      return 'instmodera://' if error
      "instmodera://authorize/?auth_token=#{auth_hash['credentials']['token']}"
    end

    def auth_hash
      ah = request.env["omniauth.auth"]
      hash = {
        uid: ah['uid'],
        provider: ah['provider'],
        token: ah['credentials']['token'],
        secret_token: ah['credentials']['secret'],
        username: ah['info']['nickname'],
        name: ah['info']['name'],
        email: ah['info']['email'],
        url: nil,
        avatar: nil,
        sex: nil
      }

      case ah['provider']
      when 'facebook'
        hash[:url] = ah['info']['urls']["#{ah['provider'].capitalize}"]
        hash[:avatar] = "http://graph.facebook.com/#{ah['uid']}/picture?type=large"
        hash[:sex] = ah['extra']['raw_info']['gender']
      when 'twitter'
        hash[:url] = ah['info']['urls']["#{ah['provider'].capitalize}"]
        hash[:avatar] = "http://api.twitter.com/1/users/profile_image?screen_name=#{ah['info']['nickname']}&size=original"
      when 'instagram'
        hash[:avatar] = ah['info']['image']
        hash[:url] = "http://instagram.com/#{ah['info']['nickname']}"
      end
        
      hash
    end

    def update_user_auth user_auth
      user_auth.token = auth_hash[:token]
      user_auth.secret_token = auth_hash[:secret_token]
      if current_user
        case auth_hash[:provider]
        when 'facebook'
          current_user.update_attributes(facebook: auth_hash[:url])
        when 'twitter'
          current_user.update_attributes(twitter: auth_hash[:url])
        when 'instagram'
          current_user.update_attributes(instagram: auth_hash[:url])
        end
      end
      user_auth.user.save
      user_auth.save
    end

    def redirect_back_or_default(default)
      redirect_url = session['user_return_to'] || default
      session[:user_return_to] = nil
      redirect_to(redirect_url)
    end
end