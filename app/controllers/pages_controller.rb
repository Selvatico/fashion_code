class PagesController < ApplicationController
  skip_before_filter :authenticate_user!, only: :show
  skip_before_filter :style_choosen?

  layout 'static'

  def show
    permalink = [params[:sub], params[:id]].select(&:present?)
    @page = Page.find_by_permalink!(permalink.join('/'))
  end
end
