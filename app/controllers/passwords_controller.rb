class PasswordsController < Devise::PasswordsController
  respond_to :json, :html
  before_filter :set_layout
  skip_before_filter :style_choosen?

  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)

    if successfully_sent?(resource)
      respond_to do |format|
        format.html { respond_with({}, :location => after_sending_reset_password_instructions_path_for(resource_name))}
        format.json { render json: {status: "OK"} }
      end
    else
      respond_to do |format|
        format.html { respond_with({}, :location => after_sending_reset_password_instructions_path_for(resource_name))}
        format.json { render json: {status: "FAIL", message: "User with this email not found!"}, status: 404 }
      end
    end
  end

  def edit
    user = User.find_by_reset_password_token(params[:reset_password_token])
    session[:reset_password_token] = user.nil? ? 'nil' : params[:reset_password_token]
    redirect_to '/#new_password'
  end

  def update
    session[:reset_password_token] = nil
    user = User.find_by_reset_password_token(params[:reset_password_token])
    if user
      user.reset_password!(params[:password], params[:password_confirmation])
      sign_in :user, user
      render json: { redirect_uri: profiles_path(user.username) }, status: 302
    else
      render json: {error: 'User not found!'}, :status => :not_found
    end
  end

  protected
    def after_sending_reset_password_instructions_path_for(resource_name)
      home_path
    end
    
    def set_layout
      self.class.layout(request.xhr? ? false : 'layouts/welcome')
    end
end