class PhotosController < ApplicationController
  protect_from_forgery except: 'upload_photo'
  respond_to :json, only: [:index, :by_tag]
  helper_method :owner?

  def index
    @user = params[:profile_id].present? ? User.where(username: params[:profile_id]).first : current_user
    @view = params[:view] || 'grid'
    if @user
      # @saved_photos = @user.saved_photo_ids
      @photos = @user.photos.active.by_date.page(params[:page])
      @photos_count = @photos.total_count
    end
  end

  def new
    #@photo = current_user.photos.new
  end


  def show
    @photo_id = params[:id]
    @user = User.where(username: params[:profile_id]).first
    @view = 'list'
    params[:page] ||= 1
    @photos = []
    if params[:page].present? && params[:page].to_i == 1
      @photos << @user.photos.active.find(params[:id])
    end
    @user.photos.active.where("id <> ?", params[:id]).order('created_at desc').page(params[:page]).each { |p| @photos << p }
    @photos_count = @user.photos.active.count
    render 'photos/index'
  end

  def create
    @photo = current_user.photos.new
    @photo.tag_list.add(current_user.fashion_style.name.downcase) if current_user.fashion_style.present?
    crop_size = params[:photo_crop_w].to_i > params[:photo_crop_h].to_i ? params[:photo_crop_w].to_i : params[:photo_crop_h].to_i
    if crop_size == 0 || crop_size < params[:photo_min_size].to_i
      @photo.crop_x = 0
      @photo.crop_y = 0
      @photo.crop_h = params[:photo_crop_default]
      @photo.crop_w = params[:photo_crop_default]
    else
      @photo.crop_x = params[:photo_crop_x]
      @photo.crop_y = params[:photo_crop_y]
      @photo.crop_h = crop_size
      @photo.crop_w = crop_size
    end
    # if Rails.env.development? || Rails.env.test?
    #   @photo.image = File.open("#{Rails.root}/public/uploads/tmp/#{params[:photo_image_cache]}", 'r') if params[:photo_image_cache]
    # else
    #   @photo.remote_image_url = params[:photo_image_cache]
    # end
    @photo.image = File.open("#{Rails.root}/public/uploads/tmp/#{params[:photo_image_cache]}", 'r') if params[:photo_image_cache]
    return head(:internal_server_error) unless @photo.save
  end

  def auto_share
    @photo = Photo.find_by_id(params[:id])
    provider = Photo.share_providers[params[:order].to_i]
    if provider
      auth_method = current_user.user_authentications.where(provider: provider).first
      if auth_method
        provider = SocialNetworks::Provider.new(
            {
                name: provider,
                token: auth_method.token,
                token_secret: auth_method.secret_token,
                api_key: Settings.omniauth_keys.send(provider.capitalize).app_token,
                api_key_secret: Settings.omniauth_keys.send(provider.capitalize).app_token_secret
            })
        begin
          provider.share({image_path: @photo.image_url, title: 'Title', share_url: @photo.share_url, own: @photo.user_id==current_user.id})
        rescue
          redirect_to auto_share_photo_path(@photo, params[:order].to_i + 1) and return
        end
      end
      redirect_to auto_share_photo_path(@photo, params[:order].to_i + 1) and return
    else
      redirect_to profile_photo_path(current_user, @photo)
    end
  end

  def comments

  end

  def vote
    photo = Photo.find(params[:id])
    photo.vote_for(current_user)
    head :ok
  end

  def inappropriate
    @photo = Photo.find(params[:id])
    if @photo.user_id == current_user.id
      head 401
    else
      @photo.report_inappropriate_by current_user
      head :ok
    end
  end

  def share
    @photo = Photo.find(params[:id])
    cookies[:user_return_to] = share_photo_path(share: true, provider: params[:provider])

    if params[:share].present?
      auth = current_user.user_authentications.where(provider: params[:provider].downcase).first
      if auth.present?
        provider = SocialNetworks::Provider.new({
          name: params[:provider].downcase,
          token: auth.token,
          token_secret: auth.secret_token,
          api_key: Settings.omniauth_keys.send(params[:provider].capitalize).app_token,
          api_key_secret: Settings.omniauth_keys.send(params[:provider].capitalize).app_token_secret
        })
        begin
          provider.share({
            image_path: @photo.image_url, 
            share_url: @photo.share_url, 
            text: params[:text], 
            uid: auth.uid
          })
          redirect_to share_photo_path(shared: true, provider: params[:provider]) and return
        rescue Exception => e
          Rails.logger.error e.inspect
        end
      end
      redirect_to(user_omniauth_authorize_path(params[:provider].downcase)) and return
    end
    render layout: false
  end

  def upload_photo
    @photo = current_user.photos.new(image: params[:qqfile])

    if @photo.valid? && params[:qqfile].present?
      json = {success: true, image_cache: @photo.image_cache, image_url: @photo.image_url.to_s}.to_json
    else
      json = {success: false}.to_json
    end

    render text: json,:content_type => "text/plain"
  end

  def load_cache
    @photo = Photo.find(params[:id])
    @photo.image = params[:photo][:image_cache]
    @photo.save
    head :ok
  end

  def by_hashtag
    @photos = Photo.active.by_date.includes(:user, :recent_comments).tagged_with(params[:tag]).page(params[:page])
  end

  def voters
    @voters = User.includes(:votes).where('votes.voted_photo_id = ?', params[:id]).page(params[:page])
    @following_ids = current_user.following_ids #TODO: метод following_ids из гема following не должен использоваться в геме core
  end

  #TODO: добавить проверку авторизации, отвечать соответстующим код и текстом ошибки
  def destroy
    @photo = current_user.photos.where(id: params[:id]).first
    if @photo && @photo.active_challenge.nil?
      @photo.delete_file
      head :ok
    else
      head 500
    end
  end

  protected
  def owner?
    current_user.username == @profile.username
  end

end
