  class PowerHourController < ApplicationController
    def index
      @current_user_photo_ids = current_user.photo_ids
      @versuses = current_user.photos_votes.where('NOT votes.challenge_id IS NULL').order('challenge_id desc, created_at desc').page(params[:page]).per(5)
      render 'modera/power_hour/list'
    end

    def list
      @current_user_photo_ids = current_user.photo_ids
      @versuses = current_user.photos_votes.where('NOT votes.challenge_id IS NULL').order('challenge_id desc, created_at desc').page(params[:page]).per(5)
    end

    def new
      redirect_to home_path unless current_user.can_create_challenge?
    end

    def create
      if current_user.can_create_challenge?
        photo = current_user.photos.active.where(id: params[:photo_id]).first
        challenge_type = ChallengeType.where(id: params[:challenge_type_id]).first
        if photo && challenge_type
          score = challenge_type.send("#{params[:level]}_score")
          @challenge = current_user.challenges.build(challenge_type: challenge_type, photo: photo, score: score, mentions: params[:mentions], level: params[:level])
          @challenge.save
        end
      end
      redirect_to challenges_path
    end
  end
