class ProfilesController < ApplicationController
  protect_from_forgery except: 'upload_avatar'
  respond_to :json
  before_filter :load_profile, except: [:suggestions]
  # --------- following ------------
  before_filter :load_following_ids, only: [:followers, :following, :top_voters]
  # --------- end following --------
  helper_method :owner?, :followed_by_you?


  def show
    @share_preferences = @profile.share_preferences

  end

  def edit
  end

  #TODO: проверить нужен ли этот экшн
  def photos
    @photos = @profile.photos.active.by_date.page(params[:page]).per(params[:per_page])
    @photos_count = @photos.total_count
  end

  #TODO: разбораться с ActionView::MissingTemplate
  def friends
    @friends = current_user.friends 'facebook'
    @following =  current_user.following.inject({}){|h, user| h[user.id]=user.username; h}
    @uids = UserAuthentication.where("provider = 'facebook' and user_id in (?)",@following.keys).inject({}){|h,auth| h[auth.uid]=auth.user_id; h}
  end

  def versuses
    user = params[:id] == current_user.username ? current_user : User.find_by_username(params[:id])
    @current_user_photo_ids = user.photo_ids
    @versuses = user.photos_votes.order('votes.created_at desc').page(params[:page]).per(params[:per_page])
  end

  def invite_friends
    title = CGI.escape('Modera invite')
    message = CGI.escape('Try Modera!')
    redirect_url = CGI.escape(profile_url(params[:id]))
    redirect_to "http://www.facebook.com/dialog/apprequests?app_id=#{Settings.omniauth_keys.Facebook.app_token}&redirect_uri=#{redirect_url}&new_style_message=true&title=#{title}&message=#{message}"
  end

  def upload_avatar
    current_user.avatar = params[:qqfile]
    if current_user.save
      render json: {avatar: current_user.avatar_url, avatar_cache: current_user.avatar_cache},:content_type => "text/plain" 
    else
      head :internal_server_error
    end
  end

  def suggestions
    @users = current_user.suggested_followings(10)
  end

  def popular_photos
    # TODO - real popular 3 photos for last 7 days
    @popular_photos = @profile.photos.order('RANDOM()').limit(3)
  end

  def top_voters
    # Newest one vote for each of voters
    votes = Vote.where("voted_photo_id in (:photos) or unvoted_photo_id in (:photos)", photos: current_user.photos)
    .order('voter_id, created_at desc').select('DISTINCT ON (voter_id) id')
    # Top 6 voters
    top_voters_ids = Vote.where(id: votes).order('created_at desc').limit(6).pluck(:voter_id)
    @voters = User.where(id: top_voters_ids)
  end

  #-------- following
  
  def follow
    current_user.follow!(@profile)
    render nothing: true
  end

  def unfollow
    current_user.unfollow!(@profile)
    render nothing: true
  end

  def followers
    @users = @profile.followers.page(params[:page]).per(30)
  end

  def following
    @users = @profile.following.page(params[:page]).per(30)
  end
  # ----------- end following --------

  protected
  def load_profile
    @profile = User.find_by_username!(params[:id]) if params[:id]
  end

  def owner?
    current_user.username == @profile.username
  end

  def set_layout
    self.class.layout(request.xhr? ? false : 'modera/layouts/application')
  end

  def followed_by_you?
    return false if @profile = current_user
    current_user.following_ids.include? @profile.id
  end

  # -------- following ---------
  def load_following_ids
    @following_ids = current_user.following_ids
  end
  #---------- end following ----

end
