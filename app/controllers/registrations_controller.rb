class RegistrationsController < Devise::RegistrationsController
  protect_from_forgery except: :upload_avatar
  prepend_before_filter :require_no_authentication, :only => [ :new, :create, :cancel, :upload_avatar ]
  before_filter :set_layout
  skip_before_filter :style_choosen?

  def new
    @resource = build_resource({username: ''})
    @resource.apply_omniauth(session[:omniauth]) if session[:omniauth]
  end

  def create
    user = User.new(params[:user])
    if session[:omniauth]
      user.apply_omniauth(session[:omniauth])
      user.password = User.generate_pass(6)
    end
    render(json: user.errors, status: 500) and return unless user.save
    session[:omniauth] = nil
    sign_in user
    if session[:invitation_token]
      invite = Invitation.find_by_invitation_token session[:invitation_token]
      if invite
        user.invited_by = invite.user_id
        render(json: user.errors, status: 500) and return unless user.save
        invite.destroy
        session[:invitation_token] = nil
      end
    end
    render json: { csrf_token: form_authenticity_token, redirect_uri: after_sign_up_path_for(user) }, status: 200
  end

  def upload_avatar
    file = params[:qqfile].is_a?(ActionDispatch::Http::UploadedFile) ? params[:qqfile] : params[:file]
    u = User.new(avatar: file)
    u.valid?
    if u.errors.messages[:avatar]
      head 500
    else
      render(content_type: "text/plain", json: {avatar_cache: u.avatar_cache, avatar: u.avatar_url.to_s})
    end
  end


  def change_password
    @user = User.find(current_user.id)
    if @user.update_with_password(params[:user])
      # Sign in the user by passing validation in case his password changed      
      sign_in @user, :bypass => true
      head :ok
    else
      render json: @user.errors, status: 500
    end
  end


  def stored_after_sign_up
    (session[:origin_path] if session[:origin_path]) || choose_style_path
  end


  def sign_up(resource_name, resource)
    sign_in(resource_name, resource)
  end

  protected

  def after_sign_up_path_for(resource)
    '/first-login'
  end

  def set_layout
    self.class.layout(request.xhr? ? false : 'modera/layouts/welcome')
  end
end
