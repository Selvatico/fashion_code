  class SavingsController < ApplicationController
    def index
      @photos = current_user.saved_photos.page(params[:page])
      @photos_count = current_user.savings.count
    end

    def create
      current_user.save_photo!(params[:photo_id])
      render nothing: true
    end

    def destroy
      current_user.delete_saved_photo!(params[:photo_id])
      render nothing: true
    end
  end
