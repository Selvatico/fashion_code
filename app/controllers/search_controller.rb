class SearchController < ApplicationController
  respond_to :json, only: [:users, :tags]

  def users
    @follower_ids = current_user.following_ids
    @users = User.search(params[:q]).includes(:fashion_style).page(params[:page])
  end

  def tags
    tags = ActsAsTaggableOn::Tag.search(params[:q]).page(params[:page])
    @tags = []
    tags.each do |tag|
      photos = Photo.active.tagged_with(tag.name).pluck(:user_id)
      has_followers = (current_user.follower_ids & photos).length > 0 ? true : false
      @tags << {name: tag.name, has_followers: has_followers, users_count: photos.size}
    end
  end

  def following
    @follower_ids = current_user.following_ids
    @users = User.where('id in (?)', @follower_ids).search(params[:q]).includes(:fashion_style).page(params[:page])
    render 'search/users'
  end

  def results
  end
end