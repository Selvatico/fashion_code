class SessionsController < Devise::SessionsController
  respond_to :json, :html
  before_filter :set_layout
  skip_before_filter :verify_authenticity_token
  skip_before_filter :style_choosen?

  def new
    redirect_to "/#signin"
  end

  def create
    resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#new")

    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    result = resource.errors.any? ? {error: resource.errors} :
        { csrf_token: form_authenticity_token, :redirect_uri =>  after_sign_in_path_for(resource) }
    respond_to do |format|
      format.html { respond_with resource,  :location => after_sign_in_path_for(resource) }
      format.json { render json: result }
    end
  end

  def create_by_provider
    #TODO: check user's token
    auth = UserAuthentication.where(provider: params['provider'], uid: params[:uid]).first
    if auth
      sign_in(:user, auth.user)
      auth.update_attributes(token: params['token'], secret_token: params['secret_token'])
      render json: {redirect_uri: after_sign_in_path_for(auth.user)}
    else
      render json: {error: 'Given user not found'}, status: 401
    end
  end


  protected
  def set_layout
    self.class.layout(request.xhr? ? false : 'modera/layouts/welcome')
  end
end
