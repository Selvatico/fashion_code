class SocialPhotosController < ApplicationController
  def index
    auth_method = current_user.user_authentications.where(provider: params[:provider]).first
    provider = SocialNetworks::Provider.new({
      name: params[:provider],
      token: auth_method.token,
      api_key: Settings.omniauth_keys.send(params[:provider].capitalize).app_token,
      api_key_secret: Settings.omniauth_keys.send(params[:provider].capitalize).app_token_secret
    })
    render json: provider.photos
  end

  def ask_auth
    auth_method = current_user.user_authentications.where(provider: params[:provider]).first
    unless auth_method
      session[:redirect_url] = new_photos_url(auth: true)
      redirect_to user_omniauth_authorize_path(params[:provider]) and return
    end
    redirect_to new_photos_path(auth: true)
  end

  def upload_photo
    @photo = current_user.photos.new(remote_image_url: params[:image_url])
    render json: {image_cache: @photo.image_cache, image_url: @photo.image_url.to_s}
  end
end