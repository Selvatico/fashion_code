class SocialRegistrationsController < ApplicationController
  respond_to :html, :json
  layout 'modera/layouts/welcome'
  before_filter :authenticate_user!

  def invitation
    facebook = current_user.user_authentications.where(provider: 'facebook').first
    unless facebook
      session[:redirect_url] = "/#invite_friends"
      redirect_to user_omniauth_authorize_path(:facebook) and return
    end
    redirect_to "/#invite_friends"
  end

  def invite_friends
    if request.post?
      redirect_to profile_path(current_user.username) and return unless params[:to]
      facebook = current_user.user_authentications.where(provider: 'facebook').first
      unless facebook
        session[:redirect_url] = invite_friends_url
        redirect_to user_omniauth_authorize_path(:facebook) and return
      end
      friends = params[:to].split(',')
      redirect_to profile_path(current_user.username) and return if friends.length == 0
      if friends.count > 50
        File.open("#{current_user.id}_invite_friends_tmp.txt", 'w') {|f| f.write("#{params[:to]}") }
        redirect_path = batch_invite_friends_url
      else
        redirect_path = profile_url(current_user.username)
      end
      provider = current_userSocialNetworks::Provider.new({
        name: 'facebook',
        token: facebook.token,
        api_key: Settings.omniauth_keys.Facebook.app_token,
        api_key_secret: Settings.omniauth_keys.Facebook.app_token_secret
      })
      redirect_to provider.invite_friends({
        title: 'Modera invitation',
        message: 'Try Modera!',
        to: friends[0..49].join(','),
        redirect_url: redirect_path
      }) and return
    end
    @friends = current_user.friends "facebook"
  end

  def batch_invite_friends
    friends = File.read("#{current_user.id}_invite_friends_tmp.txt")
    friends = friends.split(',')
    friends.slice!(0, 50)
    if friends.empty?
      File.delete("#{current_user.id}_invite_friends_tmp.txt")
      redirect_to profile_path(current_user.username) and return
    else
      File.open("#{current_user.id}_invite_friends_tmp.txt", 'w') {|f| f.write(friends.join(',')) }
      facebook = current_user.user_authentications.where(provider: 'facebook').first
      provider = current_userSocialNetworks::Provider.new({
        name: 'facebook',
        token: facebook.token,
        api_key: Settings.omniauth_keys.Facebook.app_token,
        api_key_secret: Settings.omniauth_keys.Facebook.app_token_secret
      })
      redirect_to provider.invite_friends({
        title: 'Modera invitation',
        message: 'Try Modera!',
        to: friends[0..49].join(','),
        redirect_url: batch_invite_friends_url
      }) and return
    end
  end
end