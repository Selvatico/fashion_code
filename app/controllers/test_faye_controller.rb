class TestFayeController < ApplicationController
  def send_stats
    ::PrivatePub.publish_to("/notifications/#{current_user.id}", stats: current_user.notification_stats)
    head :ok
  end
end
