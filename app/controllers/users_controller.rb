class UsersController < ApplicationController
  skip_before_filter :style_choosen?, only: [:choose_style]
  #skip_before_filter :verify_authenticity_token

  def choose_style
    session[:origin_path] = invitation_path if current_user.fashion_style.nil? && session[:origin_path].nil?
    fashion_style = FashionStyle.find(params[:fashion_style_id])
    current_user.update_without_password(fashion_style: fashion_style)
    render json: {redirect_uri: after_sign_up_path_for(current_user)}
  end

  def invite_friends
  end

  def oauth
    if params[:done].nil?
      auth = current_user.user_authentications.where(provider: params[:provider]).first
      if auth.nil?
        session[:user_return_to] = oauth_users_path(provider: params[:provider], done: true)
        redirect_to user_omniauth_authorize_path(params[:provider]) and return
      end
      # auth.destroy
      redirect_to(oauth_users_path(provider: params[:provider], done: true)) and return
    end
    render layout: false
  end

  def avatar
    if params[:done].nil?
      unless params[:omni]
        session[:user_return_to] = social_avatar_users_path(provider: params[:provider], omni: true)
        redirect_to user_omniauth_authorize_path(params[:provider]) and return
      end
      if session[:omniauth]
        if session[:omniauth][:provider] == 'twitter'
          uri = URI.parse(session[:omniauth][:avatar])
          res = Net::HTTP.start(uri.host, uri.port) { |http| http.get("#{uri.path}?#{uri.query}") }
          io = open(res['location'])
          def io.original_filename
            File.basename(base_uri.path)
          end
          current_user.avatar = io
        else
          io = open(session[:omniauth][:avatar])
          def io.original_filename
            File.basename(base_uri.path)
          end
          current_user.avatar = io
        end
        current_user.save
      end
      redirect_to(social_avatar_users_path(provider: params[:provider], done: true)) and return
    end
    render layout: false
  end

  def update
    @errors = {}
    user = current_user
    user.attributes = params[:user].except(:birth)
    user.set_birth_date(params[:user][:birth]) if params[:user][:birth]
    if user.save
      if params[:user][:password]
        sign_in user, bypass: true
      end
      head :ok
    else
      user.errors.each { |attribute, errors_array| @errors[attribute] = errors_array }
      render json: @errors.to_json, status: 500
    end
  end



  def share_preferences
    current_user.share_preferences.update_attributes(params[:share_preferences])
  end
end