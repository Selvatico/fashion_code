class VersusesController < ApplicationController
  before_filter :load_saved_photos, except: :vote

  def index
    @following_ids = current_user.following_ids
    @versuses = current_user.photo_versus_list(params[:offset].to_i)
  end

  def style
    if params[:challenge_id]
      @versuses = VsTopPhoto.trial_simple(10, params[:used_users], current_user.id, params[:challenge_id])
    else
      @versuses = VsTopPhoto.trial_simple(10, params[:used_users], current_user.id)
    end
  end

  def friends
    @versuses = VsTopPhoto.trial_followings_home(10, current_user.id)
    # render 'style'
  end


  def vote
    voted_photo = Photo.active.where( id: params[:voted_photo_id]).first
    unvoted_photo = Photo.active.where( id: params[:unvoted_photo_id]).first
    challenge = Challenge.where( id: params[:challenge_id]).first
    hashtag_contest = HashtagContest.where(name: params[:hashtag_id]).first

    if current_user.vote_photo(voted_photo, unvoted_photo, challenge, hashtag_contest)
      render nothing: true
    else
      render nothing: true, status: 500
    end
  end

  def by_hashtag
    @versuses = []
    @contest = HashtagContest.find_by_name(params[:tag].downcase)
    @photos_count = Photo.tagged_with(params[:tag].downcase).count
    @users_count = @contest ? @contest.participants.includes(:user).count : 0
    photos = Photo.active.tagged_with(params[:tag]).where('not photos.user_id = ?', current_user.id).all
    photos_size = photos.size
    quantity = params[:count].nil? ? 10 : params[:count].to_i
    @following_ids = current_user.following_ids
    used_photos = []
    i = 0
    while i < quantity && used_photos.size < photos_size
      @versuses.push([])
      while @versuses.last.size < 1 && used_photos.size < photos_size
        random_index = rand(0..photos_size-1)
        unless used_photos.include?(random_index)
          @versuses.last.push(photos[random_index])
          used_photos.push(random_index)
        end
      end
      while @versuses.last.size < 2 && used_photos.size < photos_size
        random_index = rand(0..photos_size-1)
        unless used_photos.include?(random_index) || @versuses.last[0].user_id == photos[random_index].user_id
          @versuses.last.push(photos[random_index])
          used_photos.push(random_index)
        end
      end
      i = i+1
    end
    @versuses.delete(@versuses.last) if @versuses.last && @versuses.last.size < 2
  end

  protected
    def load_saved_photos
      @saved_photos = current_user.saved_photo_ids
    end
end