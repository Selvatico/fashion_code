class Web::Admin::ApplicationController < ApplicationController
  before_filter :authenticate_user!
  before_filter do
    raise ActionController::RoutingError.new('Not Found')  unless current_user && current_user.admin?
  end
end
