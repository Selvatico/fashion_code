class Web::Admin::BotChallengesBotSettingsController < Web::Admin::ApplicationController
  before_filter :load_resource, only: [:edit, :update]
  respond_to :html

  def index
  end

  def edit
  end

  def update
    if @bot_challenges_bot_setting.save(params[:bot_challenges_bot_setting])
      redirect_to admin_bot_challenges_bot_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  protected

  def load_resource
    @bot_challenges_bot_setting = Admin::BotChallengesBotSetting.new
    @bot_challenges_bot_setting.fill_fields(Settings.bot_challenges_bots)
  end
end