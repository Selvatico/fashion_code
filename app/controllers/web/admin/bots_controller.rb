class Web::Admin::BotsController < Web::Admin::ApplicationController
  def interest
    @users = User.real.all
  end

  def set_interest
    for j in 1..params[:votes_quantity].to_i
      rand_time = rand(0..params[:minutes].to_i)
      Resque.enqueue_in(rand_time.minutes, BotsWorker, 'like_user_photo', {user_id: params[:user_id]})
    end
    for j in 1..params[:follows_quantity].to_i
      rand_time = rand(0..params[:minutes].to_i)
      Resque.enqueue_in(rand_time.minutes, BotsWorker, 'follow', {user_id: params[:user_id]})
    end
    redirect_to interest_admin_bots_path, notice: 'Set'
  end
end