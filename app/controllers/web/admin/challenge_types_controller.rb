class Web::Admin::ChallengeTypesController < Web::Admin::ApplicationController
  def index
    @challenge_types = ChallengeType.all
  end

  def show
    @challenge_type = ChallengeType.find params[:id]
  end

  def new
    @challenge_type = ChallengeType.new
  end

  def create
    @challenge_type = ChallengeType.create params[:challenge_type]
    respond_with :admin, @challenge_type
  end

  def edit
    @challenge_type = ChallengeType.find params[:id]
  end

  def update
    @challenge_type = ChallengeType.find params[:id]
    @challenge_type.update_attributes params[:challenge_type]
    respond_with :admin, @challenge_type
  end

  def destroy
    @challenge_type = ChallengeType.destroy params[:id]
    respond_with :admin, @challenge_type
  end
end