class Web::Admin::ChallengesBotSettingsController < Web::Admin::ApplicationController
  before_filter :load_periods, only: [:edit, :update]
  respond_to :html

  def index
  end

  def new
    @challenges_bot_setting = Admin::ChallengesBotSetting.new
  end

  def create
    @challenges_bot_setting = Admin::ChallengesBotSetting.new
    if @challenges_bot_setting.save_period(params[:challenges_bot_setting])
      redirect_to admin_challenges_bot_settings_path, notice: 'Successfully created'
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if Admin::ChallengesBotSetting.save(@periods, params[:periods]) && Admin::ChallengesBotSetting.save(@bot_periods, params[:bot_periods])
      redirect_to admin_challenges_bot_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    Admin::ChallengesBotSetting.erase_last_period!(params[:scope])
    redirect_to admin_challenges_bot_settings_path, notice: 'Successfully deleted'
  end

  protected

  def load_periods
    @periods = []
    Settings.challenges_bots.periods.each do |period, value|
      p = Admin::ChallengesBotSetting.new
      p.fill_fields(value)
      @periods.push(p)
    end
    @bot_periods = []
    Settings.challenges_bots.bot_periods.each do |period, value|
      p = Admin::ChallengesBotSetting.new
      p.fill_fields(value)
      @bot_periods.push(p)
    end
  end
end