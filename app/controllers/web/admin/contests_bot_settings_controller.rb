class Web::Admin::ContestsBotSettingsController < Web::Admin::ApplicationController
  before_filter :load_periods, only: [:edit, :update]
  respond_to :html

  def index
  end

  def new
    @contests_bot_setting = ::Admin::ContestsBotSetting.new
  end

  def create
    @contests_bot_setting = ::Admin::ContestsBotSetting.new
    if @contests_bot_setting.save_period(params[:contests_bot_setting])
      redirect_to admin_contests_bot_settings_path, notice: 'Successfully created'
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if ::Admin::ContestsBotSetting.save(@periods, params[:periods])
      redirect_to admin_contests_bot_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    ::Admin::ContestsBotSetting.erase_last_period!
    redirect_to admin_contests_bot_settings_path, notice: 'Successfully deleted'
  end

  protected

  def load_periods
    @periods = []
    Settings.contests_bots.periods.each do |period, value|
      p = ::Admin::ContestsBotSetting.new
      p.fill_fields(value)
      @periods.push(p)
    end
  end
end