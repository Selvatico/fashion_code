class Web::Admin::ContestsController < Web::Admin::ApplicationController
  def index
    @contests = Contest.page(params[:page])
  end

  def show
    @contest = Contest.find params[:id]
  end

  def new
    @contest = Contest.new
  end

  def create
    @contest = Contest.create params[:contest]
    respond_with :admin, @contest
  end

  def edit
    @contest = Contest.find params[:id]
  end

  def update
    @contest = Contest.find params[:id]
    @contest.update_attributes params[:contest]
    respond_with :admin, @contest
  end

  def destroy
    @contest = Contest.destroy params[:id]
    respond_with :admin, @contest
  end

  def add_participants
    @not_participants = @contest.user_ids.empty? ? User.bots.all : User.bots.where('not users.id in (?)', @contest.user_ids).all
  end

  def create_participants
    if params[:contest][:batch_participants]
      users = params[:contest][:batch_participants]=='bots' ? User.bots.all : (params[:contest][:batch_participants]=='users' ? User.real.all : User.all)
      users.each do |b|
        unless Participant.where(contest_id: @contest.id, user_id: b.id).exists?
          p = @contest.participants.new
          p.user = b
        end
      end
    elsif params[:contest][:bots_list]
      params[:contest][:bots_list].each do |b|
        p = @contest.participants.new
        p.user_id = b
      end
    end
    @contest.save
    redirect_to [:admin, @contest], notice: 'Successfully updated'
  end

  def delete_participant
    @contest.participants.find(params[:participant_id]).destroy
    redirect_to [:admin, @contest]
  end
end