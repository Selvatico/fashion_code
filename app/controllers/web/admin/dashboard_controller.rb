class Web::Admin::DashboardController < Web::Admin::ApplicationController
  def index
    render 'web/admin/application/dashboard'
  end
end