class Web::Admin::FashionStylesController < Web::Admin::ApplicationController
  def index
    @fashion_styles = FashionStyle.all
  end

  def show
    @fashion_style = FashionStyle.find params[:id]
  end

  def new
    @fashion_style = FashionStyle.new
  end

  def create
    @fashion_style = FashionStyle.create params[:fashion_style]
    respond_with :admin, @fashion_style
  end

  def edit
    @fashion_style = FashionStyle.find params[:id]
  end

  def update
    @fashion_style = FashionStyle.find params[:id]
    @fashion_style.update_attributes params[:fashion_style]
    respond_with :admin, @fashion_style
  end

  def destroy
    @fashion_style = FashionStyle.destroy params[:id]
    respond_with :admin, @fashion_style
  end
end