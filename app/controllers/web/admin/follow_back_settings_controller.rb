class Web::Admin::FollowBackSettingsController < Web::Admin::ApplicationController
  before_filter :load_resource, only: [:edit, :update]
  respond_to :html

  def index
  end

  def edit
  end

  def update
    if @follow_back_setting.save(params[:follow_back_setting])
      redirect_to admin_follow_back_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  protected

  def load_resource
    @follow_back_setting = ::Admin::FollowBackSetting.new
    @follow_back_setting.fill_fields(Settings.follow_back_bots)
  end
end