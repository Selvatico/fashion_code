class Web::Admin::HomeActivitySettingsController < Web::Admin::ApplicationController
  before_filter :load_periods, only: [:edit, :update]
  respond_to :html

  def index
  end

  def new
    @home_activity_setting = ::Admin::HomeActivitySetting.new
  end

  def create
    @home_activity_setting = ::Admin::HomeActivitySetting.new
    if @home_activity_setting.save_period(params[:home_activity_setting])
      redirect_to admin_home_activity_settings_path, notice: 'Successfully created'
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if ::Admin::HomeActivitySetting.save(@periods, params[:periods])
      redirect_to admin_home_activity_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    ::Admin::HomeActivitySetting.erase_last_period!
    redirect_to admin_home_activity_settings_path, notice: 'Successfully deleted'
  end

  protected

  def load_periods
    @periods = []
    Settings.home_activity_bots.periods.each do |period, value|
      p = ::Admin::HomeActivitySetting.new
      p.fill_fields(value)
      @periods.push(p)
    end
  end
end