class Web::Admin::InappropriatesController < Web::Admin::ApplicationController

  def index
    @inappropriates = Inappropriate.all
  end

  def show
    @inappropriate = Inappropriate.find_by_id(params[:id])
    @object = @inappropriate.inappropriateable
  end

  def destroy_photo
    @inappropriate = Inappropriate.find_by_id(params[:id])
    @object = @inappropriate.inappropriateable
    owner = @object.user
    if @object.active_challenge.nil?
      Emailer.inappropriate_photo(@object, owner).deliver!

      @object.delete_file
      owner.votes_score = (owner.votes_score - @object.votes_score) > 0 ? (owner.votes_score - @object.votes_score) : 0
       owner.save!
       owner.participants.each do |p|
         p.votes_score = (p.votes_score - @object.votes_score) > 0 ? (p.votes_score - @object.votes_score) : 0
         p.save!
       end
      #TODO FIX ME!!!  I don't understand this code.
      #send notification mail to owner  # this is code!
      redirect_to admin_inappropriates_path, notice: 'Successfully destroyed'
    else
      redirect_to admin_inappropriate_path(@inappropriate), notice: 'Photo is in challenge'
    end
  end

  def destroy_comment
    @inappropriate = Inappropriate.find_by_id(params[:id])
    @object = @inappropriate.inappropriateable
    @owner = @object.user
    @receiver = @object.photo.user

    Emailer.inappropriate_comment(@object, @owner, @receiver).deliver!
    @object.destroy
    redirect_to admin_inappropriates_path, notice: 'Successfully destroyed'
  end
end