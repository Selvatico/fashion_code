class Web::Admin::MembersController < Web::Admin::ApplicationController
  def index
    @members = Member.all
  end

  def show
    @member = Member.find params[:id]
    @questionaries = @member.questionaries.all
  end

  def new
    @member = Member.new
  end

  def create
    @member = Member.create params[:member]
    respond_with :admin, @member
  end

  def edit
    @member = Member.find params[:id]
  end

  def update
    @member = Member.find params[:id]
    @member.update_attributes params[:member]
    respond_with :admin, @member
  end

  #def destroy
  #  @member = Member.destroy params[:id]
  #  respond_with :admin, @member
  #end
end