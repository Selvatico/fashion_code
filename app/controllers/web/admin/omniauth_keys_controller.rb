class Web::Admin::OmniauthKeysController < Web::Admin::ApplicationController
  before_filter :load_resource, only: [:edit, :update, :destroy]
  respond_to :html

  def index
    @omniauth_keys = ::Admin::OmniauthKey.all
  end

  def new
    @omniauth_key = ::Admin::OmniauthKey.new
  end

  def create
    @omniauth_key = ::Admin::OmniauthKey.new
    if @omniauth_key.save(params[:omniauth_key])
      redirect_to admin_omniauth_keys_path, notice: 'Successfully created'
    else
      flash[:error] = @omniauth_key.errors.messages
      render 'new'
    end
  end

  def edit
  end

  def update
    if @omniauth_key.save(params[:omniauth_key])
      redirect_to admin_omniauth_keys_path, notice: 'Successfully updated'
    else
      flash[:error] = @omniauth_key.errors.messages
      render 'edit'
    end
  end

  def destroy
    @omniauth_key.erase!
    redirect_to admin_omniauth_keys_path, notice: 'Successfully destroyed'
  end

  protected

  def load_resource
    @omniauth_key = ::Admin::OmniauthKey.find(params[:id])
  end
end