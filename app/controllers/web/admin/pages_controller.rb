class Web::Admin::PagesController < Web::Admin::ApplicationController
  def index
    @pages = Page.all
  end

  def show
    @page = Page.find params[:id]
  end

  def new
    @page = Page.new
  end

  def create
    @page = Page.create params[:page]
    respond_with :admin, @page
  end

  def edit
    @page = Page.find params[:id]
  end

  def update
    @page = Page.find params[:id]
    @page.update_attributes params[:page]
    respond_with :admin, @page
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
    redirect_to admin_pages_url
  end
end
