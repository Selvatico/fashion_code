class Web::Admin::PhotosBotSettingsController < Web::Admin::ApplicationController
  before_filter :load_periods, only: [:edit, :update]
  respond_to :html

  def index
  end

  def new
    @photos_bot_setting = ::Admin::PhotosBotSetting.new
  end

  def create
    @photos_bot_setting = ::Admin::PhotosBotSetting.new
    if @photos_bot_setting.save_period(params[:photos_bot_setting])
      redirect_to admin_photos_bot_settings_path, notice: 'Successfully created'
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if ::Admin::PhotosBotSetting.save(@periods, params[:periods])
      redirect_to admin_photos_bot_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  def destroy
    ::Admin::PhotosBotSetting.erase_last_period!
    redirect_to admin_photos_bot_settings_path, notice: 'Successfully deleted'
  end

  protected

  def load_periods
    @periods = []
    Settings.photos_bots.periods.each do |period, value|
      p = ::Admin::PhotosBotSetting.new
      p.fill_fields(value)
      @periods.push(p)
    end
  end
end