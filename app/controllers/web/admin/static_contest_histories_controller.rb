class Web::Admin::StaticContestHistoriesController < Web::Admin::ApplicationController
  def index
    @static_contest_histories = StaticContestHistory.page(params[:page])
  end

  def show
    @static_contest_history = StaticContestHistory.find params[:id]
  end

  def new
    @static_contest_history = StaticContestHistory.new
  end

  def create
    @static_contest_history = StaticContestHistory.create params[:static_contest_history]
    respond_with :admin, @static_contest_history
  end

  def edit
    @static_contest_history = StaticContestHistory.find params[:id]
  end

  def update
    @static_contest_history = StaticContestHistory.find params[:id]
    @static_contest_history.update_attributes params[:static_contest_history]
    respond_with :admin, @static_contest_history
  end

  def destroy
    @static_contest_history = StaticContestHistory.destroy params[:id]
    respond_with :admin, @static_contest_history
  end
end