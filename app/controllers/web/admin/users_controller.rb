class Web::Admin::UsersController < Web::Admin::ApplicationController
  def index
    if params[:username]
      @users = User.where('username LIKE ?', "%#{params[:username]}%").order('created_at desc').page(params[:page])
    else
      @users = User.order('created_at desc').page(params[:page])
    end
  end

  def show
    @user = User.find params[:id]
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user].except(:role))
    @user.role = params[:user][:role]
    @user.save
    respond_with :admin, @user
  end

  def edit
    @user = User.find params[:id]
  end

  def update
    @user = User.find params[:id]
    @user.role = params[:user][:role]
    @user.update_attributes params[:user].except(:role)
    respond_with :admin, @user
  end

  def destroy
    @user = User.destroy params[:id]
    respond_with :admin, @user
  end

  def delete_avatar
    @user.remove_avatar!
    render 'edit'
  end

  def block
    @user.block!
    redirect_to admin_users_path, notice: 'Successfully updated'
  end
end
