class Web::Admin::VsTopSettingsController < Web::Admin::ApplicationController
  before_filter :load_resource, only: [:edit, :update]
  respond_to :html

  def index
  end

  def edit
  end

  def update
    if @vs_top_setting.save(params[:vs_top_setting])
      redirect_to admin_vs_top_settings_path, notice: 'Successfully updated'
    else
      render 'edit'
    end
  end

  protected

  def load_resource
    @vs_top_setting = ::Admin::VsTopSetting.new
    @vs_top_setting.fill_fields(Settings.vs_ranking)
  end
end
