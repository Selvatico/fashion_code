module ApplicationHelper
  def nav_link_to(link_text, link_path)
    class_name = current_page?(link_path) ? 'active' : ''
    link_to link_text, link_path, class: class_name
  end

  def linking(text)
    username_matches = text.scan(/(@([a-zA-Z\d_]{3,50}))/)
    username_matches.each do |match|
      user = User.where('username ILIKE ?', match[1]).first
      text = text.gsub(match[0], '<a href=\'/profiles/'+user.username+'\'>'+match[0]+'</a>') if user
    end
    text.gsub(/(#((?!x27)[A-Za-z0-9]+))/, '<a href=\'/home/#/hashtag_contests/\2\'>\1</a>')
  end

  def small_number(number)
    number > 1000 ? number_to_human(number, units: {thousand: 'k'}, precision: 1, significant: false) : number
  end

  def current_user_info
    result = {}
    result[:current_user] = { 
      username: current_user.username,
      name: current_user.name,
      avatar: current_user.avatar.medium.to_s,
      fashion_style: current_user.fashion_style.nil? ? '' : current_user.fashion_style.name
    }.to_json if current_user
    if session[:omniauth]
      user = Modera::User.new({username: ''})
      user.apply_omniauth(session[:omniauth])
      result[:social_user_info] = {
        username: user.username,
        name: user.name,
        provider: session[:omniauth][:provider],
        email: user.email,
        avatar: user.avatar_url,
        avatar_cache: user.avatar_cache
      }.to_json
    end
    result
  end
end