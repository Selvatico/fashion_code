# encoding: utf-8

class Emailer < ActionMailer::Base
  layout 'layouts/mail'


  def welcome(user)
    @user = User.find(user)
    setup_email('Modera <noreply@modera.co>')
    mail(:to => @user.email, :subject => "Welcome to Modera")
  end

  def grow_up(user)
    @user = User.find(user)
    setup_email('Modera <noreply@modera.co>')
    mail(:to => @user.email, :subject => "You aren’t a newbie anymore!")
  end


  def inappropriate_photo(object, owner)
    setup_email('Modera <noreply@modera.co>')
    @photo = object
    @user = owner
    attachments.inline['photo.jpg'] = File.read(Rails.root.join(@photo.image.small.path))
    mail(:to => @user.email, :subject => "Inappropriate Content")
  end

  def inappropriate_comment(object, owner, receiver)
    setup_email('Modera <noreply@modera.co>')
    @object = object
    @user = owner
    @receiver = receiver
    mail(:to => @user.email, :subject => "Inappropriate Content")
  end

  def daily_notifications(receiver, commented, mentioned, followers, facebook)
    @receiver = receiver
    @commented = commented
    @mentioned = mentioned
    @followers = followers
    @facebook = facebook
    setup_email('Modera <noreply@modera.co>')
    mail(:to => receiver.email, :subject => 'New comment')
  end


  def invitation(email, token, inviter)
    @email = email
    @token = token
    @inviter = User.find(inviter)
    setup_email('Modera <noreply@modera.co>')
    mail(:to => @email, :subject => "Welcome to Modera") do |format|
      format.html { render layout: false }
    end
  end

  def new_comment(receiver, actor, comment)
    Rails.logger.debug "Email: send new_comment to: #{receiver.email}"
    @comment = comment
    @receiver = receiver
    @actor = actor
    setup_email('Modera <noreply@modera.co>')
    mail(:to => receiver.email, :subject => 'New Comment')
  end

  def mentioned(receiver, actor, comment)
    Rails.logger.debug "Email: send mentioned to: #{receiver.email}"
    @comment = comment
    @receiver = receiver
    @actor = actor
    setup_email('Modera <noreply@modera.co>')
    mail(:to => receiver.email, :subject => 'You have been @mentioned')
  end

  def new_follower(receiver, actor)
    Rails.logger.debug "Email: send new_follower to: #{receiver.email}"
    @receiver = receiver
    @actor = actor
    setup_email('Modera <noreply@modera.co>')
    mail(:to => receiver.email, :subject => 'New follower')
  end

  def new_fb_friend(receiver, actor)
    Rails.logger.debug "Email: send new_fb_friend to: #{receiver.email}"
    @receiver = receiver
    @actor = actor
    setup_email('Modera <noreply@modera.co>')
    mail(:to => receiver.email, :subject => 'Facebook Friend joined')
  end

  #-------- auth
  include Devise::Mailers::Helpers

  def confirmation_instructions(record, opts={})
    devise_mail(record, :confirmation_instructions, opts)
  end

  def reset_password_instructions(record, opts={})
    @skip_unsubscribe = true
    devise_mail(record, :reset_password_instructions, opts)
    @user = @resource
  end

  def unlock_instructions(record, opts={})
    devise_mail(record, :unlock_instructions, opts)
  end
  #------------

  protected
  def setup_email(sender)
    headers['Reply-To'] = sender
    headers['Return-Path'] = sender
    headers['Organization'] = 'Tebecom Inc.'
    headers['Content-type'] = 'text/html'
    headers['Sender'] = sender
    headers['From'] = sender
  end
end
