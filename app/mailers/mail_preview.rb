if defined? MailView
  class MailPreview < MailView
    # Pull data from existing fixtures
    def daily_notifications
      user = User.order('RANDOM()').first
      commented = {
          users: User.where('id <> ?', user.id).order('RANDOM()').limit(16)
          #comment: Comment.order('RANDOM()').first
      }
      mentioned = {
          users: User.where('id <> ?', user.id).order('RANDOM()').limit(1)
          #comment: Comment.order('RANDOM()').first
      }
      followers =  {
          users: User.where('id <> ?', user.id).order('RANDOM()').limit(10)
          #comment: Comment.order('RANDOM()').first
      }
      facebook = {
          users: User.where('id <> ?', user.id).order('RANDOM()').limit(2)
          #comment: Comment.order('RANDOM()').first
      }
      Emailer.daily_notifications(user, commented, mentioned, followers, facebook)
    end

    def new_comment
      user = User.order('RANDOM()').first
      actor = User.where('id <> ?', user.id).order('RANDOM()').first
      comment = Comment.order('RANDOM()').first
      Emailer.new_comment(user, actor, comment)
    end

    def mentioned
      user = User.order('RANDOM()').first
      actor = User.where('id <> ?', user.id).order('RANDOM()').first
      comment = Comment.order('RANDOM()').first
      Emailer.mentioned(user, actor, comment)
    end

    def new_fb_friend
      user = User.order('RANDOM()').first
      actor = User.where('id <> ?', user.id).order('RANDOM()').first
      Emailer.new_fb_friend(user, actor)
    end

    def new_follower
      user = User.order('RANDOM()').first
      actor = User.where('id <> ?', user.id).order('RANDOM()').first
      Emailer.new_follower(user, actor)
    end

    def inappropriate_photo
      photo = Photo.active.order('RANDOM()').first
      Emailer.inappropriate_photo(photo, photo.user)
    end

    # Stub-like
    #def forgot_password
    #  user = Struct.new(:email, :name).new('name@example.com', 'Jill Smith')
    #  mail = UserMailer.forgot_password(user)
    #end
  end
end