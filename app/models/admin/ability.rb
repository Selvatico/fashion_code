class Admin::Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :index, :dashboard
    end
  end
end