class Admin::BotChallengesBotSetting < Admin::SettingBase
  attr_accessor :max_time, :min_number, :max_number

  validates_each :max_time, :min_number, :max_number do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/bot_challenges_bots.yml"
  end

  def save(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      Settings.bot_challenges_bots = to_write
      self.write!({:bot_challenges_bots => Settings.bot_challenges_bots.to_hash})
      true
    else
      false
    end
  end
end