class Admin::ChallengesBotSetting < Admin::SettingBase
  attr_accessor :min_time, :max_time, :min_unvotes, :max_unvotes, :min_votes, :max_votes, :bots

  validates_each :min_time, :max_time, :min_unvotes, :max_unvotes, :min_votes, :max_votes do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/challenges_bots.yml"
  end

  def self.save(periods, options={})
    to_write = {}
    validation = true
    i = 0
    options.each do |period, value|
      to_write["#{period}"] = {}
      p = Admin::ChallengesBotSetting.new
      filling = p.fill_fields(value, to_write["#{period}"])
      validation = validation && filling
      periods[i] = p
      i = i + 1
    end
    if validation
      p = Admin::ChallengesBotSetting.new
      pref = options.keys[0].to_s.include?('bot_') ? 'bot_' : ''
      Settings.challenges_bots.send("#{pref}periods=", to_write)
      p.write!({:challenges_bots => Settings.challenges_bots.to_hash})
      true
    else
      false
    end
  end

  def save_period(options={})
    to_write = {}
    pref = options[:bots] ? 'bot_' : ''
    if self.fill_fields(options.except(:bots), to_write)
      count = Settings.challenges_bots.send("#{pref}periods").count
      Settings.challenges_bots.send("#{pref}periods").send("#{pref}period#{count+1}=", to_write.except(:bots))
      self.write!({:challenges_bots => Settings.challenges_bots.to_hash})
      true
    else
      false
    end
  end

  def self.erase_last_period!(scope)
    pref = (scope=='bots') ? 'bot_' : ''
    for_delete = Settings.challenges_bots.send("#{pref}periods").to_hash
    count = Settings.challenges_bots.send("#{pref}periods").count
    for_delete.delete("#{pref}period#{count}".to_sym)
    Settings.challenges_bots.send("#{pref}periods=", for_delete)
    p = Admin::ChallengesBotSetting.new
    p.write!({:challenges_bots => Settings.challenges_bots.to_hash})
  end
end