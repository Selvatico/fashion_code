class Admin::ContestsBotSetting < Admin::SettingBase
  attr_accessor :min_time, :max_time, :min_follows, :max_follows, :min_votes, :max_votes, :min_bot_follows, :max_bot_follows, :min_bot_votes, :max_bot_votes

  validates_each :min_time, :max_time, :min_follows, :max_follows, :min_votes, :max_votes, :min_bot_follows, :max_bot_follows, :min_bot_votes, :max_bot_votes do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/contests_bots.yml"
  end

  def self.save(periods, options={})
    to_write = {}
    validation = true
    i = 0
    options.each do |period, value|
      to_write["#{period}"] = {}
      p = Admin::ContestsBotSetting.new
      filling = p.fill_fields(value, to_write["#{period}"])
      validation = validation && filling
      periods[i] = p
      i = i + 1
    end
    if validation
      p = Admin::ContestsBotSetting.new
      Settings.contests_bots.periods = to_write
      p.write!({:contests_bots => Settings.contests_bots.to_hash})
      true
    else
      false
    end
  end

  def save_period(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      count = Settings.contests_bots.periods.count
      Settings.contests_bots.periods.send("period#{count+1}=", to_write)
      self.write!({:contests_bots => Settings.contests_bots.to_hash})
      true
    else
      false
    end
  end

  def self.erase_last_period!
    to_write = Settings.contests_bots.periods.to_hash
    count = Settings.contests_bots.periods.count
    to_write.delete("period#{count}".to_sym)
    p = Admin::ContestsBotSetting.new
    p.write!({:contests_bots => {:periods => to_write}})
  end
end