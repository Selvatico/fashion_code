class Admin::FollowBackSetting < Admin::SettingBase
  attr_accessor :min_time, :max_time, :min_percent, :max_percent

  validates_each :min_time, :max_time, :min_percent, :max_percent do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/follow_back_bots.yml"
  end

  def save(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      Settings.follow_back_bots = to_write
      self.write!({:follow_back_bots => Settings.follow_back_bots.to_hash})
      true
    else
      false
    end
  end
end