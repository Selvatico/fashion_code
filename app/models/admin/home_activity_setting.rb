class Admin::HomeActivitySetting < Admin::SettingBase
  attr_accessor :min_time, :max_time, :min_bot_votes, :max_bot_votes, :min_bot_follows, :max_bot_follows, :min_votes, :max_votes, :min_follows, :max_follows, :like_percent

  validates_each :min_time, :max_time, :min_bot_votes, :max_bot_votes, :min_bot_follows, :max_bot_follows, :min_votes, :max_votes, :min_follows, :max_follows, :like_percent do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/home_activity_bots.yml"
  end
  
  def self.save(periods, options={})
    to_write = {}
    validation = true
    i = 0
    options.each do |period, value|
      to_write["#{period}"] = {}
      p = Admin::HomeActivitySetting.new
      filling = p.fill_fields(value, to_write["#{period}"])
      validation = validation && filling
      periods[i] = p
      i = i + 1
    end
    if validation
      p = Admin::HomeActivitySetting.new
      Settings.home_activity_bots.send("periods=", to_write)
      p.write!({:home_activity_bots => Settings.home_activity_bots.to_hash})
      true
    else
      false
    end
  end

  def save_period(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      count = Settings.home_activity_bots.send("periods").count
      Settings.home_activity_bots.send("periods").send("period#{count+1}=", to_write)
      self.write!({:home_activity_bots => Settings.home_activity_bots.to_hash})
      true
    else
      false
    end
  end

  def self.erase_last_period!
    for_delete = Settings.home_activity_bots.send("periods").to_hash
    count = Settings.home_activity_bots.send("periods").count
    for_delete.delete("period#{count}".to_sym)
    Settings.home_activity_bots.send("periods=", for_delete)
    p = Admin::ChallengesBotSetting.new
    p.write!({:home_activity_bots => Settings.home_activity_bots.to_hash})
  end
end