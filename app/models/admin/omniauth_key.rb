class Admin::OmniauthKey < Admin::SettingBase
  attr_accessor :provider, :app_token, :app_token_secret, :auth

  validates_each :provider, :app_token, :app_token_secret do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/omniauth_keys.yml"
  end

  def initialize
    self.auth = false
  end

  def save(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      Settings.omniauth_keys.send("#{self.provider}=", to_write.merge({:auth => self.auth=='true'}))
      self.write!({:omniauth_keys => Settings.omniauth_keys.to_hash})
      true
    else
      false
    end
  end

  def erase!
    to_write = Settings.omniauth_keys.to_hash
    to_write.delete(self.provider.to_sym)
    self.write!({:omniauth_keys => to_write})
  end

  def to_param
    provider
  end

  def self.find(provider)
    key = Admin::OmniauthKey.new
    key.fill_fields(Settings.omniauth_keys.send(provider).to_hash)
    key
  end

  def self.all
    Settings.omniauth_keys.nil? ? [] : Settings.omniauth_keys
  end
end