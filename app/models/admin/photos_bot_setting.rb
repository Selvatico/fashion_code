class Admin::PhotosBotSetting < Admin::SettingBase
  attr_accessor :min_time, :max_time, :min_follows, :max_follows, :min_votes, :max_votes

  validates_each :min_time, :max_time, :min_follows, :max_follows, :min_votes, :max_votes do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/photos_bots.yml"
  end

  def self.save(periods, options={})
    to_write = {}
    validation = true
    i = 0
    options.each do |period, value|
      to_write["#{period}"] = {}
      p = Admin::PhotosBotSetting.new
      filling = p.fill_fields(value, to_write["#{period}"])
      validation = validation && filling
      periods[i] = p
      i = i + 1
    end
    if validation
      p = Admin::PhotosBotSetting.new
      Settings.photos_bots.periods = to_write
      p.write!({:photos_bots => Settings.photos_bots.to_hash})
      true
    else
      false
    end
  end

  def save_period(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      count = Settings.photos_bots.periods.count
      Settings.photos_bots.periods.send("period#{count+1}=", to_write)
      self.write!({:photos_bots => Settings.photos_bots.to_hash})
      true
    else
      false
    end
  end

  def self.erase_last_period!
    to_write = Settings.photos_bots.periods.to_hash
    count = Settings.photos_bots.periods.count
    to_write.delete("period#{count}".to_sym)
    p = Admin::PhotosBotSetting.new
    p.write!({:photos_bots => {:periods => to_write}})
  end
end