class Admin::SettingBase
  include ActiveModel::Validations

  def fill_fields(options, to_write={})
    options.each do |field, value|
      if self.respond_to?("#{field}=")
        self.send("#{field}=", value)
        to_write[field.to_sym] = value
      else
        return false
      end
    end
    return false unless self.valid?
    true
  end

  def write!(to_write)
    File.open(self.filename, "w") do |f|
      f.write(to_write.to_yaml.sub("---",""))
    end
    Settings.add_source!(self.filename)
    Settings.reload!
  end
end