class Admin::VsTopSetting < Admin::SettingBase
  attr_accessor :alpha, :beta, :gamma, :challenge_percent, :new_age, :top_count, :style_percent, :mentions_percent, :followings_percent

  validates_each :alpha, :beta, :gamma, :challenge_percent, :new_age, :top_count, :style_percent, :mentions_percent, :followings_percent do |record, attr, value|
    record.errors.add :base, "#{attr} cannot be blank" if value.nil? || value.blank?
  end

  validate :multpls_sum
 
  def multpls_sum
    if (self.alpha.to_r + self.beta.to_r + self.gamma.to_r).to_f != 1
      errors.add(:base, "alpha + beta + gamma != 1")
    end
  end

  def filename
    "#{Rails.root}/config/settings/#{Rails.env}/vs_ranking.yml"
  end

  def save(options={})
    to_write = {}
    if self.fill_fields(options, to_write)
      Settings.vs_ranking = to_write
      self.write!({:vs_ranking => Settings.vs_ranking.to_hash})
      true
    else
      false
    end
  end
end