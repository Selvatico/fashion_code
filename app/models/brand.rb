class Brand < ActiveRecord::Base
  belongs_to :user, class_name: 'User'
  attr_accessible :name, :image, :description

  default_scope includes(:user)

  mount_uploader :image, BrandUploader

  def profile_url
    self.user.present? ? "/profiles/#{self.user.username}" : "http://modera.co"
  end
end