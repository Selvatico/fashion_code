class Challenge < ActiveRecord::Base
  belongs_to :user
  belongs_to :challenge_type
  belongs_to :photo
  has_many :votes

  attr_accessible :name, :expired_at, :finished, :challenge_type, :photo, :score, :level, :mentions, :votes_count

  scope :active, where(finished: false)

  before_save :notify_before_save
  after_commit :notify_on_create, on: :create
  before_create :generate_name
  after_create :start
  after_create :start_bots

  # validate :no_active_challenges
  #validate :no_self_in_mentions

  # def no_active_challenges
  #   if user.challenges.active.where('not id = ?', id).exists?
  #     errors.add(:active_challenge, "already exists")
  #   end
  # end

  def no_self_in_mentions
    if mentions && mentions.include?("@#{self.user.username}")
      errors.add(:mentions, "can't mention self")
    end
  end


  def mentioned_users
    return [] if self.mentions.nil? || self.mentions.empty?
    users = self.mentions.gsub('@','').gsub(' ','').split(',')
  end

  protected

  #-------- notification
  def notify_on_create
    Resque.enqueue(Notifications::ChallengeStartedWorker, self.id)
  end

  def notify_before_save
    if self.id && self.finished_changed? && !self.user.bot
      Resque.enqueue(Notifications::ChallengeFinishedWorker, self.id)
    end
  end
  #------------------

  def generate_name
    self.name = "#{self.challenge_type.fashion_style.name[0..2]}vs#{self.challenge_type.opponent_fashion_style.name[0..2]}#{DateTime.now.strftime('%d%b')}"
  end

  def start
    self.update_attributes!(expired_at: CHALLENGE_EXPIRED_TIME.minutes.since(self.created_at))
    Resque.enqueue_at(self.expired_at, ChallengeWorker, self.id)
  end

  #------- bots
  def start_bots
    return unless self.valid?

    is_bot = self.user.bot ? "bot_" : ""

    for i in 1..Settings.challenges_bots.send("#{is_bot}periods").count do
      period = Settings.challenges_bots.periods.send("period#{i}")
      votes = rand(period.min_votes.to_i..period.max_votes.to_i)
      for j in 1..votes
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, ChallengeBotsWorker, 'vote_photo', {challenge_id: self.id, photo_id: self.photo_id})
      end
      unvotes = rand(period.min_unvotes.to_i..period.max_unvotes.to_i)
      for j in 1..unvotes
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, ChallengeBotsWorker, 'unvote_photo', {challenge_id: self.id, photo_id: self.photo_id})
      end
    end
  end
end
