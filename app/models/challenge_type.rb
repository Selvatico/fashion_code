class ChallengeType < ActiveRecord::Base
  belongs_to :fashion_style
  belongs_to :opponent_fashion_style, class_name: 'FashionStyle'

  attr_accessible :easy_score, :medium_score, :hard_score, :fashion_style_id, :opponent_fashion_style_id

  validates :medium_score, :hard_score, :easy_score, presence: true

  scope :by_style, lambda { |f| where('fashion_style_id = ? OR opponent_fashion_style_id = ?', f.id, f.id) }

  #admin
  validate :unique_fashion_styles
  scope :by_styles, lambda{|style_id, opponent_id| where('(challenge_types.fashion_style_id = ? AND challenge_types.opponent_fashion_style_id = ?) OR (challenge_types.fashion_style_id = ? AND challenge_types.opponent_fashion_style_id = ?)', style_id, opponent_id, opponent_id, style_id)}
  def unique_fashion_styles
    if fashion_style_id && opponent_fashion_style_id && ChallengeType.by_styles(fashion_style_id, opponent_fashion_style_id).any?
      errors.add(:fashion_style_id, "is already used in another challenge type with the same opponent")
    end
  end
  #/admin
end
