class Comment < ActiveRecord::Base
  include Twitter::Extractor

  belongs_to :user
  belongs_to :photo
  has_many :inappropriates, as: :inappropriateable

  attr_accessible :user, :comment, :photo

  after_create :parse_text
  after_destroy :destroy_tag

  #default_scope order('created_at ASC')


  def report_inappropriate_by(user)
    inappropriates.where(user_id: user.id).first_or_create
  end

  #-------- notification
  after_commit :notify_on_create, :on => :create

  def mentioned_users
    usernames = extract_mentioned_screen_names(self.comment)
    User.where(username: usernames)
  end

  protected

  def notify_on_create
    #unless self.photo.user == self.user
      Resque.enqueue(Notifications::CommentedMentionedWorker, self.id)
    #end
  end

  #-------------

  def parse_text
    usernames = extract_mentioned_screen_names(self.comment)
    tags = extract_hashtags(self.comment)
    photo_tags = self.photo.tag_list
    tags.each { |tag| photo_tags << tag unless photo_tags.include?(tag) }
    self.photo.save
  end

  def destroy_tag
    Resque.enqueue(AfterCommentDestroyWorker, self.id, self.comment, self.photo_id)
  end
end
