class Contest < ActiveRecord::Base
  has_many :prizes
  accepts_nested_attributes_for :prizes, reject_if: :all_blank, allow_destroy: true
  belongs_to :winner, class_name: 'User'
  has_many :participants
  has_many :users, through: :participants
  has_many :inappropriates, as: :inappropriateable

  attr_accessible :started_at, :expired_at, :finished, :name, :votes_count, :prizes_attributes, :description

  validates :started_at, :expired_at, :name, presence: true

  scope :active, where(finished: false)
  scope :wun_by_user, lambda{|user_id| where(winner_id: user_id)}

  scope :started, lambda{ |user_id| includes(:users).active.where('started_at <= current_date and not exists (select 1 from participants where contest_id = contests.id AND user_id = ?)', user_id) }
  scope :new_scope, lambda{ |user_id| includes(:users).active.where('contests.started_at > current_date and not exists (select 1 from participants where contest_id = contests.id AND user_id = ?)', user_id) }
  scope :joined, lambda{ |user_id| includes(:users).active.where('exists (select 1 from participants where contest_id = contests.id AND user_id = ?)', user_id) }
  scope :finished_scope, includes(:users).where(finished: true)

  after_create :start
  before_save :notify_before_save
  after_commit :notify_on_create, :on => :create
  after_create :start_bots

  def prize
    self.prizes.first
  end

  def participant_place(user_id)
    # p = Participant.find_by_sql(%{SELECT * FROM (SELECT participants.*, row_number() OVER (order by participants.votes_score desc, participants.created_at asc) AS rownum FROM participants) AS participants WHERE participants.contest_id=#{self.id} and participants.user_id = #{user_id}})[0]
    # p = self.participants.where(user_id: user_id).first
    fame = self.participants.fame_order.all
    participant = fame.select{|p| p.user_id == user_id}[0]
    participant.nil? ? nil : (fame.index(participant) + 1)
  end

  def fame(limit=nil)
    result = self.users.includes(:participants).order('participants.votes_score desc')
    if limit
      result.limit(limit)
    else
      result
    end
  end

  #admin
  validate :expired_smaller_started
  def expired_smaller_started
    if !expired_at.blank? and !started_at.blank? and expired_at.to_datetime < started_at.to_datetime
      errors.add(:started_at, "can't be later than expired at")
    end
  end
  #/admin

  protected

  def notify_on_create
    Resque.enqueue(Notifications::ContestStartedWorker, self.id)
    #Resque.enqueue_at(self.started_at, Notifications::ContestStartedWorker, self.id)
  end


  def notify_before_save
    if self.id && self.finished_changed? && self.finished
      Resque.enqueue(Notifications::ContestFinishedWorker, self.id)
    end
  end

  def start_bots
    return unless self.valid?

    for i in 1..Settings.contests_bots.send("periods").count do
      period = Settings.contests_bots.periods.send("period#{i}")
      bot_votes = rand(period.min_bot_votes.to_i..period.max_bot_votes.to_i)
      for j in 1..bot_votes
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, BotsWorker, 'bot_contest_vote', {contest_id: self.id})
      end
      bot_follows = rand(period.min_bot_follows.to_i..period.max_bot_follows.to_i)
      for j in 1..bot_follows
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, BotsWorker, 'bot_contest_follow', {contest_id: self.id})
      end
      votes = rand(period.min_votes.to_i..period.max_votes.to_i)
      for j in 1..votes
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, BotsWorker, 'contest_vote', {contest_id: self.id})
      end
      follows = rand(period.min_follows.to_i..period.max_follows.to_i)
      for j in 1..follows
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, BotsWorker, 'contest_follow', {contest_id: self.id})
      end
    end
  end

  def start
    Resque.enqueue_at(self.expired_at, ContestWorker, self.id)
  end
end
