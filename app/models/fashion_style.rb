class FashionStyle < ActiveRecord::Base
  has_many :users

  attr_accessible :name, :image, :presentation, :tag_list

  mount_uploader :image, FashionImage

  validates :name, presence: true
  validates :image, presence: true

  acts_as_taggable
end
