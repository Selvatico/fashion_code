class HashtagContest < ActiveRecord::Base
  belongs_to :owner, class_name: 'User'
  has_many :participants, class_name: 'HashtagParticipant', dependent: :destroy
  has_many :users, through: :participants
  has_many :inappropriates, as: :inappropriateable, dependent: :destroy

  validates :owner_id, :name, presence: true

  scope :top, lambda { |count|
        select('hashtag_contests.*, count(hashtag_participants.id) AS hashtag_participants_count').
            joins('LEFT OUTER JOIN hashtag_participants ON hashtag_contests.id = hashtag_participants.hashtag_contest_id').
            group('hashtag_contests.id').
            order('hashtag_participants_count DESC').
            limit(count)
  }

  def participant_place(user_id)
    # p = HashtagParticipant.find_by_sql(%{SELECT * FROM (SELECT hashtag_participants.*, row_number() OVER (order by hashtag_participants.votes_score desc, hashtag_participants.created_at asc) AS rownum FROM hashtag_participants) AS participants WHERE participants.hashtag_contest_id=#{self.id} and participants.user_id = #{user_id}})[0]
    # p = self.participants.where(user_id: user_id).first
    fame = self.participants.fame_order.all
    p = fame.select{|p| p.user_id == user_id}[0]
    p.nil? ? nil : (fame.index(p) + 1)#p.rownum
  end

  def fame(limit=nil)
    result = self.users.includes(:participants).order('hashtag_participants.votes_score desc')
    if limit
      result.limit(limit)
    else
      result
    end
  end

  def to_param
    name
  end
end