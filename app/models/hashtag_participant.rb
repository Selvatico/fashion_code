class HashtagParticipant < ActiveRecord::Base
  belongs_to :contest, foreign_key: :hashtag_contest_id, class_name: 'HashtagContest'
  belongs_to :user

  attr_accessible :previous_place, :current_place

  scope :fame_order, order('hashtag_participants.votes_score desc, hashtag_participants.created_at asc')

  after_commit :set_place, on: :create

  #------ notification
  #before_save :notify_before_save
  #
  #protected
  #
  #def notify_before_save
  #  # TODO private pub
  #  # if self.id && self.current_place_changed? && self.previous_place && self.current_place && !self.user.bot && self.receive_notifications
  #end
  #-------------

  protected

  def set_place    
    if self.current_place.nil?
      place = self.contest.participants.count
      place = place==1 ? place : place + 1
      self.current_place = place
      self.previous_place = place
      self.fame_previous_place = place
      self.save
    end
  end
end