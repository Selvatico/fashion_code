class Inappropriate < ActiveRecord::Base
  belongs_to :inappropriateable, polymorphic: true
  belongs_to :user

  attr_accessible :user

  after_commit :notify, :on => :create

  protected

  def notify
    Resque.enqueue(Notifications::InappropriateWorker, self.id)
  end
end
