class Invitation < ActiveRecord::Base
  belongs_to :user
  attr_accessible :invitation_token, :email, :user_id

  before_create :generate_token


  def self.remove_by_token_or_email(token, email = nil)
    find_by_token_or_email(token, email).destroy_all
  end

  def self.find_by_token_or_email(token, email = nil)
    table = self.arel_table
    where(table[:invitation_token].in(token).or(table[:email].in(email)))
  end

  def generate_token
    self.invitation_token = self.class.friendly_token
  end


  def self.friendly_token
    SecureRandom.base64(15).tr('+/=lIO0', 'pqrsxyz')
  end


  #mailer
  after_commit :send_invitation_email, on: :create
  def send_invitation_email
    Emailer.invitation(self.email, self.invitation_token, self.user_id).deliver
  end
  #/mailer
end
