class Member < ActiveRecord::Base
  default_scope order('ord')
  has_many :questionaries
  
  attr_accessible :fullname, :position, :avatar, :background, :ord, :questionaries_attributes
  accepts_nested_attributes_for :questionaries, reject_if: :all_blank, allow_destroy: true

  mount_uploader :avatar, Avatar
  mount_uploader :background, Background
end