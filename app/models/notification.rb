require 'enumerated_attribute'
  
class Notification < ActiveRecord::Base
  belongs_to :receiver, class_name: 'User', foreign_key: 'receiver_id'
  belongs_to :actor, class_name: 'User', foreign_key: 'actor_id'
  belongs_to :opponent, class_name: 'User', foreign_key: 'opponent_id'
  belongs_to :notifible, polymorphic: true

  mount_uploader :poster, PosterUploader

  attr_accessible :kind, :grouping_kind, :receiver, :receiver_id, :actor, :actor_id, :poster, :notifible,
    :contest_name, :comment, :score, :place, :picture_id, :picture, :read, :following, 
    :challenge_level, :challenge_style, :challenge_opponent, :contest_start, :contest_fame, 
    :contest_finish, :opponent_id, :opponent_photo_id, :opponent_photo

  enum_attr :kind, %w(
    level_up
    level_down
    ten
    three
    first
    contest_level_up
    contest_level_down
    contest_ten
    contest_three
    contest_first
    contest_daily_score
    hash_cont_level_up
    hash_cont_level_down
    hash_cont_ten
    hash_cont_three
    hash_cont_first
    challenge_started
    challenge_finished
    commented
    mentioned
    contest_started
    contest_finish_line
    contest_finished
    new_follower
    facebook_friend
    up_challenge_versus_voted
    up_hashtag_versus_voted
    up_friend_versus_voted
    up_versus_voted
    down_challenge_versus_voted
    down_hashtag_versus_voted
    down_friend_versus_voted
    down_versus_voted
    photo_voted
    voted
    challenge_start
    joined_contest
    left_contest
    photo_uploaded
    new_following
    photo_inappropriated
    friend_won
    won_contest
  )
  enum_attr :grouping_kind, %w(
    level_up
    level_down
    ten
    three
    first
    contest_level_up
    contest_level_down
    contest_ten
    contest_three
    contest_first
    contest_daily_score
    hash_cont_level_up
    hash_cont_level_down
    hash_cont_ten
    hash_cont_three
    hash_cont_first
    challenge_started
    challenge_start
    challenge_finished
    contest_started
    contest_finish_line
    contest_finished
    joined_contest
    left_contest
    photo_uploaded
    new_following
    facebook_friend
    voted
    commented
    mentioned
    new_follower
    challenge_versus_voted
    hashtag_versus_voted
    friend_versus_voted
    versus_voted
    photo_voted
    photo_inappropriated
    friend_won
    won_contest
  )

  attr_accessor :grouped_count, :up_voters_count, :down_voters_count, :only_down

  scope :fresh, where(read: false)
  scope :by_following, lambda{|f| where(following: f)}
  scope :by_user, lambda{|user_id| where(receiver_id: user_id)}
  scope :by_followings, where("kind = 'new_follower' OR kind =  'friend_won' OR kind = 'facebook_friend'")
  scope :by_actions, where("kind = 'contest_started' OR kind = 'voted' OR kind = 'won_contest' OR kind = 'contest_finished' OR kind = 'photo_inappropriated' OR kind = 'level_up' OR kind = 'level_down' OR kind = 'contest_ten'")
  scope :by_comments, where("kind = 'commented' OR kind = 'mentioned'")
  scope :order_by_new, order('created_at DESC')

  after_create :enqueue_apns, :push_to_private_pub

  #default_scope order('created_at DESC')

  def set_read!
    self.read = true
    self.save!
  end

  def self.group_notifications(following, user, last_day)
    result = []
    count = Settings.general.notifications_on_page.to_i
    last_day = last_day.nil? || last_day.empty? ? user.incoming_notifications.maximum('created_at') : last_day.to_date - 1.days
    return result if last_day.nil?
    last_day = last_day.to_date.to_s(:simple_date)
    have_notifications = user.incoming_notifications.by_following(following).where("to_char(created_at, 'DD-MM-YYYY') <= ?", last_day).exists?
    if following == 'true'
      while result.count < count && have_notifications
        nots = user.incoming_notifications.by_following(following).where("to_char(created_at, 'DD-MM-YYYY') = ?", last_day).all.group_by{|n| n.grouping_kind}
        have_notifications = user.incoming_notifications.by_following(following).where("to_char(created_at, 'DD-MM-YYYY') < ?", last_day).exists?
        nots.each do |k,v|
          if k.to_s =~ /.*_contest\z/
            n = v
            n.last.grouped_count = 0
          else
            n = []
            v.group_by{|n| n.actor_id}.each do |act, notif|
              n.push(notif[0])
              n.last.grouped_count = notif.count - 1
            end
          end
          result.concat(n)
        end
        last_day = (last_day.to_date - 1.days).to_date.to_s(:simple_date)
      end
    else
      while result.count < count && have_notifications
        nots = user.incoming_notifications.by_following(following).where("to_char(created_at, 'DD-MM-YYYY') = ?", last_day).all.group_by{|n| n.grouping_kind}
        have_notifications = user.incoming_notifications.by_following(following).where("to_char(created_at, 'DD-MM-YYYY') < ?", last_day).exists?
        nots.each do |k,v|
          if k.to_s =~ /.*versus_voted\z/
            up_voters = v.select{|n| n.kind =~ /\Aup_.*/}
            if up_voters.empty?
              n = v[0]
              n.grouped_count = v.count - 1
              n.up_voters_count = 0
              n.down_voters_count = v.count - 1
              n.only_down = true
            else
              n = up_voters[0]
              n.grouped_count = v.count - 1
              n.up_voters_count = up_voters.count - 1
              n.down_voters_count = v.select{|n| n.kind =~ /\Adown_.*/}.count
              n.only_down = false
            end
          else
            n = v[0]
            n.grouped_count = v.count - 1
          end
          result.push(n)
        end
        last_day = (last_day.to_date - 1.days).to_date.to_s(:simple_date)
      end      
    end
    result
  end

  protected

  def enqueue_apns
    # device_token = self.receiver.device_token
    # unless device_token.nil?
    #   n = Rapns::Apns::Notification.new
    #   n.app = Rapns::Apns::App.find_by_name("co.modera.ios")
    #   n.device_token = device_token.downcase
    #   n.alert = self.message
    #   #n.attributes_for_device = {:foo => :bar}
    #   n.save!
    # end
  #rescue
  #  logger.error 'Cannot connect to PrivatePub'
  end

  def push_to_private_pub
    logger.debug 'Add CometCallbackWorker'
    Resque.enqueue(Notifications::CometCallbackWorker, receiver_id)
  end
end
