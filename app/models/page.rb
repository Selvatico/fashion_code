class Page < ActiveRecord::Base
  attr_accessible :content, :description, :keywords, :name, :permalink, :title
  validates_uniqueness_of :permalink
  validates_presence_of :title, :name, :content
end