class Participant < ActiveRecord::Base
  belongs_to :contest
  belongs_to :user

  attr_accessible :previous_place, :current_place

  scope :global, where(contest_id: nil)
  scope :fame_order, order('participants.votes_score desc, participants.created_at asc')

  after_create :set_place

  #------- notification
  before_save :notify_before_save
  after_commit :notify_on_create, on: :create
  before_destroy :notify_before_destroy

  protected

  def notify_before_destroy
    logger.info 'LeftContestWorker was disabled'
    #Resque.enqueue(Notifications::LeftContestWorker, self.user_id, self.contest_id)
  end

  def notify_on_create
    logger.info 'JoinedContestWorker was disabled'
    #Resque.enqueue(Notifications::JoinedContestWorker, self.id)
  end

  def notify_before_save
    # TODO private pub
    # if self.id && self.current_place_changed? && self.previous_place && self.current_place && !self.user.bot
  end
  #-------------

  def set_place
    return if self.contest.nil?

    if self.current_place.nil?
      place = self.contest.participants.count
      self.current_place = place
      self.previous_place = place
      self.fame_previous_place = place
      self.save
    end
  end
end
