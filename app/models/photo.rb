class Photo < ActiveRecord::Base
  include PhotoRepository

  belongs_to :user
  has_many :savings
  has_many :savers, source: :user, foreign_key: :user_id, dependent: :destroy, through: :savings
  has_many :up_votes, class_name: "Vote", foreign_key: :voted_photo_id
  has_many :down_votes, class_name: "Vote", foreign_key: :unvoted_photo_id
  has_many :challenges
  has_many :inappropriates, as: :inappropriateable
  has_many :comments
  #TODO move to settings
  has_many :recent_comments, class_name: "Comment", limit: 5
  has_many :pos_voters, through: :up_votes, source: :voter
  has_many :neg_voters, through: :down_votes, source: :voter
  has_one :vs_top_photo

  attr_accessible :image, :votes_score, :up_votes_sum, :votes_sum, :rank, :image_cache, :tag_list
  attr_accessor :one_vote_weight, :crop_x, :crop_y, :crop_h, :crop_w

  scope :active, where(deleted: false)
  scope :by_date, order('photos.created_at DESC')

  mount_uploader :image, PhotoUploader
  #validates_presence_of :image

  after_commit :notify_on_create, on: :create
  after_create :start_bots

  acts_as_taggable

  def self.share_providers
    %w(facebook twitter)
  end

  def vote_for(user)
    return false if self.voted?(user) || self.deleted

    vote = user.votes.build({vote_score: 1})
    vote.voted_photo = self
    vote.voted_photo_score = self.votes_score + 1
    if vote.save
      Notification.create!(kind: 'voted', grouping_kind: 'voted', receiver: self.user, actor: user, notifible: vote, following: false) unless self.user.bot
    end
    vote
  end

  def set_one_vote_weight!(value)
    self.one_vote_weight = value
    self
  end

  def share_url
    #TODO - use code
    if Rails.env.staging?
      "http://staging.modera.co/?u=#{Base64.urlsafe_encode64("/profiles/#{self.user.username}/#browse_legacy/#{self.id}")}"
    elsif Rails.env.production?
      "http://modera.co/?u=#{Base64.urlsafe_encode64("/profiles/#{self.user.username}/#browse_legacy/#{self.id}")}"
    else
      "http://dev.modera.streamcase.com/?u=#{Base64.urlsafe_encode64("/profiles/#{self.user.username}/#browse_legacy/#{self.id}")}"
    end
  end

  def age
    (DateTime.now.to_i - self.created_at.to_datetime.to_i)/1.day
  end

  def has_active_challenge?
    self.challenges.active.all.size > 0
  end

  def active_challenge
    self.challenges.active.first
  end

  def last_voters(quantity)
    User.includes(:votes).where('votes.voted_photo_id = ?', self.id).order('votes.created_at desc').limit(quantity).all
  end

  def voters_count
    User.includes(:votes).where('votes.voted_photo_id = ?', self.id).count
  end

  def voted? voter
    self.user_id == voter.id || self.up_votes.where(voter_id: voter.id, unvoted_photo_id: nil).exists?
  end

  def report_inappropriate_by(user)
    inappropriates.where(user_id: user.id).first_or_create
    Resque.enqueue(Notifications::InappropriateWorker, self.id, self.user)
  end

  def rate
    return 0 if up_votes.count + down_votes.count == 0
    (up_votes.count.to_f / (up_votes.count + down_votes.count) * 100).to_i
  end

  def delete_file
    self.remove_image!
    self.deleted = true
    self.save!
    Resque.enqueue(PhotoAfterDeletedWorker, self.id)
  end

  def social_url(provider)
    p = SocialNetworks::Provider.new({name: provider})
    p.share_url({
      title: "The Modera",
      url: share_url,
      description: "Rate this photo on Modera",
      image: image.url
    })
  end

  protected

  #--------- notification
  def notify_on_create
    logger.info 'PhotoUploadedWorker was disabled'
    #Resque.enqueue(Notifications::PhotoUploadedWorker, self.id)
  end
  #--------------------

  #------ bots
  def start_bots
    return if self.user.bot || !self.valid?

    for i in 1..Settings.photos_bots.periods.count do
      period = Settings.photos_bots.periods.send("period#{i}")
      votes = rand(period.min_votes.to_i..period.max_votes.to_i)
      for j in 1..votes
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, BotsWorker, 'like_photo', {photo_id: self.id})
      end
      follows = rand(period.min_follows.to_i..period.max_follows.to_i)
      for j in 1..follows
        rand_time = rand(period.min_time.to_i..period.max_time.to_i)
        Resque.enqueue_in(rand_time.minutes, BotsWorker, 'follow', {user_id: self.user_id})
      end
    end
  end

end
