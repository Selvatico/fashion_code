class Prize < ActiveRecord::Base
  belongs_to :contest
  attr_accessible :name, :description, :image

  validates :name, presence: true

  mount_uploader :image, PrizeImage
end
