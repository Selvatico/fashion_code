class Questionary < ActiveRecord::Base
  belongs_to :member
  attr_accessible :question, :answer

  validates :question, :answer, presence: true
end