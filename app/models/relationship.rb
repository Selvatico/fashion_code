class Relationship < ActiveRecord::Base
  attr_accessible :followed_id

  belongs_to :follower, class_name: 'User', foreign_key: 'follower_id'
  belongs_to :followed, class_name: 'User', foreign_key: 'followed_id'


  validates :follower_id, presence: true
  validates :followed_id, presence: true

  #after_commit :update_count
  after_commit :notify_on_create, on: :create
  after_commit :start_bots, on: :create

  #def update_count
  #  Resque.enqueue(FollowingsWorker, self.follower_id, self.followed_id)
  #end


  protected

  #------- notification
  def notify_on_create
    receiver = self.followed
    follower = self.follower
    unless receiver.bot
      Notification.create!(kind: 'new_follower', grouping_kind: 'new_follower', receiver: receiver, actor: follower, following: false)
      Emailer.new_follower(receiver, follower).deliver! if receiver.new_follower
    end
    #Resque.enqueue(Notifications::FollowingsWorker, self.id)
  end

  #--------------

  #------ bots
  def start_bots
    if User.find(followed_id).bot
      rand_time = rand(Settings.follow_back_bots.min_time.to_i..Settings.follow_back_bots.max_time.to_i)
      Resque.enqueue_in(rand_time, BotsWorker, 'follow_back', {follower_id: self.follower_id, followed_id: self.followed_id})
    end
  end

end
