class Saving < ActiveRecord::Base
  belongs_to :user
  belongs_to :photo

  attr_accessible :photo_id
  validates_presence_of :photo_id

end
