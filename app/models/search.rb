class Search

  def self.results_for(object, prefix, opts = {} )
    options = {page: 1, per_page: 10}.merge(opts.reject{|k,v| v.nil?})
    page = options[:page].to_i
    per_page = options[:per_page].to_i
    response  = $redis.zrevrange "search-#{object.to_s}:#{prefix.downcase}", (page - 1)*per_page, page*per_page - 1
    result = []
    if response
      response.each do |item|
        result << JSON.parse(item)
      end
    end
    result
  end

  def self.index_term(object, term, expected_result, opts = {})
    options = {remove: false, offset: 3}.merge(opts.reject{|k,v| v.nil?})
    options[:offset].upto(term.length) do |n|
      unless options[:remove]
        $redis.zincrby "search-#{object.to_s}:#{term[0,n].downcase}", 1, expected_result  
      else
        $redis.zrem "search-#{object.to_s}:#{term[0,n].downcase}", expected_result  
      end 
    end
  end
end