class SharePreferences < ActiveRecord::Base
  belongs_to :user
  attr_accessible :active, :social_network, :upload_photo, :follow, :join_contest, :new_contest, :post_comment, :win_prize
end