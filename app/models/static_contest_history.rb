class StaticContestHistory < ActiveRecord::Base
  attr_accessible :title, :image

  mount_uploader :image, ContestHistory
  
  default_scope order('created_at desc')

  validates_presence_of :title, :image

  before_save :add_permalink

  protected

  def add_permalink
    self.permalink = self.title.gsub(' ','_')
  end
end