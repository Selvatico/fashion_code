class User < ActiveRecord::Base
  include UserRepository

  has_many :photos, dependent: :destroy
  has_many :invitations, dependent: :destroy
  belongs_to :fashion_style
  has_many :participants, dependent: :destroy
  has_many :contests, through: :participants
  has_many :votes, class_name: "Vote", foreign_key: :voter_id
  has_many :challenges, dependent: :destroy
  has_many :savings, dependent: :destroy
  has_many :saved_photos, through: :savings, source: :photo, order: 'savings.created_at desc', include: :user
  has_many :hashtag_participants
  has_many :share_preferences, class_name: "SharePreferences"

  # ---- folowing ---------
  has_many :relationships, class_name: 'Relationship',
           foreign_key: 'follower_id',
           dependent: :destroy
  has_many :following, through: :relationships, source: :followed

  has_many :reverse_relationships, foreign_key: 'followed_id',
           class_name: 'Relationship',
           dependent: :destroy
  has_many :followers, through: :reverse_relationships, source: :follower
  # ------ end following -------------

  #----------- notification
  has_many :incoming_notifications, class_name: 'Notification', foreign_key: 'receiver_id', dependent: :nullify
  has_many :outgoing_notifications, class_name: 'Notification', foreign_key: 'actor_id', dependent: :nullify

  after_commit :notify_on_create, :on => :create
  # ----------- end notification
  accepts_nested_attributes_for :share_preferences

  attr_accessible :fullname, :website, :about, :phone, :sex, :birth, :votes_score, :username, :avatar,
                  :avatar_cache, :facebook, :twitter, :instagram, :pinterest, :photos_private, :store_original_photos,
                  :store_filtered_photos, :allow_push, :fashion_style, :device_token, :new_follower, :new_comment,
                  :new_mention, :join_contest, :friend_joined, :daily_activity, :weekly_activity, :activity,
                  :photos_attributes, :fashion_style_id, :bot, :share_preferences_attributes

  validates :username, presence: true, uniqueness: {:case_sensitive => false}, format: {with: /\A[A-za-z\d_]{3,50}\z/, message: "can contain letters, numbers and \"_\""}
  before_validation :downcase_username
  accepts_nested_attributes_for :photos, reject_if: :all_blank, allow_destroy: true
  devise :database_authenticatable, :omniauthable, :registerable, :recoverable, :trackable, :token_authenticatable, :validatable

  mount_uploader :avatar, Avatar

  before_create :set_global_participant
  after_commit :create_share_preferences, on: :create


  #------- auth
  devise :database_authenticatable, :omniauthable, :registerable, :recoverable, :trackable, :token_authenticatable, :validatable

  has_many :user_authentications, dependent: :destroy

  attr_accessor :login
  attr_accessible :email, :password, :password_confirmation, :login
  validates :email, presence: true, email: true

  #TODO: можно опритимизровать и получить все одним зпросом из базы данных (с подзапросами?). Арай
  scope :daily_notifications, includes(:incoming_notifications).where(daily_activity: true).where('notifications.created_at >= ?', 1.day.ago)
  scope :weekly_notifications, includes(:incoming_notifications).where(weekly_activity: true).where('notifications.created_at >= ?', 1.week.ago)


  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end


  def self.from_omniauth(auth)
    # попытка найти пользователя по provider и uid
    user = includes(:user_authentications)
    .where("user_authentications.provider = :provider AND user_authentications.uid = :uid", auth.slice(:provider, :uid))
    .first

    user_auth = { provider: auth['provider'],
                  uid: auth['uid'],
                  token: auth['credentials']['token'],
                  secret_token: auth['credentials']['secret'] }
    # если не найден, то попытка найти пользователя по email
    email = auth['info']['email']
    if user.nil? && email.present?
      user = find_by_email(email)
      if user.present?
        user.reverse_update_omniauth(auth)
        user.save!
        user.user_authentications.create!(user_auth)
      end
    end

    if user.nil? && email.present?
      user = User.new
      user.reverse_update_omniauth(auth)
      user.password = User.generate_pass(6)
      user.save!
      user.user_authentications.create!(user_auth)
    end

    user
  end

  def reverse_update_omniauth(auth)
    self.email = auth['info']['email'] if self.email.blank? && auth['info']['email']
    self.username = auth['info']['nickname'].gsub('.', '') if self.username.blank? && auth['info']['nickname']
    self.fullname = auth['info']['name'] if self.fullname.blank? && auth['info']['name']
    image = auth['info']['image']
    if image && self.avatar_cache.blank?
      io = open(image)

      def io.original_filename;
        File.basename(base_uri.path);
      end

      self.avatar = io
    end
  end

  def self.from_social provider, token, token_secret = nil
    omniauth_key = Settings.omniauth_keys.send(provider.camelize)
    @provider = SocialNetworks::Provider.new({
                                                 name: provider,
                                                 api_key: omniauth_key.app_token,
                                                 api_key_secret: omniauth_key.app_token_secret,
                                                 token: token,
                                                 token_secret: token_secret
                                             })
    user_info = @provider.user_info({})
    User.includes(:user_authentications).
        where('user_authentications.provider = ? AND user_authentications.uid = ?', provider, user_info.id).first
  end

  def apply_omniauth(omniauth)
    self.email = omniauth[:email] if self.email.blank? && omniauth[:email]
    self.username = omniauth[:username].gsub('.', '') if username.blank? && omniauth[:username]
    self.fullname = omniauth[:name] if fullname.blank? && omniauth[:name]
    self.sex = 1 if omniauth[:sex] == 'male'
    self.sex = 0 if omniauth[:sex] == 'female'
    case omniauth[:provider]
    when 'facebook'
      self.facebook = omniauth[:url]
    when 'twitter'
      self.twitter = omniauth[:url]
    when 'instagram'
      self.instagram = omniauth[:url]
    end
    if avatar.blank? && omniauth[:avatar]
      if omniauth[:provider] == 'twitter'
        uri = URI.parse(omniauth[:avatar])
        res = Net::HTTP.start(uri.host, uri.port) { |http| http.get("#{uri.path}?#{uri.query}") }
        Rails.logger.debug "==================== #{omniauth[:avatar].inspect} ========================="
        io = open(res['location'])
        def io.original_filename;
          File.basename(base_uri.path);
        end
        self.avatar = io
      else
        io = open(omniauth[:avatar])
        def io.original_filename;
          File.basename(base_uri.path);
        end
        self.avatar = io
      end
    end
    user_authentications.build(
      provider: omniauth[:provider], 
      uid: omniauth[:uid], 
      token: omniauth[:token], 
      secret_token: omniauth[:secret]
    )
  end

  def create_oauth_authentications(omniauth)
    user_authentications.build(provider: omniauth['provider'], uid: omniauth['uid'], token: omniauth['credentials']['token'], secret_token: omniauth['credentials']['secret'])
  end


  def active_for_authentication?
    super and !self.blocked
  end


  def friends provider
    user_auth = self.user_authentications.where(provider: provider).first
    if user_auth
      @provider = SocialNetworks::Provider.new({
                                                   name: provider,
                                                   api_key: Settings.omniauth_keys.send(provider.camelize).app_token,
                                                   api_key_secret: Settings.omniauth_keys.send(provider.camelize).app_token_secret,
                                                   token: user_auth.token,
                                                   token_secret: user_auth.secret_token
                                               })
      user_authentications = UserAuthentication.where('user_authentications.provider = ? AND user_authentications.uid in (?)', provider, @provider.friends.collect { |f| f.id }).select('uid')
      @friends = @provider.friends
      @friends.each do |f|
        f.in_modera = user_authentications.include?(f.id)
      end
      return @friends
    end
    nil
  end

  #-------

  def downcase_username
    self.username = self.username.downcase if self.username.present?
  end

  # this fields are used in contests by custom find_by_sql query
  def contest_votes_score
    attributes['contest_votes_score']
  end

  def place
    attributes['place']
  end

  def previous_place
    attributes['previous_place']
  end

  def followers_count
    followers.count
    # attributes['followers_count']
  end

  def set_birth_date(options)
    self.birth ||= Date.today
    self.birth = self.birth.change(year: options[:year].to_i, month: options[:month].to_i, day: options[:day].to_i)
  end

  def vote_photo(voted_photo, unvoted_photo, challenge = nil, hashtag_contest = nil)
    if !voted_photo.nil? && !unvoted_photo.nil? && !voted_photo.deleted && !unvoted_photo.deleted && voted_photo.user_id!=self.id && unvoted_photo.user_id!=self.id
      vote_score, unvote_score = 1, -1
      if challenge
        # check if voted photo or unvoted photo responds to challenge if challenge not null
        finded_vote = self.votes.find_by_voted_photo_id_and_unvoted_photo_id_and_challenge_id(voted_photo.id, unvoted_photo.id, challenge.id)
        challenge_check = (challenge.photo_id != voted_photo.id && challenge.photo_id != unvoted_photo.id)
        return false if finded_vote || challenge_check
        challenge_voted = challenge.photo_id == voted_photo.id
        vote_score *= challenge.score if challenge_voted
        unvote_score *= challenge.score unless challenge_voted
      end
      vote = self.votes.build({vote_score: vote_score, unvote_score: unvote_score})
      vote.voted_photo = voted_photo
      vote.unvoted_photo = unvoted_photo
      vote.voted_photo_score = voted_photo.votes_score + vote_score
      vote.hashtag_contest = hashtag_contest
      vote.unvoted_photo_score = (unvoted_photo.votes_score + unvote_score) > 0 ? (unvoted_photo.votes_score + unvote_score) : 0
      if challenge
        photo = challenge_voted ? voted_photo : unvoted_photo
        vote.challenge_votes_count = challenge_voted ? (photo.challenge_votes_count + vote_score) : (photo.challenge_votes_count + unvote_score)
        photo.challenge_votes_count = vote.challenge_votes_count
        photo.save!
      end
      vote.challenge = challenge
      return vote.save
    end
    false
  end

  def save_photo!(photo_id)
    if Photo.active.where(id: photo_id).exists?
      return savings.where(photo_id: photo_id).first_or_create
    end
    false
  end

  def delete_saved_photo!(photo_id)
    savings.where(photo_id: photo_id).destroy_all.blank?
  end

  def to_param
    username.parameterize
  end

  def serialize_for_search
    {username: username, fullname: fullname, avatar: avatar.small.url}.to_json
  end

  def name
    fullname.blank? ? username : fullname
  end

  def can_create_challenge?
    #must not have active challenges and can't create more than 1 in a day
    result = true
    result = (challenges.maximum('created_at').localtime.to_date < Date.today) if challenges.exists?
    !challenges.active.exists? && result
  end

  def power_hour_info
    result = {status: "", time: ""}
    if can_create_challenge?
      result[:status] = "new"
    else
      if challenges.active.exists?
        result[:status] = "started"
        result[:time] = {minutes: (active_challenge.expired_at.to_i - DateTime.now.to_i) / 1.minutes, seconds: (active_challenge.expired_at.to_i - DateTime.now.to_i) % 1.minutes}
      else
        result[:status] = "finished"
        result[:time] = to_next_challenge
      end
    end
    result
  end

  def to_next_challenge
    last_challenge = challenges.order(:id).last
    return {:hours => 0, :minutes => 0, :seconds => 0} unless last_challenge
    last_created = last_challenge.created_at.localtime
    next_date = last_challenge.created_at.localtime.to_date + 1.days
    if next_date <= Date.today
      return {:hours => 0, :minutes => 0, :seconds => 0}
    else
      result = {}
      result[:hours] = (next_date.to_time.to_i - last_created.to_i)/1.hours
      result[:minutes] = ((next_date.to_time.to_i - last_created.to_i)/1.minutes)%60
      result[:seconds] = (next_date.to_time.to_i - last_created.to_i)%60
      return result
    end
  end

  def last_challenge
    last_challenge = challenges.order(:id).last
    last_challenge.name if last_challenge
  end

  def suggested_followings(limit=nil)
    FashionStyle.users.where('NOT users.id = ?', self.id).includes(:followers).where('NOT relationships.follower_id = ?', self.id).all if limit.nil?

    self_style = self.fashion_style
    now_following = self.following_ids.concat([self.id])
    higher_suggest = self_style.users.where('NOT users.id IN (?) AND users.votes_score >= ?', now_following, self.votes_score).limit(limit/2 + limit%2).all
    lower_suggest = self_style.users.where('NOT users.id IN (?) AND users.votes_score <= ?', now_following, self.votes_score).limit(limit/2).all
    higher_suggest | lower_suggest
  end

  def grow_up!
    self.newbie = false
    self.save!
    send_grow_up_email
  end

  #TODO move to separate gem
  def send_invitations(emails, message)
    Resque.enqueue(InvitationsWorker, emails, message, self.id)
  end

  def self.generate_pass(n)
    chars = ('A'..'Z').to_a+('a'..'z').to_a+(0..9).to_a
    Array.new(n) { chars[rand(chars.length-1)] }.join
  end

  def base_info
    {
        username: username,
        name: name,
        avatar: avatar.medium.to_s,
        fashion_style: fashion_style.nil? ? '' : fashion_style.name
    }
  end


  #def self.top50(contest_id)
  #  User.top_users(50, 0, contest_id)
  #end

  #------- following
  def self.unfollowed_by(user)
    User.all - user.followings
  end

  def following?(followed)
    relationships.where(followed_id: followed.id).exists?
  end

  def follow!(followed)
    return if self.id == followed.id
    relationships.where(followed_id: followed.id).first_or_create
  end

  def unfollow!(followed)
    relationships.where(followed_id: followed.id).destroy_all
  end

  #------------ end following
  def notification_stats
    {
        following: self.incoming_notifications.fresh.by_followings.count,
        action: self.incoming_notifications.fresh.by_actions.count,
        comment: self.incoming_notifications.fresh.by_comments.count
    }
  end

  protected

  def create_share_preferences
    self.share_preferences.create!(social_network: 'facebook')
    self.share_preferences.create!(social_network: 'twitter')
  end

  #def self.hall_of_fame_query(contest_id)
  #  %{SELECT users.*, participants.place, participants.fame_previous_place AS previous_place, participants.votes_score AS contest_votes_score,
  #  (SELECT count(*) FROM relationships WHERE relationships.followed_id = participants.user_id) AS followers_count
  #  FROM (SELECT participants.*, row_number() OVER (ORDER BY participants.votes_score DESC) AS place
  #        FROM participants WHERE contest_id #{contest_id && ' = '+contest_id.to_i.to_s || 'IS NULL'}) AS participants
  #  INNER JOIN users ON  users.id = participants.user_id
  #  ORDER BY participants.place
  #  LIMIT ? OFFSET ?}
  #end

  def set_global_participant
    place = User.count + 1
    par = self.participants.new(previous_place: place, current_place: place)
    par.fame_previous_place = place
  end

  #admin gem
  #method for asking if user is admin, moderator or just simple user
  def method_missing(method_name, *args, &block)
    if method_name.to_s =~ /\A(#{Settings.roles.join('|')})\?\z/
      self.role.inquiry.send(method_name)
    else
      super
    end
  end

  def block!
    self.blocked = !self.blocked
    self.save!
  end

  #/admin


  #mailer
  after_commit :send_welcome_email, on: :create

  def change_subscription!
    self.receive_emails = !self.receive_emails
    self.save!
  end


  def send_welcome_email
    Emailer.welcome(self.id).deliver unless  self.bot
  end

  def send_grow_up_email
    Emailer.grow_up(self.id).deliver unless self.bot
  end

  #/mailer

  #-------- notification -----------
  def notify_on_create
    Resque.enqueue(Notifications::UserRegisteredWorker, self.id) unless self.bot
  end
  #--------- end notificaiton ------

end
