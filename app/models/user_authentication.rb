class UserAuthentication < ActiveRecord::Base
  attr_accessible :provider, :uid, :token, :secret_token
  belongs_to :user

  after_commit :update_share_preferences, on: :create
  
  #TODO move to config
  validates :provider, inclusion: { in: %w(facebook twitter instagram flickr tumblr) }
  validates_uniqueness_of :uid, scope: :provider

  protected
  def update_share_preferences
    share_preferences = user.share_preferences.where(social_network: provider).first
    if share_preferences
      share_preferences.update_attributes(
        active: true, 
        upload_photo: true, 
        follow: true,
        join_contest: true,
        new_contest: true,
        post_comment: true,
        win_prize: true
      )
    end
  end
end