class Vote < ActiveRecord::Base
  belongs_to :voter, class_name: 'User'
  belongs_to :voted_photo, class_name: 'Photo'
  belongs_to :unvoted_photo, class_name: 'Photo'
  belongs_to :challenge
  belongs_to :hashtag_contest

  attr_accessible :vote_score, :unvote_score

  after_create :delay_updates

  #--------- notification
  after_commit :notify_on_create, on: :create

  protected

  def notify_on_create
    Notification.create!(kind: 'voted', grouping_kind: 'voted', receiver: voted_photo.user, actor_id: voter_id, notifible: self, following: false) unless voted_photo.user.bot
  end
  #-----------

  def delay_updates
    if self.voter.bot && self.challenge_id.nil?
      Resque.enqueue(BotVoteWorker, self.id)
    else
      Resque.enqueue(VoteWorker, self.id)
    end
  end
end
