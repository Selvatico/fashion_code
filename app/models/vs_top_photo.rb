class VsTopPhoto < ActiveRecord::Base
  belongs_to :photo
  
  attr_accessible :photo_id, :rank

  # def self.random_top(quontity, options={})
  #   options[:user_id].nil? ? simple_home(quontity, options[:challenge_id], options[:current_user_id]) : home_by_followings(quontity, options[:user_id])
  # end

  #protected

  def self.simple_home(quontity, current_user_id, challenge_id=nil)
    result = []
    vs_top = Photo.active.includes(:user).includes(:vs_top_photo).where('not vs_top_photos.id is null and not users.id = ?', current_user_id).all
    vs_top_size = vs_top.size
    used_vs_top = []

    if challenge_id
      challenge = Challenge.find(challenge_id)
      photo = challenge.photo
      opponent_photo = take_mentioned_photo(challenge, [])
      if opponent_photo
        result.push([photo, opponent_photo])
        used_vs_top.push(photo.id) unless vs_top.select{|t| t.id == photo.id}.empty?
        used_vs_top.push(opponent_photo.id) unless vs_top.select{|t| t.id == opponent_photo.id}.empty?
      else
        challenge_type = challenge.challenge_type
        opponent_style_id = challenge_type.fashion_style_id == challenge.user.fashion_style_id ? challenge_type.opponent_fashion_style_id : challenge_type.fashion_style_id
        while opponent_photo.nil?
          opponent_photo = User.where(fashion_style_id: opponent_style_id).order('RANDOM()').first.random_photo
        end
        result.push([photo, opponent_photo])
        used_vs_top.push(photo.id) unless vs_top.select{|t| t.id == photo.id}.empty?
        used_vs_top.push(opponent_photo.id) unless vs_top.select{|t| t.id == opponent_photo.id}.empty?
      end
      quontity = quontity - 1
    end
    
    return result if vs_top_size==0
    i = 0
    while i < quontity
      #three cases - when no element in last versus, when 1 element and when no last or 2 elements
      if result.last && result.last.size < 1
        #take random top photo that wasn't used
        while result.last.size < 1 && used_vs_top.size < vs_top_size
          random_index = rand(0..vs_top_size-1)
          unless used_vs_top.include?(vs_top[random_index].id)
            p = vs_top[random_index]
            result.last.push(p)
            used_vs_top.push(p.id)
          end
        end
        #check if we have not used photos and didn't take first photo for versus
        if used_vs_top.size == vs_top_size && result.last && result.last.size == 0
          result.delete(result.last)
          return result
        end
        #check challenges mentions
        opponent = take_mentioned_photo(p.active_challenge, used_vs_top)
        if opponent
          #if we have mentioned user's photo, than we have versus so increment i
          result.last.push(opponent)
          used_vs_top.push(opponent.id) unless vs_top.select{|t| t.id==opponent.id}.empty?
        end
      elsif result.last && result.last.size == 1
        #if we have first photo in versus take random top photo as opponent challenge_type = challenge.challenge_type
        challenge = result.last[0].active_challenge
        opponent_style_id = nil
        if challenge
          challenge_type = challenge.challenge_type
          opponent_style_id = challenge_type.fashion_style_id == result.last[0].user.fashion_style_id ? challenge_type.opponent_fashion_style_id : challenge_type.fashion_style_id
        end
        while result.last.size == 1 && used_vs_top.size < vs_top_size
          random_index = rand(0..vs_top_size-1)
          unless used_vs_top.include?(vs_top[random_index].id) || result.last[0].user_id == vs_top[random_index].user_id
            if (opponent_style_id && vs_top[random_index].user.fashion_style_id == opponent_style_id) || opponent_style_id.nil?
              p = vs_top[random_index]
              result.last.push(p)
              used_vs_top.push(p.id)
            end
          end
        end
        #return if we don't have any not used photos and didn't complete last pair
        if used_vs_top.size == vs_top_size && result.last && result.last.size == 1
          result.delete(result.last)
          return result
        end
        #check challenge mentions for second photo in versus
        opponent = take_mentioned_photo(p.active_challenge, used_vs_top)
        if opponent
          #if we have mentioned user photo than move first photo from challenge to next pair and make versus with second and mentioned and increment i
          next_photo = result.last[0]
          result.last[0] = opponent
          result.push([next_photo])
          used_vs_top.push(opponent.id) unless vs_top.select{|t| t.id==opponent.id}.empty?
        end
      #create another pair if last pair is full
      elsif result.last.nil? || result.last.size == 2
        result.push([])
      end
      #increment i in case if we had complete versus
      i = i + 1 if result.last.size == 2
    end
    result.delete(result.last) if result.last && result.last.size < 2
    result
  end

  def self.take_mentioned_photo(challenge, used)
    if challenge
      #if there are mentions that was not used still, take random mentioned and random photo
      #cycle - because all mentioned may have no photos
      mentions = Array.new(challenge.mentioned_users) - challenge.used_mentions.split(',')
      while !mentions.empty?
        rand_mentioned_index = rand(0..mentions.size-1)
        rand_mentioned = User.where(username: mentions[rand_mentioned_index]).includes(:photos).where('not photos.id is null').first
        if rand_mentioned
          rand_mentioned.photos.active.each do |p|
            unless used.include?(p.id)
              challenge.used_mentions = "#{challenge.used_mentions},#{rand_mentioned.username}"
              challenge.save!
              return p
            end
          end
        end
        mentions.delete_at(rand_mentioned_index)
      end
    end
    nil
  end

  def self.home_by_followings(quontity, user_id)
    result = []
    user = User.find(user_id)
    #take all following that have any photos, so that random_photo can't return nil
    all_following = user.following.includes(:photos).where('not photos.id is null').all
    all_following_size = all_following.size
    used_following = []
    #take all users, that are not following for the user, and not the user himself and that have any photos
    all_users = User.where('NOT users.id in (?)', all_following.collect{|f| f.id}.push(user_id)).includes(:photos).where('not photos.id is null').all
    all_users_size = all_users.size
    used_users = []
    #if we don't have following or don't have users for opponents return empty array
    return [] if all_users_size==0 || all_following_size==0
    for i in 1..quontity
      result.push([])
      #cycle while we didn't push first photo for versus from following and we have not used followings
      while result.last.size < 1 && used_following.size < all_following_size
        index_following = rand(0..all_following_size-1)
        #push only if we didn't use this following before
        unless used_following.include?(index_following)
          result.last.push(all_following[index_following].random_photo)
          used_following.push(index_following)
        end
      end
      #if we used all followings and didn't push first photo for versus, we delete last pair and return all that already have
      #if we pushed following - will wait untill next iteration and return from there - so we have last pair with opponent
      if used_following.size == all_following_size && result.last && result.last.size == 0
        result.delete(result.last)
        return result
      end
      #check if photo have active challenge in order to make right pair of styles
      challenge = result.last[0].active_challenge
      if challenge
        challenge_type = challenge.challenge_type
        opponent_style_id = challenge_type.fashion_style_id == result.last[0].user.fashion_style_id ? challenge_type.opponent_fashion_style_id : challenge_type.fashion_style_id
        #100 (if we have this quontity) users with opponent fashion style that are close to chosen following by rank
        for_random = all_users.select{|u| u.votes_score >= all_following[index_following].votes_score && u.fashion_style_id == opponent_style_id}[0..49] | all_users.select{|u| u.votes_score <= all_following[index_following].votes_score && u.fashion_style_id == opponent_style_id}[0..49]
      else
        #100 (if we have this quontity) users that are close to chosen following by rank
        for_random = all_users.select{|u| u.votes_score >= all_following[index_following].votes_score}[0..49] | all_users.select{|u| u.votes_score <= all_following[index_following].votes_score}[0..49]
      end
      for_random_size = for_random.size
      used_random = []
      #cycle while we don't push opponent photo for last pair and didn't use all free users or all random users
      while result.last.size < 2 && used_random.size < for_random_size && used_users.size < all_users_size
        index_opponent = rand(0..for_random_size-1)
        unless used_users.include?(for_random[index_opponent].id)
          result.last.push(for_random[index_opponent].random_photo)
          used_users.push(for_random[index_opponent].id)
          used_random.push(index_opponent)
        end
      end
      #if we used all free users we can't make any versus - so we return, if we didn't make last pair we delete it and then return
      if used_users.size == all_users_size
        result.delete(result.last) if result.last && result.last.size < 2          
        return result
      end
    end
    result
  end

  def self.trial_simple(quontity, used_photos, current_user_id, challenge_id=nil)
    result = []

    if challenge_id
      share_versus = ActiveRecord::Base.connection.execute("select versus_by_challenge(#{challenge_id}, #{current_user_id}, #{Settings.vs_ranking.mentions_percent}, #{Settings.vs_ranking.top_count.to_i / 2});")[0]['versus_by_challenge']
      result = share_versus.gsub('{','').gsub('}','').split(',')
      result = [] if result.include?('NULL')
      unless result.include?('NULL')
        result_users = Photo.where('id in (?)', result).pluck(:user_id).join(',')
        used_users = used_users.blank? ? "#{result_users}" : "#{used_users},#{result_users}"
      end
      quontity = quontity - 1
    end

    simple_versuses = ActiveRecord::Base.connection.execute("select home_versuses(#{quontity}, '#{used_photos}', #{current_user_id}, #{Settings.vs_ranking.challenge_percent}, #{Settings.vs_ranking.style_percent}, #{Settings.vs_ranking.mentions_percent}, #{Settings.vs_ranking.followings_percent}, #{Settings.vs_ranking.top_count.to_i / 2});")[0]['home_versuses'].gsub('{','').gsub('}','').split(',')
    result = result.concat(simple_versuses)

    photos_result = []
    i = 0
    j = 0
    while j < result.size
      photos_result.push(result[j]) if i==1 || i==2
      i = i==2 ? 0 : (i + 1)
      j = j + 1
    end

    photos_result = Photo.where('id in (?)', photos_result).all

    versuses = []
    i = 0
    while i < result.size
      versuses.push([result[i], photos_result.select{|p| p.id==result[i+1].to_i || p.id==result[i+2].to_i}].flatten)
      i = i + 3
      versuses.delete(versuses.last) if versuses.last.size < 3
    end
    return versuses
  end

  def self.trial_followings_home(quontity, user_id)    
    result = ActiveRecord::Base.connection.execute("select following_versuses(#{user_id}, #{quontity});")[0]['following_versuses']
    result = result.gsub('{','').gsub('}','').split(',')
    photos = Photo.where('id in (?)', result.select{|r| r!='NULL'}).all
    versuses = []
    i = 0
    while i < result.size
      versuses.push(photos.select{|p| p.id==result[i].to_i || p.id==result[i+1].to_i}) if result[i]!='NULL' && result[i+1]!='NULL'
      i = i + 2
      versuses.delete(versuses.last) if versuses.last.size < 2
    end
    return versuses
  end
end