module PhotoRepository
  extend ActiveSupport::Concern

  module ClassMethods
    def for_versus_list(options = {})
      users           = options.fetch :users, []
      excluded_users  = options.fetch :excluded_users, []
      excluded_photos = options.fetch :excluded_photos, []
      tags            = options.fetch :tags, []
      offset          = options.fetch :offset, 0
      limit           = options.fetch :limit, 10

      photos = active.by_date.offset(offset).limit(limit)
      photos = photos.where("photos.id NOT IN (?)", excluded_photos) if excluded_photos.any?
      photos = photos.where("photos.user_id NOT IN (?)", excluded_users) if excluded_users.any?

      if users.any?
        users_photos = photos.where("photos.user_id IN (?)", users)
        return users_photos if users_photos.count == limit
        photos = photos.where("photos.user_id NOT IN (?)", users)
      end

      if tags.any?
        photos_with_tags = photos.tagged_with(tags, any: true)
        return photos_with_tags if photos_with_tags.count(distinct: true) == limit
        photos = photos.tagged_with(tags, exclude: true)
      end

      return [] if photos.count < limit
      photos
    end
  end

  included do

  end

  def first_last_comments
    all_comments = comments.all
    if all_comments.size > 1
      [all_comments.last, all_comments.first]
    elsif all_comments.size == 1
      all_comments
    end
  end

  def first_comment
    comment = comments.order('created_at asc').first
    return comment if comment && comment.user_id == user_id
    comments.new(comment: '', user: user)
  end

  def comments_count
    self.comments.count
  end

  def last_comments(quantity)
    self.comments.order('created_at DESC').limit(quantity).all
  end
end
