module UserRepository
  extend ActiveSupport::Concern
  include PgSearch

  module ClassMethods
    def find_first_by_auth_conditions(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions).where(["lower(username) = :value OR lower(email) = :value", {:value => login.downcase}]).first
      else
        where(conditions).first
      end
    end

    def by_emails_or_phones(options = {})
      table = self.arel_table
      where(table[:email].in(options[:emails]).or(table[:phone].in(options[:phones])))
    end

    #def hall_of_fame(contest_id, page = 1, per = 20)
    #  page = page ? page.to_i : 1
    #  per = per ? per.to_i : 20
    #  query= %{SELECT users.*, participants.place, participants.fame_previous_place AS previous_place, participants.votes_score AS contest_votes_score,
    #          (SELECT count(*) FROM relationships WHERE relationships.followed_id = participants.user_id) AS followers_count
    #          FROM (SELECT participants.*, row_number() OVER (ORDER BY participants.votes_score DESC) AS place
    #                FROM participants WHERE contest_id #{contest_id && ' = '+contest_id.to_i.to_s || 'IS NULL'}) AS participants
    #          INNER JOIN users ON  users.id = participants.user_id
    #          ORDER BY participants.place
    #          LIMIT ? OFFSET ?}
    #  User.find_by_sql [query, per, per*(page-1)]
    #end
  end

  included do
    pg_search_scope :search,
                    against:  [:username, :fullname],
                    using: { tsearch: { dictionary: "english", prefix: true } }

    scope :bots, where(bot: true)
    scope :real, where(bot: false)

    scope :hall_of_fame, lambda{|contest_id| select('users.*, participants.place, participants.fame_previous_place AS previous_place, participants.votes_score AS contest_votes_score,
    (SELECT count(*) FROM relationships WHERE relationships.followed_id = participants.user_id) AS followers_count').
        joins("INNER JOIN (SELECT participants.*, row_number() OVER (ORDER BY participants.votes_score DESC) AS place
             FROM participants WHERE contest_id #{contest_id && ' = '+contest_id.to_i.to_s || 'IS NULL'}) AS participants
             ON  users.id = participants.user_id").
        order('participants.place')
    }
    scope :hashtag_hall_of_fame, lambda{|hashtag_contest_id| select('users.*, participants.place, participants.fame_previous_place AS previous_place, participants.votes_score AS contest_votes_score,
    (SELECT count(*) FROM relationships WHERE relationships.followed_id = participants.user_id) AS followers_count').
        joins(sanitize_sql_array(['INNER JOIN (SELECT hashtag_participants.*, row_number() OVER (ORDER BY hashtag_participants.votes_score DESC) AS place
             FROM hashtag_participants WHERE hashtag_contest_id = ?) AS participants
             ON  users.id = participants.user_id', hashtag_contest_id])).
        order('participants.place')
    }
  end

  def current_place(contest_id = nil)
    p = Participant.where(contest_id: contest_id, user_id: id).first
    p.present? ? p.current_place : 0
    # User.hall_of_fame(contest_id).where(id: id).limit(1).first
  end

  def best_photo
    photos.order('photos.votes_score desc').first
  end

  def active_challenge
    challenges.active.first
  end

  def random_photo
    self.photos.active.order('RANDOM()').first
  end

  def photos_votes
    own_photos = self.photo_ids
    Vote.where('(votes.voted_photo_id in (?) AND NOT votes.unvoted_photo_id is null) OR votes.unvoted_photo_id in (?)', own_photos, own_photos)
  end

  # def photo_versus_list(offset = 0, limit = 10)
  #   offset = offset * 2

  #   tags = photos.all_tags
  #   voted_photos_ids = votes.pluck(:voted_photo_id)
  #   un_voted_photos_ids = votes.pluck(:unvoted_photo_id)
  #   excluded_photos = voted_photos_ids | un_voted_photos_ids

  #   following_ids = following.pluck(:id)
  #   excluded_users = [id]

  #   left_photos = Photo.for_versus_list users: following_ids,
  #                                       excluded_users: excluded_users,
  #                                       tags: tags,
  #                                       excluded_photos: excluded_photos,
  #                                       offset: offset, limit: limit
  #   left_photos = left_photos.shuffle!


  #   excluded_users.push *left_photos.map(&:user_id)

  #   right_photos = Photo.for_versus_list users: following_ids,
  #                                        excluded_users: excluded_users,
  #                                        tags: tags,
  #                                        excluded_photos: excluded_photos,
  #                                        offset: offset, limit: limit
  #   right_photos.shuffle!

  #   return [] if left_photos.size < limit or right_photos.size < limit
  #   left_photos.zip(right_photos)
  # end

  def photo_versus_list(offset = 0, limit = 10)
    tags = photos.all_tags
    voted_photos_ids = votes.select(:voted_photo_id).map(&:voted_photo_id)

    excluded_photo_ids = []
    excluded_user_ids = [id]

    left_photos = Photo.active
                       .where("user_id <> ?", id)
                       .by_date
                       .offset(offset).limit(limit)

    left_photos = left_photos.where("photos.id NOT IN (?)", voted_photos_ids) if voted_photos_ids.any?
    left_photos = left_photos.tagged_with(tags) if tags.any?

    # just for test
    if left_photos.size < limit
      excluded_photo_ids << left_photos.pluck(:id)
      excluded_photo_ids << voted_photos_ids
      while left_photos.size < limit
        lp = random_photo(excluded_user_ids, excluded_photo_ids)
        left_photos << lp
        excluded_photo_ids << lp.id
      end
    end
    # --------------

    left_photos.shuffle!

    left_users_ids = left_photos.map(&:user_id)

    right_photos = Photo.active
                        .where("user_id <> ?", id)
                        .where("user_id NOT IN (?)", left_users_ids)
                        .by_date
                        .offset(offset).limit(limit)
    right_photos = right_photos.tagged_with(tags) if tags.any?

    # just for test
    if right_photos.size < limit
      left_users_ids << id
      excluded_photo_ids << right_photos.pluck(:id)
      excluded_photo_ids << voted_photos_ids
      while right_photos.size < limit
        rp = random_photo(excluded_user_ids, excluded_photo_ids)
        right_photos << rp
        excluded_photo_ids << rp.id
      end
    end
    # ----------------

    right_photos.shuffle!

    return [] if left_photos.size < limit or right_photos.size < limit
    left_photos.zip(right_photos)
  end

  def random_photo(excluded_user_ids, excluded_photo_ids)
    photo = Photo.active
              .where("user_id NOT IN(?) AND id NOT IN (?)", excluded_user_ids.flatten, excluded_photo_ids.flatten)
              .order('RANDOM()')
              .first
  end

  def fame_place
    # Participant.global.where(user_id: self.id).first.current_place

    # Participant.find_by_sql(%{SELECT * FROM (SELECT participants.*, row_number() OVER (order by participants.votes_score desc) AS rownum FROM participants) AS participants WHERE participants.contest_id is null and participants.user_id = #{self.id}})[0].rownum
    fame = Participant.includes(:user).global.order('users.votes_score DESC, users.created_at ASC').all
    p = fame.select { |p| p.user_id == self.id }[0]
    fame.index(p) + 1
  end

  def followers_last_week
    self.followers.where("relationships.created_at > ?", DateTime.now.prev_week)
  end

  def followers_last(limit=6)
    self.followers.limit(limit)
  end
end
