object false
node(:status) { @status }
child @challenge_types => :challenge_types do |challenge_type|
  attributes :id, :easy_score, :medium_score, :hard_score
  node(:fashion_style){|challenge_type| challenge_type.fashion_style_id == @current_user.fashion_style_id ? challenge_type.fashion_style.name : challenge_type.opponent_fashion_style.name}
  node(:opponent_fashion_style){|challenge_type| challenge_type.fashion_style_id != @current_user.fashion_style_id ? challenge_type.fashion_style.name : challenge_type.opponent_fashion_style.name}
end