object false
node(:status) { @status }
child(@challenge) do 
  attributes :id, :name, :created_at, :expired_at
  node(:photo) { |challenge| challenge.photo.image.url }
end