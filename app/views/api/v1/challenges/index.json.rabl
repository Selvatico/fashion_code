object false
node(:status) { @status }
child(@versuses => :versuses) do
  extends 'api/v1/challenges/versus'
end