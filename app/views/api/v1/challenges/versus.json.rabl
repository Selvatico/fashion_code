object @versus
attributes :id
node(:challenge){ |versus| partial('api/v1/challenges/challenge', object: versus.challenge) }
node(:last_voter){ |versus| partial('api/v1/users/user', object: versus.voter) }
node(:photo){ |versus| partial('api/v1/challenges/vote_photo', object: versus.voted_photo.set_one_vote_weight!(versus.vote_score)) }
node(:opponent_photo){ |versus| versus.unvoted_photo.nil? ? nil : partial('api/v1/challenges/vote_photo', object: versus.unvoted_photo.set_one_vote_weight!(versus.unvote_score)) }