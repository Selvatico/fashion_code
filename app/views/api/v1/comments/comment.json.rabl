object @comment
attributes :id, :created_at, :comment
node(:user){ |comment| partial('api/v1/users/user', object: comment.user) }