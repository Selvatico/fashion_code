object false
node(:status) { @status }
node(:comments_count){ @comments.total_count}
child @comments => :comments do |comment|
  attributes :id, :created_at, :comment
  node(:user) do |comment|
    child comment.user => :user do
      attributes :username
      node(:fullname){|user| user.name}
      node(:avatar){|user| user.avatar.url}
    end
  end
end