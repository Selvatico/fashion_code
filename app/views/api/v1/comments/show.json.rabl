object false
node(:status) { @status }
child @comment => :comment do
  attributes :id, :created_at, :comment
  child @comment.user => :user do
    attributes :username
    node(:fullname){|user| user.name}
    node(:avatar){|user| user.avatar.url}
  end
end