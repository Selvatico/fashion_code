object false
node(:status) { @status }
node (:users_found) {@users.count}
child @users => :users do |user|
  attributes :username
  node(:fullname){|user| user.name}
  node(:avatar){|user| user.avatar.url}
  node(:is_following){|user| @following.include?(user.id)}
end