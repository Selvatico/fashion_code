object @contest
attributes :name
node(:prize){|contest| contest.prize.name}
node(:image){|contest| contest.prize.image_url.to_s}