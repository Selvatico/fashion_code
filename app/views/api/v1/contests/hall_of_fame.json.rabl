object false
node(:status) { @status }
child(@participants => :users) do |participant|
  attributes :votes_score
  node(:fullname){|participant| participant.user.name}
  node(:username){|participant| participant.user.username}
  node(:avatar){|participant| participant.user.avatar_url.to_s}
  node(:place){|participant| participant.current_place}
  node(:photos_count){|participant| participant.user.photos.count}
  node(:wons){|participant| partial("api/v1/contests/contest", object: Modera::Contest.wun_by_user(participant.user_id)) }
end