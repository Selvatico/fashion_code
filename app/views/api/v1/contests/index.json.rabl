object false
node(:status) { @status }
child(@contests) do |contest|
  attributes :id, :name, :votes_count, :started_at, :expired_at, :description
  node(:prize){|contest| contest.prize.name}
  node(:image){|contest| contest.prize.image_url.to_s}
  node(:user_place){|contest| @place = contest.participant_place(@user_id)}
  node(:best_photo){ @best_photo}
  node(:joined_time){|contest| contest.participants.where(user_id: @user_id).first.created_at  unless @place.nil?  }
  node(:users){|contest| partial("api/v1/users/users", object: Contest.find(contest.id).fame(5)) }
end