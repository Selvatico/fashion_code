object false
node(:status) { @status }
child @fashion_styles => :fashion_styles do |fashion_style|
  attributes :id, :name
  node(:tags){|fashion_style| fashion_style.tag_list}
  node(:image){|fashion_style| fashion_style.image.url}
  node(:users_count){|fashion_style| fashion_style.users.count}
end