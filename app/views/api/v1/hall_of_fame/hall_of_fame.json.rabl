object false
node(:status) { @status }
child(@users) do |user|
  attributes :username, :votes_score
  node(:fullname){|user| user.name}
  node(:avatar){|user| user.avatar_url.to_s}
  node(:place){|user| user.fame_place}
  node(:photos_count){|user| user.photos.count}
  node(:wons){|user| partial("api/v1/contests/contest", object: Modera::Contest.wun_by_user(user.id)) }
end