object false
node(:status) { @status }
child(@notifications => :notifications) do |notification|
  attributes :id, :created_at
  node(:type){|n| (n.kind =~ /versus_voted\z/) ? 6 : Settings.notifications.send(n.kind.capitalize)}
  node(:notification){|n| partial("api/v1/notifications/types/#{(n.kind =~ /versus_voted\z/) ? 'versus_voted' : n.kind}", object: n) }
end