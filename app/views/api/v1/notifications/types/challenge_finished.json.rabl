object @notification
node(:read){|n| n.read}
node(:id){|n| n.notifible_id}
node(:votes_count){|n| n.score}
node(:photo){|n| {:id => n.picture_id, :image => n.picture}}