object @notification
node(:read){|n| n.read}
node(:user){|n| partial('api/v1/users/user', object: n.actor)}
node(:challenge){|n| {:id => n.notifible_id, :photo => {:id => n.picture_id, :image => n.picture}} }