object @notification
node(:read){|n| n.read}
node(:id){|n| n.notifible_id}
node(:level){|n| n.challenge_level}
node(:score){|n| n.score}
node(:fashion_style){|n| n.challenge_style}
node(:opponent_fashion_style){|n| n.challenge_opponent}
node(:photo){|n| {:id => n.picture_id, :image => n.picture} }