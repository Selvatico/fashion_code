object @notification
node(:read){|n| n.read}
node(:id){|n| n.notifible_id}
node(:image){|n| n.picture}
node(:name){|n| n.contest_name }
node(:place){|n| n.place}
node(:users){|n| JSON.parse(n.contest_fame) }