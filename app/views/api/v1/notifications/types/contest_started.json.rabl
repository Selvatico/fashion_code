object @notification
node(:read){|n| n.read}
node(:id){|n| n.notifible_id}
node(:image){|n| n.picture}
node(:name){|n| n.contest_name }
node(:created_at){|n| n.contest_start.strftime('%d %B') }