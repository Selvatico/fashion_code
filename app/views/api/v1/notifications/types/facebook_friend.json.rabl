object @notification
node(:read){|n| n.read}
node(:social_network){'facebook'}
node(:user){|n| partial('api/v1/users/user', object: n.actor)}