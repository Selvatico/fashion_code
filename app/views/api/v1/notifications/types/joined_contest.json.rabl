object @notification
node(:read){|n| n.read}
node(:contest){|n| {:id => n.notifible_id, :image => n.picture, :name => n.contest_name}}
node(:user){|n| partial('api/v1/users/user', object: n.actor) }