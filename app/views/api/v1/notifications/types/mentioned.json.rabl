object @notification
node(:read){|n| n.read}
node(:comment){|n| {:id => n.notifible_id, :text => n.comment}}
node(:user){|n| partial('api/v1/users/user', object: n.actor) }