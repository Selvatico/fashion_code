object @notification
node(:read){|n| n.read}
node(:photo){|n| {:id => n.picture_id, :image => n.picture}}
node(:user){|n| partial('api/v1/users/user', object: n.actor) }