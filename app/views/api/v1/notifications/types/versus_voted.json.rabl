object @notification
node(:read){|n| n.read}
node(:versus){|n| {:id => n.notifible_id, :in_challenge => !(n.kind =~ /challenge/).nil?, :score => n.score, :photos => {:photo => {id: n.picture_id, image: n.picture}, :opponent_photo => {id: n.opponent_photo_id, image: n.opponent_photo}}}}
node(:user){|n| partial('api/v1/users/user', object: n.actor) }