object @notification
node(:read){|n| n.read}
node(:user){|n| partial('api/v1/users/user', object: n.actor)}
node(:photo){|n| {:id => n.picture_id, :image => n.picture}}
node(:username){|n| n.opponent.name }