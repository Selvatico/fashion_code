object false
node(:status) { @status }
child :pages do
  @pages.each do |page|
    node(page) do
      "http://modera.co/#{page}"
    end
  end
end