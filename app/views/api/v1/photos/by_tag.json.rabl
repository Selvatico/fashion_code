object false
node(:status) { @status }
child @photos => :photos do |photo|
  node(:image){|photo| photo.image.url}
  node(:saved){ |photo| @saved_photos.include?(photo.id) }
  child :user do |user|
    node(:username){|user| user.username}
    node(:fullname){|user| user.name}
    node(:avatar){|user| user.avatar.small.url}
  end
  child :recent_comments do |comment|
    node(:comment){|comment| comment.comment }
    child :user do |user|
      node(:username){|user| user.username}
      node(:fullname){|user| user.name}
      node(:avatar){|user| user.avatar.small.url}
    end
  end
end