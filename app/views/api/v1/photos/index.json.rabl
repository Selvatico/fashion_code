object false
node(:status) { @status }
node(:user) {partial("api/v1/users/user", object: @user)}
node(:photos_count){ @photos_count }
child @photos => :photos do |photo|
  attributes :id, :created_at, :share_url
  node(:image){|photo| photo.image.url}
  node(:saved){ |photo| @saved_photos.include?(photo.id) }
  node(:voted){ |photo| photo.voted?(@current_user) }
  node(:last_voters) {|photo| partial("api/v1/users/user", object: photo.last_voters(5)) }
  node(:comments_count){ |photo| photo.comments.count }
  node(:comments){ |photo| partial("api/v1/comments/comments", object: photo.first_last_comments) }
end