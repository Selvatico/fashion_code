object false
node(:status) { @status }
node(:followers_count){ @users ? @users.total_count : 0}
child @users => :users do |user|
  attributes :username
  node(:fullname){|user| user.name}
  node(:avatar){|user| user.avatar.url}
  node(:is_following){|user| @current_user.following?(user)}
  node(:followers_count){|user| user.followers.count}
  node(:following_count){|user| user.following.count}
end