object false
node(:status) { @status }
child @users => :users do |user|
  attributes :username
  node(:fullname){|user| user.name}
  node(:avatar){|user| user.avatar.url}
  node(:followers_count){|user| user.followers.count}
  node(:following_count){|user| user.following.count}
end