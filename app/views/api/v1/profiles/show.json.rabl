object false
node(:status) { @status }
child @profile => :profile do |profile|
  attributes :username, :about, :website
  node(:fullname){|profile| profile.name}
  node(:followers_count){|profile| profile.followers.count}
  node(:following_count){|profile| profile.following.count}
  node(:is_following){|profile| @current_user.following_ids.include?(profile.id)}
  node(:photos_count){|profile| profile.photos.active.count}
  node(:fashion_style){|profile| profile.fashion_style.name rescue nil}
  node(:place){|profile| profile.fame_place}
  node(:avatar){|profile| profile.avatar.url}
  node(:social_links){ [] }
end