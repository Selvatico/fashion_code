object false
node(:status) { @status }
child @photo => :photo do |photo|
  attributes :id, :share_url
end