object @photo
attributes :id, :created_at, :share_url
node(:user) { |photo| partial('api/v1/users/user', object: photo.user) }
node(:image){ |photo| photo.image_url.to_s}
node(:saved){ |photo| @saved_photos.include?(photo.id) }
#node(:vote_weight, if: @own_profile){ |photo| photo.one_vote_weight }
node(:last_voters) {|photo| partial("api/v1/users/user", object: photo.last_voters(5)) }
node(:comments_count){ |photo| photo.comments.count }
node(:comments){ |photo| partial("api/v1/comments/comments", object: photo.first_last_comments) }