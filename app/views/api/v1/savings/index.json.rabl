object false
node(:status) { @status }
node(:photos_count){ @photos_count }
child @photos => :photos do |comment|
  attributes :id, :created_at, :share_url
  node(:image){|photo| photo.image.url}
  node(:voted){ |photo| photo.voted?(@current_user) }
  node(:user){|photo| partial("api/v1/users/user", object: photo.user) }
  node(:last_voters) {|photo| partial("api/v1/users/user", object: photo.last_voters(5)) }
  node(:comments_count){ |photo| photo.comments.count }
  node(:comments){ |photo| partial("api/v1/comments/comments", object: photo.last_comments(5)) }
end