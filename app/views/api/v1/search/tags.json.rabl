object false
node(:status) { @status }
child @tags => :tags do |tag|
  node(:name){|tag| tag[:name]}
  node(:has_followers){|tag| tag[:has_followers]}
  node(:photos_count){|tag| tag[:photos_count]}
end