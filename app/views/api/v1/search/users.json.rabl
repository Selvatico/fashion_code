object false
node(:status) { @status }
child @users => :users do |user|
  node(:username){|user| user.username}
  node(:fullname){|user| user.name}
  node(:avatar){|user| user.avatar.url}
end