object false
node(:status) { @status }
unless @friends.nil?
  child @friends => :friends do |friend|
    node(:uid){|friend| friend.id}
    node(:fullname){|friend| friend.fullname}
    node(:username){|friend| friend.username}
    node(:avatar){|friend| friend.avatar}
    node(:in_modera){|friend| friend.in_modera}
  end
end