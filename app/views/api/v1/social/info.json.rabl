object false
node(:status) { @status }
child @user_info => :user_info do |user|
  attributes :id, :username, :email, :avatar
  node(:fullname){|user| user.name}
end