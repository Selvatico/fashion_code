object false
node(:status) { @status }
unless @status.code == 500
  child(@user) do
    attributes :authentication_token, :username
    extends 'api/v1/users/user_info'
  end
else
  child :errors do
    @user.errors.messages.each_pair do |field, error|
      node(field) { error}
    end
  end
end