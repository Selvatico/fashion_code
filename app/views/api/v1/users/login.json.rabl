object false
node(:status) { @status }
if @status.code == 200
  child(@user) do
    attributes :authentication_token, :username
    extends 'api/v1/users/user_info'
  end
end