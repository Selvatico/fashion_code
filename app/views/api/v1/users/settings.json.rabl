object false
node(:status) { @status }
child @current_user => :settings do |user|
  %w(email fullname website about phone sex birth facebook twitter instagram pinterest photos_private store_filtered_photos store_original_photos allow_push).each do |field|
    node(field.to_sym){|user| user.send(field)}
  end
end
child :social do
  %w(facebook twitter instagram).each do |provider|
    node(provider.to_sym) {@auths.include?(provider)}
  end
end