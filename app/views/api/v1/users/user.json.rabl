object @user
attributes :username
node(:fullname) {|user| user.name}
node(:avatar){|user| user.avatar_url.to_s}
node(:sex){|user| user.sex ? 'male' : 'female' }