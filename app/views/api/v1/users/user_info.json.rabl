object @user
child @user => :profile do |user|
  attributes :username, :about, :website
  node(:fullname){|user| user.name}
  node(:followers_count){|user| user.followers.count}
  node(:following_count){|user| user.following.count}
  node(:fashion_style){|user| user.fashion_style.name rescue nil}
  node(:place){|user| 0}
  node(:avatar){|user| user.avatar.url}
  node(:social_links){ [] }
end
child @user => :settings do |user|
  %w(email fullname website about phone sex birth facebook twitter instagram pinterest photos_private store_filtered_photos store_original_photos allow_push).each do |field|
    node(field.to_sym){|user| user.send(field)}
  end
end