object false
node(:status) { @status }
node(:versuses_count, :if => @own_profile ){ @versuses.total_count}
child @versuses => :versuses do |versus|
  attributes :id, :if => @own_profile
  attributes :challenge_id, :created_at

  node(:voter){ |versus| partial('api/v1/users/user', object: versus.voter) }
  node(:photo){ |versus| partial('api/v1/profiles/versus_photo', object: versus.voted_photo.set_one_vote_weight!(versus.vote_score)) }
  node(:opponent_photo){ |versus| versus.unvoted_photo.nil? ? nil : partial('api/v1/profiles/versus_photo', object: versus.unvoted_photo.set_one_vote_weight!(versus.unvote_score)) }

  # node(:photo) do |versus|
  #   node(:id){|versus| versus.own_photo(@user).id}
  #   node(:created_at){|versus| versus.own_photo(@user).created_at}
  #   node(:image){|versus| versus.own_photo(@user).image.url}
  #   node(:share_url){|versus| versus.own_photo(@user).share_url}
  #   node(:vote_weight, :if => @own_profile){|versus| versus.own_photo(@user).vote_weight}
  #   child @user => :user do
  #     attributes :username, :fullname
  #     node(:avatar){|user| user.avatar.url}
  #   end
  # end

  # node(:opponent_photo) do |versus|
  #   node(:id){|versus| versus.opponent_photo(@user).id}
  #   node(:created_at){|versus| versus.opponent_photo(@user).created_at}
  #   node(:image){|versus| versus.opponent_photo(@user).image.url}
  #   node(:share_url){|versus| versus.opponent_photo(@user).share_url}
  #   node(:vote_weight, :if => @own_profile){|versus| versus.opponent_photo(@user).vote_weight}
  #   child versus.opponent_photo(@user).user => :user do
  #     attributes :username, :fullname
  #     node(:avatar){|user| user.avatar.url}
  #   end
  # end
end