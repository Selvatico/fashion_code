object @photo
attributes :id, :created_at
node(:user) { |photo| partial('api/v1/users/user', object: photo.user) }
node(:image){ |photo| photo.image_url.to_s}
node(:saved){ |photo| @saved_photos.include?(photo.id) }
node(:share_url){ |photo| photo.share_url }
node(:comments_count){ |photo| photo.comments.count }
node(:comments){ |photo| partial("api/v1/comments/comments", object: photo.last_comments(3)) }
node(:last_voters) {|photo| partial("api/v1/users/user", object: photo.last_voters(5)) }