object false
node(:status) { @status }
child(@versuses => :versuses) do |versus|
  node(:challenge_id){ |versus|
    if versus[0].active_challenge.nil?
      (versus[1].active_challenge.nil? ? nil : versus[1].active_challenge.id)
    else
       versus[0].active_challenge.id
    end}
  node(:photo){ |versus| partial("api/v1/versuses/photo", object: versus[0]) }
  node(:opponent_photo){ |versus| partial("api/v1/versuses/photo", object: versus[1]) }
end