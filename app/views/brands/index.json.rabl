collection @brands => :brands
attributes :name, :image
node(:profile) {|brand| brand.profile_url}