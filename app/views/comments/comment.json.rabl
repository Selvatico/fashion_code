object @comment
attributes :id
node(:comment){ |comment| linking(h(comment.comment)) }
node(:inappropriated){|comment| comment.inappropriates.where(user_id: current_user.id).exists?}
node (:created_at){ |comment| comment.created_at}
node(:user){ |comment| partial('users/user_minimal', object: comment.user) }