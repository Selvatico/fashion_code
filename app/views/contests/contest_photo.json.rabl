object @photo
attributes :id
node(:created_at){|photo| distance_of_time_in_words(photo.created_at, Time.now)}
node(:image){|photo| photo.image_url}
node(:user){|photo| partial("users/user_small", object: photo.user)}