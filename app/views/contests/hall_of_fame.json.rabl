collection @users
node(:username){|p| p.user.username }
node(:name){|p| p.user.username }
node(:place){|p| p.current_place}
node(:votes_score){|p| @contest.nil? ? p.user.votes_score : p.votes_score}
node(:avatar){|p| p.user.avatar.medium.url}
node(:fashion_style){|p| p.user.fashion_style && p.user.fashion_style.name}
node(:previous_place){|user| user.previous_place }
node(:followers_count){|p| p.user.followers_count }
node(:verified){|user| false } #TODO: stub
node(:wons){|p| partial("contests/contest", object: Contest.wun_by_user(p.user.id).limit(2)) }