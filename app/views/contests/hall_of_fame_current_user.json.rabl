object @user
node(:votes_score){|participant| @contest.nil? ? small_number(participant.user.votes_score) : small_number(participant.votes_score)}
node(:username){|participant| participant.user.username}
node(:name){|participant| participant.user.name}
node(:avatar){|participant| participant.user.avatar.medium.url}
node(:fashion_style){|participant| participant.user.fashion_style.name if participant.user.fashion_style}
# node(:place){|participant| small_number(participant.current_place)}
# node(:level){|participant| participant.current_place==participant.previous_place ? 'none' : (participant.current_place>participant.previous_place ? 'up' : 'down')}
node(:previous_fame_place){|participant| participant.fame_previous_place}
node(:place){@current_user_place}
node(:photos_count){|participant| small_number(participant.user.photos.active.count)}
node(:wons){|participant| partial("contests/contest", object: Contest.wun_by_user(participant.user_id).limit(2)) }