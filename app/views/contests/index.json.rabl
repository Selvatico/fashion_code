collection @contests
attributes :id, :name, :started_at, :description, :finished
node(:votes_count) {|contest| small_number(contest.votes_count)}
node(:expired_at){|contest| contest.expired_at.strftime('%d %B')}
node(:reported){|contest| contest.inappropriates.where(user_id: current_user.id).exists?}
node(:prize){|contest| contest.prize.name}
node(:image){|contest| contest.prize.image_url.to_s}
node(:best_photo){ partial("contests/contest_photo", object: @best_photo) }
node(:users){|contest| partial("users/user_small", object: Contest.find(contest.id).fame(5)) }
node(:finish_poster) {|contest| "http://images.modera.co/uploads/modera/contests/#{contest.id}/contest.jpg"}
node(:winner) {|contest| "http://images.modera.co/uploads/modera/contests/#{contest.id}/winner.jpg"}
node(:best_photo){ partial("contests/contest_photo", object: @best_photo) }
node(:user_place){|contest| small_number(@place = contest.participant_place(@user_id)) if contest.participants.where(user_id: @user_id).present? }