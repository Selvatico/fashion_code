collection @contests => :contests
attributes :id, :name, :description, :started_at, :expired_at
node(:prize){|contest| contest.prize ? contest.prize.image_url : nil}
node(:user_place){|contest| current_user.current_place(contest.id) if contest.participants.where(user_id: current_user.id).present? }
