collection @fashion_styles => :fashion_styles
attributes :id, :name
node(:tags) {|style| style.tag_list.join(', ')}
node(:image) {|style| style.image_url.to_s}
node(:users_count) {|style| small_number(style.users.count)}