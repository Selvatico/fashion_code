collection @users
attribute :username, :name

node(:place){|user| user.place}
node(:votes_score){|user| @contest_id && user.contest_votes_score || user.votes_score}
node(:avatar){|user| user.avatar.medium.url}
node(:fashion_style){|user| user.fashion_style && user.fashion_style.name}
node(:previous_place){|user| user.previous_place }
node(:followers_count){|user| user.followers_count }
node(:verified){|user| false } #TODO: stub
#TODO: n+1 query problem. need to be optimized!
node(:wons){|user| partial("contests/contest", object: Contest.wun_by_user(user.id).limit(2)) }