object false
node(:owner){partial('users/user_medium', object: @contest.owner)}
node(:users_count){@users_count}
child @photos => :photos do
  attributes :id, :created_at
  node(:user) {|photo| partial("versuses/user", object: photo.user) }
  node(:image){|photo| photo.image.url}
end