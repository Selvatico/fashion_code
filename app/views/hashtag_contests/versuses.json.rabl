object false
node(:owner){partial('users/user_medium', object: @contest ? @contest.owner : nil)}
node(:users_count){@users_count}
child @versuses => :versuses do
  node(:challenge_id){ |versus|
    if versus[0].active_challenge.nil?
      (versus[1].active_challenge.nil? ? nil : versus[1].active_challenge.id)
    else
       versus[0].active_challenge.id
    end}
  node(:photo){ |versus| partial("versuses/photo", object: versus[0]) }
  node(:opponent_photo){ |versus| partial("versuses/photo", object: versus[1]) }
end