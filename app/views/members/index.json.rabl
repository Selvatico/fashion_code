child @members => :members do
  attributes :id, :fullname, :position
  node(:avatar){|member| member.avatar.url}
end