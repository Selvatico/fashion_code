child @member => :member do
  attributes :id, :fullname, :position
  node(:avatar){|member| member.avatar.url}
  node(:background){|member| member.background.url}
end
child @member.questionaries => :questionaries do
  attributes :question, :answer
end