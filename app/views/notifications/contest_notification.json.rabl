object @contest
attributes :id, :name
node(:prize) { |contest| contest.prize ? contest.prize.image.url : nil }