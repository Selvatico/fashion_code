collection @notifications
attributes :id, :created_at, :read
attribute :kind => :type
node :notification do |notification|
  {
      contest: [:contest_ten, :contest_finished, :won_contest, :contest_started].include?(notification.kind) ? partial('notifications/contest_notification', object: notification.notifible) : nil,
      user: [:won_contest, :new_follower, :friend_won, :facebook_friend, :mentioned, :commented, :voted].include?(notification.kind) ? partial('notifications/user', object: notification.actor) : nil,
      photo: if [:voted, :photo_inappropriated, :commented, :mentioned].include?(notification.kind)
               object = case notification.kind
                          when :commented, :mentioned
                            notification.notifible.photo
                          when :photo_inappropriated
                            notification.notifible
                          when :voted
                            vote = notification.notifible
                            vote.voted_photo.user_id == notification.receiver_id ? vote.voted_photo : vote.unvoted_photo
                        end
               partial('notifications/photo_notification', object: object)
             end,
      comment: [:commented, :mentioned].include?(notification.kind) ? partial('notifications/comment_notification', object: notification.notifible) : nil,
      place: [:level_up, :level_down].include?(notification.kind) ? notification.place : nil
  }
end