object @notification
attributes :id, :kind, :grouping_kind, :receiver_id, :actor_id, :notifible_id, :notifible_type, :contest_name, :challenge_level, :challenge_style, :challenge_opponent, :contest_fame, :followed_name, :comment, :score, :place, :picture_id, :picture, :read, :created_at, :opponent_photo, :opponent_photo_id
node(:poster_thumb){|notification| notification.poster_url(:thumb)}
node(:poster){|notification| notification.poster_url}
if root_object.contest_start
  node(:contest_start){|notification| notification.contest_start.strftime('%d %B')}
end
if root_object.contest_finish
  node(:contest_finish){|notification| notification.contest_finish.strftime('%d %B')}
end
if root_object.notifible_type == 'User'
  node(:new_following){|notification| partial('modera/notifications/user', object: notification.notifible) }
end
unless root_object.opponent_id.nil?  
  node(:user){|notification| partial('modera/notifications/user', object: notification.opponent) }
end
unless root_object.actor_id.nil?  
  node(:actor){|notification| partial('modera/notifications/user', object: notification.actor) }
end