object @user
attributes :username, :name, :avatar

node(:avatar){|user| user.avatar_url(:medium)}