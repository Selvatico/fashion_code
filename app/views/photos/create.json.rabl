object @photo
attributes :id, :created_at
node(:image){ |photo| photo.image.big.url.to_s}
node(:share_url){ |photo| photo.share_url }
node(:pinterest_url) { |photo| photo.social_url('pinterest') }
node(:facebook_url) { |photo| photo.social_url('facebook') }
node(:twitter_url) { |photo| photo.social_url('twitter') }
node(:tumblr_url) { |photo| photo.social_url('tumblr') }