object false
node(:photos_count){ @photos_count }
child @photos => :photos do
  attributes :id, :created_at, :votes_score, :share_url
  if @view == 'list'
    node(:image){|photo| photo.image.url}
  elsif @view == 'grid'
    node(:image){|photo| photo.image.big.url}
  end
  node(:voted){ |photo| photo.voted?(current_user) }
  node(:comments_count){ |photo| photo.comments.count }
  node(:comment){ |photo| partial("comments/comment", object: photo.first_comment) }
  node(:rate){ |photo| photo.rate }
  node(:compares_count){ |photo| photo.up_votes.count + photo.down_votes.count }
  node(:pinterest_url) { |photo| photo.social_url('pinterest') }
  node(:facebook_url) { |photo| photo.social_url('facebook') }
  node(:twitter_url) { |photo| photo.social_url('twitter') }
  node(:tumblr_url) { |photo| photo.social_url('tumblr') }
end