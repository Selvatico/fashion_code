object @photo
attributes :id, :share_url
node(:photos_count){|photo| small_number(@photos_count)}
node(:user){|photo| partial("users/user_small", object: photo.user)}
node(:created_at){|photo| distance_of_time_in_words_to_now(photo.created_at)}
node(:image){|photo| photo.image.url}
node(:is_saved){|photo| @saved_photos.include?(photo.id)}
node(:votes_score){|photo| small_number(photo.votes_score)}
node(:voted){ |photo| photo.voted?(@current_user) }
node(:last_voters) {|photo| partial('users/user', object: photo.last_voters(7))}
node(:comments_count){ |photo| small_number(photo.comments.count) }
node(:comment){ |photo| partial("comments/comment", object: photo.comments.last) }