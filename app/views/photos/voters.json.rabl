child @voters => :voters do |voter|
  attributes :username
  node(:name){|voter| voter.name}
  node(:avatar){|voter| voter.avatar.small.url}
  node(:fashion_style){|voter| voter.fashion_style.name}
  node(:is_following){|voter| @following_ids.include? voter.id}
end