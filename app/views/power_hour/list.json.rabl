collection @versuses
attributes :id
node(:voted_photo_score){|versus| small_number(versus.voted_photo_score)}
node(:unvoted_photo_score){|versus| small_number(versus.unvoted_photo_score)}
node(:challenge_votes_count){|versus| small_number(versus.challenge_votes_count)}
node(:challenge){ |versus| partial('power_hour/challenge', object: versus.challenge) }
node(:last_voter){ |versus| partial('users/user_small', object: versus.voter) }
node(:photo){ |versus| partial('power_hour/versus_photo', object: (@current_user_photo_ids.include?(versus.voted_photo_id) ? versus.voted_photo.set_one_vote_weight!(versus.vote_score) : versus.unvoted_photo.set_one_vote_weight!(versus.unvote_score))) }
node(:opponent_photo){ |versus| versus.unvoted_photo.nil? ? nil : partial('power_hour/versus_photo', object: (@current_user_photo_ids.include?(versus.voted_photo_id) ? versus.unvoted_photo.set_one_vote_weight!(versus.unvote_score) : versus.voted_photo.set_one_vote_weight!(versus.vote_score))) }