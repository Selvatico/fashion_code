object @photo
attributes :id
node(:created_at) { |photo| photo.created_at }
node(:user) { |photo| partial('users/user_small', object: photo.user) }
node(:image){ |photo| photo.image_url.to_s}
node(:share_url){ |photo| photo.share_url }
node(:comments_count){ |photo| photo.comments.count }
node(:comment){ |photo| partial("comments/comment", object: photo.first_comment) }
node(:score) { |photo| photo.votes_score }
node(:vote_weight){ |photo| photo.one_vote_weight }
node(:pinterest_url) { |photo| photo.social_url('pinterest') }
node(:facebook_url) { |photo| photo.social_url('facebook') }
node(:twitter_url) { |photo| photo.social_url('twitter') }
node(:tumblr_url) { |photo| photo.social_url('tumblr') }