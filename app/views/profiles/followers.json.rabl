object false
node(:count){ @users ? @users.total_count : 0}
child @users => :users do |user|
  attributes :username
  node(:name){|user| user.name}
  node(:avatar){|user| user.avatar.medium.url}
  node(:is_following){|user| @following_ids.include?(user.id)}
  node(:followers_count){|user| user.followers.count}
  node(:following_count){|user| user.following.count}
  node(:fashion_style){|user| user.fashion_style.name unless user.fashion_style.nil?}
end