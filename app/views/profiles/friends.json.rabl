object false
node(:friends_count){ @friends && @friends.count || 0 }
node(:friends) do
    @friends&&@friends.map do |f|
        is_follow = @uids.keys.include? f.id
        {
            uid: f.id,
            name: f.fullname,
            avatar: f.avatar,
            profile_url: is_follow && "/profiles/#{@following[@uids[f.id]]}" || "http://facebook.com/#{f.id}",
            is_follow: is_follow

        }
    end || []
end
