object false
child @profile => :info do
  node(:place) { @profile.current_place }
  node(:username) { @profile.username }
  node(:fullname) { @profile.name }
  node(:avatar) { @profile.avatar.big.to_s }
  node(:photos) { @profile.photos.active.count }
  node(:followers) { @profile.followers_count.to_i }
  node(:following) { @profile.following.count }
  node(:style) { @profile.fashion_style.present? ? @profile.fashion_style.presentation : nil }
  node(:about) { @profile.about }
  node(:facebook) { @profile.facebook }
  node(:twitter) { @profile.twitter }
  node(:instagram) { @profile.instagram }
  node(:followed_by_you) { followed_by_you? }
  node(:owner) { owner? }
  node(:website) { @profile.website }
  if @profile == current_user
    node(:birth) { @profile.birth ? @profile.birth : Date.today }
    node(:sex) { @profile.sex }
    node(:phone) { @profile.phone }
    node(:email) { @profile.email }
    node(:new_follower) { @profile.new_follower }
    node(:new_comment) { @profile.new_comment }
    node(:new_mention) { @profile.new_mention }
    node(:join_contest) { @profile.join_contest }
    node(:friend_joined) { @profile.friend_joined }
    node(:daily_activity) { @profile.daily_activity }
    node(:weekly_activity) { @profile.weekly_activity }
    node(:activity) { @profile.activity }
  end
end
child @profile => :legacy do
  child @profile => :best_photo do
    node(:id) { @profile.best_photo ? @profile.best_photo.id : nil }
    node(:url) { @profile.best_photo ? @profile.best_photo.image.url : nil }
    node(:votes) { @profile.best_photo ? @profile.best_photo.votes_score : nil }
    node(:wins_ratio) { '75' }
  end
  child @profile => :last_followers do
    node(:followers_all) { @profile.followers.count }
    node(:followers_last_week) { @profile.followers_last_week.count }
    node :followers_last do |f|
      node(:username) { f.username }
      node(:fullname) { f.name }
      node(:avatar) { f.avatar.big.to_s }
    end
  end
end
if @profile == current_user
  node(:share_preferences_attributes) do
    [
         partial('profiles/share_preferences', object: current_user.share_preferences.where(social_network: 'facebook').first),
         partial('profiles/share_preferences', object: current_user.share_preferences.where(social_network: 'twitter').first)
    ]
  end
end

