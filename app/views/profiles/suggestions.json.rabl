object false
child @users => :users do |user|
  attributes :username
  node(:name){|user| user.name}
  node(:avatar){|user| user.avatar.url}
  node(:followers_count){|user| user.followers.count}
  node(:following_count){|user| user.following.count}
  node(:fashion_style){|user| user.fashion_style.name unless user.fashion_style.nil?}
end
