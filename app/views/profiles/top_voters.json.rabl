collection @voters
attributes :username
node(:name){|user| user.name}
node(:avatar){|user| user.avatar.medium.url}
node(:is_following){|user| @following_ids.include?(user.id)}
node(:fashion_style){|user| user.fashion_style.name unless user.fashion_style.nil?}