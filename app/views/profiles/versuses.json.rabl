collection @versuses
attributes :id, :created_at
node(:voter){ |versus| partial('users/user_small', object: versus.voter) }

node :photo do |versus|
  partial 'power_hour/versus_photo', object: if @current_user_photo_ids.include?(versus.voted_photo_id)
                                               versus.voted_photo.set_one_vote_weight!(versus.vote_score)
                                             else
                                               versus.unvoted_photo.set_one_vote_weight!(versus.unvote_score)
                                             end
end

node :opponent_photo do |versus|
  if versus.unvoted_photo.present?
    partial 'power_hour/versus_photo', object: if @current_user_photo_ids.include?(versus.voted_photo_id)
                                                 versus.unvoted_photo.set_one_vote_weight!(versus.unvote_score)
                                               else
                                                 versus.voted_photo.set_one_vote_weight!(versus.vote_score)
                                               end
  end
end
