object @resource
attributes :username, :email
node(:avatar) { |resource| resource.avatar_url }
node(:avatar_cache) { |resource| resource.avatar_cache }