object false
node(:photos_count){ small_number(@photos_count) }
child @photos => :photos do |photo|
  attributes :id, :share_url
  node(:created_at){|photo| distance_of_time_in_words_to_now(photo.created_at)}
  node(:image){|photo| photo.image.url}
  node(:voted){ |photo| photo.voted?(@current_user) }
  node(:user){|photo| partial("users/user_small", object: photo.user)}
  node(:last_voters) {|photo| partial('users/user', object: photo.last_voters(7))}
  node(:comments_count){ |photo| small_number(photo.comments.count) }
  node(:comment){ |photo| partial("comments/comment", object: photo.comments.last) }
  node(:voters_count) { |photo| small_number(photo.voters_count) }
  node(:voters_count_number) { |photo| photo.voters_count }
end