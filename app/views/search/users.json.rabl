child @users => :users do |user|
  attributes :username
  node(:name){|user| user.name}
  node(:avatar){|user| user.avatar.small.url}
  node(:is_following){|user| @follower_ids.include? user.id}
end