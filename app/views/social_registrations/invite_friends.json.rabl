collection @friends
node(:uid){|friend| friend.id}
node(:fullname){|friend| friend.fullname}
node(:username){|friend| friend.username}
node(:avatar){|friend| friend.avatar}