object false
child @versuses => :versuses do
  node(:challenge_id){nil}
  node(:photo){ |versus| partial("versuses/photo", object: versus[0]) }
  node(:opponent_photo){ |versus| partial("versuses/photo", object: versus[1]) }
end