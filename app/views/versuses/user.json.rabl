object @user
attributes :username, :name
node(:avatar){|user| user.avatar.minimal.to_s}
node(:is_following){|user| @following_ids.include? user.id}