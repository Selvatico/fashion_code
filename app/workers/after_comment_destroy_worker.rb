module AfterCommentDestroyWorker
  include Twitter::Extractor
  @queue = :after_comment_destroy_queue

  def self.perform comment_id, text, photo_id
    tags = Twitter::Extractor.extract_hashtags(text)
    self_photo = Photo.find(photo_id)
    tags.each do |tag|
      need_to_remove = true
      self_photo.comments.where('not id = ?', comment_id).each do |c|
        need_to_remove = false and break if Twitter::Extractor.extract_hashtags(c.comment).include?(tag)
      end
      if need_to_remove
        self_photo.tag_list.remove(tag)
        self_photo.save!
      end
    end 
  end
end
