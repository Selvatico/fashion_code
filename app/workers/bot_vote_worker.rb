class BotVoteWorker
  @queue = :bot_votes_queue

  def self.perform vote_id
    #begin
      ActiveRecord::Base.transaction do
        vote = Vote.find(vote_id)

        update_photo(vote.voted_photo_id, vote.vote_score)
        update_photo(vote.unvoted_photo_id, vote.unvote_score) unless vote.unvoted_photo_id.nil?

        challenge = vote.challenge
        unless challenge.nil?
          score = challenge.photo_id == vote.voted_photo_id ? vote.vote_score : vote.unvote_score
          update_challenge(challenge, score)
        end
      end
    #rescue
    #  Resque.enqueue_at(1.minutes, BotVoteWorker, vote_id)
    #end
  end

  def self.update_photo(photo_id,score)
    photo = Photo.find(photo_id)
    up_votes_sum = score>0 ? photo.up_votes_sum + score : photo.up_votes_sum
    votes_sum = photo.votes_sum + score.abs
    rank = up_votes_sum/votes_sum.to_f
    photo.update_attributes!(votes_score: ((photo.votes_score + score) > 0 ? (photo.votes_score + score) : 0), up_votes_sum: up_votes_sum, votes_sum: votes_sum, rank: rank)

    #update user's votes_score, for counting vs_top_ranking
    update_user(photo.user_id,score)
    #update all active contest - increase or decrease votes_count - and update participants - votes_score - in active contests
    update_contest(photo.user_id,score)
  end

  def self.update_user(user_id,score)
    user = User.find(user_id)
    user.update_attributes!(votes_score: ((user.votes_score + score) > 0 ? (user.votes_score + score) : 0))

    #TODO move to config
    if user.newbie? && user.votes_score >= 100
      user.grow_up!
    end
  end

  def self.update_contest(user_id,score)
    user = User.find(user_id)
    user.participants.includes(:contest).where('contests.finished = ? and contests.started_at < current_date', false).each do |p|
      p.votes_score = (p.votes_score + score) > 0 ? (p.votes_score + score) : 0
      p.save!
      contest = Contest.find(p.contest_id)
      contest.votes_count = (contest.votes_count + score) if score > 0
      contest.save!
    end
    user.hashtag_participants.includes(:contest).each do |p|
      p.votes_score = (p.votes_score + score) > 0 ? (p.votes_score + score) : 0
      p.save!
      contest = HashtagContest.find(p.hashtag_contest_id)
      contest.votes_count = (contest.votes_count + score) if score > 0
      contest.save!
    end
  end

  def self.update_challenge(challenge, score)
    challenge.votes_count = (challenge.votes_count + score) > 0 ? (challenge.votes_count + score) : 0
    challenge.save
  end
end
