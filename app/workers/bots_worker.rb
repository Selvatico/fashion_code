class BotsWorker
  @queue = :bots_queue

  def self.perform(action, options={})
    send(action, options)
  end

  def self.bot_contest_follow(options)
    contest_follow(options, true)
  end

  def self.contest_follow(options, bots = false)
    contest = Contest.active.where(id: options['contest_id']).first
    if contest
      users = bots ? contest.users.bots.all : contest.users.real.all
      users.each do |user|
        bot = User.bots.where('not users.id in (?)', user.follower_ids).order('RANDOM()').first
        bot.follow!(user) and return true if bot
      end
    end
  end

  def self.bot_contest_vote(options)
    contest_vote(options, true)
  end

  def self.contest_vote(options, bots = false)
    contest = Contest.active.where(id: options['contest_id']).first
    if contest
      users = bots ? contest.users.bots.all : contest.users.real.all
      users.each do |user|
        challenge = user.active_challenge
        if challenge
          photo = challenge.photo
          challenge_type = challenge.challenge_type
          opponent_style_id = (challenge_type.fashion_style_id == user.fashion_style_id) ? challenge_type.opponent_fashion_style_id : challenge_type.fashion_style_id
          opponent_photos = Photo.active.includes(:user).where('NOT photos.user_id = ? AND users.fashion_style_id = ?', user.id, opponent_style_id).order('RANDOM()').all
          User.bots.order('RANDOM()').each do |b|
            opponent_photos.each do |opponent|
              return true if b.vote_photo(photo, opponent, challenge)
            end
          end
        else
          photos = user.photos.active.all
          opponent_photos = Photo.active.where('NOT photos.user_id = ?', user.id).order('RANDOM()').all
          User.bots.order('RANDOM()').each do |b|
            opponent_photos.each do |opponent|
              photos.each do |photo|
                return true if b.vote_photo(photo, opponent, photo.active_challenge)
              end
            end
          end
        end
      end
    end
  end

  def self.unvote_photo(options)
    vote_photo(options, false)
  end

  def self.vote_photo(options, like = true)
    photo = Photo.active.where(id: options['photo_id']).first
    challenge = Challenge.where(id: options['challenge_id']).first
    if photo && !photo.deleted && challenge && !challenge.finished
      challenge_type = challenge.challenge_type
      user_style_id = photo.user.fashion_style_id
      opponent_style_id = (challenge_type.fashion_style_id == user_style_id) ? challenge_type.opponent_fashion_style_id : challenge_type.fashion_style_id
      opponent_photos = Photo.active.includes(:user).where('NOT photos.user_id = ? AND users.fashion_style_id = ?', photo.user_id, opponent_style_id).order('RANDOM()').all
      if like
        User.bots.order('RANDOM()').each do |b|
          opponent_photos.each do |p|
            return true if b.vote_photo(photo, p, challenge)
          end
        end
      else
        User.bots.order('RANDOM()').each do |b|
          opponent_photos.each do |p|
            return true if b.vote_photo(p, photo, challenge)
          end
        end
      end
    end
  end

  def self.like_photo(options)
    photo = Photo.active.where(id: options['photo_id']).first
    if photo
      User.bots.order('RANDOM()').each do |b|
        return true if photo.vote_for(b)
      end
    end
  end

  def self.follow(options)
    user = User.where(id: options['user_id']).first
    if user
      bot = user.follower_ids.empty? ? User.bots.order('RANDOM()').first : User.bots.where('not users.id in (?)', user.follower_ids).order('RANDOM()').first
      bot.follow!(user) if bot
    end
  end

  def self.like_user_photo(options)
    user = User.where(id: options['user_id']).first
    if user
      photos = user.photos.active.all
      User.bots.order('RANDOM()').each do |b|
        photos.each do |photo|
          return true if photo.vote_for(b)
        end
      end
    end
  end

  def self.bot_challenges(options)
    rand_bot_challenges = rand(Settings.bot_challenges_bots.min_number.to_i..Settings.bot_challenges_bots.max_number.to_i)
    for i in 1..rand_bot_challenges
      rand_time = rand(0..Settings.bot_challenges_bots.max_time.to_i)
      Resque.enqueue_in(rand_time.minutes, BotsWorker, 'create_challenge')
    end
  end

  def self.create_challenge(options)
    User.bots.order('RANDOM()').each do |b|
      unless b.active_challenge
        c = b.challenges.new
        c.photo = b.random_photo
        c.level = 'easy'
        ch_t = ChallengeType.by_style(b.fashion_style).first
        c.score = ch_t.easy_score
        c.challenge_type = ch_t
        return if c.save
      end
    end
  end

  def self.follow_back(options)
    bot = User.where(id: options['followed_id']).first
    user = User.where(id: options['follower_id']).first
    if bot && user
      percent = rand(Settings.follow_back_bots.min_percent.to_i..Settings.follow_back_bots.max_percent.to_i)
      bot.follow!(user) if rand(0..100) < percent
    end
  end

  def self.home_activity(options)
    User.bots.all.each do |b|
      for i in 1..Settings.home_activity_bots.send("periods").count do
        period = Settings.home_activity_bots.periods.send("period#{i}")
        bot_votes = rand(period.min_bot_votes.to_i..period.max_bot_votes.to_i)
        for j in 1..bot_votes
          rand_time = rand(period.min_time.to_i..period.max_time.to_i)
          Resque.enqueue_in(rand_time.minutes, BotsWorker, 'bot_home_vote', {for_bot: true, bot_id: b.id})
        end
        bot_follows = rand(period.min_bot_follows.to_i..period.max_bot_follows.to_i)
        for j in 1..bot_follows
          rand_time = rand(period.min_time.to_i..period.max_time.to_i)
          Resque.enqueue_in(rand_time.minutes, BotsWorker, 'bot_home_follow', {for_bot: true, bot_id: b.id, like_percent: period.like_percent})
        end
        votes = rand(period.min_votes.to_i..period.max_votes.to_i)
        for j in 1..votes
          rand_time = rand(period.min_time.to_i..period.max_time.to_i)
          Resque.enqueue_in(rand_time.minutes, BotsWorker, 'bot_home_vote', {for_bot: false, bot_id: b.id})
        end
        follows = rand(period.min_follows.to_i..period.max_follows.to_i)
        for j in 1..follows
          rand_time = rand(period.min_time.to_i..period.max_time.to_i)
          Resque.enqueue_in(rand_time.minutes, BotsWorker, 'bot_home_follow', {for_bot: false, bot_id: b.id, like_percent: period.like_percent})
        end
      end
    end
  end

  def self.bot_home_vote(options)
    bot = User.where(id: options['bot_id']).first
    if bot
      users = options['for_bot'] ? User.bots.where('not id = ?', bot.id).order('RANDOM()').all : User.real.order('RANDOM()').all
      users.each do |u|
        #unless u.active_challenge
        photo = u.photos.active.order('RANDOM()').first
        if photo
          opponent_photo = Photo.active.where('not user_id in (?)', [bot.id, u.id]).order('RANDOM()').first
          return true if bot.vote_photo(photo, opponent_photo)
        end
        #end
      end
    end
  end

  def self.bot_home_follow(options)
    bot = User.where(id: options['bot_id']).first
    if bot
      for_follow = options['for_bot'] ? (bot.following_ids.empty? ? User.bots.where('not id in (?)', [bot.id]).order('RANDOM()').first : User.bots.where('not id in (?)', bot.following_ids.concat([bot.id])).order('RANDOM()').first) : (bot.following_ids.empty? ? User.real.order('RANDOM()').first : User.real.where('not id in (?)', bot.following_ids).order('RANDOM()').first)
      if for_follow
        bot.follow!(for_follow)
        for_follow.photos.each do |p|
          p.vote_for(bot) if rand(0..100) < options['like_percent'].to_i
        end
      end
    end
  end
end