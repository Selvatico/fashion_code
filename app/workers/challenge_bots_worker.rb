class ChallengeBotsWorker
  @queue = :challenge_bots_queue

  def self.perform(action, options={})
    send(action, options)
  end

  def self.unvote_photo(options)
    vote_photo(options, false)
  end

  def self.vote_photo(options, like = true)
    photo = Photo.active.where(id: options['photo_id']).first
    challenge = Challenge.where(id: options['challenge_id']).first
    if photo && !photo.deleted && challenge && !challenge.finished
      challenge_type = challenge.challenge_type
      user_style_id = photo.user.fashion_style_id
      opponent_style_id = (challenge_type.fashion_style_id == user_style_id) ? challenge_type.opponent_fashion_style_id : challenge_type.fashion_style_id
      opponent_photos = Photo.active.includes(:user).where('NOT photos.user_id = ? AND users.fashion_style_id = ?', photo.user_id, opponent_style_id).order('RANDOM()').all
      if like
        User.bots.order('RANDOM()').each do |b|
          opponent_photos.each do |p|
            return true if b.vote_photo(photo, p, challenge)
          end
        end
      else
        User.bots.order('RANDOM()').each do |b|
          opponent_photos.each do |p|
            return true if b.vote_photo(p, photo, challenge)
          end
        end
      end
    end
  end
end