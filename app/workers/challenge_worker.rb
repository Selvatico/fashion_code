class ChallengeWorker
  @queue = :challenges_queue

  def self.perform challenge_id
    challenge = Challenge.find(challenge_id)
    challenge.update_attributes!(finished: true)
    photo = challenge.photo
    photo.challenge_votes_count = 0
    photo.save!
  end
end
