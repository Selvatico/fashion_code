class ClearNotificationsWorker
  @queue = :clear_notifications_queue

  def self.perform(action, options=nil)
    case action
    when 'by_user'
      send(action, options['user_id'], options['owner'])
    when 'global'
      send(action)
    end
  end

  def self.by_user(user_id, owner)
    if owner
      User.find(user_id).incoming_notifications.by_following(false).delete_all
    else
      User.find(user_id).incoming_notifications..by_following(true).delete_all
    end
  end

  def self.global
    safe_days = Settings.global.safe_notifications_days.to_i
    Notifications::Notification.where('notifications.created_at < ? AND notifications.viewed = ?', DateTime.now - safe_days.days, true).delete_all
  end
end