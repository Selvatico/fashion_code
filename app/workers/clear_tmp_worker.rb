require 'fileutils'

class ClearTmpWorker
  @queue = :clear_tmp_queue

  def self.perform
    Dir.glob("#{Rails.root}/public/uploads/tmp/**").each do |dir|
      if dir.split('/').last.split('-').first < Date.today.strftime("%Y%m%d")
        FileUtils.rm_r(dir)
      end
    end
  end
end

