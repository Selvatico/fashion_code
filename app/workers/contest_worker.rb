class ContestWorker
  @queue = :contests_queue

  def self.perform contest_id
    contest = Contest.find(contest_id)
    winner = contest.fame(1)[0]
    contest.finished = true
    contest.winner = winner
    contest.save
  end
end
