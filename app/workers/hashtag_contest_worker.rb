module HashtagContestWorker
  @queue = :hashtag_contest_queue

  def self.perform action, options={}
    send(action, options)
  end

  def self.create_hashtag_participant options
    tagging = ActsAsTaggableOn::Tagging.find_by_id(options['tagging_id'])
    return unless tagging
    if tagging.taggable_type == 'Photo'
      self_tag = tagging.tag
      c = HashtagContest.find_by_name(self_tag.name)
      if c
        if c.participants.where(user_id: tagging.taggable.user_id).empty?
          p = c.participants.new
          p.user_id = tagging.taggable.user_id
          p.hashtag_contest_id = c.id
          place = c.participants.count
          p.current_place = place + 1
          p.previous_place = place + 1
          p.fame_previous_place = place + 1
          p.save!
        end
      else
        c = HashtagContest.new
        c.owner_id = tagging.taggable.user_id
        c.name = self_tag.name
        p = c.participants.new
        p.user_id = tagging.taggable.user_id
        p.current_place = 1
        p.previous_place = 1
        p.fame_previous_place = 1
        c.save!
      end
    end
  end

  def self.destroy_hashtag_participant options
    self_tag = ActsAsTaggableOn::Tag.find_by_id(options['tag_id'])
    photo = Photo.find_by_id(options['photo_id'])
    return unless self_tag && photo
    photo_ids = photo.user.photo_ids
    if self_tag.taggings.where('taggings.taggable_id in (?) and taggings.taggable_type = ?', photo_ids, 'Photo').count == 0 && !self_tag.frozen?
      contest = HashtagContest.find_by_name(self_tag.name)
      if contest
        p = HashtagParticipant.where(user_id: photo.user_id, hashtag_contest_id: contest.id).first
        p.destroy
        if contest.owner_id == photo.user_id && contest.participants.count > 0
          contest.owner_id = contest.participants.order('created_at asc').first.user_id
          contest.save!
        end
        begin
          ActiveRecord::Base.connection.execute("select hall_of_fame()")
        rescue
        end
      end
    end
  end
end
