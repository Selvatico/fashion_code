class InvitationsWorker
  @queue = :invitations_queue


  def self.perform(emails, message, inviter)
    #TODO move somwhere
    email_regex = /\A[^@]+@[^@]+\z/

    extract_emails(emails).each do |e|
      if e =~ email_regex
        unless User.where(email: e).exists?
          Invitation.create(email: e, user_id: inviter)
        end
      end
    end
  end


  def self.extract_emails(emails)
    e = emails.gsub(" ","").split(",")
  end

end
