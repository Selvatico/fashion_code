class Notifications::ChallengeFinishedWorker
  @queue = :notifications_queue

  def self.perform challenge_id
    challenge = Challenge.find_by_id(challenge_id)
    if challenge && challenge.finished
      user = challenge.user
      photo = challenge.photo
      unless user.bot
        poster_path = Notifications::Modster.congratulations(challenge.photo.image_url, challenge.votes_count) #make poster with challenge.photo.votes_score
        n = Notification.new(kind: 'challenge_finished', grouping_kind: 'challenge_finished', receiver_id: user.id, notifible: challenge, score: challenge.votes_count, picture: photo.image_url(:small), picture_id: photo.id, following: false)
        n.poster = File.open(poster_path, 'r')
        n.save!
        File.delete(poster_path)
      end
    end
  end
end