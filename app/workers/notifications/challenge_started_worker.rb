class Notifications::ChallengeStartedWorker
  @queue = :notifications_queue

  def self.perform challenge_id
    challenge = Challenge.find_by_id(challenge_id)
    if challenge
      user = challenge.user
      challenge_type = challenge.challenge_type
      photo = challenge.photo
      unless user.bot
        poster_path = Notifications::Modster.power_hour(challenge.photo.image_url, photo.votes_score)
        n = Notification.new(kind: 'challenge_started', grouping_kind: 'challenge_started', receiver_id: user.id, notifible: challenge, challenge_level: challenge.level, challenge_style: challenge_type.fashion_style.name, challenge_opponent: challenge_type.opponent_fashion_style.name, score: challenge.score, picture_id: challenge.photo_id, picture: photo.image_url(:medium), following: false)
        n.poster = File.open(poster_path, 'r')
        n.save!
        File.delete(poster_path)
      end
      photo.user.followers.each do |friend|
        Notification.create!(kind: 'challenge_start', grouping_kind:'challenge_start', receiver_id: friend.id, actor: user, notifible: challenge, picture_id: challenge.photo_id, picture: photo.image_url(:small), following: true) unless friend.bot
      end
    end
  end
end