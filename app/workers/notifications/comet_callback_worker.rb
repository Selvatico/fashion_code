class Notifications::CometCallbackWorker
  @queue = :notifications_queue

  def self.perform user_id
    user = User.find user_id
    PrivatePub.publish_to("/notifications/#{user.id}", stats: user.notification_stats)
  end
end
