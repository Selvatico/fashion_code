class Notifications::CommentedMentionedWorker
  @queue = :notifications_queue

  def self.perform comment_id
    comment = Comment.find_by_id(comment_id)
    if comment
      photo = comment.photo
      photo_user = photo.user
      actor = comment.user
      if actor != photo_user && !photo_user.bot
        Rails.logger.debug 'Create commented notification'
        Notification.create!(kind: 'commented', grouping_kind: 'commented', receiver: photo_user, actor: actor, notifible: comment, comment: comment.comment, following: false)
        Emailer.new_comment(photo_user, actor, comment).deliver! if photo_user.new_comment
      end

      users = comment.mentioned_users
      users.uniq.each do |user|
        if user != actor && !user.bot
          Rails.logger.debug 'Create mentioned notification'
          Notification.create!(kind: 'mentioned', grouping_kind: 'mentioned', receiver: user, actor: actor, notifible: comment, comment: comment.comment, following: false)
          Emailer.mentioned(user, actor, comment).deliver! if photo_user.new_mention
        end
      end
    end
  end
end