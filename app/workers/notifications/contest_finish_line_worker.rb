class Notifications::ContestFinishLineWorker
  @queue = :notifications_queue

  def self.perform contest_id
    contest = Contest.find_by_id(contest_id)
    if contest
      prize = contest.prize
      contest.users.each do |user|
        Notification.create!(kind: 'contest_finish_line', grouping_kind: 'contest_finish_line', receiver_id: user.id, notifible: contest, contest_name: contest.name, picture: prize.image_url, following: false) unless user.bot
      end
    end
  end
end