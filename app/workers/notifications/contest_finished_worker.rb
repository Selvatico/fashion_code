class Notifications::ContestFinishedWorker
  @queue = :notifications_queue

  def self.perform(contest_id)
    Rails.logger.debug 'ContestFinishedWorker started'
    contest = Contest.find_by_id(contest_id)
    if contest && contest.finished
      fame_data = []
      contest.fame(5).each_with_index do |u,i|
        fame_data.push({username: u.username, fullname: u.name, avatar: u.avatar_url(:medium), place: i+1})
      end
      fame_text = fame_data.to_json
      #prize = contest.prize
      contest.users.each do |recipient|
        Notification.create!(kind: 'contest_finished', grouping_kind: 'contest_finished', receiver_id: recipient.id, notifible: contest, contest_name: contest.name, contest_fame: fame_text, place: contest.participant_place(recipient.id), following: false) unless recipient.bot
      end
      winner =  contest.winner
      Notification.create!(kind: 'won_contest', grouping_kind: 'won_contest', receiver_id: winner.id, actor: winner, notifible: contest, contest_name: contest.name, contest_fame: fame_text, following: false) unless winner.bot
      winner.followers.each do |follower|
        Notification.create!(kind: 'friend_won', grouping_kind: 'friend_won', receiver_id: follower.id, notifible: contest, contest_name: contest.name, actor: winner, contest_fame: fame_text, following: false) unless follower.bot
      end
    end
    Rails.logger.debug 'ContestFinishedWorker ended'
  end
end