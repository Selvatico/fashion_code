class Notifications::ContestStartedWorker
  @queue = :notifications_queue

  def self.perform contest_id
    contest = Contest.find_by_id(contest_id)
    if contest
      Resque.enqueue_at(contest.expired_at - 10.minutes, Notifications::ContestFinishLineWorker, contest.id)
      
      prize = contest.prize
      User.find_each(:batch_size => 5000) do |user|
        Notification.create!(kind: 'contest_started', grouping_kind: 'contest_started', receiver_id: user.id, notifible: contest, contest_name: contest.name, contest_start: contest.created_at.strftime('%d %B'), contest_finish: contest.expired_at.strftime('%d %B'), picture: prize.image_url, following: false) unless user.bot
      end
    end
  end
end