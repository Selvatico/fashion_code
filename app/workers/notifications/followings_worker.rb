class Notifications::FollowingsWorker
  @queue = :notifications_queue

  def self.perform relationship_id
    relationship = Relationship.find_by_id(relationship_id)
    if relationship
      receiver = relationship.followed
      follower = relationship.follower
      unless receiver.bot
        Notification.create!(kind: 'new_follower', grouping_kind: 'new_follower', receiver: receiver, actor: follower, following: false)
        Emailer.new_follower(receiver, follower).deliver! if receiver.new_follower
        #follower.followers.each do |friend|
        #  n = Notification.new(kind: 'new_following', grouping_kind: 'new_following', notifible: user, following: true)
        #  n.receiver = friend
        #  n.actor = follower
        #  n.save!
        #end
      end
    end
  end
end