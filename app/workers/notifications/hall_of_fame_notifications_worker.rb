class Notifications::HallOfFameNotificationsWorker
  @queue = :daily_hall_of_fame_queue

  def self.perform
    begin
      ActiveRecord::Base.connection.execute("select daily_hall_of_fame();")
    rescue
      Resque.enqueue_at(2.minutes, Notifications::HallOfFameNotificationsWorker) and return
    end

    Participant.global.includes(:user).where('users.bot = ?', false).each do |participant|
      user = participant.user
      if participant.current_place < participant.fame_previous_place
        if participant.fame_previous_place>10 and participant.current_place<=10 and participant.current_place>3
        Notification.create!(kind: 'ten', grouping_kind: 'ten', place: participant.current_place, receiver_id: user.id, following: false)
        elsif participant.fame_previous_place>3 and participant.current_place<=3 and participant.current_place!=1
          Notification.create!(kind: 'three', grouping_kind: 'three', place: participant.current_place, receiver_id: user.id, following: false)
        elsif participant.current_place == 1
          Notification.create!(kind: 'first', grouping_kind: 'first', place: participant.current_place, receiver_id: user.id, following: false)
        else
          Notification.create!(kind: 'level_up', grouping_kind: 'level_up', place: participant.current_place, receiver_id: user.id, following: false)
        end
      elsif participant.current_place > participant.fame_previous_place
        Notification.create!(kind: 'level_down', grouping_kind: 'level_down', place: participant.current_place, receiver_id: user.id, following: false)
      end
    end

    # TODO think of giving photo with this tag of participant
    HashtagContest.all.each do |c|
      c.participants.includes(:user).where('users.bot = ? AND hashtag_participants.receive_notifications = ?', false, true).each do |participant|
        user = participant.user
        if participant.current_place < participant.fame_previous_place
          if participant.fame_previous_place>10 and participant.current_place<=10 and participant.current_place>3
            Notification.create!(kind: 'hash_cont_ten', grouping_kind: 'hash_cont_ten', contest_name: c.name, place: participant.current_place, receiver_id: user.id, following: false)
          elsif participant.fame_previous_place>3 and participant.current_place<=3 and participant.current_place!=1
            Notification.create!(kind: 'hash_cont_three', grouping_kind: 'hash_cont_three', contest_name: c.name, place: participant.current_place, receiver_id: user.id, following: false)
          elsif participant.current_place == 1
            Notification.create!(kind: 'hash_cont_first', grouping_kind: 'hash_cont_first', contest_name: c.name, place: participant.current_place, receiver_id: user.id, following: false)
          else
            Notification.create!(kind: 'hash_cont_level_up', grouping_kind: 'hash_cont_level_up', contest_name: c.name, place: participant.current_place, receiver_id: user.id, following: false)
          end
        elsif participant.current_place > participant.fame_previous_place
          Notification.create!(kind: 'hash_cont_level_down', grouping_kind: 'hash_cont_level_down', contest_name: c.name, place: participant.current_place, receiver_id: user.id, following: false)
        end
      end
    end

    Contest.active.each do |c|
      p = c.prize
   #   two_winners = c.fame(2)
 #     first_place_photo = two_winners.first.best_photo.image_url
  #    second_place_photo = two_winners[1].best_photo.image_url
      c.participants.includes(:user).where('users.bot = ?', false).each do |participant|
        user = participant.user
#        right_image = two_winners[0].id==user.id ? second_place_photo : first_place_photo
        poster_path = Notifications::Modster.contest(user.name, participant.votes_score, user.best_photo.image_url, p.image_url)
        #make poster with participant.votes_score, user.best_photo.image_url, two_winners[0].user_id==user.id ? second_place_photo : first_place_photo
        n = Notification.new(kind: 'contest_daily_score', grouping_kind: 'contest_daily_score', notifible: c, contest_name: c.name, score: participant.votes_score, receiver_id: user.id, following: false)
        n.poster = File.open(poster_path, 'r')
        n.save!
        File.delete(poster_path)
        if participant.current_place < participant.fame_previous_place
          if participant.fame_previous_place>10 and participant.current_place<=10 and participant.current_place>3
            Notification.create!(kind: 'contest_ten', grouping_kind: 'contest_ten', notifible: c, contest_name: c.name, picture: p.image_url, place: participant.current_place, score: participant.votes_score, receiver_id: user.id, following: false)
          elsif participant.fame_previous_place>3 and participant.current_place<=3 and participant.current_place!=1
            Notification.create!(kind: 'contest_three', grouping_kind: 'contest_three', notifible: c, contest_name: c.name, picture: p.image_url, place: participant.current_place, score: participant.votes_score, receiver_id: user.id, following: false)
          elsif participant.current_place == 1
            Notification.create!(kind: 'contest_first', grouping_kind: 'contest_first', notifible: c, contest_name: c.name, picture: p.image_url, place: participant.current_place, score: participant.votes_score, receiver_id: user.id, following: false)
          else
            Notification.create!(kind: 'contest_level_up', grouping_kind: 'contest_level_up', notifible: c, contest_name: c.name, picture: p.image_url, place: participant.current_place, score: participant.votes_score, receiver_id: user.id, following: false)
          end
        elsif participant.current_place > participant.fame_previous_place
          Notification.create!(kind: 'contest_level_down', grouping_kind: 'contest_level_down', notifible: c, contest_name: c.name, picture: p.image_url, place: participant.current_place, score: participant.votes_score, receiver_id: user.id, following: false)
        end
      end
    end
  end
end