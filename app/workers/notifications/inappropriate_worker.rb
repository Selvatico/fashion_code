class Notifications::InappropriateWorker
  @queue = :notifications_queue

  def self.perform(inappropriate_id)
    Rails.logger.debug 'InappropriateWorker started'
    inappropriate = Inappropriate.find(inappropriate_id)
    inappropriateable = inappropriate.inappropriateable
    receiver = inappropriateable.user
    Rails.logger.debug 'owner '+receiver.inspect
    return if receiver.bot
    case inappropriate.inappropriateable_type.downcase
      when Photo.model_name.downcase
        Notification.create!(kind: 'photo_inappropriated', grouping_kind: 'photo_inappropriated', receiver: receiver, actor: inappropriate.user, notifible: inappropriateable, following: false)
    end
  end
end
