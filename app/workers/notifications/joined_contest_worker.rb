class Notifications::JoinedContestWorker
  @queue = :notifications_queue

  def self.perform participant_id
    participant = Participant.find_by_id(participant_id)
    unless participant.nil? || participant.contest_id.nil?
      participant.user.followers.each do |friend|
        user = participant.user
        Notification.create!(kind: 'joined_contest', grouping_kind: 'joined_contest', receiver_id: friend.id, actor: participant.user, notifible: contest, contest_name: contest.name, picture: prize.image_url, following: true) unless friend.bot
      end
    end
  end
end