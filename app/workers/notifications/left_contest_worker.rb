class Notifications::LeftContestWorker
  @queue = :notifications_queue

  def self.perform user_id, contest_id
    user = User.find_by_id(user_id)
    contest = Contest.find_by_id(contest_id)
    if user && contest
      prize = contest.prize
      user.followers.each do |friend|
        Notification.create!(kind: 'left_contest', grouping_kind: 'left_contest', receiver: friend, actor: user, notifible: contest, contest_name: contest.name, picture: prize.image_url, following: true) unless friend.bot
      end
    end
  end
end