class Notifications::PhotoUploadedWorker
  @queue = :notifications_queue

  def self.perform photo_id
    photo = Photo.find_by_id(photo_id)
    if photo
      user = photo.user
      user.followers.each do |friend|
        Notification.create!(kind: 'photo_uploaded', grouping_kind: 'photo_uploaded', receiver_id: friend.id, actor: user, notifible: photo, picture: photo.image_url(:small), picture_id: photo.id, following: true) unless friend.bot
      end
    end
  end
end