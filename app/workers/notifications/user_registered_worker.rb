class Notifications::UserRegisteredWorker
  @queue = :notifications_queue

  def self.perform user_id
    user = User.find_by_id(user_id)
    if user.user_authentications.where(provider: 'facebook').first
      friends = user.friends('facebook')
      friends_to_notify = UserAuthentication.where(uid: friends.select{|f| f.in_modera}.collect{|f| f.id})
      friends_to_notify.each do |receiver|
        Notification.create!(kind: 'facebook_friend', grouping_kind: 'facebook_friend', receiver: receiver, actor: user, following: false)
        Emailer.new_fb_friend(receiver, user).deliver! if receiver.friend_joined
      end
    end
  end
end