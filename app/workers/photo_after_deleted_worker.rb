class PhotoAfterDeletedWorker
  @queue = :after_photo_delete_queue

  def self.perform(photo_id)
    photo = Photo.find(photo_id)
    photo.comments.delete_all
    photo.savings.delete_all
  end
end
