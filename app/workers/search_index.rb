class SearchIndex
  @queue = :search_index_queue

  def self.perform(object, term, expected_result, remove = false)
    Search.index_term(object, term, expected_result, remove)
  end

end
