class VsRankingWorker
  @queue = :vs_ranking_queue

  def self.perform
    all_photos = Photo.active.select('id,user_id,votes_score,up_votes_sum,votes_sum,rank,created_at').all

    av = 0
    ar = 0
    count = 0
    all_photos.each do |p|
      if p.votes_sum > 0
        av = av + p.votes_sum
        ar = ar + p.rank
        count = count + 1
      end
    end
    av = count==0 ? 0 : av / count.to_f
    ar = count==0 ? 0 : ar / count.to_f

    all_users = User.select('id,votes_score').all
    max_rank = User.maximum(:votes_score)

    vs_ranking = []
    vs_ranking_length = 0
    all_photos.each do |p|
      age = p.age
      bayes_factor = av+p.votes_sum==0 ? 0 : (av*ar+p.votes_sum*p.rank)/(av+p.votes_sum)
      age_factor = age>=1 ? 1/age**0.001 : 1
      user_rank_factor = max_rank==0 ? 0 : (all_users.select{|u| u.id==p.user_id}[0].votes_score/max_rank)**(2/3)
      newby_factor = age>=Settings.vs_ranking.new_age.to_i ? Settings.vs_ranking.new_age.to_i/Math.sqrt(age) : 1
      vs_ranking.push([p.id, ((Settings.vs_ranking.alpha.to_r*bayes_factor*age_factor + Settings.vs_ranking.beta.to_r*user_rank_factor + Settings.vs_ranking.gamma.to_r*newby_factor)*10000).to_i])
      vs_ranking_length = vs_ranking_length + 1
    end

    vs_ranking.sort_by!{|x,y|y}

    top = VsTopPhoto.all
    VsTopPhoto.transaction do
      top_count = Settings.vs_ranking.top_count.to_i > vs_ranking_length ? vs_ranking_length : Settings.vs_ranking.top_count.to_i
      for i in 1..top_count
        if top[i-1]
          top[i-1].update_attributes!(photo_id: vs_ranking[vs_ranking_length - i][0], rank: vs_ranking[vs_ranking_length - i][1]) if top[i-1].photo_id!=vs_ranking[vs_ranking_length - i][0]
        else
          VsTopPhoto.create(photo_id: vs_ranking[vs_ranking_length - i][0], rank: vs_ranking[vs_ranking_length - i][1])
        end
      end
    end

  end
end
