set :application, 'modera'

set :stages, %w(production staging)
set :default_stage, 'staging'


set :scm, :git
set :repository,  'git@github.com:Tebecom/modera_web_new.git'

set :rbenv_ruby_version, "1.9.3-p392"

set :use_sudo, false

#TODO: fix tasks in /lib/cap_tasks
after "deploy:create_symlink", "uploads:symlink"
#after "deploy:create_symlink", "settings:symlink"

after "deploy:finalize_update", "capi:symlink_db"
after "deploy:finalize_update", "capi:symlink_config_model"
after "deploy:update", "deploy:cleanup"

after "deploy:restart", "unicorn:restart"
after "deploy:restart", "resque:restart"
after "deploy:restart", "resque:scheduler:restart"

after 'deploy:restart', 'faye:restart'
