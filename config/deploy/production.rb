set :user, 'deploy'
set :rails_env, 'production'

set :branch, 'stable'
set :deploy_to, "/home/#{user}/apps/#{application}"

role :web, 'node00.modera.co','node01.modera.co'
role :app, 'node00.modera.co','node01.modera.co'
role :resque_worker, 'node00.modera.co'
role :resque_scheduler, 'node00.modera.co'
role :db,  'node00.modera.co', primary: true

set :server_name, 'node00.modera.co'

set :keep_releases, 5

set :workers, {
  "votes_queue" => 2,
  "bots_queue" => 3,
  "after_comment_destroy_queue" => 1,
  "bot_votes_queue" => 2,
  "clear_notifications_queue" => 1,
  "clear_tmp_queue" => 1,
  "contests_queue" => 1,
  "hashtag_contest_queue" => 1,
  "invitations_queue" => 1,
  "after_photo_delete_queue" => 1,
  "notifications_queue" => 3,
  "mailer_queue" => 1
}
