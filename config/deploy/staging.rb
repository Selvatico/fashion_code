set :user, 'modera'
set :rails_env, 'staging'

set :branch, 'master'
set :deploy_to, "/srv/#{user}/apps/#{application}"

role :web, 'staging.modera.co'
role :app, 'staging.modera.co'
role :resque_worker, 'staging.modera.co'
role :resque_scheduler, 'staging.modera.co'
role :db,  'staging.modera.co', primary: true

set :server_name, 'staging.modera.co'

set :keep_releases, 3

set :workers, {
  "votes_queue" => 1,
  #"bots_queue" => 1,
  "after_comment_destroy_queue" => 1,
  #"bot_votes_queue" => 1,
  "clear_notifications_queue" => 1,
  #"clear_tmp_queue" => 1,
  "contests_queue" => 1,
  #"hashtag_contest_queue" => 1,
  #"invitations_queue" => 1,
  "after_photo_delete_queue" => 1,
  "notifications_queue" => 1,
  "mailer_queue" => 1
}
