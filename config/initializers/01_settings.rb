Settings.add_source!("#{Rails.root}/config/settings/notification_types.yml")
Settings.add_source!("#{Rails.root}/config/settings/omniauth_keys.yml")
Settings.add_source!("#{Rails.root}/config/settings/vs_ranking.yml")
Settings.add_source!("#{Rails.root}/config/settings/general_settings.yml")
Settings.add_source!("#{Rails.root}/config/settings/roles.yml")
Settings.add_source!("#{Rails.root}/config/settings/browser_support.yml")

#bots
Settings.add_source!("#{Rails.root}/config/settings/bot_challenges_bots.yml")
Settings.add_source!("#{Rails.root}/config/settings/challenges_bots.yml")
Settings.add_source!("#{Rails.root}/config/settings/contests_bots.yml")
Settings.add_source!("#{Rails.root}/config/settings/photos_bots.yml")
Settings.add_source!("#{Rails.root}/config/settings/follow_back_bots.yml")
Settings.add_source!("#{Rails.root}/config/settings/home_activity_bots.yml")
#bots


Settings.reload!