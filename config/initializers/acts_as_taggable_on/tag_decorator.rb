ActsAsTaggableOn::Tag.class_eval do
  include PgSearch
  pg_search_scope :search, :against => [:name],
      using: {tsearch: {dictionary: "english", prefix: true}}

  after_destroy :destroy_hashtag_contest

  #after_create :index_name
  #before_destroy :remove_index

  def serialize_for_search
    { name: name }.to_json
  end

  #TODO refactor
  def index_name
    Resque.enqueue(SearchIndex, :tag, name, serialize_for_search, offset: 1)
  end

  def remove_index
    Resque.enqueue(SearchIndex, :tag, name, serialize_for_search, remove: true)
  end

  protected

  def destroy_hashtag_contest
    c = HashtagContest.find_by_name(self.name)
    c.destroy if c
  end
end