ActsAsTaggableOn::Tagging.class_eval do
  after_commit :create_hashtag_participant, on: :create
  after_destroy :destroy_hashtag_participant

  protected

  def create_hashtag_participant
    Resque.enqueue(HashtagContestWorker, 'create_hashtag_participant', {tagging_id: self.id})
  end

  def destroy_hashtag_participant
    if self.taggable_type == 'Photo'
      Resque.enqueue(HashtagContestWorker, 'destroy_hashtag_participant', {photo_id: self.taggable_id, tag_id: self.tag_id})
    end
  end
end