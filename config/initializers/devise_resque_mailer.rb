module Devise
  module Mailers
    module Helpers
      def initialize_from_record(record)
        record = hack_record(record)
        @scope_name = Devise::Mapping.find_scope!(record)
        @resource = instance_variable_set("@#{devise_mapping.name}", record)
      end
      
      protected
      # monkey patch :D
      def hack_record(record)
        record.kind_of?(Hash) ? User.where(:email=>(record['email'] || record['user']['email'])).first : record
      end
    end
  end
end