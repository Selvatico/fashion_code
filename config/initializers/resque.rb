require 'resque'
require 'resque_scheduler'
require 'resque/scheduler'
require 'resque_scheduler/server'

class CanAccessResque
  def matches?(request)
    current_user = request.env['warden'].user
    current_user.present? and current_user.admin?
  end
end