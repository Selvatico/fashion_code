ModeraWebNew::Application.routes.draw do
  resources :pages, only: :show
  get '/info/:id' => 'pages#show', sub: :info
  get 'send_stats' => 'test_faye#send_stats'
  if Rails.env.development?
    mount MailPreview => 'mail_view'
  end

  scope module: :web do
    namespace :admin do
      root to: 'dashboard', controller: :dashboard, action: :index

      resources :fashion_styles
      resources :challenge_types
      resources :pages
      resources :members
      resources :static_contest_histories

      resources :inappropriates, only: [:index, :show] do
        member do
          delete 'destroy_comment'
          delete 'destroy_photo'
        end
      end

      get 'users/new', controller: :users, action: :new, as: :new_user
      resources :users, except: :new do
        member do
          put :block
          put :delete_avatar
        end
      end

      resources :contests do
        member do
          get :add_participants
          post :create_participants
          delete 'participants/:participant_id', action: :delete_participant, as: :delete_participant
        end
      end


      resources :omniauth_keys, except: :show
      resources :bot_settings, only: :index


      resources :vs_top_settings, only: [:index] do
        collection do
          get :edit
          put :update
        end
      end
      resources :follow_back_settings, only: [:index] do
        collection do
          get :edit
          put :update
        end
      end
      resources :photos_bot_settings, only: [:index, :new, :create] do
        collection do
          get :edit
          put :update
          delete :destroy
        end
      end
      resources :challenges_bot_settings, only: [:index, :new, :create] do
        collection do
          get :edit
          put :update
          delete :destroy
        end
      end
      resources :contests_bot_settings, only: [:index, :new, :create] do
        collection do
          get :edit
          put :update
          delete :destroy
        end
      end
      resources :home_activity_settings, only: [:index, :new, :create] do
        collection do
          get :edit
          put :update
          delete :destroy
        end
      end
      resources :bot_challenges_bot_settings, only: [:index] do
        collection do
          get :edit
          put :update
        end
      end
      resources :bots, only: [] do
        collection do
          get 'interest'
          put 'set_interest'
        end
      end

      constraints(CanAccessResque.new) do
        mount Resque::Server.new, at: '/resque', as: 'resque'
      end
    end

    get '404', to: 'errors#not_found'
    get '500', to: 'errors#internal_server_error'
  end

  root :to => 'home#welcome'
  get 'choose' => 'home#choose', as: :choose
  get 'home' => 'home#index', as: :home
  get 'first-login' => 'home#first_login'
  get 'subscription' => 'notifications#subscription'

  #------------ auth
  devise_for :users,
    controllers: {
    sessions: 'sessions',
    omniauth_callbacks: 'omniauth_callbacks',
    registrations: 'registrations',
    passwords: 'passwords'
  }, path: '', skip: [:sessions, :registrations]
    as :user do
      post '/social/:provider/login' => 'sessions#create_by_provider'
      get 'login' => 'sessions#new', :as => :new_user_session
      post 'login' => 'sessions#create', :as => :user_session
      get 'signup' => 'registrations#new', :as => :new_user_registration
      post 'signup' => 'registrations#create', :as => :user_registration
      match 'logout' => 'sessions#destroy', :as => :destroy_user_session,
        :via => Devise.mappings[:user].sign_out_via
      put 'password' => "passwords#update", :as => :user_password
      post 'upload_avatar' => 'registrations#upload_avatar', as: :upload_avatar
      match 'choose_style' => 'users#choose_style', as: :choose_style, via: [:get, :post]
      match 'invite_friends' => 'social_registrations#invite_friends', as: :invite_friends, via: [:get, :post]
      match 'batch_invite_friends' => 'social_registrations#batch_invite_friends', as: :batch_invite_friends, via: :get
      match 'invitation' => 'social_registrations#invitation', as: :invitation, via: [:get, :post]
      put 'change_password' => "registrations#change_password", as: :change_password
    end
    #------------
    #------------ notification
    resources :notifications do
      collection do
        delete :clear_all
        post :mark_read
        get :ungroup
        get :stats
      end
    end
    #------------
    #------------ core
    get 'home' => 'home#index', as: :home
    #
    #get 'about' => 'pages#about'
    #get 'privacy' => 'pages#privacy'
    #get 'tos' => 'pages#tos'
    #
    #get 'pages/content' => 'pages#content'
    #get 'content/:permalink' => 'pages#static_contest_history_info'
    #get 'pages/:id' => 'pages#page_content'
    #get 'info/:id' => 'pages#info'
    #
    #

    match 'users/', to: 'users#update', via: :post

    resources :users, only: [:update]  do
      collection do
        post 'share_preferences'
        #not sure
        get 'oauth/:provider', action: :oauth, as: :oauth
        get 'social/:provider/avatar', action: :avatar, as: :social_avatar
      end
    end
    #
    #
    resources :invitations, only: [:create]
    #
    #get 'friends' => 'profiles#friends'
    get 'suggestions' => "profiles#suggestions"
    resources :profiles, except: [:index] do
      member do
        get :friends #TODO: moved to get :friends => 'profiles#friends' ?
        get :followers
        get :versuses
        get :invite_friends
        get :popular_photos
        get :top_voters
        get :following
        post :upload_avatar
        post :follow
        post :unfollow
      end
      resources :photos, only: [:index, :show]
      match :upload_avatar, on: :collection
    end
    resources :brands, only: [:index]
    #
    resources :search, only: [] do
      collection do
        get 'users', action: :users
        get 'tags', action: :tags
        get 'results', action: :results
        get 'following', action: :following
      end
    end

    resources :photos do
      resources :comments, only: [:index, :create]
      collection do
        get :upload
        post :upload_photo
        get 'by_hashtag/:tag', action: :by_hashtag
      end
      member do
        put :load_cache
        get :voters
        post :vote
        match :share, via: [:get, :post]
        post :inappropriate
        get 'auto_share/:order', as: :auto_share, action: :auto_share
      end
    end
    #
    #resources :social_photos, only: [] do
    #  collection do
    #    get '/:provider', action: :index, constraints: { provider: /facebook|instagram/ }
    #    post :upload_photo
    #    post :ask_auth
    #  end
    #end
    #
    #resources :comments, except: [:update, :index, :create] do
    #  post 'inappropriate', :on => :member
    #end
    #
    #resources :power_hour
    ## do
    ##   get :list, on: :collection
    ## end
    #
    resources :contests do
      member do
        post :join
        post :leave
        post :inappropriate
      end
      collection do
        get :hall_of_fame
        get :user_place
        get 'list/:type' => 'contests#list'
      end
    end
    #
    resources :hashtag_contests, only: [] do
      collection do
        get ':tag/hall_of_fame' => 'hashtag_contests#hall_of_fame'
        get :trending
        get 'tags/:tag' => 'hashtag_contests#tags', :as => :hashtag_contest_tag
      end
      member do
        get :versuses
        get :photos
      end
    end
    #
    #resources :notifications do
    #  collection do
    #    delete :clear_all
    #    post :mark_read
    #    get :ungroup
    #    post :share
    #  end
    #end
    #
    resources :versuses, only: [:index] do
      collection do
        post :vote, action: :vote
        get 'by_hashtag/:tag' => 'versuses#by_hashtag'
      end
    end
    #
    resources :fashion_styles, only: :index
    #
    ##get :hall_of_fame, controller: :hall_of_fame
    #
    #resources :challenge_types, only: :index
    #
    #resources :savings, only: [:index, :create] do
    #  delete '/', action: :destroy, on: :collection
    #end
    #
    #match '*a', :to => 'application#not_found'
    #------------


    #mailer
    resources :subscriptions, only: [] do
      get :change, on: :collection
    end
    #/mailer

    #team
    resources :members, only: [:index, :show]
    #/team

    #makeup
    #TODO remove them after  markup is complete
    match 'makeup/home' => 'makeup#home'
    match 'makeup' => 'makeup#index'
    match 'makeup/choose_style' => 'makeup#choose_style'
    match 'makeup/upload' => 'makeup#upload'
    match 'makeup/invite' => 'makeup#invite'
    match 'makeup/get_prize' => 'makeup#get_prize'
    match 'makeup/tutorial' => 'makeup#tutorial'
    match 'makeup/power_hour_create' => 'makeup#power_hour_create'
    match 'makeup/power_hour' => 'makeup#power_hour'
    match 'makeup/contest_hash' => 'makeup#contest_hash'
    match 'makeup/contest_hash_photo' => 'makeup#contest_hash_photo'
    match 'makeup/hash_hall_of_fame' => 'makeup#hash_hall_of_fame'
    match 'makeup/hall_of_fame' => 'makeup#hall_of_fame'
    match 'makeup/hall_of_fame_25' => 'makeup#hall_of_fame_25'
    match 'makeup/notification_your_news' => 'makeup#notification_your_news'
    match 'makeup/notification_follow' => 'makeup#notification_follow'
    match 'makeup/notifications' => 'makeup#notifications'
    match 'makeup/profile' => 'makeup#profile'
    match 'makeup/profile_settings' => 'makeup#profile_settings'
    match 'makeup/about' => 'makeup#about'


    match 'makeup/alex' => 'makeup#alex'
    match 'makeup/anvarshakirov' => 'makeup#anvarshakirov'
    match 'makeup/anvartaishev' => 'makeup#anvartaishev'
    match 'makeup/artemchekhlov' => 'makeup#artemchekhlov'
    match 'makeup/elvina' => 'makeup#elvina'
    match 'makeup/gunaydonmez' => 'makeup#gunaydonmez'
    match 'makeup/jonathanlenoch' => 'makeup#jonathanlenoch'
    match 'makeup/julerizzardo' => 'makeup#julerizzardo'
    match 'makeup/lynnebarker' => 'makeup#lynnebarker'
    match 'makeup/mahmudkaraev' => 'makeup#mahmudkaraev'
    match 'makeup/maratibragimov' => 'makeup#maratibragimov'
    match 'makeup/michael' => 'makeup#michael'
    match 'makeup/nodirmirsaid' => 'makeup#nodirmirsaid'
    match 'makeup/rustamkhamraev' => 'makeup#rustamkhamraev'
    match 'makeup/sanjarbabadjanov' => 'makeup#sanjarbabadjanov'
    match 'makeup/shanebarker' => 'makeup#shanebarker'
    match 'makeup/timothymunden' => 'makeup#timothymunden'
    match 'makeup/volodyapinchuk' => 'makeup#volodyapinchuk'
    match 'makeup/yaroslav' => 'makeup#yaroslav'

    match 'makeup/our_story' => 'makeup#our_story'
    match 'makeup/partnership' => 'makeup#partnership'
    match 'makeup/business_proposal' => 'makeup#business_proposal'
    match 'makeup/videos' => 'makeup#videos'
    match 'makeup/contact_us' => 'makeup#contact_us'
    match 'makeup/guidlines' => 'makeup#guidlines'
    match 'makeup/tos' => 'makeup#tos'
    match 'makeup/privacy' => 'makeup#privacy'
    match 'makeup/content' => 'makeup#content'

    match 'makeup/error_401' => 'makeup#error_401'
    match 'makeup/error_404' => 'makeup#error_404'
    match 'makeup/error_500' => 'makeup#error_500'

    match 'makeup/landing' => 'makeup#landing'
    match 'makeup/share' => 'makeup#share'

    match 'makeup/layout_static' => 'makeup#layout_static'
    match 'makeup/old_browser' => 'makeup#old_browser'
#/makeup



  namespace :api do
    namespace :v1 do
      resources :contacts, only: [] do
        post :find, on: :collection
      end

      resources :savings, only: [:index, :create] do
        delete '/', action: :destroy, on: :collection
      end

      resources :challenges, only: [:index, :create] do
        get :can_create, on: :collection
      end


      resources :challenge_types, only: [:index]

      resources :contests, only: [:index] do
        member do
          post 'join' => 'contests#join'
          post 'leave' => 'contests#leave'
          post 'inappropriate' => 'contests#inappropriate'
          get :hall_of_fame
        end
      end

      resources :comments, except: [:update, :index, :create] do
        post 'inappropriate', :on => :member
      end

      resources :fashion_styles, only: [:index] do
        post 'choose', :on => :member
      end

      resources :notifications, only: [:index]

      resources :photos, only: [:destroy] do
        resources :comments, only: [:index, :create]
        get 'by_tag/:tag', on: :collection, action: :by_tag
        member do
          post 'inappropriate' => 'photos#inappropriate'
          post 'vote'
          post '/share/:provider', as: :share, action: :share
        end
        get 'voters'
      end

      resources :profiles, only: [:show] do
        member do
          get 'following' => 'profiles#following'
          get 'followers' => 'profiles#followers'
          post 'follow'    => 'profiles#follow'
          post 'unfollow'  => 'profiles#unfollow'
          post 'upload_avatar' => 'profiles#upload_avatar'
          put  :remove_avatar, action: :remove_avatar
          post 'photos' => 'profiles#upload_photo'
        end
        get :suggestions, on: :collection, action: :suggestions
        resources :photos, only: [:index]
        resources :versuses, only: [:index]
      end

      resources :pages, only: [:index]

      resources :search, only: [] do
        get 'users', on: :collection, action: :users
        get 'tags', on: :collection, action: :tags
      end

      resources :social, only: [] do
        member do
          get 'info' => 'social#info'
          post "sign_in", action: :login
        end
        get 'facebook/friends', on: :collection, action: :fb_friends
      end


      # TODO small cosmetic cleanup if it whorse way :)
      resources :users, only: [:create] do
        collection do
          put '/', action: :update
          post 'sign_up' => 'users#create'
          post 'sign_in' => 'users#login'
          post 'password' => 'users#password'
          get :settings, action: :settings
          put :social, action: :social
          put :update_password, action: :update_password
        end
      end

      resources :versuses, only: [] do
        collection do
          post 'vote'
          get :style
          get 'friends'
          get 'by_hashtag'
        end
      end

      get 'hall_of_fame', controller: :hall_of_fame

      match 'unfollow_all', to: 'profiles#unfollow_all', via: :post
      match 'follow_all', to: 'profiles#follow_all', via: :post
    end
  end

  mount Resque::Server.new, at: '/resque', as: 'resque'
  get '/:id' => 'pages#show'
end