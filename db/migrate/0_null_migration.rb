class NullMigration < ActiveRecord::Migration
  def change
    unless table_exists?("modera_fashion_styles")
      create_table "modera_fashion_styles" do |t|
        t.string   "name"
        t.string   "presentation"
        t.string   "image"
        t.datetime "created_at",   :null => false
        t.datetime "updated_at",   :null => false
      end
    end

    unless table_exists?("modera_brands")
      create_table "modera_brands" do |t|
        t.integer  "user_id"
        t.string   "name"
        t.text     "description"
        t.string   "image"
        t.datetime "created_at",  :null => false
        t.datetime "updated_at",  :null => false
      end
      add_index "modera_brands", ["user_id"], :name => "index_modera_brands_on_user_id"
    end

    unless table_exists?("modera_challenge_types")
      create_table "modera_challenge_types" do |t|
        t.integer  "fashion_style_id"
        t.integer  "opponent_fashion_style_id"
        t.integer  "easy_score",                :default => 0
        t.integer  "medium_score",              :default => 0
        t.integer  "hard_score",                :default => 0
        t.datetime "created_at",                               :null => false
        t.datetime "updated_at",                               :null => false
      end
      add_index "modera_challenge_types", ["fashion_style_id"], :name => "index_modera_challenge_types_on_fashion_style_id"
      add_index "modera_challenge_types", ["opponent_fashion_style_id"], :name => "index_modera_challenge_types_on_opponent_fashion_style_id"
    end

    unless table_exists?("modera_challenges")
      create_table "modera_challenges" do |t|
        t.integer  "user_id"
        t.integer  "challenge_type_id"
        t.integer  "photo_id"
        t.string   "name"
        t.integer  "score",             :default => 0
        t.datetime "expired_at"
        t.boolean  "finished",          :default => false
        t.string   "mentions"
        t.datetime "created_at",                           :null => false
        t.datetime "updated_at",                           :null => false
        t.integer  "votes_count",       :default => 0
        t.string   "level"
        t.string   "used_mentions",     :default => ""
      end
      add_index "modera_challenges", ["challenge_type_id"], :name => "index_modera_challenges_on_challenge_type_id"
      add_index "modera_challenges", ["name"], :name => "index_modera_challenges_on_name"
      add_index "modera_challenges", ["user_id"], :name => "index_modera_challenges_on_user_id"
    end

    unless table_exists?("modera_comments")
      create_table "modera_comments" do |t|
        t.integer  "user_id"
        t.integer  "photo_id"
        t.text     "comment"
        t.datetime "created_at", :null => false
        t.datetime "updated_at", :null => false
      end
      add_index "modera_comments", ["photo_id"], :name => "index_modera_comments_on_photo_id"
      add_index "modera_comments", ["user_id"], :name => "index_modera_comments_on_user_id"
    end

    unless table_exists?("modera_contests")
      create_table "modera_contests" do |t|
        t.string   "name"
        t.datetime "started_at"
        t.datetime "expired_at"
        t.boolean  "finished",    :default => false
        t.integer  "winner_id"
        t.integer  "votes_count", :default => 0
        t.string   "description"
        t.datetime "created_at",                     :null => false
        t.datetime "updated_at",                     :null => false
      end
      add_index "modera_contests", ["winner_id"], :name => "index_modera_contests_on_winner_id"
    end

    unless table_exists?("modera_hashtag_contests")
      create_table "modera_hashtag_contests" do |t|
        t.string   "name"
        t.integer  "owner_id"
        t.integer  "votes_count", :default => 0
        t.datetime "created_at",                 :null => false
        t.datetime "updated_at",                 :null => false
      end
      add_index "modera_hashtag_contests", ["name"], :name => "index_modera_hashtag_contests_on_name", :unique => true
      add_index "modera_hashtag_contests", ["owner_id"], :name => "index_modera_hashtag_contests_on_owner_id"
    end

    unless table_exists?("modera_hashtag_participants")
      create_table "modera_hashtag_participants" do |t|
        t.integer  "hashtag_contest_id"
        t.integer  "user_id"
        t.integer  "previous_place"
        t.integer  "current_place"
        t.integer  "votes_score",           :default => 0
        t.datetime "created_at",                              :null => false
        t.datetime "updated_at",                              :null => false
        t.integer  "fame_previous_place"
        t.boolean  "receive_notifications", :default => true
      end
      add_index "modera_hashtag_participants", ["hashtag_contest_id", "user_id"], :name => "m_rel_hashcont_id_and_user_id", :unique => true
      add_index "modera_hashtag_participants", ["hashtag_contest_id"], :name => "m_rel_hashcont_particip_contest_id"
      add_index "modera_hashtag_participants", ["user_id"], :name => "m_rel_hashcont_particip_user_id"
    end

    unless table_exists?("modera_inappropriates")
      create_table "modera_inappropriates" do |t|
        t.integer  "user_id"
        t.integer  "inappropriateable_id"
        t.string   "inappropriateable_type"
        t.datetime "created_at",             :null => false
        t.datetime "updated_at",             :null => false
      end
    end

    unless table_exists?("modera_invitations")
      create_table "modera_invitations" do |t|
        t.integer  "user_id"
        t.string   "email"
        t.string   "invitation_token",       :limit => 60
        t.datetime "invitation_accepted_at"
        t.datetime "created_at",                           :null => false
        t.datetime "updated_at",                           :null => false
      end
      add_index "modera_invitations", ["invitation_token"], :name => "index_modera_invitations_on_invitation_token"
      add_index "modera_invitations", ["user_id"], :name => "index_modera_invitations_on_user_id"
    end

    unless table_exists?("modera_notifications")
      create_table "modera_notifications" do |t|
        t.string   "kind"
        t.string   "grouping_kind"
        t.integer  "receiver_id"
        t.integer  "actor_id"
        t.string   "poster"
        t.integer  "notifible_id"
        t.string   "notifible_type"
        t.string   "contest_name"
        t.string   "challenge_level"
        t.string   "challenge_style"
        t.string   "challenge_opponent"
        t.datetime "contest_start"
        t.text     "contest_fame"
        t.text     "comment"
        t.integer  "score"
        t.integer  "place"
        t.integer  "picture_id"
        t.string   "picture"
        t.boolean  "read",               :default => false
        t.boolean  "following"
        t.datetime "created_at",                            :null => false
        t.datetime "updated_at",                            :null => false
        t.datetime "contest_finish"
        t.integer  "opponent_id"
        t.integer  "opponent_photo_id"
        t.string   "opponent_photo"
      end
      add_index "modera_notifications", ["actor_id"], :name => "index_modera_notifications_on_actor_id"
      add_index "modera_notifications", ["notifible_type", "notifible_id"], :name => "m_notification_notifible"
      add_index "modera_notifications", ["receiver_id"], :name => "index_modera_notifications_on_receiver_id"
    end

    unless table_exists?("modera_pages")
      create_table "modera_pages" do |t|
        t.string   "link"
        t.string   "title"
        t.text     "content"
        t.datetime "created_at", :null => false
        t.datetime "updated_at", :null => false
      end
      add_index "modera_pages", ["link"], :name => "index_modera_pages_on_link"
    end

    unless table_exists?("modera_participants")
      create_table "modera_participants" do |t|
        t.integer  "contest_id"
        t.integer  "user_id"
        t.integer  "previous_place"
        t.integer  "current_place"
        t.integer  "votes_score",         :default => 0
        t.datetime "created_at",                         :null => false
        t.datetime "updated_at",                         :null => false
        t.integer  "fame_previous_place"
      end
      add_index "modera_participants", ["user_id"], :name => "index_modera_participants_on_user_id"
    end

    unless table_exists?("modera_photos")
      create_table "modera_photos" do |t|
        t.integer  "user_id"
        t.string   "image"
        t.integer  "votes_score",           :default => 0
        t.integer  "up_votes_sum",          :default => 0
        t.integer  "votes_sum",             :default => 0
        t.float    "rank",                  :default => 0.0
        t.datetime "created_at",                               :null => false
        t.datetime "updated_at",                               :null => false
        t.boolean  "deleted",               :default => false
        t.integer  "challenge_votes_count", :default => 0
      end
      add_index "modera_photos", ["user_id"], :name => "index_modera_photos_on_user_id"
    end

    unless table_exists?("modera_prizes")
      create_table "modera_prizes" do |t|
        t.string   "name"
        t.string   "description"
        t.string   "image"
        t.integer  "contest_id"
        t.datetime "created_at",  :null => false
        t.datetime "updated_at",  :null => false
      end
    end

    unless table_exists?("modera_relationships")
      create_table "modera_relationships" do |t|
        t.integer  "follower_id"
        t.integer  "followed_id"
        t.datetime "created_at",  :null => false
        t.datetime "updated_at",  :null => false
      end
      add_index "modera_relationships", ["followed_id"], :name => "m_rel_followed_id"
      add_index "modera_relationships", ["follower_id", "followed_id"], :name => "m_rel_follower_id_and_followed_id", :unique => true
      add_index "modera_relationships", ["follower_id"], :name => "m_rel_follower_id"
    end

    unless table_exists?("modera_savings")
      create_table "modera_savings" do |t|
        t.integer  "user_id"
        t.integer  "photo_id"
        t.datetime "created_at", :null => false
        t.datetime "updated_at", :null => false
      end
      add_index "modera_savings", ["photo_id"], :name => "index_modera_savings_on_photo_id"
      add_index "modera_savings", ["user_id"], :name => "index_modera_savings_on_user_id"
    end

    unless table_exists?("modera_user_authentications")
      create_table "modera_user_authentications" do |t|
        t.string   "provider"
        t.string   "uid"
        t.string   "secret_token"
        t.string   "token"
        t.integer  "user_id"
        t.datetime "created_at",   :null => false
        t.datetime "updated_at",   :null => false
      end
      add_index "modera_user_authentications", ["user_id"], :name => "index_modera_user_authentications_on_user_id"
    end

    unless table_exists?("modera_user_settings")
      create_table "modera_user_settings" do |t|
        t.integer  "user_id"
        t.string   "key"
        t.string   "value"
        t.datetime "created_at", :null => false
        t.datetime "updated_at", :null => false
      end
      add_index "modera_user_settings", ["user_id"], :name => "index_modera_user_settings_on_user_id"
    end

    unless table_exists?("modera_users")
      create_table "modera_users" do |t|
        t.string   "fullname"
        t.string   "username"
        t.string   "avatar"
        t.string   "website"
        t.string   "about"
        t.string   "phone"
        t.boolean  "sex"
        t.datetime "birth"
        t.integer  "fashion_style_id"
        t.integer  "votes_score",            :default => 0
        t.string   "facebook"
        t.string   "twitter"
        t.string   "instagram"
        t.string   "pinterest"
        t.boolean  "photos_private",         :default => false
        t.boolean  "store_filtered_photos",  :default => true
        t.boolean  "store_original_photos",  :default => false
        t.boolean  "allow_push",             :default => true
        t.datetime "created_at",                                :null => false
        t.datetime "updated_at",                                :null => false
        t.string   "device_token"
        t.integer  "invited_by"
        t.boolean  "newbie",                 :default => true
        t.boolean  "verified",               :default => false
        t.string   "email",                  :default => "",    :null => false
        t.string   "encrypted_password",     :default => "",    :null => false
        t.string   "reset_password_token"
        t.datetime "reset_password_sent_at"
        t.integer  "sign_in_count",          :default => 0
        t.datetime "current_sign_in_at"
        t.datetime "last_sign_in_at"
        t.string   "current_sign_in_ip"
        t.string   "last_sign_in_ip"
        t.boolean  "blocked",                :default => false
        t.string   "authentication_token"
        t.string   "role",                   :default => ""
        t.boolean  "bot",                    :default => false
      end
      add_index "modera_users", ["email"], :name => "index_modera_users_on_email"
      add_index "modera_users", ["fullname"], :name => "index_modera_users_on_fullname"
      add_index "modera_users", ["username"], :name => "index_modera_users_on_username", :unique => true
    end

    unless table_exists?("modera_votes")
      create_table "modera_votes" do |t|
        t.integer  "voter_id"
        t.integer  "voted_photo_id"
        t.integer  "vote_score"
        t.integer  "unvoted_photo_id"
        t.integer  "unvote_score"
        t.integer  "challenge_id"
        t.datetime "created_at",            :null => false
        t.datetime "updated_at",            :null => false
        t.integer  "voted_photo_score"
        t.integer  "unvoted_photo_score"
        t.integer  "challenge_votes_count"
        t.integer  "hashtag_contest_id"
      end
      add_index "modera_votes", ["challenge_id"], :name => "index_modera_votes_on_challenge_id"
      add_index "modera_votes", ["unvoted_photo_id"], :name => "index_modera_votes_on_unvoted_photo_id"
      add_index "modera_votes", ["voted_photo_id"], :name => "index_modera_votes_on_voted_photo_id"
      add_index "modera_votes", ["voter_id"], :name => "index_modera_votes_on_voter_id"
    end

    unless table_exists?("modera_vs_top_photos")
      create_table "modera_vs_top_photos" do |t|
        t.integer  "photo_id"
        t.integer  "rank"
        t.datetime "created_at", :null => false
        t.datetime "updated_at", :null => false
      end
      add_index "modera_vs_top_photos", ["photo_id"], :name => "index_modera_vs_top_photos_on_photo_id"
    end

    unless table_exists?("rapns_apps")
      create_table "rapns_apps" do |t|
        t.string   "name",                       :null => false
        t.string   "environment"
        t.text     "certificate"
        t.string   "password"
        t.integer  "connections", :default => 1, :null => false
        t.datetime "created_at",                 :null => false
        t.datetime "updated_at",                 :null => false
        t.string   "type",                       :null => false
        t.string   "auth_key"
      end
    end

    unless table_exists?("rapns_feedback")
      create_table "rapns_feedback" do |t|
        t.string   "device_token", :limit => 64, :null => false
        t.datetime "failed_at",                  :null => false
        t.datetime "created_at",                 :null => false
        t.datetime "updated_at",                 :null => false
        t.string   "app"
      end
      add_index "rapns_feedback", ["device_token"], :name => "index_rapns_feedback_on_device_token"
    end

    unless table_exists?("rapns_notifications")
      create_table "rapns_notifications" do |t|
        t.integer  "badge"
        t.string   "device_token",      :limit => 64
        t.string   "sound",                           :default => "default"
        t.string   "alert"
        t.text     "data"
        t.integer  "expiry",                          :default => 86400
        t.boolean  "delivered",                       :default => false,     :null => false
        t.datetime "delivered_at"
        t.boolean  "failed",                          :default => false,     :null => false
        t.datetime "failed_at"
        t.integer  "error_code"
        t.text     "error_description"
        t.datetime "deliver_after"
        t.datetime "created_at",                                             :null => false
        t.datetime "updated_at",                                             :null => false
        t.boolean  "alert_is_json",                   :default => false
        t.string   "type",                                                   :null => false
        t.string   "collapse_key"
        t.boolean  "delay_while_idle",                :default => false,     :null => false
        t.text     "registration_ids"
        t.integer  "app_id",                                                 :null => false
        t.integer  "retries",                         :default => 0
      end
      add_index "rapns_notifications", ["app_id", "delivered", "failed", "deliver_after"], :name => "index_rapns_notifications_multi"
    end

    unless table_exists?("taggings")
      create_table "taggings" do |t|
        t.integer  "tag_id"
        t.integer  "taggable_id"
        t.string   "taggable_type"
        t.integer  "tagger_id"
        t.string   "tagger_type"
        t.string   "context",       :limit => 128
        t.datetime "created_at"
      end
      add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
      add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"
    end

    unless table_exists?("tags")
      create_table "tags" do |t|
        t.string "name"
      end
    end

    unless table_exists?("modera_members")
      create_table "modera_members" do |t|
        t.string :avatar
        t.string :background
        t.string :fullname
        t.string :position
        t.integer :ord, default: 0
        t.timestamps
      end
    end

    unless table_exists?("modera_questionaries")
      create_table "modera_questionaries" do |t|
        t.string :question
        t.string :answer
        t.references :member
        t.timestamps
      end
    end

    unless table_exists?("modera_static_contest_histories")
      create_table "modera_static_contest_histories" do |t|
        t.string :title
        t.string :permalink
        t.string :image
        t.timestamps
      end
    end
  end
end
