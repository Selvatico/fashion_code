class RemoveModeraPrefix < ActiveRecord::Migration
  def change
    rename_table :modera_fashion_styles, :fashion_styles

    rename_table :modera_brands, :brands
    rename_index :brands, :index_modera_brands_on_user_id, :index_brands_on_user_id

    rename_table :modera_challenge_types, :challenge_types
    rename_index :challenge_types, :index_modera_challenge_types_on_fashion_style_id, :index_challenge_types_on_fashion_style_id
    rename_index :challenge_types, :index_modera_challenge_types_on_opponent_fashion_style_id, :index_challenge_types_on_opponent_fashion_style_id

    rename_table :modera_challenges, :challenges
    rename_index :challenges, :index_modera_challenges_on_challenge_type_id, :index_challenges_on_challenge_type_id
    rename_index :challenges, :index_modera_challenges_on_name, :index_challenges_on_name
    rename_index :challenges, :index_modera_challenges_on_user_id, :index_challenges_on_user_id

    rename_table :modera_comments, :comments
    rename_index :comments, :index_modera_comments_on_photo_id, :index_comments_on_photo_id
    rename_index :comments, :index_modera_comments_on_user_id, :index_comments_on_user_id

    rename_table :modera_contests, :contests
    rename_index :contests, :index_modera_contests_on_winner_id, :index_contests_on_winner_id

    rename_table :modera_hashtag_contests, :hashtag_contests
    rename_index :hashtag_contests, :index_modera_hashtag_contests_on_name, :index_hashtag_contests_on_name
    rename_index :hashtag_contests, :index_modera_hashtag_contests_on_owner_id, :index_hashtag_contests_on_owner_id

    rename_table :modera_hashtag_participants, :hashtag_participants

    rename_table :modera_inappropriates, :inappropriates

    rename_table :modera_invitations, :invitations
    rename_index :invitations, :index_modera_invitations_on_invitation_token, :index_invitations_on_invitation_token
    rename_index :invitations, :index_modera_invitations_on_user_id, :index_invitations_on_user_id

    rename_table :modera_notifications, :notifications
    rename_index :notifications, :index_modera_notifications_on_actor_id, :index_notifications_on_actor_id
    rename_index :notifications, :index_modera_notifications_on_receiver_id, :index_notifications_on_receiver_id

    rename_table :modera_pages, :pages
    rename_index :pages, :index_modera_pages_on_link, :index_pages_on_link

    rename_table :modera_participants, :participants
    rename_index :participants, :index_modera_participants_on_user_id, :index_participants_on_user_id

    rename_table :modera_photos, :photos
    rename_index :photos, :index_modera_photos_on_user_id, :index_photos_on_user_id

    rename_table :modera_prizes, :prizes

    rename_table :modera_relationships, :relationships

    rename_table :modera_savings, :savings
    rename_index :savings, :index_modera_savings_on_photo_id, :index_savings_on_photo_id
    rename_index :savings, :index_modera_savings_on_user_id, :index_savings_on_user_id

    rename_table :modera_user_authentications, :user_authentications
    rename_index :user_authentications, :index_modera_user_authentications_on_user_id, :index_user_authentications_on_user_id

    rename_table :modera_user_settings, :user_settings
    rename_index :user_settings, :index_modera_user_settings_on_user_id, :index_user_settings_on_user_id

    rename_table :modera_users, :users
    rename_index :users, :index_modera_users_on_email, :index_users_on_email
    rename_index :users, :index_modera_users_on_fullname, :index_users_on_fullname
    rename_index :users, :index_modera_users_on_username, :index_users_on_username

    rename_table :modera_votes, :votes
    rename_index :votes, :index_modera_votes_on_challenge_id, :index_votes_on_challenge_id
    rename_index :votes, :index_modera_votes_on_unvoted_photo_id, :index_votes_on_unvoted_photo_id
    rename_index :votes, :index_modera_votes_on_voted_photo_id, :index_votes_on_voted_photo_id
    rename_index :votes, :index_modera_votes_on_voter_id, :index_votes_on_voter_id

    rename_table :modera_vs_top_photos, :vs_top_photos
    rename_index :vs_top_photos, :index_modera_vs_top_photos_on_photo_id, :index_vs_top_photos_on_photo_id

    rename_table :modera_members, :members
    rename_table :modera_questionaries, :questionaries
    rename_table :modera_static_contest_histories, :static_contest_histories
  end
end
