class AddSettingsColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :new_follower, :boolean, default: true
    add_column :users, :new_comment, :boolean, default: true
    add_column :users, :new_mention, :boolean, default: true
    add_column :users, :join_contest, :boolean, default: true
    add_column :users, :friend_joined, :boolean, default: true
    add_column :users, :daily_activity, :boolean, default: true
    add_column :users, :weekly_activity, :boolean, default: true
    add_column :users, :activity, :string
  end
end
