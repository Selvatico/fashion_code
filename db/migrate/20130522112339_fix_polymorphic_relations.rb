class FixPolymorphicRelations < ActiveRecord::Migration
  def up
    tables = {
        inappropriates: %w(inappropriateable_type),
        notifications: %w(notifible_type),
        taggings: %w(taggable_type tagger_type)
    }

    conn = ActiveRecord::Base.connection
    tables.each do |table, fields|
      fields.each do |field|
        conn.execute "UPDATE #{table} SET #{field} = replace(#{field}, 'Modera::', '')"
      end
    end
  end

  def down
  end
end
