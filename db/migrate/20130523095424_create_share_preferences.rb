class CreateSharePreferences < ActiveRecord::Migration
  def change
    create_table :share_preferences do |t|
      t.references  :user
      t.boolean     :facebook
      t.boolean     :twitter
      t.boolean     :upload_photo
      t.boolean     :follow
      t.boolean     :join_contest
      t.boolean     :new_contest
      t.boolean     :post_comment
      t.boolean     :win_prize

      t.timestamps
    end
    add_index :share_preferences, :user_id
  end
end
