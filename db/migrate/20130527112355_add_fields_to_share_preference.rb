class AddFieldsToSharePreference < ActiveRecord::Migration
  def change
    add_column :share_preferences, :social_network, :string
    add_column :share_preferences, :active, :boolean
  end
end
