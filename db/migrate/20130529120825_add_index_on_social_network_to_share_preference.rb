class AddIndexOnSocialNetworkToSharePreference < ActiveRecord::Migration
  def change
    SharePreferences.delete_all
    add_index :share_preferences, [:user_id, :social_network], unique: true, name: 'index_share_preferences_on_social_network'
  end
end
