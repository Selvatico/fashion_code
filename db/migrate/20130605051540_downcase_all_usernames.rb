class DowncaseAllUsernames < ActiveRecord::Migration
  def up
    User.all.each { |u| u.update_attributes(username: u.username.downcase) }
  end
end
