class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string   :name
      t.string   :permalink
      t.string   :title
      t.text     :content
      t.string   :keywords
      t.text     :description

      t.timestamps
    end
    add_index :pages, :permalink, unique: true
  end
end
