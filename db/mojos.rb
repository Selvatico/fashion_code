Mojo.all.each do |m|
  m.destroy
end
Mojo.create(:name=>'first_photo', :votes_score=>10, :title=>'Dive right in! Upload your first photo',:description=>' - 10 votes - Great Start! Don\'t stop now! Let everyone know who you are!',:url=>'/photos/new')
Mojo.create(:name=>'first_challenge',:votes_score=>10, :title=>'Time to Power Up! Make your first power hour', :description=>' - 10 votes - Now that the votes are roling in it\'s time to find some friends!',:url=>'/challenges')
Mojo.create(:name=>'personal_info',:votes_score=>10, :title=>'Who are you? Fill out your profile information', :description=>' - 10 votes - Now you are ready to connect your social networks!',:url=>'#profile')
Mojo.create(:name=>'first_connect',:votes_score=>5, :title=>"Let's get connected! Connect you social networks",   :description=>' - 5 votes for each - Awesome! Would you like to invite your Facebook friends to join you!',:url=>'/invitation')
Mojo.create(:name=>'invited_friends',:votes_score=>5, :title=>'All your friends are missing out! Invite your Facebook Friends',  :description=>' - 5 votes per 50 invites - You\'re on your way to Modera Fame. Would you like to invite you email contacts?',:url=>'/invitation')
Mojo.create(:name=>'invites_by_email',:votes_score=>30, :title=>'Don\'t hog all the votes! Invite Email Contacts', :description=>' - 30 votes for mail Awesome now you are ready to share your picture with all of you networks!',:url=>'/invitation')
Mojo.create(:name=>'shared_photo',:votes_score=>10, :title=>'Sharing is Caring. Share your picture on Twitter & Facebook', :description=>' - 10 votes for each - Have you tried the power hour feature yet? Let\'s play leap frog!',:url=>'/')
Mojo.create(:name=>'follow_suggested', :votes_score=>1,:title=>'Find users to follow. Follow users from Suggested Users page', :description=>' - 1 vote for each - You aren\'t the new kid on the block anymore! Say hello!',:url=>'/')
Mojo.create(:name=>'first_mention', :votes_score=>3,:title=>'You\'re halfway there! @mention someone for the first time',  :description=>' - 3 votes - Fame is just around the corner! Don\'t forget to take Modera on the go and share your lifestyle wherever you go!',:url=>'/home')
Mojo.create(:name=>'shared_notification',:votes_score=>5,:title=>'This time sharing is bragging rights! Share your accomplishments', :description=>' - 5 votes for each type - Hashtag your photos for more visibility!',:url=>'/notifications')
Mojo.create(:name=>'contest_owner',:votes_score=>30, :title=>'You\'re the leader of the pack now! Create & Invite your friends to your own #contest',  :description=>' - 30 votes - We are adding new features all the time follow us to stay up to date!',:url=>'/')
Mojo.create(:name=>'upload_avatar',:votes_score=>20,:title=>'First Impression is everything! Select a Profile Pic', :description=>' - 20 votes - Time to see how well you are doing head over to the Hall of Fame to see where you rank and who your competition is!',:url=>'/contests/hall_of_fame')
Mojo.create(:name=>'check_popular',:votes_score=>2, :title=>'How popular are you? Check your Hall of Fame ranking',  :description=>' - 2 votes - Congratulations! You have completed your profile! Keep uploading pictures and creating contests to climb the rankings!',:url=>'/contests/hall_of_fame')
#(:name=>'joined_first',:votes_score=>, :title=>'Join contest!',   :description=>'',:url=>'/')
Mojo.create(:name=>'high_votes_score',:votes_score=>100,:title=>'Reach 5,000 votes', :description=>'-100 votes',:url=>'/contests/hall_of_fame')
