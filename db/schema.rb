# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130605091312) do

  create_table "brands", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "brands", ["user_id"], :name => "index_brands_on_user_id"

  create_table "challenge_types", :force => true do |t|
    t.integer  "fashion_style_id"
    t.integer  "opponent_fashion_style_id"
    t.integer  "easy_score",                :default => 0
    t.integer  "medium_score",              :default => 0
    t.integer  "hard_score",                :default => 0
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
  end

  add_index "challenge_types", ["fashion_style_id"], :name => "index_challenge_types_on_fashion_style_id"
  add_index "challenge_types", ["opponent_fashion_style_id"], :name => "index_challenge_types_on_opponent_fashion_style_id"

  create_table "challenges", :force => true do |t|
    t.integer  "user_id"
    t.integer  "challenge_type_id"
    t.integer  "photo_id"
    t.string   "name"
    t.integer  "score",             :default => 0
    t.datetime "expired_at"
    t.boolean  "finished",          :default => false
    t.string   "mentions"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "votes_count",       :default => 0
    t.string   "level"
    t.string   "used_mentions",     :default => ""
  end

  add_index "challenges", ["challenge_type_id"], :name => "index_challenges_on_challenge_type_id"
  add_index "challenges", ["name"], :name => "index_challenges_on_name"
  add_index "challenges", ["user_id"], :name => "index_challenges_on_user_id"

  create_table "comments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "photo_id"
    t.text     "comment"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "comments", ["photo_id"], :name => "index_comments_on_photo_id"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "contests", :force => true do |t|
    t.string   "name"
    t.datetime "started_at"
    t.datetime "expired_at"
    t.boolean  "finished",    :default => false
    t.integer  "winner_id"
    t.integer  "votes_count", :default => 0
    t.string   "description"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "contests", ["winner_id"], :name => "index_contests_on_winner_id"

  create_table "fashion_styles", :force => true do |t|
    t.string   "name"
    t.string   "presentation"
    t.string   "image"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "hashtag_contests", :force => true do |t|
    t.string   "name"
    t.integer  "owner_id"
    t.integer  "votes_count", :default => 0
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "hashtag_contests", ["name"], :name => "index_hashtag_contests_on_name", :unique => true
  add_index "hashtag_contests", ["owner_id"], :name => "index_hashtag_contests_on_owner_id"

  create_table "hashtag_participants", :force => true do |t|
    t.integer  "hashtag_contest_id"
    t.integer  "user_id"
    t.integer  "previous_place"
    t.integer  "current_place"
    t.integer  "votes_score",           :default => 0
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "fame_previous_place"
    t.boolean  "receive_notifications", :default => true
  end

  add_index "hashtag_participants", ["hashtag_contest_id", "user_id"], :name => "m_rel_hashcont_id_and_user_id", :unique => true
  add_index "hashtag_participants", ["hashtag_contest_id"], :name => "m_rel_hashcont_particip_contest_id"
  add_index "hashtag_participants", ["user_id"], :name => "m_rel_hashcont_particip_user_id"

  create_table "inappropriates", :force => true do |t|
    t.integer  "user_id"
    t.integer  "inappropriateable_id"
    t.string   "inappropriateable_type"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "invitations", :force => true do |t|
    t.integer  "user_id"
    t.string   "email"
    t.string   "invitation_token",       :limit => 60
    t.datetime "invitation_accepted_at"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
  end

  add_index "invitations", ["invitation_token"], :name => "index_invitations_on_invitation_token"
  add_index "invitations", ["user_id"], :name => "index_invitations_on_user_id"

  create_table "members", :force => true do |t|
    t.string   "avatar"
    t.string   "background"
    t.string   "fullname"
    t.string   "position"
    t.integer  "ord",        :default => 0
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "notifications", :force => true do |t|
    t.string   "kind"
    t.string   "grouping_kind"
    t.integer  "receiver_id"
    t.integer  "actor_id"
    t.string   "poster"
    t.integer  "notifible_id"
    t.string   "notifible_type"
    t.string   "contest_name"
    t.string   "challenge_level"
    t.string   "challenge_style"
    t.string   "challenge_opponent"
    t.datetime "contest_start"
    t.text     "contest_fame"
    t.text     "comment"
    t.integer  "score"
    t.integer  "place"
    t.integer  "picture_id"
    t.string   "picture"
    t.boolean  "read",               :default => false
    t.boolean  "following"
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.datetime "contest_finish"
    t.integer  "opponent_id"
    t.integer  "opponent_photo_id"
    t.string   "opponent_photo"
  end

  add_index "notifications", ["actor_id"], :name => "index_notifications_on_actor_id"
  add_index "notifications", ["notifible_type", "notifible_id"], :name => "m_notification_notifible"
  add_index "notifications", ["receiver_id"], :name => "index_notifications_on_receiver_id"

  create_table "pages", :force => true do |t|
    t.string   "name"
    t.string   "permalink"
    t.string   "title"
    t.text     "content"
    t.string   "keywords"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "pages", ["permalink"], :name => "index_pages_on_permalink", :unique => true

  create_table "participants", :force => true do |t|
    t.integer  "contest_id"
    t.integer  "user_id"
    t.integer  "previous_place"
    t.integer  "current_place"
    t.integer  "votes_score",         :default => 0
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "fame_previous_place"
  end

  add_index "participants", ["user_id"], :name => "index_participants_on_user_id"

  create_table "photos", :force => true do |t|
    t.integer  "user_id"
    t.string   "image"
    t.integer  "votes_score",           :default => 0
    t.integer  "up_votes_sum",          :default => 0
    t.integer  "votes_sum",             :default => 0
    t.float    "rank",                  :default => 0.0
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "deleted",               :default => false
    t.integer  "challenge_votes_count", :default => 0
  end

  add_index "photos", ["user_id"], :name => "index_photos_on_user_id"

  create_table "prizes", :force => true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "image"
    t.integer  "contest_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "questionaries", :force => true do |t|
    t.string   "question"
    t.string   "answer"
    t.integer  "member_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "rapns_apps", :force => true do |t|
    t.string   "name",                       :null => false
    t.string   "environment"
    t.text     "certificate"
    t.string   "password"
    t.integer  "connections", :default => 1, :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "type",                       :null => false
    t.string   "auth_key"
  end

  create_table "rapns_feedback", :force => true do |t|
    t.string   "device_token", :limit => 64, :null => false
    t.datetime "failed_at",                  :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "app"
  end

  add_index "rapns_feedback", ["device_token"], :name => "index_rapns_feedback_on_device_token"

  create_table "rapns_notifications", :force => true do |t|
    t.integer  "badge"
    t.string   "device_token",      :limit => 64
    t.string   "sound",                           :default => "default"
    t.string   "alert"
    t.text     "data"
    t.integer  "expiry",                          :default => 86400
    t.boolean  "delivered",                       :default => false,     :null => false
    t.datetime "delivered_at"
    t.boolean  "failed",                          :default => false,     :null => false
    t.datetime "failed_at"
    t.integer  "error_code"
    t.text     "error_description"
    t.datetime "deliver_after"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.boolean  "alert_is_json",                   :default => false
    t.string   "type",                                                   :null => false
    t.string   "collapse_key"
    t.boolean  "delay_while_idle",                :default => false,     :null => false
    t.text     "registration_ids"
    t.integer  "app_id",                                                 :null => false
    t.integer  "retries",                         :default => 0
  end

  add_index "rapns_notifications", ["app_id", "delivered", "failed", "deliver_after"], :name => "index_rapns_notifications_multi"

  create_table "relationships", :force => true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "relationships", ["followed_id"], :name => "m_rel_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], :name => "m_rel_follower_id_and_followed_id", :unique => true
  add_index "relationships", ["follower_id"], :name => "m_rel_follower_id"

  create_table "savings", :force => true do |t|
    t.integer  "user_id"
    t.integer  "photo_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "savings", ["photo_id"], :name => "index_savings_on_photo_id"
  add_index "savings", ["user_id"], :name => "index_savings_on_user_id"

  create_table "share_preferences", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "facebook"
    t.boolean  "twitter"
    t.boolean  "upload_photo"
    t.boolean  "follow"
    t.boolean  "join_contest"
    t.boolean  "new_contest"
    t.boolean  "post_comment"
    t.boolean  "win_prize"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "social_network"
    t.boolean  "active"
  end

  add_index "share_preferences", ["user_id", "social_network"], :name => "index_share_preferences_on_social_network", :unique => true
  add_index "share_preferences", ["user_id"], :name => "index_share_preferences_on_user_id"

  create_table "static_contest_histories", :force => true do |t|
    t.string   "title"
    t.string   "permalink"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "user_authentications", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "secret_token"
    t.string   "token"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "user_authentications", ["user_id"], :name => "index_user_authentications_on_user_id"

  create_table "user_settings", :force => true do |t|
    t.integer  "user_id"
    t.string   "key"
    t.string   "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "user_settings", ["user_id"], :name => "index_user_settings_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "fullname"
    t.string   "username"
    t.string   "avatar"
    t.string   "website"
    t.string   "about"
    t.string   "phone"
    t.boolean  "sex"
    t.datetime "birth"
    t.integer  "fashion_style_id"
    t.integer  "votes_score",            :default => 0
    t.string   "facebook"
    t.string   "twitter"
    t.string   "instagram"
    t.string   "pinterest"
    t.boolean  "photos_private",         :default => false
    t.boolean  "store_filtered_photos",  :default => true
    t.boolean  "store_original_photos",  :default => false
    t.boolean  "allow_push",             :default => true
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "device_token"
    t.integer  "invited_by"
    t.boolean  "newbie",                 :default => true
    t.boolean  "verified",               :default => false
    t.string   "email",                  :default => "",    :null => false
    t.string   "encrypted_password",     :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "blocked",                :default => false
    t.string   "authentication_token"
    t.string   "role",                   :default => ""
    t.boolean  "bot",                    :default => false
    t.boolean  "new_follower",           :default => true
    t.boolean  "new_comment",            :default => true
    t.boolean  "new_mention",            :default => true
    t.boolean  "join_contest",           :default => true
    t.boolean  "friend_joined",          :default => true
    t.boolean  "daily_activity",         :default => true
    t.boolean  "weekly_activity",        :default => true
    t.string   "activity"
  end

  add_index "users", ["email"], :name => "index_users_on_email"
  add_index "users", ["fullname"], :name => "index_users_on_fullname"
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

  create_table "votes", :force => true do |t|
    t.integer  "voter_id"
    t.integer  "voted_photo_id"
    t.integer  "vote_score"
    t.integer  "unvoted_photo_id"
    t.integer  "unvote_score"
    t.integer  "challenge_id"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.integer  "voted_photo_score"
    t.integer  "unvoted_photo_score"
    t.integer  "challenge_votes_count"
    t.integer  "hashtag_contest_id"
  end

  add_index "votes", ["challenge_id"], :name => "index_votes_on_challenge_id"
  add_index "votes", ["unvoted_photo_id"], :name => "index_votes_on_unvoted_photo_id"
  add_index "votes", ["voted_photo_id"], :name => "index_votes_on_voted_photo_id"
  add_index "votes", ["voter_id"], :name => "index_votes_on_voter_id"

  create_table "vs_top_photos", :force => true do |t|
    t.integer  "photo_id"
    t.integer  "rank"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "vs_top_photos", ["photo_id"], :name => "index_vs_top_photos_on_photo_id"

end
