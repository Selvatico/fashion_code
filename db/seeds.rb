puts 'Clearing tables'
conn = ActiveRecord::Base.connection
tables = conn.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public'").map { |r| r['table_name'] }
tables.delete "schema_migrations"
tables.each { |t| conn.execute("TRUNCATE #{t}") }

if ENV['SKIP_BOTS'] == 'true'
  Photo.skip_callback(:create, :after, :start_bots)
  Challenge.skip_callback(:create, :after, :start_bots)
  Contest.skip_callback(:create, :after, :start_bots)
  Relationship.skip_callback(:create, :after, :start_bots)
end

# file_path = File.expand_path '../', __FILE__
# puts 'Creating fashion styles...'
# tags = {'Glamour' => 'ladylike, romantic, lolita', 'hipster' => 'indie, raver, emo', 'Urban' => 'classic, military, dandy, gangster, Ska, cowboy, western, hiphop, rock', 'Casual' => 'allamerican, californian, collegiate, preppy, english, minimalist, nautical, safari, skater, sporty, tomboy', 'Vintage' => 'avantgarde, disco, ethnic, mod, retro, bohemian, hippie, victorian'}
# for i in ['Glamour', 'Hipster', 'Urban', 'Casual', 'Vintage']
#   style = FashionStyle.new(name: i.downcase, presentation: i)
#   style.image = File.open("#{file_path}/images/style#{i}.jpg", 'r')
#   style.tag_list = tags[i]
#   style.save
# end
# puts 'Fashion styles created.'
# puts 'Creating challenge types...'
# stylesi = FashionStyle.pluck(:id)
# stylesj = FashionStyle.pluck(:id)
# for i in stylesi
#   for j in stylesj
#     chal_type = ChallengeType.new(easy_score: 2, medium_score: 4, hard_score: 6)
#     chal_type.fashion_style_id = i
#     chal_type.opponent_fashion_style_id = j
#     chal_type.save
#   end
#   stylesj.delete(i)
# end
# # puts 'Created challenge types.'