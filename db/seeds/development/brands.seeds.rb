after "development:users" do
  file_path = File.expand_path '../../../', __FILE__

  puts 'Creating brands...'
  for i in 1..14
    brand = Brand.new(name: "Brand #{i}", description: "Brand #{i} description")
    brand.user = User.find_by_username("user#{i}")
    brand.image = File.open("#{file_path}/images/brands/#{i}.jpg", 'r')
    brand.save
  end
end