after "development:users" do
  file_path = File.expand_path '../../../', __FILE__

  puts 'Creating contests and winners...'
  puts '!!! Contest.start_bots can raise Error connecting to Redis: Radis should be started or start_bots should be commented !!!'
  
  users = User.all

  puts "Creating started contests in which users already have joined"
  5.times do |i|
    contest = Contest.new(name: "Started Contest #{i} with users", description: "Started Contest #{i} with users description", started_at: DateTime.now - 1.month, expired_at: DateTime.now + 1.year)
    contest.prizes.new(name: "Test prize #{i}", description: "Prize for test contest #{i}", image: File.open("#{file_path}/images/prize.jpg"))
    users.each do |user|
      p = contest.participants.new
      p.user_id = user.id
    end
    contest.save!
  end

  puts "Creating started contests in which users have not join yet..."
  5.times do |i|
    contest = Contest.new(name: "Started Contest #{i}", description: "Started Contest #{i} description", started_at: DateTime.now - 1.month, expired_at: DateTime.now + 1.year)
    contest.prizes.new(name: "Test prize #{i}", description: "Prize for test contest #{i}", image: File.open("#{file_path}/images/prize.jpg"))
    contest.save!
  end

  puts "Creating contests which are not started..."
  5.times do |i|
    contest = Contest.new(name: "Not started Contest #{i}", description: "Not started Contest #{i} description", started_at: DateTime.now + 1.month, expired_at: DateTime.now + 1.year)
    contest.prizes.new(name: "Test prize #{i}", description: "Prize for test contest #{i}", image: File.open("#{file_path}/images/prize.jpg"))
    contest.save!
  end

  puts "Creating finished contests"
  5.times do |i|
    contest = Contest.new(name: "Finished Contest #{i}", description: "Finished Contest #{i} description", started_at: DateTime.now - 2.month, expired_at: DateTime.now - 1.month)
    contest.prizes.new(name: "Test prize #{i}", description: "Prize for test contest #{i}", image: File.open("#{file_path}/images/prize.jpg"))
    contest.winner_id = User.order('RANDOM()').first.id
    contest.finished = true
    users.each do |user|
      p = contest.participants.new
      p.user_id = user.id
    end
    contest.save!
  end

  puts "Contests created."
end