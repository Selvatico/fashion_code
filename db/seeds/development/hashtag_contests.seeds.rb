after "development:users" do
  puts "Creating hashtag contests"
  ActsAsTaggableOn::Tagging.all.each do |tagging|
    if tagging.taggable_type == 'Photo'
      self_tag = tagging.tag
      c = HashtagContest.find_by_name(self_tag.name)
      if c
        if c.participants.where(user_id: tagging.taggable.user_id).empty?
          p = c.participants.new
          p.user_id = tagging.taggable.user_id
          p.hashtag_contest_id = c.id
          place = c.participants.count
          p.current_place = place + 1
          p.previous_place = place + 1
          p.fame_previous_place = place + 1
          p.save!
        end
      else
        c = HashtagContest.new
        c.owner_id = tagging.taggable.user_id
        c.name = self_tag.name
        p = c.participants.new
        p.user_id = tagging.taggable.user_id
        p.current_place = 1
        p.previous_place = 1
        p.fame_previous_place = 1
        c.save!
      end
    end
  end

  puts 'Created photos and all follow.'
end