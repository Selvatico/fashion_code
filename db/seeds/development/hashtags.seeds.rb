after "development:users" do
  puts "Adding hashtags..."
  Photo.all.each do |photo|
    photo.tag_list << ["wonderful", "beautiful", "great", "cool", "sucks"]
    photo.save
  end
  puts "Hashtags added."
end