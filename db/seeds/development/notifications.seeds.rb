after "development:users", "development:challenges", "development:contests" do
  puts 'Creating notifications...'
  User.all.each do |receiver|
    actor = User.where('id <> ?', receiver.id).order('RANDOM()').first
    #photo =
    # User started following you
    Notification.create!(
      kind: 'new_follower', 
      grouping_kind: 'new_follower',
      receiver: receiver,
      actor: actor,
      notifible: actor,
      following: false
    )
    # User you follow just won a contest
    Notification.create!(
      kind: "friend_won",
      grouping_kind: "friend_won",
      receiver: receiver,
      actor: actor,
      notifible: Contest.order('RANDOM()').first,
      following: false
    )
    # Your friend from Facebook just joined Modera
    Notification.create!(
      kind: "facebook_friend",
      grouping_kind: "facebook_friend",
      receiver: receiver,
      actor: actor,
      notifible: actor,
      following: false
    )
    # __ contest just started
    Notification.create!(
      kind: "contest_started",
      grouping_kind: "contest_started",
      receiver: receiver,
      notifible: Contest.order('RANDOM()').first,
      following: false
    )

    # User voted for your photo
    receiver.photos.active.order('RANDOM()').first.vote_for(actor)
    #Notification.create!(
    #  kind: "voted",
    #  grouping_kind: "voted",
    #  receiver: receiver,
    #  actor: actor,
    #  notifible: vote,
    #  following: false
    #)

    # User voted for your photo in versus
    voted_photo = receiver.photos.active.order('RANDOM()').first
    unvoted_photo = Photo.where('user_id <> ? AND user_id <> ?', receiver.id, actor.id).active.order('RANDOM()').first
    actor.vote_photo(voted_photo, unvoted_photo)
    #Notification.create!(
    #    kind: "voted",
    #    grouping_kind: "voted",
    #    receiver: receiver,
    #    actor: actor,
    #    notifible: receiver.photos.active.order('RANDOM()').first,
    #    following: false
    #)
    # You won contest
    Notification.create!(
      kind: "won_contest",
      grouping_kind: "won_contest",
      receiver: receiver,
      actor: receiver,
      notifible: Contest.order('RANDOM()').first,
      following: false
    )
    # __ contest has just finished
    Notification.create!(
      kind: "contest_finished",
      grouping_kind: "contest_finished",
      receiver: receiver,
      notifible: Contest.order('RANDOM()').first,
      following: false
    )
    # Your photo has been reported
    Notification.create!(
      kind: "photo_inappropriated",
      grouping_kind: "photo_inappropriated",
      receiver: receiver,
      notifible: receiver.photos.active.order('RANDOM()').first,
      following: false
    )
    # You just moved to _th place
    # up
    Notification.create!(
      kind: "level_up",
      grouping_kind: "level_up",
      receiver: receiver,
      place: 12,
      following: false
    )
    # down
    Notification.create!(
      kind: "level_down",
      grouping_kind: "level_down",
      receiver: receiver,
      place: 21,
      following: false
    )
    # You are in the top 10 users in _ contest
    Notification.create!(
      kind: "contest_ten",
      grouping_kind: "contest_ten",
      receiver: receiver,
      notifible: Contest.order('RANDOM()').first,
      following: false
    )
    # User commented on your photo
    Notification.create!(
      kind: "commented",
      grouping_kind: "commented",
      receiver: receiver,
      actor: actor,
      notifible: receiver.photos.active.order('RANDOM()').first.comments.order('RANDOM()').first,
      following: false
    )
    # User mentioned you in a comment
    Notification.create!(
      kind: "mentioned",
      grouping_kind: "mentioned",
      receiver: receiver,
      actor: actor,
      notifible: receiver.photos.active.order('RANDOM()').first.comments.order('RANDOM()').first,
      following: false
    )
  end
  puts 'Notifications are created.'
end