after "development:fashion_styles" do
  require 'benchmark'

  #Photo.skip_callback(:create, :after, :start_bots)
  #Challenge.skip_callback(:create, :after, :start_bots)
  #Contest.skip_callback(:create, :after, :start_bots)
  #Relationship.skip_callback(:create, :after, :start_bots)
  #
  #User.skip_callback(:commit, :after, :send_welcome_email)
  #
  #Relationship.skip_callback(:commit, :after, :notify_on_create)
  #Participant.skip_callback(:commit, :after, :notify_on_create)
  #Challenge.skip_callback(:commit, :after, :notify_on_create)
  #Comment.skip_callback(:commit, :after, :notify_on_create)
  #Contest.skip_callback(:commit, :after, :notify_on_create)
  #Photo.skip_callback(:commit, :after, :notify_on_create)
  #Vote.skip_callback(:commit, :after, :notify_on_create)
  #User.skip_callback(:commit, :after, :notify_on_create)

  file_path = File.expand_path '../../../', __FILE__

  total_time = Benchmark.bm(7) do |bm|
    bm.report('Creating users:') do
      styles = FashionStyle.pluck(:id)
      for i in 1..30
        user = User.new(fullname: "User#{i}", username: "user#{i}", email: "user#{i}@modera.co", website: "site_user#{i}.co", about: "About User#{i}.", phone: "#{i}#{i}#{i}#{i}#{i}#{i}#{i}", sex: i%2, birth: DateTime.now - i.years)
        user.fashion_style_id = styles[i%5]
        user.password = 'password'
        user.password_confirmation = 'password'
        user.avatar = File.open("#{file_path}/images/avatar.jpg", 'r')
        user.save
      end
    end
    users = User.all
    puts 'Following and free users and users photos...'

    bm.report('Following:') do
      users.each do |u|
        users.select{|f| u.id!=f.id}.each do |follower|
          u.follow!(follower)
        end
      end
    end

    # for i in 31..40
    #   user = User.new(fullname: "User#{i}", username: "user#{i}", email: "user#{i}@modera.co", website: "site_user#{i}.co", about: "About User#{i}.", phone: "#{i}#{i}#{i}#{i}#{i}#{i}#{i}", sex: i%2, birth: DateTime.now - i.years)
    #   user.fashion_style_id = styles[i%5]
    #   user.password = 'password'
    #   user.password_confirmation = 'password'
    #   user.avatar = File.open("#{file_path}/images/avatar.jpg", 'r')
    #   user.save
    # end

    photos = []
    (1..5).each { |i| photos << File.open("#{file_path}/images/photo#{i}.jpg", 'r') }

    users.each do |u|
      (1..15).each do |p|
        photo = u.photos.new(image: photos[(p - 1) % 5])
        photo.tag_list = "beautiful, wonderful"
        (1..3).each do |c|
          comment = photo.comments.new(comment: "Comment#{c}")
          comment.user_id = u.id
        end
      end
      bm.report("#{u.username}.save with photos:") { u.save }
    end
  end

  puts "users.seeds.rb total: #{total_time.map(&:real).reduce(:+)} seconds"
end