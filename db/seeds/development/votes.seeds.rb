after "development:users" do
  puts "Creating votes..."
  User.all.each do |user|
    3.times do
      opponent = User.where('id not in (?)', user.id).order('RANDOM()').first
      voter = User.where('id not in (?)', [user.id, opponent.id]).order('RANDOM()').first
      vote = Vote.new(vote_score: 1, unvote_score: 1)
      vote.voter = voter
      vote.voted_photo = user.photos.active.order('RANDOM()').first
      vote.unvoted_photo = opponent.photos.active.order('RANDOM()').first
      vote.save
    end
  end
  puts "Votes created."
end