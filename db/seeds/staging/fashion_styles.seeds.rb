file_path = File.expand_path '../../../', __FILE__

puts 'Creating fashion styles...'
for i in ['Glamour', 'Hipster', 'Urban', 'Casual', 'Vintage']
  style = FashionStyle.new(name: i, presentation: i)
  style.image = File.open("#{file_path}/images/style#{i}.jpg", 'r')
  style.tag_list = "#{i.downcase}1, #{i.downcase}2, #{i.downcase}3"
  style.save
end
puts 'Fashion styles created.'