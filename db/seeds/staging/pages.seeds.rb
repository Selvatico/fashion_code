puts "Creating some static pages..."

# info content

alex = <<-CONTENT
<div class="fixed static">
  <img alt="Alexander Kobelev" src="/assets/about/bg_Sasha.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Programmer</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>It's my life</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>What is 36? Only 48 hours in a row!</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Casual and Urban</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Sneakers</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Classic</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Johnny Depp</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>Mac</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Kirk</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>I'm Superman already</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Sure!</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

anvarshakirov = <<-CONTENT
<div class="fixed static">
  <img alt="Anvar Shakirov" src="/assets/about/bg_Anvar2.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info right">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>I'm member of the Promo team and assistant/supporter of the IT and BD department.</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>Having fun! Also, some resources to get some tools that, makes my work much more comfortable :3</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Yeap : D</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Casual</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Pumps looks more beautiful</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Dunno D:</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>JESSICA ALBA!!!</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>PC ofc *_*</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Gandalf the Grey!</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Time traveling!</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>YES I DO :3</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

anvartaishev = <<-CONTENT
<div class="fixed static">
  <img alt="Anvar Taishev" src="/assets/about/bg_Anvar.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Designer &amp; Illustrator</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Yes.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Steampunk.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Pumps.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Tissot.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>JIM CARREY.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>PC</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Spock.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Mind Reading &amp; Telepathy</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Yes :3</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT


artemchekhlov = <<-CONTENT
<div class="fixed static">
  <img alt="Anvar Taishev" src="/assets/about/bg_Anvar.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Designer &amp; Illustrator</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Yes.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Steampunk.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Pumps.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Tissot.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>JIM CARREY.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>PC</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Spock.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Mind Reading &amp; Telepathy</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Yes :3</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

elvina = <<-CONTENT
<div class="fixed static">
  <img alt="El'vina Ziyadinova" src="/assets/about/bg_Elya.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Front-end developer</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>My contribution to the project</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>No, I work worse when I want to sleep</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Don't know</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Barefoot</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Happy hours are not observing</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Bruce Willis</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>I use PC, but I want MAC</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Who are they?</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Imagine that it is already in your life, and it immediately appears! It is the materialization of thought!</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Yes, in Amber world.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

gunaydonmez = <<-CONTENT
<div class="fixed static">
  <img alt="Gunay Donmez" src="/assets/about/bg_gunay.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Marketing Research</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>Identify and define marketing opportunities in the competitive marketing environment</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Very often</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Urban and Hipster</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Pumps</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Digital waterproof watches</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Jacqueline Fernandez</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>PC</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Spock</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Being able to pick a new super power everyday…then sell it on eBay the next day</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>I love unicorns</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

jonathanlenoch = <<-CONTENT
<div class="fixed static">
  <img alt="Jonathan Lenoch" src="/assets/about/bg_Lenoch.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Video Files.</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>Make and play with moving pictures.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>On occasion.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Casual.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Pumped up wedge.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Phone.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Choi Min-sik.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>Mac.</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Kirk.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Animorph.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Sure.</span>
        </p>
      </div>
    </div>
  </div>
  </div>
CONTENT

julerizzardo = <<-CONTENT
<div class="fixed static">
  <img alt="Jule Rizzardo" src="/assets/about/bg_Jule.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Top Secret 007 Agent</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>I am a Word Artist... I turn fashion and lifestyle topics into artful, fun, fresh blog posts.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Yes, in the 1997 floods!</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Most definitely love the Urban style!</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Wedges for sure.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>The classic pocket watch.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Morgan Freeman</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>PC</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Captain Kirk</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>If just one choice i had, Jedi Mind Power, it would be.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Yes.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

lynnebarker = <<-CONTENT
<div class="fixed static">
  <img alt="Lynne Pattalock-Barker" src="/assets/about/bg_Lynne.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Fashion and Photography evangelist and intense foodie, oh and don't forget content editor.</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>I have the most amazing job in the world. I get to play with the newest trends in fashion, be visually stimulated by photografy and blog all about it.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>I am pleading the fifth as it would violate a few laws to admit that I have.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Yes!!! Oh wait, you mean I have to pick one? Sorry, I am an equal opportunist.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Wedges as they allow me to keep up with my dogs, son and husband without breaking my ankle.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Love the bracelet watch in platinum or leather.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Morgan Freeman, and I have confirmed that he is still alive.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>I use both but create magic on :my iMac</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Spock of course, who do you think tells Kirk what to do and saves his butt.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>To control the elements like an Airbender.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Unicorns represent the purest form of imagination and creativity... of course I do.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

mahmudkaraev = <<-CONTENT
<div class="fixed static">
  <img alt="Maxmud Karaev" src="/assets/about/bg_Mahmud.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Promotion</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>No</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Glamour, urban, hipster</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Sneakers</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Rolex, Breitling, Cartier</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Dominic Cooper, George Clooney, Quentin Tarantino, Johnny Depp, Al Pacino, Robert De Niro</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>PC</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Captain Kirk</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Flying</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Yes</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

maratibragimov = <<-CONTENT
<div class="fixed static">
  <img alt="Marat Ibragimov" src="/assets/about/bg_Marat.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info right">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Co-founder and CFO</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>To get some rest. The only way to have me get some rest is by paying me!</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Only being drunk.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>I only wear casual.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>No comments.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Maurice Lacroix.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Johnny Depp.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>Pfffff…. Mac, of course!</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Grand Master Yoda.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Accelerated healing process.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>I talk to them on the 36th hour of working… Then I realize I could get some sleep.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

michael = <<-CONTENT
<div class="fixed static">
  <img alt="Michael Paulmarie" src="/assets/about/bg_Michael.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>I honestly couldn't tell you.  I'm not even sure what my title is half the time.</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>I'm paid to do.  And do it well.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>It depends on what you consider work.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Whichever style I am in for the day.  I get bored easily so it just depends on my mood.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>My cell phone.  It tells time well.  I'm not nearly successful enough to know anything about watches, or cigars for that matter.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Jamie Foxx</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>I'm a PC guy but some things just need that Mac touch.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>I have one.  I call it being alive.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Modera is a Unicorn.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

nodirmirsaid = <<-CONTENT
<div class="fixed static">
  <img alt="Nodir Mirsaid" src="/assets/about/bg_Nodir.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info right">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Co-founder and COO</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>I can’t take money from my child.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>In a day? There is only 24 hours, but I’m doing my best to work more than 24 hours.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Casual is mine.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>I like Pumps, think Gava pumps from Isabel Marant are good. But not so comfortable to wear for me.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Breitling are my favorite. GT edition are awesome</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Russell Crowe.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>What's the question? Mac of course.</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Spock \/</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>I already have the force, I’m just raising it to became Sith Lord.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>I was pitching my project to one of them at the plane</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

rustamkhamraev = <<-CONTENT
<div class="fixed static">
  <img alt="Rustam R. Khamraev" src="/assets/about/bg_Rustam.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Everything</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Hah</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>More Casual</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>I am not a woman, so sneakers</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>None</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Jason Statham</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>What is MAC? PC baby!</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Have no idea who they are!</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>You don't say!</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>I beleive in horses, dogs, cats etc.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

sanjarbabadjanov = <<-CONTENT
<div class="fixed static">
  <img alt="Sanjar Babadjanov" src="/assets/about/bg_Sanjar.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info right">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Co-founder CEO</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>Modera - it's my life, it's my crazy dream.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Yes, and I would like more.</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Casual, sometimes Glam.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Ulysse Nardin.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>I think it's Joe Pesci.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>Mac.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>See the future</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>In another world, yes.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

shanebarker = <<-CONTENT
<div class="fixed static">
  <img alt="Shane Barker" src="/assets/about/bg_Shane.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Co-founder and VP of Marketing</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>People are getting paid?  What!?!</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Is there any other option?  We are a startup!</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>I really enjoy the Urban style.</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Both are very uncomfortable for me to wear.  But as a women, pumps.</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Limited Edition Navitimer Super Constellation Watch by Breitling.</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Robert Downey Jr.</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>What is a PC?  Mac baby!</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Captain Kirk.</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>I would have to super power of being able to work and NOT need sleep.  Yes I just said that.</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>I have two in my backyard, as we speak.</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

timothymunden = <<-CONTENT
<div class="fixed static">
  <img alt="Timothy Munden" src="/assets/about/bg_Tim.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Community Manager</span>
        </p>
        <p>
          <b>What are you paid to do at Modera?</b>
          <span>Manage multiple social media accounts, post blogs and engage followers.</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>No</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Steampunk and Luxury</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>I don't wear womens shoes</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Rolex</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Tom Cruise</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>Either or</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Neither</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Jedi / Sith Powers - Star Wars</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>No, just the Force</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

volodyapinchuk = <<-CONTENT
<div class="fixed static">
  <img alt="Vladimir Pinchuk" src="/assets/about/bg_Volodya.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Everything</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>Yes, it's happened</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Only casual</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Watch in my car</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Gérard Depardieu</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>No Mac, no PC...DELL XPS ONLY!!!</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>I don't know who these guys are</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>I'd like to fly</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>Yes</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

yaroslav = <<-CONTENT
<div class="fixed static">
  <img alt="Yaroslav Borzenkov" src="/assets/about/bg_Yarik.jpg" style="width:100%; height:auto; margin-top:106px;">
  <div class="info left">
    <div class="nano">
      <div class="content">
        <p>
          <b>What is your Clearance?</b>
          <span>Promotion</span>
        </p>
        <p>
          <b>Have you ever worked longer than 36 hours in a row?</b>
          <span>No</span>
        </p>
        <p>
          <b>What is your favorite style from Modera?</b>
          <span>Hipster, Steampunk</span>
        </p>
        <p>
          <b>Pumps or wedges?</b>
          <span>Pumps</span>
        </p>
        <p>
          <b>Favorite watch style?</b>
          <span>Tissot, Rolex</span>
        </p>
        <p>
          <b>Who is your favorite actor?</b>
          <span>Jason Statham, Hugh Michael Jackman</span>
        </p>
        <p>
          <b>Mac or PC?</b>
          <span>Mac</span>
        </p>
        <p>
          <b>Captain Kirk or Spock?</b>
          <span>Captain Kirk</span>
        </p>
        <p>
          <b>If you could have a super power what would it be?</b>
          <span>Super speed</span>
        </p>
        <p>
          <b>Do you believe in unicorns?</b>
          <span>No, I'm a realist</span>
        </p>
      </div>
    </div>
  </div>
</div>
CONTENT

# ---

about = <<-CONTENT
<div class="base-content static">
  <h1>About Us</h1>
  <p>Modera began as a dream to satisfy the need for choices in fashion and through our combined efforts we have developed perfection. We have carefully crafted the ultimate team composed of the the brightest and most innovative engineers, unparalleled pioneers in social media and marketing, the ultimate design stars, supreme fashionistas, along with creative and engaging writers.  All this to bring you Modera, more than just another social media site or app on your phone Modera will change the way you share your unique lifestyle with friends, family and the world.</p>
  <p>From Uzbekistan to California we embrace a diverse love of all things beautiful and creative and are united under one flag…the Modera flag.</p>
  <div class="team thin">
    <h3>Our Team</h3>
    <ul class="nopad overhide c">
      <li>
        <a href="/info/elvina">
          <img alt="El'vina Ziyadinova" height="126" src="/assets/about/Elya.png" width="126">
          <span>El'vina Ziyadinova</span>
          <span class="department">Developer</span>
        </a>
      </li>
      <li>
        <a href="/info/artemchekhlov">
          <img alt="Artem Chekhlov" height="126" src="/assets/about/Artem.png" width="126">
          <span>Artem Chekhlov</span>
          <span class="department">Intern</span>
        </a>
      </li>
      <li>
        <a href="/info/rustamkhamraev">
          <img alt="Rustam R. Khamraev" height="126" src="/assets/about/Rustam.png" width="126">
          <span>Rustam R. Khamraev</span>
          <span class="department">Startup guy</span>
        </a>
      </li>
      <li>
        <a href="/info/volodyapinchuk">
          <img alt="Vladimir Pinchuk" height="126" src="/assets/about/Volodya.png" width="126">
          <span>Vladimir Pinchuk</span>
          <span class="department">Business Development</span>
        </a>
      </li>
      <li>
        <a href="/info/sanjarbabadjanov">
          <img alt="Sanjar Babadjanov" height="126" src="/assets/about/Sanjar.png" width="126">
          <span>Sanjar Babadjanov</span>
          <span class="department">CEO</span>
        </a>
      </li>
      <li>
        <a href="/info/nodirmirsaid">
          <img alt="Nodir Mirsaid" height="126" src="/assets/about/Nodir.png" width="126">
          <span>Nodir Mirsaid</span>
          <span class="department">Business Development</span>
        </a>
      </li>
      <li>
        <a href="/info/mahmudkaraev">
          <img alt="Maxmud Karaev" height="126" src="/assets/about/Mahmud.png" width="126">
          <span>Maxmud Karaev</span>
          <span class="department">Intern</span>
        </a>
      </li>
      <li>
        <a href="/info/anvarshakirov">
          <img alt="Anvar Shakirov" height="126" src="/assets/about/Anvar2.png" width="126">
          <span>Anvar Shakirov</span>
          <span class="department">Intern</span>
        </a>
      </li>
      <li>
        <a href="/info/yaroslav">
          <img alt="Yaroslav Borzenkov" height="126" src="/assets/about/Yarik.png" width="126">
          <span>Yaroslav Borzenkov</span>
          <span class="department">Intern</span>
        </a>
      </li>
      <li>
        <a href="/info/anvartaishev">
          <img alt="Anvar Taishev" height="126" src="/assets/about/Anvar.png" width="126">
          <span>Anvar Taishev</span>
          <span class="department">Design &amp; Magic</span>
        </a>
      </li>
      <li>
        <a href="/info/alex">
          <img alt="Alexander Kobelev" height="126" src="/assets/about/Sasha.png" width="126">
          <span>Alexander Kobelev</span>
          <span class="department">Programmer</span>
        </a>
      </li>
      <li>
        <a href="/info/lynnebarker">
          <img alt="Lynne Pattalock-Barker" height="126" src="/assets/about/Lynne.png" width="126">
          <span>Lynne Barker</span>
          <span class="department">Marketing</span>
        </a>
      </li>
      <li>
        <a href="/info/shanebarker">
          <img alt="Shane Barker" height="126" src="/assets/about/Shane.png" width="126">
          <span>Shane Barker</span>
          <span class="department">Marketing</span>
        </a>
      </li>
      <li>
        <a href="/info/maratibragimov">
          <img alt="Marat Ibragimov" height="126" src="/assets/about/Marat.png" width="126">
          <span>Marat Ibragimov</span>
          <span class="department">Business Development</span>
        </a>
      </li>
      <li>
        <a href="/info/timothymunden">
          <img alt="Timothy Munden" height="126" src="/assets/about/Tim.png" width="126">
          <span>Timothy Munden</span>
          <span class="department">Community Manager</span>
        </a>
      </li>
      <li>
        <a href="/info/gunaydonmez">
          <img alt="Gunay Donmez" height="126" src="/assets/about/Gunay.png" width="126">
          <span>Gunay Donmez</span>
          <span class="department">Marketing Research</span>
        </a>
      </li>
      <li>
        <a href="/info/julerizzardo">
          <img alt="Jule" height="126" src="/assets/about/Jule.png" width="126">
          <span>Jule Rizzardo</span>
          <span class="department">Blogger</span>
        </a>
      </li>
      <li>
        <a href="/info/michael">
          <img alt="Michael" height="126" src="/assets/about/Michael.png" width="126">
          <span>Michael Paulmarie</span>
          <span class="department">Intern</span>
        </a>
      </li>
      <li>
        <a href="/info/jonathanlenoch">
          <img alt="Jonathan" height="126" src="/assets/about/Lenoch.png" width="126">
          <span>Jonathan Lenoch</span>
          <span class="department">Videographer</span>
        </a>
      </li>
    </ul>
  </div>
</div>
CONTENT

our_story = <<-CONTENT
<div class="base-content static">
  <h1>Our story</h1>
  <p class="c">
    <img alt="Our story in graphic" src="/assets/static/office_copy.jpg" width="630">
  </p>
  <p>The Modera story is one of perseverance, tenacity, integrity and soulful people with a utopian idea for a start-up that is based around the idea of family, putting the team before the individual.  Modera is the story of an Uzbek start-up in the US that is far surpassing expectations and gaining great support along the way. See how the story unfolds as this international start-up rises like a phoenix from the ashes of minimal funding and some of the most turbulent economic times in recorded history to create the American dream.</p>
  <p>Our story begins in the Fall of 2010. The economic crisis is in full swing in Uzbekistan.  People have stopped buying, taking their money out of the banks causing an immediate crash and spreading scarcity across the economic landscape.</p>
  <p>At the time our CEO Sanjar was a top manager at the largest investment company in Uzbekistan. This gave him a magnified view of exactly what the nation was facing from a financial perspective. People were afraid of spending money as they could find no financial certainty anywhere, causing investment to slow to a halt.</p>
  <p>At this point we had established a performance proven team of individuals who had built various businesses, successfully ran hotels and obtained million dollar contracts. Our reputation preceded us and we were known not only as the top young professionals in the country, but as the team who could bring any project from zero to profitable.</p>
  <p>With the combined brilliance of these innovative and progressive thinking business minds we collectively decided to create a startup that would become what is known as Modera today. The team had an enormous amount of talent and experience in developing businesses, but not tackling the beast known as a “startup”.</p>
  <p>Countless hours and days were spent working on investment proposals and the startup concept. Soon two developers joined our team followed by several more. It seemed that everyone had a consuming passion for what we were doing, giving their time to our startup without asking for a salary but buying into the bigger picture with everyone working towards the same dream. The idea of creating an alternative sales channel and bringing brands and consumers closer together was just far more exciting than anything on the market at the time or presently.</p>
  <p>Within six month we all left our prestigious jobs and the stability they provided in exchange for complete and utter uncertainty. Without any experience, or a clear understanding of what we were doing, we left our income sources and used our own money to fund the venture, earning our start-up stripes in the process.</p>
  <p>Soon our CFO Marat joined the team and with four accomplished businessmen and three developers we spent a year and created the first prototype app. It was nothing special and as to be expected we launched and failed.  It didn’t stop us though because we knew we were on the right track.</p>
  <p>More often than not the best learning experience is failure. By this time Sanjar had spent all his personal savings and had to sell his car, and later his house.  We spent this money on hiring a professional team of developers and moved from a “garage” into a house in order to have an opportunity to work with the team of first-class developers we had assembled. We developed a project called PickItApp (Polyvore.com with an affiliate system).</p>
  <p>We then found Pillsbury - a top law firm in the United States that agreed to work with us on a deferred fee basis.  They helped us register Inc. in Delaware. We then we started testing our idea in New York, as we targeted the US market as it was then and remains now the most developed.</p>
  <p>Our team has already developed a solid core and hired the best developers in the region, who left their high salary jobs to work for free with Modera, all because they believed in the team and the dream.</p>
  <p>As a result of our field research with PickItApp in New York, Moscow, Munich and Tashkent we have ensured that our product is competitive and started working on finding the seed money investment necessary to launch.</p>
  <p>Our main strength was the team we had formed, capable of solving problems of any complexity level, and that the fact that we had survived the failures with our own money and become stronger, wiser and more pragmatic as a result. We had a feasible product and we had partners.  However, our main weakness was that we lived in Tashkent.  This was one of the reasons we had difficulties on every level of negotiations with the investors. Bryan Schreier, on behalf of Sequoia Capital, told us it was too early for them to invest in us and suggested that we join YCombinator, offering to send a recommendation letter to Paul Graham. We then decided it was critically important to be located in the United States in order to recruit investors from the Silicon Valley. Ramzi Fawakhiri, of N2V, told us we were too late for their early stage funding, while being too young for their growth stage investment. Therefore, we decided to attract not startup investors, but investors from other industries who were able to see the vision.</p>
  <p>We had more than 200 meetings looking for what we called “the right investor” and with that came many interesting stories of ego’s, bartering and paying it forward.</p>
  <p>
    Big fish:
    <br>
    Right after the second Skype conversation he purchased tickets for us, booked a 5-star hotel in Dubai and along with that gave us an offer of a couple million dollars but it had a controlling stake attached to it. The offer seemed too good to be true, and it was. Having done intense research on this investor we learned that he needed controlling stake in order to resell our company, which meant we would be left in a very uncertain situation had we accepted his offer. We graciously rejected the generous deal, learning later that it was the right decision as it would have put us in a compromising position.
  </p>
  <p>
    The Hustler:
    <br>
    This was a very colorful character, as is the story that goes along with him.  We were very satisfied with the deal that he proposed.  He agreed to all of our terms and the documents were about to be signed. Then on the morning of the signing this man called Marat and asked “Why don’t you and Nodir leave Sanjar and come to Dubai with me? You are smart and prospective! Why would you need him on board? We will have an equal share and I’ll invest the $500k that you need to get started.” At Modera, integrity is incredibly important to us and we would never conduct business in such a manner. Needless to say, from then on we ceased any talks with him.
  </p>
  <p>
    The Middleman:
    <br>
    Portraying himself as a middleman this man tried to persuade us to give him 10% just for the opportunity to work with an Iranian investor. He did this in such an unprofessional manner, and pursued us for such a long time that we actually managed to obtain the investment and launch our beta without him.  The one positive that did come of these negotiations is that they prepared us for the future, as we now had all of the proper documentation that we would need in order properly solicit an investor. Ironically enough, he showed up right after we accomplished that agreeing to our terms.
  </p>
  <p>
    Real Angel:
    <br>
    At times things happen that can’t be explained, as though they are just meant to be.  At a time when we were in desperate need of capital for our start-up, a local businessman generously donated $30,000 dollars without any expectation in return. He gave the money because he had it to give and gave it to us on one condition.  The one thing he asked of us was that we one day do the same thing for another business. The transaction was sealed with a handshake and an exchange of the money in a paper bag, a scene seemingly straight out of a movie. It was exactly the inspiration we needed and we will be eternally grateful and will definitely make sure to pay it forward.
  </p>
  <p>The next six months we consumed ourselves in the task of finding an investor that would believe in our team and our utopian ideas.  We were able to pull two investors, one from the Middle East and one from South East Asia.  We obtained a combined amount of $500,000 from the two investors. It was a perfect match as they trusted in our vision and experience.  All they asked was that we provide the necessary reports and product information along with updates on growth.</p>
  <p>The time to tell was over it was time to prove and for that we needed to enter the US market.  In five months we collected an inventory of more than 150,000 products and went to conquer the US market with our PickitApp. The purpose of going to the United States was to start the direct marketing and distribution of our product, as well as to establish our office there.</p>
  <p>However, Mother Nature had other plans in mind.  We got stuck in Manhattan, New York watching Superstorm Sandy wreak havoc on the people.   Without Sandy we would have never decided to have the series of meetings that we had withour mentors during the storm.</p>
  <p>These meetings would change the direction of our company forever (special thanks to Artem Mikhlin and Sergei Burkoff). As a result of testing our product in the real world and of meeting with the mentors, we realized there wasn’t a problem getting people onto the app initially, the problem was that the users would never return and rarely invited friends to join. This gave us an extremely low ROI, because the customer acquisition cost was higher than the lifetime value of customer.</p>
  <p>We immediately made a decision to pivot and work hard on the virility of the product. In just one day we developed the prototype for Modera. We gathered test teams in Tashkent, Germany and the US and let them test our prototype. It was successful. Modera became the cherry on the top of the PickItApp pie.</p>
  <p>We then looked for a marketing partner in the US with the purpose to find our Guy Kawasaki.  Among the dozens of marketers we chose Shane Barker based upon his social media presence and marketing expertise.  He left his firm and joined us bringing along with him his team of vast resources while willingly covering many of his own expenses.</p>
  <p>Having ensured that the business concept of Modera worked and solving the two main problems of PickItApp by reducing CAC and increasing ROI, we started working day and night on our dream. This was the Aha! Moment that propelled us into another gear, and it pushes us forward every day.</p>
  <p>One of our biggest challenges had been successfully completed with the development of our website and iPhone app, the two main parts that we divided the project into. The campaign to start marketing Modera through social media has been a great triumph for us. As a result of our marketing campaign we have obtained 24,000 followers in just two months with more than 1,000 people waiting to be a part of our beta testing. That is a 450% growth rate!</p>
  <p>From Uzbekistan to California our passion for fashion, photography, design and more has brought this amazing bi-continent power team together as we build the ultimate lifestyle website. Our “muscle center” as we affectionately call it is located in Tashkent with 12 people, our 5 iOS developers are located in Ukraine, while the front office is in Sacramento with 6 employees.</p>
  <p>Modera – The site allows you to compete with others when it comes to anything having to do with lifestyle.  Modera has created a unique and creative way to showcase your favorite aspects of your lifestyle and compete against others to see whose life is more celebrated. Think of it as a kind of Instagram with more incentive to upload new content daily, it’s a competition among friends, enemies, and strangers about who is coolest and who has the most interesting lifestyle. The site will soon be divided into various categories ranging from fashion and dining, to photography, travel and cars.</p>
  <p>It all works due to the algorithm developed by our team from the Faculty of Physics. You simply upload your photo, apply hashtags, and with the help of this algorithm it is shown in the news feed compared to other pictures in the same category. Users with the same interests see one head to head competition at a time and vote on who is the winner of the two then are moved down the page to the next matchup.  There is no bias and everyone receives the same exposure.</p>
  <p>This means that your picture lives its own life and has its own history not subject to a timestamp as most social media sites are, it wins some and it loses some but it remains active as long as it is on your profile.  It is in continual motion amongst the users of the site, never stagnant, constantly being voted on and seen even by people that are not following you.</p>
  <p>The most popular users win prizes from our sponsors, like BOUJOI, the prestigious new jewelry brand that is offering a $500 gift card to the winner of their style contest hosted on Modera.  New sponsors will continue to partner with Modera to hold contests and promotions for their brands and increase the reach in their target markets.</p>
  <p>Modera 1.0 – The main feature will be the various brand sponsored contests and we are very excited to unveil the friend contest feature allowing you to create your very own contests and invite your friends to join.</p>
  <p>Modera 2.0 – The biggest addition will be the add-on of the PickItApp. This is where we will begin monetizing the site with mutually beneficial partnerships between the customer and the brand.  Users whose pictures get the most votes have a chance to make money from their outfits.  Users will make money from brands when someone visits the brand’s website by clicking on any piece of their outfit. In sorting the votes, the style and the outfits, we solve the issue of searching for outfits. The user has the opportunity to click onto the outfit directly from the customer’s picture and it will redirect them to the brand’s web store. This gives the user who posted the outfit an opportunity to make money for simply having good taste and being popular.</p>
  <p>Our platform is a great way for new designers and brands to specifically market to their demographic and test new designs and products.</p>
  <p>As our site has just went live and our iOS app is scheduled for release in the coming weeks, we are also working on attracting sponsors, brands, and hiring brilliant team players to the US office. We are paying special attention to attracting celebrities to our campaigns that will act as ambassadors of Modera.</p>
  <p>Simultaneously we are working on getting prepared for Series A funding. We already have preliminary agreements in place of which the details cannot be disclosed.</p>
  <p>This is a story about how to overcome by belief in self, team and the process of learning through failure.</p>
  <p>This is the American Dream. The only question left to answer is whether or not there will be a happy ending.  Having read this inspirational story we think you are already convinced there will be.</p>
</div>
CONTENT

partnership = <<-CONTENT
<div class="base-content static">
  <h1>Partnership</h1>
  <iframe frameborder="0" height="476" marginheight="0" marginwidth="0" scrolling="no" src="http://www.slideshare.net/slideshow/embed_code/16918418" width="630"></iframe>
</div>
CONTENT

business_proposal = <<-CONTENT
<div class="base-content static">
  <h1>Business proposal</h1>
  <p>
    <b>What is Modera?</b>
    <br>
    Modera is a lifestyle that is represented by fashion, photography, design and much more.  It encompasses an iOS app that instantly uploads outfits with immediate response and continually drives traffic unlike any other app.
  </p>
  <p>
    <b>What doesModera solve?</b>
    <br>
    The emotional response of acceptance through photos as well as unparalleled number of impressions for Brands. An uploaded photo of the users outfit is selectedinto a “lifestyle genre/ fashion genre” and is immediately immersed amongst its peers to view.  Modera’s algorithm will astonish you with how all processes flow through the style challenge to make it possible to receive a large amount of instant feedback.
  </p>
  <p>
    <b>Why your Brand can benefit from Modera?</b>
    <br>
    Modera creates an environment in which your Brand is constantly being put in front of your target market with the uploaded photos through the Style Challenge. Your Brand is in a never-ending competition and comes to life virally in these contests for all to see.  The ultimate Style Challenge showdown for your Brand with the impressions you deserve.
  </p>
  <p>
    <b>What Modera has accomplished to date?</b>
  </p>
  <ul>
    <li>22,000 followers with average monthly growth rate of 30% on Instagram, Facebook, Twitter etc.</li>
    <li>Over 1,100 users with a 60% monthly growth rate on site leading up to beta release</li>
    <li>Team of 25 dedicated professionals.</li>
    <li>$500,000 as a seed stage money</li>
    <li>Exclusive sponsorship with BOUJOI</li>
  </ul>
  <p>
    <b>What we offer?</b>
    <br>
    Three fulltime promoters, talented staff content writers, outstanding designers along with video experts.  The entire staff at Modera works 24 hours a day to promote your contest, partnering to create a buzz for your Brand along with impactful advertising in the social media world.
  </p>
  <p>
    <b>Why Sponsor this Contest?</b>
    <br>
    On the Modera site you will have a target audience that is specifically geared toward your Brand and competing for your “Prize”.   During the Style Challenge over 2,000+ people will continually share your Brand on all of their social networks for the month of the contest and that will increase your reach in ways you could never obtain, and we drive them to you.
  </p>
  <p>
    <b>What we ask for that?</b>
    <br>
    Sponsorship isaimed at converting your visitors into ambassadors of your Brand, by simply offering a $500 gift-card for your products to the winner of the Modera Style Challenge contest.
  </p>
</div>
CONTENT

content = <<-CONTENT
<div class="base-content static">
  <h1>Content</h1>
  <div class="grid">
    <a href="/content/BOUJOI_GOLD_LOVE_CHARM">
      <img alt="BOUJOI GOLD LOVE CHARM" src="http://images.modera.co/uploads/modera/static_contest_history/14/thumb_e909be14-ec9b-47b8-9fa5-27bd75a08cab.jpg">
    </a>
    <a href="/content/BOUJOI_DIAMOND_GOLD_CHARM_URBAN">
      <img alt="BOUJOI DIAMOND GOLD CHARM URBAN" src="http://images.modera.co/uploads/modera/static_contest_history/13/thumb_ed8f3369-3b12-4431-80fb-b2da40091b7e.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_NAMESAKE_CHARM">
      <img alt="BOUJOI GOLD NAMESAKE CHARM" src="http://images.modera.co/uploads/modera/static_contest_history/12/thumb_e892145b-6714-40eb-be7a-f535c1472061.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_NAMESAKE_CHARM_CASUAL">
      <img alt="BOUJOI GOLD NAMESAKE CHARM CASUAL" src="http://images.modera.co/uploads/modera/static_contest_history/11/thumb_b8d6dfc9-80a4-408a-8f0a-900a1f3a1141.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_CLOVERPENDANT">
      <img alt="BOUJOI GOLD CLOVERPENDANT" src="http://images.modera.co/uploads/modera/static_contest_history/10/thumb_bdd42892-e2fd-488c-b517-ccda5cfdde04.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_RING_STARS_SIDE_BY_SIDE">
      <img alt="BOUJOI GOLD RING STARS SIDE BY SIDE" src="http://images.modera.co/uploads/modera/static_contest_history/9/thumb_cbba9cf4-43cd-44e2-8635-0a4490548b5b.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_NAMESAKE_CHARM_GOLD_RING_STARS">
      <img alt="BOUJOI GOLD NAMESAKE CHARM GOLD RING STARS" src="http://images.modera.co/uploads/modera/static_contest_history/8/thumb_bb2f7812-4794-4347-a865-a76bf44e2d2a.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_CLOVERPENDANT_GOLD_STARS_RING_URBAN">
      <img alt="BOUJOI GOLD CLOVERPENDANT GOLD STARS RING URBAN" src="http://images.modera.co/uploads/modera/static_contest_history/7/thumb_1142b85e-0619-4ed7-98ca-e0e2d8ec30a5.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_DIAMOND_CHARM">
      <img alt="BOUJOI GOLD DIAMOND CHARM" src="http://images.modera.co/uploads/modera/static_contest_history/6/thumb_db97a543-9cbd-43e1-865d-635658468427.jpg">
    </a>
    <a href="/content/BOUJOI_GOLD_RING_STARS_GOLD_CLASSIC_STARS">
      <img alt="BOUJOI GOLD RING STARS GOLD CLASSIC STARS" src="http://images.modera.co/uploads/modera/static_contest_history/5/thumb_fe786d80-1871-4af7-96f4-8b9a558e85aa.jpg">
    </a>
    <a href="/content/BOUJOI_SILVER_DIAMOND_CHARM">
      <img alt="BOUJOI SILVER DIAMOND CHARM" src="http://images.modera.co/uploads/modera/static_contest_history/4/thumb_db30fd32-834b-47c8-a00f-4acfbe17ee0a.jpg">
    </a>
    <a href="/content/BOUJOI_CLOVERPENDANT_GOLD_RING_STARS_SIDE">
      <img alt="BOUJOI CLOVERPENDANT GOLD RING STARS SIDE" src="http://images.modera.co/uploads/modera/static_contest_history/3/thumb_ab6148bc-983f-494c-b134-525d1018d6fe.jpg">
    </a>
    <a href="/content/BOUJOI_DIAMOND_CHARM_SILVER_GOLD_RING_STARS">
      <img alt="BOUJOI DIAMOND CHARM SILVER GOLD RING STARS" src="http://images.modera.co/uploads/modera/static_contest_history/2/thumb_45e9edd2-99b9-4771-b5cc-ffaaba83144b.jpg">
    </a>
    <a href="/content/BOUJOI_DIAMOND_SILVER_CHARM_GOLD_CLASSIC_STARS_BIG_RING">
      <img alt="BOUJOI DIAMOND SILVER CHARM GOLD CLASSIC STARS BIG RING" src="http://images.modera.co/uploads/modera/static_contest_history/1/thumb_5451b520-5cdf-43a4-a1de-a9cc5bf447c3.jpg">
    </a>
  </div>
</div>
CONTENT

videos = <<-CONTENT
<div class="base-content static">
  <h1>Videos</h1>
  <div class="grid">
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/ST47Vdhk12s?rel=0" width="320"></iframe>
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/1LpOZLXOKYI?rel=0" width="320"></iframe>
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/BG8x8tMlMQA?rel=0" width="320"></iframe>
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/Lspu6F-VXrE?rel=0" width="320"></iframe>
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/DFTHLbuHzxA?rel=0" width="320"></iframe>
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/9swJeW0a0Rk?rel=0" width="320"></iframe>
    <iframe frameborder="0" height="180" src="http://www.youtube.com/embed/l1XK0o_2FMw?rel=0" width="320"></iframe>
  </div>
</div>
CONTENT

contact_us = <<-CONTENT
<div class="base-content static">
  <h1>Contact us</h1>
  <p>
    <b>Looking to get in touch?</b>
    <br>
    Chat with us about an opportunity or idea anytime at
    <a href="mailto:hello@modera.co">Hello@modera.co</a>
  </p>
  <p>
    If you're having a problem, Send us an email at
    <a href="mailto:help@modera.co">help@modera.co</a>
    and let us know how we can help.
  </p>
  <p>
    For information on how your brand can make contest on modera reach us via email, contact
    <a href="mailto:brand@modera.co">brand@modera.co</a>
  </p>
  <p>
    <b>Looking to follow us? We have all kinds of lifestyle on</b>
  </p>
  <div class="share-panel">
    <!-- /facebook -->
    <div class="share-icons facebook">
      <i class="moderaIcons">f</i>
      <span class="thin fs14">
        <a href="http://www.facebook.com/FashionModera">Fashion</a>
        |
        <a href="http://www.facebook.com/PhotographyModera">Photography</a>
      </span>
    </div>
    <!-- /twitter -->
    <div class="share-icons twitter">
      <i class="moderaIcons">t</i>
      <span class="thin fs14">
        <a href="http://twitter.com/themodera">theModera</a>
      </span>
    </div>
    <!-- /instagram -->
    <div class="share-icons instagram">
      <i class="moderaIcons">i</i>
      <span class="thin fs14">
        <a href="http://instagram.com/themodera">themodera</a>
        |
        <a href="http://instagram.com/fashionmodera">fashionmodera</a>
        |
        <a href="http://instagram.com/photographymodera">photography</a>
        |
        <a href="http://instagram.com/artmodera">art</a>
      </span>
    </div>
    <!-- /pinterest -->
    <div class="share-icons pinterest">
      <i class="moderaIcons">p</i>
      <span class="thin fs14">
        <a href="http://pinterest.com/themodera">themodera</a>
      </span>
    </div>
  </div>
  <div class="clear"></div>
  and &nbsp;&nbsp;
  <a href="http://youtube.com/user/LifestyleModera">
    <img alt="youtube modera" src="/assets/youtube.png" style="vertical-align:middle">
  </a>
  <form action="#">
    <fieldset>
      <h3>Contact form</h3>
      <label class="thin fs14" for="name">Your Name</label>
      <div class="fieldForm">
        <input id="name" name="name" tabindex="1" type="text">
      </div>
      <label class="thin fs14" for="email">Your Email</label>
      <div class="fieldForm">
        <input id="email" name="email" tabindex="2" type="email">
      </div>
      <label class="thin fs14" for="subject">Subject</label>
      <div class="fieldForm">
        <input id="subject" name="subject" tabindex="3" type="text">
      </div>
      <label class="thin fs14" for="subject">Message</label>
      <div class="fieldForm">
        <textarea id="message" name="message" tabindex="4"></textarea>
      </div>
    </fieldset>
    <div class="c">
      <input class="btn" name="commit" tabindex="5" type="submit">
    </div>
  </form>
</div>
CONTENT

guidlines = <<-CONTENT
<div class="base-content static">
  <h1>Modera Community Guidelines</h1>
  <p>
    The detailed Community Guidelines below have been created in order to help you understand the best practices as a member of the Modera community. By utilizing Modera you are also subject to our
    <a href="/tos">Terms of Use.</a>
  </p>
  <p>What our community values:</p>
  <ol>
    <li>
      <b>Ownership.</b>
      <br>
      Modera is a great way to share your lifestyle and gain instant feedback from your peers who share your style. It’s important though that you share your own pictures and do not violate copyright ownership. When you want to express your excitement over a beautiful photo, it’s best to simply vote for it or save as favorite!
    </li>
    <li>
      <b>Diversity.</b>
      <br>
      Remember that our community is a diverse one, and that your photos are visible to people as young as 13 years old. While we respect the artistic integrity of photos, we have to keep our product and the photos within it in line with our App Store’s rating for nudity and mature content. In other words, please do not post nudity or mature content of any kind.
    </li>
    <li>
      <b>Respect.</b>
      <br>
      People from around the world with different backgrounds come together to share their lifestyle pictures on Modera. It means that not everyone will agree with what you think or believe. We ask that all users be polite and respectful in their interactions with other members.
    </li>
    <li>
      <b>Meaningful Interaction.</b>
      <br>
      Being the essential part of success on Modera lifestyle contests, excess self-promotion along with the commercial solicitation of any kind, in any form, is rarely well-received. So please be sincere with your exchanges on Modera.
    </li>
    <li>
      <b>Use.</b>
      <br>
      Modera is the best way to easily upload pictures of your style and receive a response instantly through Modera style challenge!
    </li>
  </ol>
  <p>What you shouldn’t do:</p>
  <p>Please note that violation of any of the rules outlined below may result in a disabled account, or discontinued access to Modera, without warning.</p>
  <ol>
    <li>
      <b>Don’t post pictures that don’t belong to you.</b>
      <br>
      This includes other people’s photos, or things that you have copied or collected from the Internet. Accounts that solely consist of only this type of content may be disabled at any time without warning.
    </li>
    <li>
      <b>Don’t post pictures that show nudity or any other mature content.</b>
      <br>
      Don’t post anything that children shouldn’t see. The same rule applies to your profile picture. Accounts found posting nudity or mature content will be disabled and your access to Modera may be discontinued, while the pictures will be deleted as soon as practicable.
    </li>
    <li>
      <b>Don’t post pictures of illegal content.</b>
      <br>
      If we find you posting prohibited or illegal content, including pictures of extreme violence or gore, your account will be disabled and we will take appropriate action, which may include reporting you to the authorities.
    </li>
    <li>
      <b>Don’t spam.</b>
      <br>
      Modera is a place where people can share their lifestyle images and get instant feedback from people about their outfits. It’s not a place where you should solicit, advertise or otherwise promote any of your products/content. This guideline includes repetitive comments, as well as service manipulation in order to self-promote, and extends to commercial spam comments, such as discount codes or URLs to websites. Please keep the community clean and don’t spam.
    </li>
    <li>
      <b>Don’t be rude.</b>
      <br>
      Modera is a friendly place where everyone should feel safe and comfortable sharing their lifestyles through pictures. It is not an appropriate place to abuse, attack, harass, troll or impersonate others. If we receive valid complaints about your conduct, we’ll send a warning or disable your account without notice. Alternatively, we’d recommend users block all meanies and trolls to prevent further issues.
    </li>
    <li>
      <b>Don't promote or glorify self-harm.</b>
      <br>
      Don’t post content that encourages or urges users to embrace anorexia, bulimia, or other eating disorders; or to cut, harm themselves, or commit suicide. This will result in a disabled account without warning.  Modera is not the place for active promotion or glorification of self-harm.
    </li>
  </ol>
  <p>Additional things to remember</p>
  <ul>
    <li>Modera is a diverse community, and it is likely that you will come across things that offend you. If you are offended by a photo that may not violate any of the above guidelines, we’d suggest you navigate away from the account or simply skip the comparison block. If you are unsure about the content, you can report it utilizing our built-in reporting functionality in order to bring it to the Modera Team’s attention.</li>
    <li>If you find that one of your images has been uploaded to another user’s profile, try not to panic. It is likely a misunderstanding and not ill-intended. The first step you should take is notify us and we will investigate.</li>
    <li>If you think that there is immediate cause for concern, you can send a report to the Modera Team. Please provide all necessary information for us to quickly respond to an issue that urgently needs our attention, this includes image descriptions and dates, URLs, and valid usernames.</li>
  </ul>
  <p>If you believe you will have trouble following these guidelines we’d suggest not to use Modera, as we value these guidelines and care about our users, and believe that they will help keep Modera a safe and fun place for everyone.</p>
</div>
CONTENT

tos = <<-CONTENT
<div class="base-content static">
  <h1>Terms of services</h1>
  <p>
    THESE WEB SITE TERMS OF SERVICE AND SOFTWARE USE AGREEMENT (COLLECTIVELY, "AGREEMENT") SETS FORTH THE TERMS AND CONDITIONS FOR YOUR ACCESS AND USE OF THE SOFTWARE ("SOFTWARE") PRODUCED BY TEBECOM INC. ("COMPANY") AND THE WEB SITE CURRENTLY LOCATED AT
    <a href="http://modera.co">[WWW.MODERA.CO]</a>
    ("SITE"), AND RELATED SERVICES THAT MAY BE PROVIDED BY COMPANY. THE SOFTWARE, SITE, AND SUCH RELATED SERVICES ARE REFERRED TO AS THE "SERVICE".
  </p>
  <p>PLEASE READ THIS AGREEMENT CAREFULLY. YOU MAY NOT USE ANY PART OF THE SERVICE UNLESS YOU AGREE TO THE TERMS OF THIS AGREEMENT. YOUR USE OF THE SERVICE WILL CONSTITUTE YOUR ACCEPTANCE OF THIS AGREEMENT.</p>
  <ol>
    <li>
      <p>Authorized Users. The Software may only be installed or used by (a) the owner of the device, on which the Software is installed ("Authorized Device"), and (b) any persons authorized by the owner of the Authorized Device to use the Authorized Device and to install the Software in accordance with the terms of this Agreement, provided that the individuals referred to in (a) and (b) above have read this Agreement and agree to abide by it (collectively, "Authorized Users," and each an "Authorized User"). Any references to the pronouns "you" and "your" anywhere in this Agreement are intended to refer to any one or to all of the Authorized Users of the Software. You represent and warrant that you are of legal age to form a binding contract.</p>
    </li>
    <li>
      <p>Ownership; Limited License Grant. As between you and Company, the following are and shall remain the sole property of Company: (a) all right, title and interest in and to the Service, including, without limitation, the Software and the Site; (b) all Content (as defined below); and (c) any intellectual property rights (including, without limitation, any copyrights, patents, trademarks, trade secrets and other rights) in and to any of the foregoing. Company grants you a limited, nonexclusive, nontransferable, royalty-free license to download and install the Software and to access and use the Service solely for your personal, non-commercial use and subject to the terms of this Agreement. You may not assign, sublicense or otherwise convey or transfer the rights granted to you under this Agreement to any other person, organization, or entity. Any rights not expressly granted to you hereunder are reserved by Company. The limited rights granted to you to download and install the Software and to access and use the Service comprise a limited license and do not constitute the sale of a software program.</p>
    </li>
    <li>
      <p>Use Restrictions. You may not do any of the following with or to the Service or any part thereof: (a) decompile, reverse engineer, or disassemble it, (b) tamper or interfere with its functionality, delivery or operation, (c) assign, sublicense, pledge, lease, sell, rent, transfer, resell, or distribute it, (d) duplicate, reproduce, copy, modify, or otherwise create derivative works of it, (e) alter or remove any identification of any copyright, trademark, or other proprietary rights notice which indicates Company's ownership of the Service, or (f) export or re-export the Software, directly or indirectly in violation any applicable law, rule or regulation of any jurisdiction (including the United States Export Administration Act and any regulations promulgated thereunder). You shall abide by all applicable local, state, national and international laws and regulations in connection with your use of the Service.</p>
    </li>
    <li>
      <p>Rules and Conduct. You are responsible for your use of the Service, and for any use of the Service made using your account. The Company's goal is to create a positive and safe community experience. To promote this goal, the Company prohibits certain kinds of conduct that may be harmful to Company or to others. When you use the Service, you may not:</p>
      <ul>
        <li>violate or infringe other people's intellectual property, privacy, publicity, or other legal rights;</li>
        <li>transmit anything that is illegal, abusive, harassing, harmful to reputation, pornographic, indecent, profane, obscene, hateful, racist, or otherwise objectionable;</li>
        <li>send unsolicited or unauthorized advertising, promotional or commercial communications, such as spam or conduct commercial activities and/or sales without Company's prior written consent such as contests, sweepstakes, barter, advertising, promotional offers or pyramid schemes;</li>
        <li>transmit any malicious or unsolicited software;</li>
        <li>stalk, harass, or harm another individual;</li>
        <li>impersonate or misrepresent your affiliation with someone else;</li>
        <li>share with any third-party your username, password, or any access control information used to gain access to or use the Service;</li>
        <li>use any means to "scrape," "crawl," or "spider" any Web pages contained in the Site;</li>
        <li>use automated methods to use the Service in a manner that sends more requests to the Company's servers in a given period of time than a human can reasonably produce in the same period by using a conventional Web browser;</li>
        <li>interfere with or disrupt the Service;</li>
        <li>or collect or harvest any personally identifiable information from the Service.</li>
      </ul>
      <ul>
        <li>
          <p>Content. Other than User Submissions (as defined below), Company owns or licenses the content on the Service, including software, text, visual and audio content (referred to together as "Content") and trademarks, logos, and brand elements on the Service (referred to as "Marks"). The Content and Marks are protected under U.S. and international laws.</p>
        </li>
        <li>
          <p>
            Registration. You may browse the Site and view Content without registering, but as a condition to using certain aspects of the Service, you may be required to register with Company and select a password and screen name ("User ID"). You shall provide Company with accurate, complete, and updated registration information. Failure to do so shall constitute a breach of this Agreement, which may result in immediate termination of your account. You shall not (a) select or use as a User ID a name of another person with the intent to impersonate that person; (b) use as a User ID a name subject to any rights of a person other than you without appropriate authorization; or (c) use as a User ID a name that is otherwise offensive, vulgar or obscene. Company reserves the right to refuse registration of, or cancel a User ID in its sole discretion. You are solely responsible for activity that occurs on your account and shall be responsible for maintaining the confidentiality of your password. You shall never use another user's account without such other user's express permission. You will immediately notify Company in writing of any unauthorized use of your account, or other account related security breach of which
            <span>you are aware.</span>
          </p>
        </li>
        <li>
          <p>User Submissions. The Service may provide you with the ability to create, post, or share content using the Service ("User Submissions"), such as posting, creating or recording comments, videos, photos and other content. You own your User Submissions. You grant Company a worldwide, non-exclusive, perpetual, irrevocable, royalty-free, fully paid, sublicensable and transferable license to use, edit, modify, reproduce, distribute, prepare derivative works of, display, perform, and otherwise fully exploit the User Submissions in connection with the Service and Company's (and its successors' and assignees') business, including without limitation for promoting and redistributing part or all of the Service in any media formats and through any media channels (including, without limitation, third-party Web sites). You promise that (a) you own all rights to your User Submissions or, alternatively, that you have the right to give Company the rights described above; (b) you have paid and will pay in full any fees or other payments that may be related to the use of your User Submission; and (c) your User Submissions do not infringe the intellectual property rights, privacy rights, publicity rights, or other legal rights of any third-party. Company does not endorse and has no control over any User Submission. Company has no obligation to monitor the Service or any User Submissions. We may refuse to accept or transmit User Submissions. We may remove or block any User Submissions from the Service for any reason. Some parts of the Service may be supported by advertising revenue and may display advertisements and promotions, and you hereby agree that Company may place such advertising and promotions on the Service or on, about, or in conjunction with your User Submissions in Company's sole discretion.</p>
          <p>User Submissions that you designate as "private" through the Service will not be shared publicly. Company and/or other users may copy, print or display publicly available User Submissions outside of the Service, including, without limitation, via the Site or third party Web sites or applications (for example, services allowing users to order prints of User Submissions or t-shirts and similar items containing User Submissions). After you remove your User Submissions from the Service we will cease distribution as soon as practicable. If after we have distributed your User Submissions outside the Service you change your privacy setting to "private," we will cease any further distribution of such "private" User Submissions outside the Service as soon as practicable.</p>
        </li>
        <li>
          <p>Participation in Contests. By submitting your photo you agree to participate in the general contest. Winner is determined by the number of votes for all user’s photos collectively. Contest typically last 1 month if not specified otherwise in the contest description. Joining any other specific contest on the Service, you agree to be bound by the Contest Rules of the respective contest. If any terms of the Contest Rules contradict with these specified herein, they shall be considered valid and enforceable. If your submission violates any of the Contest Rules, it will be deleted and you will be warned by email. You may be banned from using the Service at the sole discretion of the Company if you repeatedly break the Contest Rules.</p>
        </li>
        <li>
          <p>Draw Process. After judging and voting to select prize winners has finished, the potential prize winners will be notified by email or other method chosen by the Company in its discretion. Sponsor providing the prize for the contest reserves the right to verify winners collectively or one or more at a time, as it deems appropriate and expedient in its discretion, and in whatever order it chooses. To be confirmed as a winner and to be awarded the prize, user may be required to confirm full legal rights and ownership of Submission.</p>
          <p>Should a prize notification or prize addressed to a particular user be returned as undeliverable, the Company will not resend the notice (whether by the same or another method), research the user's address nor make any further attempts at delivery. That user will be deemed.</p>
          <p>In the case that winner's shipping address is located in a Country or area designated by the Company as undeliverable, the Company will attempt to ship the prize to the alternative address provided by the winner.</p>
          <p>Winners will be announced after the Required Confirmation is received if needed and the respective winners are confirmed. Company reserves the right to announce prize winners on the Service website collectively, one at a time, or in any order or clusters as it deems appropriate in its discretion. Winners will receive their prizes within thirty (30) days of Company announcement of the winners. The prizes are subject to change at any time without notice in the sole discretion of the Company.</p>
        </li>
      </ul>
      <p>DISQUALIFICATION: COMPANY RESERVES THE RIGHT IN ITS SOLE DISCRETION TO DISQUALIFY ANY PERSON WHO TAMPERS WITH OR IS SUSPECTED OF TAMPERING WITH AN ENTRY AND/OR THE JUDGING AND VOTING PROCESS, INCLUDING ANY PERSON ATTEMPTING TO SOLICIT, ENCOURAGE OR OBTAIN AUTOMATED VOTES. IN ADDITION TO THE RIGHTS RETAINED BY COMPANY SET FORTH IN SECTION 4 ABOVE, COMPANY FURTHER RESERVES THE RIGHT IN ITS DISCRETION TO DISQUALIFY ALL VOTES FROM VOTERS THAT ARE DEEMED TO BE FRAUDULENT. CAUTION: ANY ATTEMPT BY USER TO DELIBERATELY DAMAGE COMPANY, INCLUDING BUT NOT LIMITED TO, THE ENTRY SITE, OR UNDERMINE THE LEGITIMATE OPERATION OF THE CONTEST IS A VIOLATION OF CRIMINAL AND CIVIL LAW. SHOULD SUCH AN ATTEMPT BE MADE, COMPANY RESERVES THE RIGHT TO PROSECUTE AND/OR SEEK DAMAGES AND/OR OTHER RELIEF FROM ANY SUCH ENTRANT TO THE FULLEST EXTENT OF THE LAW.</p>
      <p>Delivery and Damaged Goods</p>
      <p>
        Delivery will be made by a postal or courier service such as but not limited to FEDEX, UPS, and USPS and will be paid for by the winner in case if the winner is located outside the United States. If a product is delivered to a customer, that is physically damaged, Modera will issue or request the respective Sponsor to issue a replacement copy of the product. Please email the Company at
        <a href="mailto: support@modera.co">support@modera.co</a>
        within 5 business days of receiving the product to tell us about the nature of the damage and to arrange for a new product to be sent to you at the Company's expense.
      </p>
    </li>
    <li>
      <p>Taxation responsibility.</p>
      <p>Each party is responsible and will account for any taxes imposed by governments or governing authorities, and related accounting or audit requirements arising out of, as a result of, incidental to, or in connection with obligations under this Agency Agreement.</p>
      <p>Intellectual Property. We respect the intellectual property rights of others. Upon proper notice, we will remove User Submissions or other applicable content that violates copyright or other intellectual property rights. We will also terminate the accounts of repeat infringers. If you believe your work has been copied in a way that constitutes copyright infringement, or that your intellectual property rights have been otherwise violated, please send a notification to support@modera.co with all of the following information:</p>
      <ul>
        <li>identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works are covered by a single notification, a representative list of such works;</li>
        <li>identification of the claimed infringing material and information reasonably sufficient to permit us to locate the material on the Service;</li>
        <li>information reasonably sufficient to permit us to contact you, such as an address, telephone number, and, if available, an e-mail address;</li>
        <li>a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;</li>
        <li>a statement by you, made under penalty of perjury, that the above information in your notification is accurate and that you are the copyright owner or authorized to act on the copyright owner's behalf;</li>
        <li>
          and your physical or electronic signature. Please send all of the above information to
          <a href="mailto: support@modera.co">support@modera.co</a>
        </li>
        <li>
          Privacy. The information that Company collects from you through technical means in connection with your download, installation or use of the Service will be collected, stored, used and shared by Company in accordance with the Company Privacy Policy, which you may access at
          <a href="http://www.modera.co/privacy">[www.modera.co/privacy].</a>
          The Company Privacy Policy, as may be modified from time to time in Company's sole discretion, is expressly incorporated herein by this reference and made part of this Agreement.
        </li>
        <li>Other Content or Service. Through your use of the Service, you may receive content or services provided through, or in connection with the Service, by individuals or entities other than Company, including, without limitation, data, links, articles, graphic or video messages, text, software, music, sound, graphics or other materials or services (collectively, the "Other Content or Service"). You understand and agree that you will not obtain, as a result of your use of the Software, any right, title or interest in or to such Other Content or Service delivered via the Service or in any intellectual property rights (including, without limitation, any copyrights, patents, trademarks, trade secrets or other rights) in and to such Other Content or Service. You understand and agree that such Other Content or Service shall be the responsibility of the entity that originated, provided, delivered, offered, sold, supplied, promoted, sponsored or advertised such Other Content or Service, and shall not be the responsibility of Company. You agree that you will evaluate and bear all risks associated with the viewing or use of any such Other Content or Service, including any reliance on the accuracy or completeness of such Other Content or Service. Company expressly disclaims, to the maximum extent permitted by law, any liability for any such Other Content or Service, including, without limitation, for any errors or omissions in any such Other Content or Service, or for loss or damage or injury of any kind incurred as a result of the use of any such Other Content or Service posted, transmitted, advertised or otherwise made available to you through or in connection with the Service.</li>
        <li>Third-Party Web Sites. The Service may enable you to interact with or access other Web sites operated not by the Company, but by third parties, including both affiliated and unaffiliated third parties. When you access such third-party Web sites, you do so at your own risk. These third party Web sites are not under Company's control, and you acknowledge that Company is not responsible or liable for the content, functions, accuracy, legality, appropriateness, information practices or any other aspect of such Web sites. Your use of such third party Web sites, and the collection and use of information in connection with such use, is governed by the terms and conditions and privacy policies of their respective third party Web site operators. You acknowledge and agree that Company shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, good or services available on or through any such third party Web site.</li>
        <li>Modifications to the Service; Termination. Updates, enhancements and upgrades to the Software may be made available to you automatically. Company shall have the right at any time to disable, modify, or discontinue, temporarily or permanently, the Service (or any part thereof), with or without notice. Without limiting the foregoing, Company may terminate your access to all or any part of the Service at any time, with or without cause, with or without notice, effective immediately, which may result in the forfeiture and destruction of all information associated with your account.</li>
        <li>NO SUPPORT. COMPANY HAS NO OBLIGATION TO PROVIDE YOU WITH SUPPORT, MAINTENANCE, UPGRADES, MODIFICATIONS, OR NEW RELEASES WITH RESPECT TO THE SOFTWARE.</li>
        <li>DISCLAIMER OF WARRANTIES. TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SERVICE IS PROVIDED "AS IS" AND "AS AVAILABLE" WITH NO TECHNICAL SUPPORT OR REPRESENTATIONS OR WARRANTIES OF ANY KIND, EITHER EXPRESSED OR IMPLIED, BY STATUTE OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT AND ANY WARRANTIES THAT MAY ARISE OUT OF COURSE OF PERFORMANCE, COURSE OF DEALING OR USAGE OF TRADE. YOU SPECIFICALLY AGREE THAT, TO THE MAXIMUM EXTENT PERMITTED BY LAW, COMPANY SHALL NOT BE LIABLE FOR LOSSES OR LIABILITIES ARISING IN CONNECTION WITH YOUR DOWNLOAD, INSTALLATION OR USE OF THE SOFTWARE, OR YOUR USE OF THE SERVICE, INCLUDING, WITHOUT LIMITATION, LOSS OR LIABILITY RESULTING OR ARISING FROM OR IN CONNECTION WITH: (a) SOFTWARE CONFLICTS RELATED TO THE SOFTWARE; (b) DATA NON-DELIVERY, DATA MIS-DELIVERY OR UNAUTHORIZED ACCESS TO TRANSMISSIONS OF DATA; (c) YOUR INFRINGEMENT OF A THIRD-PARTY'S RIGHT; (d) DEFECTS OR VIRUSES IN, OR DISTRIBUTED WITH, THE SOFTWARE OR CONTENT; (e) YOUR OWN USE OR MISUSE OF YOUR PERSONAL DEVICE OR THE SOFTWARE APPLICATIONS CONTAINED ON YOUR PERSONAL DEVICE; OR (f) THE UNAVAILABILITY OF THE SITE OR SERVICE. YOUR SOLE REMEDY WITH RESPECT TO ANY CLAIM IS TO STOP USING THE SERVICE.</li>
        <li>Limitation of Liability. TO THE MAXIMUM EXTENT PERMITTED BY LAW, COMPANY SHALL NOT BE LIABLE TO YOU FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, OR PUNITIVE DAMAGES OR LOST OR IMPUTED PROFITS OR ROYALTIES ARISING OUT OF OR RELATING IN ANY WAY TO THE SERVICE OR THIS AGREEMENT, IRRESPECTIVE OF WHETHER COMPANY WAS ADVISED, KNEW OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF ANY SUCH LOSS OR DAMAGE.</li>
        <li>Indemnification. Indemnification. EVERY CONTEST OR PROMOTION ON THE SERVICE IS IN NO WAY SPONSORED, ENDORSED OR ADMINISTERED BY, OR ASSOCIATED WITH, APPLE INC. YOU ARE PROVIDING YOUR INFORMATION TO AND ENGAGING WITH MODERA AND NOT WITH APPLE INC. BY PARTICIPATING IN ANY OF THE CONTESTS OR PROMOTIONS YOU AGREE TO A COMPLETE RELEASE OF APPLE FROM ANY CLAIMS. Participation in this promotion is subject to and regulated by this agreement and by the contest rules of each contest or promotion. You hereby agree to release, indemnify, defend and hold harmless Company, its parents, shareholders, subsidiaries, affiliates, officers, directors, employees, agents and advisors, from and against any and all losses, liabilities, claims (including claims without legal merit or brought in bad faith), demands, damages, costs or expenses, causes of action, suits, proceedings, judgments, awards, executions and liens, including reasonable attorneys' fees and costs (whether brought by third parties or otherwise) relating to or arising from your use, installation or download of the Software, your use of the Service, your User Submissions, your participation in the contests, or your breach of this Agreement. Company reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you hereunder, and in such event, you shall have no further obligation to provide indemnification for such matter.</li>
        <li>General Provisions. These Terms of Service will be governed by and construed in accordance with the laws of the State of California, without giving effect to any conflict of laws rules or provisions. This Agreement will not be governed by the United Nations Convention on Contracts for the International Sale of Goods, the application of which is hereby expressly excluded. The exclusive forum for any disputes arising out of or relating to this Agreement shall be an appropriate court in [Palo Alto, California] and you hereby expressly consent to personal jurisdiction in such courts and waive all venue, jurisdiction and choice of law challenges or defenses. If any part of this Agreement is determined by a court of competent jurisdiction to be invalid or unenforceable, including, without limitation, any of the warranty disclaimers or liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the Agreement shall continue in full force and effect. If no enforceable provision can be substituted for any such invalid or unenforceable provision, then that provision shall be deemed severable from the Agreement and shall not affect the validity and enforceability of any remaining provisions in this Agreement. You may not assign any rights or obligations under this Agreement. This Agreement will be binding upon and will inure to the benefit of Company and its successors and assignees. This Agreement, together with any rules, policies, or additional terms associated with the Service, constitutes the entire understanding and agreement of the parties respecting the subject matter of this Agreement.</li>
        <li>Changes to this Agreement. From time to time, Company may change this Agreement. If Company changes this Agreement, Company will inform you by posting the revised Agreement on the Site. Those changes will go into effect on the Revision Date shown in the revised Agreement. By continuing to use our Site or Service, you agree to the revised Agreement.</li>
      </ul>
    </li>
  </ol>
  <p>BY USING THE SERVICE, YOU ACKNOWLEDGE THAT (a) YOU HAVE READ AND REVIEWED THIS AGREEMENT IN ITS ENTIRETY, (b) YOU AGREE TO BE BOUND BY THE TERMS OF THIS AGREEMENT, AND (c) YOUR OBLIGATIONS UNDER THIS AGREEMENT ARE BINDING AND ENFORCEABLE.</p>
</div>
CONTENT

privacy = <<-CONTENT
<div class="base-content static">
  <h1>Privacy Policy</h1>
  <p>
    We have prepared this Privacy Policy to explain how we collect, use protect, and disclose information and data when you use the software (the "Software") produced by Tebecom Inc. ("Company") and the web site located at
    <a href="http://www.modera.co">[www.modera.co]</a>
    (the "Site"), and related services that may be provided by company. The Software, Site, and related services are referred to as the "Service."
  </p>
  <h4>
    Gathering of Non-Personally-Identifying
    <br>
    Information
  </h4>
  <p>Company collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Company's purpose in collecting non-personally identifying information is to better understand how Company's visitors use its Service. From time to time, Company may share non-personally-identifying information with others in the aggregate, e.g., reports on trends in the usage of its Service, but this information does not identify any individual users.</p>
  <p>Company also collects potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on Pose posts. Company only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.</p>
  <h4>
    Gathering of Personally-Identifying
    <br>
    Information
  </h4>
  <p>You may choose to interact with Company or the Service in ways that require Company to gather personally-identifying information. The amount and type of information that Company gathers depends on the nature of the interaction. For example, we ask to input your Twitter information to be able to share information via your Twitter account. In each case, Company collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor's interaction with Company or the Service. Company does not disclose personally-identifying information other than as described below. And visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain Service-related activities.</p>
  <h4>Aggregated Statistics</h4>
  <p>Company may collect statistics about the behavior of visitors to its Service. Company may display this information publicly or provide it to others. However, Company does not disclose personally-identifying information other than as described below.</p>
  <h4>
    Protection of Certain Personally-Identifying
    <br>
    Information
  </h4>
  <p>Company discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Company's behalf or to provide services available at via the Service, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of your home country; by using the Service, you consent to the transfer of such information to them. Company will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Company discloses potentially personally-identifying and personally-identifying information only when Company believes it is required to do so by law, or when Company believes in good faith that disclosure is reasonably necessary to protect the property or rights of Company, third parties or the public at large. If you are a registered user of the Service and have supplied your email address, Company may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with the news of the Company and the Service. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Company recognizes the importance of safeguarding the confidentiality of your information. Accordingly, we employ reasonable measures designed to protect your information from unauthorized access, use, or disclosure. However, no data transmission over the Internet or other network can be guaranteed to be 100% secure. As a result, while we strive to protect information transmitted on or through the Service, we cannot and do not guarantee the security of any information you transmit on or through the Service and you do so at your own risk.</p>
  <p>From time to time, we may establish a business relationship with other businesses whom we believe trustworthy and who have confirmed that their privacy practices are consistent with ours ("Service Providers"). For example, we may contract with Service Providers to provide certain services, such as hosting and maintenance, data storage and management, and marketing and promotions. We only provide our Service Providers with the information necessary for them to perform these services on our behalf. Each Service Provider must agree to use reasonable security procedures and practices, appropriate to the nature of the information involved, in order to protect your information from unauthorized access, use, or disclosure. Service Providers are prohibited from using such information other than as specified by Company.</p>
  <h4>Cookies</h4>
  <p>A cookie is a string of information that a web site stores on a visitor's computer, and that the visitor's browser provides to the web site each time the visitor returns. Company uses cookies to help Company identify and track visitors, their usage of the Service, and their Service access preferences. Company visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using the Service, with the drawback that certain features of the Service may not function properly without the aid of cookies.</p>
  <h4>Ads</h4>
  <p>Ads appearing on any of our Service may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Company and does not cover the use of cookies by any advertisers.</p>
  <h4>Links To Other Web Sites</h4>
  <p>The Service may contain links to other Web sites, or allow others to send you such links. A link to a third party's Web site does not mean that we endorse it or that we are affiliated with it. We do not exercise control over third-party Web sites. You access such third-party Web sites or content at your own risk. You should always read the privacy policy of a third-party Web site before providing any information to the Web site.</p>
  <h4>Children's Privacy</h4>
  <p>We do not knowingly collect personally identifiable information from children under the age of 13. If we become aware that we have inadvertently received personally identifiable information from a child under the age of 13, we will delete such information from our records. In addition, we recommend that minors 13 years of age or older ask their parents for permission before sending any information about themselves to anyone over the Internet.</p>
  <h4>Privacy Policy Changes</h4>
  <p>Company may change its Privacy Policy from time to time, and in Company's sole discretion. Company encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of the Service after any change in this Privacy Policy will constitute your acceptance of such change.</p>
</div>
CONTENT

info_pages = [
    { name: 'alex', permalink: 'info/alex', title: 'alex', content: alex, keywords: '', description: '' },
    { name: 'anvarshakirov', permalink: 'info/anvarshakirov', title: 'anvarshakirov', content: anvarshakirov, keywords: '', description: '' },
    { name: 'anvartaishev', permalink: 'info/anvartaishev', title: 'anvartaishev', content: anvartaishev, keywords: '', description: '' },
    { name: 'artemchekhlov', permalink: 'info/artemchekhlov', title: 'artemchekhlov', content: artemchekhlov, keywords: '', description: '' },
    { name: 'elvina', permalink: 'info/elvina', title: 'elvina', content: elvina, keywords: '', description: '' },
    { name: 'gunaydonmez', permalink: 'info/gunaydonmez', title: 'gunaydonmez', content: gunaydonmez, keywords: '', description: '' },
    { name: 'jonathanlenoch', permalink: 'info/jonathanlenoch', title: 'jonathanlenoch', content: jonathanlenoch, keywords: '', description: '' },
    { name: 'julerizzardo', permalink: 'info/julerizzardo', title: 'julerizzardo', content: julerizzardo, keywords: '', description: '' },
    { name: 'lynnebarker', permalink: 'info/lynnebarker', title: 'lynnebarker', content: lynnebarker, keywords: '', description: '' },
    { name: 'mahmudkaraev', permalink: 'info/mahmudkaraev', title: 'mahmudkaraev', content: mahmudkaraev, keywords: '', description: '' },
    { name: 'maratibragimov', permalink: 'info/maratibragimov', title: 'maratibragimov', content: maratibragimov, keywords: '', description: '' },
    { name: 'michael', permalink: 'info/michael', title: 'michael', content: michael, keywords: '', description: '' },
    { name: 'nodirmirsaid', permalink: 'info/nodirmirsaid', title: 'nodirmirsaid', content: nodirmirsaid, keywords: '', description: '' },
    { name: 'rustamkhamraev', permalink: 'info/rustamkhamraev', title: 'rustamkhamraev', content: rustamkhamraev, keywords: '', description: '' },
    { name: 'sanjarbabadjanov', permalink: 'info/sanjarbabadjanov', title: 'sanjarbabadjanov', content: sanjarbabadjanov, keywords: '', description: '' },
    { name: 'shanebarker', permalink: 'info/shanebarker', title: 'shanebarker', content: shanebarker, keywords: '', description: '' },
    { name: 'timothymunden', permalink: 'info/timothymunden', title: 'timothymunden', content: timothymunden, keywords: '', description: '' },
    { name: 'volodyapinchuk', permalink: 'info/volodyapinchuk', title: 'volodyapinchuk', content: volodyapinchuk, keywords: '', description: '' },
    { name: 'yaroslav', permalink: 'info/yaroslav', title: 'yaroslav', content: yaroslav, keywords: '', description: '' }
]

pages = [
    { name: 'about', permalink: 'about', title: 'about', content: about, keywords: '', description: '' },
    { name: 'our_story', permalink: 'our_story', title: 'our_story', content: our_story, keywords: '', description: '' },
    { name: 'partnership', permalink: 'partnership', title: 'partnership', content: partnership, keywords: '', description: '' },
    { name: 'business_proposal', permalink: 'business_proposal', title: 'business_proposal', content: business_proposal, keywords: '', description: '' },
    { name: 'videos', permalink: 'videos', title: 'videos', content: videos, keywords: '', description: '' },
    { name: 'contact_us', permalink: 'contact_us', title: 'contact_us', content: contact_us, keywords: '', description: '' },
    { name: 'guidlines', permalink: 'guidlines', title: 'guidlines', content: guidlines, keywords: '', description: '' },
    { name: 'tos', permalink: 'tos', title: 'tos', content: tos, keywords: '', description: '' },
    { name: 'privacy', permalink: 'privacy', title: 'privacy', content: privacy, keywords: '', description: '' }
]

info_pages.each { |page| Page.create!(page) }
pages.each { |page| Page.create!(page) }

puts "Pages created."