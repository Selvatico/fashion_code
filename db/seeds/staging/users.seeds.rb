after "staging:fashion_styles" do
  file_path = File.expand_path '../../../', __FILE__

  puts 'Creating users...'
  styles = FashionStyle.pluck(:id)
  for i in 1..30
    user = User.new(fullname: "User#{i}", username: "user#{i}", email: "user#{i}@modera.co", website: "site_user#{i}.co", about: "About User#{i}.", phone: "#{i}#{i}#{i}#{i}#{i}#{i}#{i}", sex: i%2, birth: DateTime.now - i.years)
    user.fashion_style_id = styles[i%5]
    user.password = 'password'
    user.password_confirmation = 'password'
    user.avatar = File.open("#{file_path}/images/avatar.jpg", 'r')
    user.save
  end
  puts 'Created users.'
  users = User.all
  puts 'Following and free users and users photos...'

  users.each do |u|
    users.select{|f| u.id!=f.id}.each do |follower|
      u.follow!(follower)
    end
  end
  # for i in 31..40
  #   user = User.new(fullname: "User#{i}", username: "user#{i}", email: "user#{i}@modera.co", website: "site_user#{i}.co", about: "About User#{i}.", phone: "#{i}#{i}#{i}#{i}#{i}#{i}#{i}", sex: i%2, birth: DateTime.now - i.years)
  #   user.fashion_style_id = styles[i%5]
  #   user.password = 'password'
  #   user.password_confirmation = 'password'
  #   user.avatar = File.open("#{file_path}/images/avatar.jpg", 'r')
  #   user.save
  # end
  
  #puts '!!! Adding Photos is commented because of Error connecting to Redis !!!'
  users.each do |u|
   for t in 1..3
     for i in 1..5
       photo = u.photos.new(image: File.open("#{file_path}/images/photo#{i}.jpg", 'r'))
       photo.tag_list = "beautiful, wonderful"
       c = photo.comments.new(comment: 'Comment1')
       c.user_id = u.id
       c = photo.comments.new(comment: 'Comment2')
       c.user_id = u.id
       c = photo.comments.new(comment: 'Comment3')
       c.user_id = u.id
     end
   end
   u.save
  end
end