namespace :capi do
  desc "Make symlink for database yaml"
  task :symlink_db do
    run "ln -nfs #{release_path}/config/database.yml.example #{release_path}/config/database.yml"
  end

  desc "Make symlink for ConfigModel.js"
  task :symlink_config_model do
    run "ln -nfs #{release_path}/config/ConfigModel.js.template #{release_path}/app/assets/javascripts/models/ConfigModel.js"
  end

  desc 'invoke rake task. Example: cap capi:task_invoke TASK="db:seed"'
  task :task_invoke  do
    run "cd #{deploy_to}/current && bundle exec rake #{ENV['TASK']} RAILS_ENV=#{rails_env}"
  end
end
