#set :faye_pid, "#{deploy_to}/shared/pids/faye.pid"
#set :faye_config, "#{deploy_to}/current/private_pub.ru"
namespace :faye do
	desc "Start Faye"
	task :start do
		run "cd #{deploy_to}/current && bundle exec rackup #{deploy_to}/current/private_pub.ru -s thin -E staging -D --pid #{deploy_to}/shared/pids/faye.pid"
	end
	desc "Stop Faye"
	task :stop do
		run "if [ -f #{deploy_to}/shared/pids/faye.pid ];then kill `cat #{deploy_to}/shared/pids/faye.pid `;fi || true"
	end
	desc "Restart Faye"
	task :restart do
		faye.stop
		faye.start
	end
end
