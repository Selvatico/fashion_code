namespace :uploads do
  desc "Symlimk uploads"
  task :symlink, roles: :app do
    run "ln -s #{deploy_to}/uploads #{current_path}/public"
  end
end
