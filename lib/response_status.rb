class ResponseStatus
  attr_accessor :code, :message
  def initialize code = 200, message = "OK"
    self.code = code
    self.message = message
  end
end
