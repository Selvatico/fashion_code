module SocialNetworks
  class Provider
    # params - is hash which contains these fields:
    # api_key - shared api key or application id
    # api_key_secret - secret api key
    # token - user's oauth token
    # token_secret - user's oauth secret token
    def initialize params
      @provider = "SocialNetworks::Providers::#{params[:name].camelize}".constantize.new params
    end

    def user_info params = nil
      @provider.user_info(params) if @provider.respond_to?(:user_info)
    end

    def share params = nil
      @provider.share(params) if @provider.respond_to?(:share)
    end

    def friends params = nil
      @provider.friends(params) if @provider.respond_to?(:friends)
    end

    def invite_friends params = nil
      @provider.invite_friends(params) if @provider.respond_to?(:invite_friends)
    end

    def photos params = nil
      @provider.photos(params) if @provider.respond_to?(:photos)
    end

    def like params = nil
      @provider.like(params) if @provider.respond_to?(:like)
    end

    def unlike params = nil
    end

    def share_url params = nil
      @provider.share_url(params) if @provider.respond_to?(:share_url)
    end
  end
end