module SocialNetworks
  module Providers
    class Facebook < Base
      def initialize params
        @client = Koala::Facebook::API.new(params[:token])
        @app_id = params[:api_key]
        @app_token = params[:api_key_secret]
      end

      def user_info params
        #graph = Koala::Facebook::API.new
        user_info = @client.get_object(params[:user_id] || 'me')
        user = SocialNetworks::User.new(user_info['id'], user_info['name'],
                                                user_info['username'], self.avatar({user_id: user_info['id']}))
      end

      def friends params = nil
        friends = []
        @client.get_connections('me', 'friends').each do |friend|
          friends << SocialNetworks::User.new(friend['id'], friend['name'], friend['username'],
                                                      avatar({user_id: friend['id'], type: 'square'}))
        end
        friends
      end

      def invite_friends params
        title = CGI.escape(params[:title])
        message = CGI.escape(params[:message])
        redirect_url = CGI.escape(params[:redirect_url])
        "http://www.facebook.com/dialog/apprequests?app_id=#{@app_id}&redirect_uri=#{redirect_url}&to=#{params[:to]}&new_style_message=true&title=#{title}&message=#{message}"
      end

      def like params
        send_action 'og.likes', :object => params[:url]
      end

      def unlike params
        response = @client.get_connections('me', 'og.likes', :object => params[:url])
        @client.delete_object(response.first["id"])
      end

      def photos params = nil
        urls = []
        @client.get_connections('me', 'albums').each { |album|
          @client.get_connections(album['id'], 'photos').each { |photo| urls << photo['source'] }
        }
        urls
      end

      def avatar params
        id = params[:user_id] || user_id
        type = params[:type] || 'large'
        "http://graph.facebook.com/#{id}/picture?type=#{type}"
      end

      # def send_notification params
      #   @app_api.put_connections(params[:user_id], 'notifications', params)
      # end

      def share params
        # description = params[:share_url].nil? ? "" : "#{params[:own] ? Settings.social_texts.facebook.own_photo : Settings.social_texts.facebook.others_photo} #{params[:share_url]}"
        # if Rails.env.development?
        #   @client.put_picture("#{Rails.root}/public#{params[:image_path]}", {message: description})
        # else
        #   @client.put_picture(params[:image_path], {message: description})
        # end
        message = "#{params[:share_url]} #{params[:text]}"
        if Rails.env.development?
          @client.put_picture("#{Rails.root}/public#{params[:image_path]}", {message: message})
        else
          @client.put_picture(params[:image_path], {message: message})
        end
      end

      def share_url params
        title = CGI.escape(params[:title])
        url = CGI.escape(params[:url])
        description = CGI.escape(params[:description])
        image = CGI.escape(params[:image])
        "http://www.facebook.com/sharer.php?s=100&p[title]=#{title}&p[summary]=#{description}&p[url]=#{url}&p[images][0]=#{image}"
      end

      protected
      def user_id
        @client.get_connections('me', '')['id'] rescue nil
      end

      def send_action action, args, options = {}
        @client.put_connections('me', action, args, options)
      end
    end
  end
end
