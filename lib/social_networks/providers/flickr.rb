module SocialNetworks
  module Providers
    class Flickr < Base
      def initialize params
        FlickRaw.api_key = params[:api_key]
        FlickRaw.shared_secret = params[:api_key_secret]
        FlickRaw.secure = true
        flickr.access_token = params[:token]
        flickr.access_secret = params[:token_secret]
      end

      def photos params
        urls = []
        flickr.photosets.getList(user_id: params[:user_id]).each do |photoset|
          flickr.photosets.getPhotos(photoset_id: photoset.id).photo.each do |photo|
            info = flickr.photos.getInfo(photo_id: photo.id)
            urls << FlickRaw.url(info)
          end
        end
        urls
      end

      def share params
        title = "The Modera"
        message = "#{params[:share_url]} #{params[:text]}"
        if Rails.env.development?
          flickr.upload_photo("#{Rails.root}/public#{params[:image_path]}", description: description, title: 'Modera')
        else
          flickr.upload_photo(params[:image_path], description: description, title: title)
        end

        # description = params[:share_url].nil? ? "" : "#{params[:own] ? Settings.social_texts.twitter.own_photo : Settings.social_texts.twitter.others_photo} #{params[:share_url]}"
        # if Rails.env.development?
        #   flickr.upload_photo("#{Rails.root}/public#{params[:image_path]}", description: description, title: 'Modera')
        # else
        #   flickr.upload_photo(params[:image_path], description: description, title: 'Modera')
        # end
      end
    end
  end
end
