module SocialNetworks
  module Providers
    class Instagram < Base
      def initialize params
        ::Instagram.client_id = params[:api_key]
        ::Instagram.client_secret = params[:api_key_secret]
        @client = ::Instagram.client(:access_token => params[:token])
        @token = params[:token]
        @token_secret = params[:token_secret]
      end

      def user_info params
        user = SocialNetworks::User.new(id: @client.user.id, name: @client.user.full_name,
                                                username: @client.user.username, avatar: self.avatar({user_id: @client.user.id}))
      end

      def friends params = nil
        friends = []
        @client.user_follows.each do |f|
          friends << SocialNetworks::User.new()
        end
        friends
      end

      def photos params
        urls = []
        user_id ||= @id
        #client = Instagram.client(:access_token => @access_token)
        @client.user_recent_media.each do |media_item|
          urls << media_item.images.standard_resolution.url
        end
        urls
      end

      def avatar params
        @client.user(params[:user_id]).profile_picture
      end
    end
  end
end
