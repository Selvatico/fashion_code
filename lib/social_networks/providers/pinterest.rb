module SocialNetworks
  module Providers
    class Pinterest
      def initialize params = nil
        
      end
      
      def self.form_pinterest_link(share_url, media_url, description)
        pin_url = "http://pinterest.com/pin/create/button/?url="
        pin_url << CGI.escape(share_url)
        pin_url << "&media="
        pin_url << CGI.escape(media_url)
        pin_url << "&description="
        pin_url << CGI.escape(description)
        pin_url
      end

      def share_url params
        pin_url = "http://pinterest.com/pin/create/button/?url="
        pin_url << CGI.escape(params[:url])
        pin_url << "&media="
        pin_url << CGI.escape(params[:image])
        pin_url << "&description="
        pin_url << CGI.escape(params[:description])
        pin_url
      end

    end
  end
end
