module SocialNetworks
  module Providers
    class Tumblr < Base
      def initialize params
        @client = ::Tumblr::Client.new({
                                           consumer_key: params[:api_key],
                                           consumer_secret: params[:api_key_secret],
                                           oauth_token: params[:token],
                                           oauth_token_secret: params[:token_secret]
                                       })
      end

      def share params
        title = "The Modera"
        message = "#{params[:share_url]} #{params[:text]}"
        if Rails.env.development?
          @client.photo("#{params[:uid]}.tumblr.com", {data: ["#{Rails.root}/public#{params[:image_path]}"], caption: message})
        else
          io = open(params[:image_path])
          def io.original_filename;
            File.basename(base_uri.path);
          end
          return unless File.exists?(io)
          @client.photo("#{params[:uid]}.tumblr.com", {data_raw: [io.read], caption: message})
        end
        # description = params[:share_url].nil? ? "" : "#{params[:own] ? Settings.social_texts.twitter.own_photo : Settings.social_texts.twitter.others_photo} #{params[:share_url]}"
        # if Rails.env.development?
        #   @client.photo("#{params[:uid]}.tumblr.com", {data: ["#{Rails.root}/public#{params[:image_path]}"], caption: description})
        # else
        #   io = open(params[:image_path])
        #   def io.original_filename;
        #     File.basename(base_uri.path);
        #   end
        #   return unless File.exists?(io)
        #   @client.photo("#{params[:uid]}.tumblr.com", {data_raw: [io.read], caption: description})
        # end
      end

      def friends params
        tumblr_client.followers(params[:user_id])
      end

      def photos params
        urls = []
        posts = @client.posts(params[:user_id])['posts']
        posts.each do |post|
          post['photos'].each { |photo| urls << photo['alt_sizes'].first['url'] }
        end
        urls
      end

      def avatar params
        size = params[:type] || 64
        @client.avatar(params[:user_id], params[:type])
      end

      def share_url params
        title = CGI.escape(params[:title])
        url = CGI.escape(params[:url])
        description = CGI.escape(params[:description])
        image = CGI.escape(params[:image])
        "http://www.tumblr.com/share/photo?source=#{image}&caption=#{description}&click_thru=#{url}"
      end
    end
  end
end
