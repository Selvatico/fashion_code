module SocialNetworks
  module Providers
    class Twitter < Base
      def initialize params
        @client = ::Twitter::Client.new(
            consumer_key: params[:api_key],
            consumer_secret: params[:api_key_secret],
            oauth_token: params[:token],
            oauth_token_secret: params[:token_secret]
        )
      end

      def user_info params
        #info = params[:user_id].present? ? @client.user(params[:user_id]) : @client.user
        user = SocialNetworks::User.new(@client.user.id, @client.user.name,
                                                @client.user.screen_name, self.avatar)
      end

      def share params
        message = "#{params[:share_url]} #{params[:text]}"[0..139]
        if Rails.env.development?
          return unless File.exists?("#{Rails.root}/public#{params[:image_path]}")
          @client.update_with_media(message, File.open("#{Rails.root}/public#{params[:image_path]}")) rescue nil
        else
          io = open(params[:image_path])
          def io.original_filename;
            File.basename(base_uri.path);
          end
          return unless File.exists?(io)
          @client.update_with_media(message, io) rescue nil
        end
      end

      def avatar params
        "http://api.twitter.com/1/users/profile_image?screen_name=#{@client.user.screen_name}&size=original"
        #user_info(params).profile_image_url
      end

      def friends params
        friends = []
        @client.friend_ids(format: :json).each do |user_id|
          user = user_info({user_id: user_id})
          friends << SocialNetworks::User.new(user.id, user.name, user.screen_name,
                                                      avatar({user_id: user.id}))
        end
        friends
      end
      
      def share_url params
        title = CGI.escape(params[:title])
        url = CGI.escape(params[:url])
        description = CGI.escape(params[:description])
        image = CGI.escape(params[:image])
        "https://twitter.com/intent/tweet?text=#{description}&url=#{url}&via=TheModera&related=TheModera"
      end

    end
  end
end
