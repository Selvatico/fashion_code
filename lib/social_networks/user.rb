module SocialNetworks
  class User
    attr_accessor :id, :fullname, :username, :avatar, :in_modera
    def initialize id, fullname = nil, username = nil, avatar = nil
      @id = id
      @fullname = fullname
      @username = username
      @avatar = avatar
      @in_modera = false
    end
  end
end
