require 'csv'

namespace :bots do
  task :import => :environment do
    errors=[]
    file_path = File.expand_path '../../../../bots/', __FILE__
    CSV.foreach("#{file_path}/bots.csv", {headers: true, col_sep: ','}) do |row| 
      #puts row.inspect

      bot = User.new(:fullname=>"#{row['fullname']}")
      bot.username = row['username']
      bot.email = "#{row['username']}@bot.com"
      bot.birth = Date.today
      bot.sex = row['sex'] == 'male'
      bot.fashion_style = FashionStyle.find_by_name(row['fashion_style'].capitalize)
      bot.password = 'password'
      bot.bot = true
      bot.about=row['about']
      bot.website = row['website']
      if File.exist?("#{file_path}/photos/#{row['avatar'].gsub('\\','/')}")
        bot.avatar = File.open("#{file_path}/photos/#{row['avatar'].gsub('\\','/')}", 'r')
        if !bot.save
          bot.errors.full_messages.each do |msg|
            puts "-----------#{msg}"
          end
        end
        
        for i in 1..8
          if !row["photo#{i}"].blank? && !row["photo#{i}"].empty?
            if File.exist?("#{file_path}/photos/#{row["photo#{i}"].gsub('\\','/')}")
              photo = bot.photos.new(image: File.open("#{file_path}/photos/#{row["photo#{i}"].gsub('\\','/')}", 'r'))
              photo.tag_list = row["photo#{i}_tags"]
              puts "photo saved===#{photo.save}"
            else
              errors.push("#{file_path}/images/#{row["photo#{i}"].gsub('\\','/')}")
            end
          end
        end
      else
        errors.push("#{file_path}/images/#{row['avatar'].gsub('\\','/')}")
      end
    end
    puts errors.inspect
  end
end
