require "resque/tasks"
require 'resque_scheduler/tasks'
require 'resque_scheduler/server'

namespace :resque do  
  task :setup => :environment do
    Resque.before_fork = Proc.new { ActiveRecord::Base.establish_connection }
    Resque.schedule = YAML.load_file("#{Rails.root}/config/schedule.yml")
  end
end