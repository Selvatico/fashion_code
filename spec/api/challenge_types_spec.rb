require 'spec_helper.rb'

describe Api::V1::ChallengeTypesController, type: :controller do
  before(:each) do
    @challenge_type = FactoryGirl.create(:challenge_type)
    @user = FactoryGirl.create(:user, fashion_style: @challenge_type.fashion_style)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  describe '#index' do
    it "should return challenge types for the user" do
      get api_v1_challenge_types_path(), nil, @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['challenge_types'].count.should be > 0)
    end
  end
end