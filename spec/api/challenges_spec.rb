require 'spec_helper.rb'

describe Api::V1::ChallengesController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user_with_photos)
    @challenge_type = FactoryGirl.create(:challenge_type)
    @user2 = FactoryGirl.create(:user, username: "user2", email: 'user2@gmail.com')
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  let(:params){ {page:1, per_page: 5}}

  describe '#index' do
    before(:each) do
      @challenge = FactoryGirl.build(:challenge)
      @challenge.user = @user
      @challenge.photo = @user.photos.last
      @challenge.save

      @vote = FactoryGirl.build(:vote)
      @vote.voter = @user2
      @vote.voted_photo = @user.photos.last
      @vote.unvoted_photo = @user.photos.first
      @vote.challenge = @challenge
      @vote.save
    end
    it "should show user's versuses" do
      get api_v1_challenges_path(), params, @token
      (JSON.parse(response.body)['status']['code'].should == 200) &&
        (JSON.parse(response.body)['versuses'].count.should be > 0)
    end
  end

  describe '#create' do
      let(:valid_params) do
       params.merge({
                            challenge_type_id: @challenge_type.id,
                            photo_id: @user.photos.last.id,
                            level: "easy"
                          })
      end
      let(:invalid_params) do
        params.merge({
                            challenge_type_id: @challenge_type.id+200,
                            photo_id: @user.photos.last.id+100,
                            level: "easy"
                          })

    end
    it "should create a new challenge" do
      expect do
        post api_v1_challenges_path(), valid_params, @token
        (JSON.parse(response.body)['status']['code'].should == 200)
      end.to change(Challenge, :count)
    end

    it "should return 400 if wrong parameters" do
      expect do
        post api_v1_challenges_path(), invalid_params, @token
        (JSON.parse(response.body)['status']['code'].should == 400)
      end.to_not change(Challenge, :count)
    end
  end
end