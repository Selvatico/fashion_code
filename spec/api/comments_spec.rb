require 'spec_helper.rb'

describe Api::V1::CommentsController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @user2 = FactoryGirl.create(:user, username: "user2", email: 'user2@gmail.com')
    @photo = FactoryGirl.create(:photo, user: @user)
    @comment = FactoryGirl.create(:comment, user: @user2, photo: @photo)
    @comment2 = FactoryGirl.create(:comment, user: @user, photo: @photo)
    @user.ensure_authentication_token!

    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  let(:params){ {page:1, per_page: 5}}

  describe '#index' do
    it "should return photos of the user" do
      get api_v1_photo_comments_path(@photo), params, @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['comments_count'].should be > 0)
    end
  end

  describe '#show' do
    it "should show comment" do
      get api_v1_comment_path(@comment2), nil, @token
      JSON.parse(response.body)['status']['code'].should == 200
    end
    it "should not return 404 if comment does not exist" do
      get api_v1_comment_path(1000000), nil, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
  end

  describe '#create' do
    it "should create a comment" do
        post api_v1_photo_comments_path(@photo), {comment: 'test comment'}, @token
        JSON.parse(response.body)['status']['code'].should == 200
    end
  end

  describe '#destroy' do
    it "should remove own comment of the user" do
      expect do
        delete api_v1_comment_path(@comment2), nil, @token
      end.to change(Comment, :count)
    end
    it "should not remove other users comment" do
      expect do
        delete api_v1_comment_path(@comment), nil, @token
      end.to_not change(Comment, :count)
    end
  end

  describe '#inappropriate' do
    it "should not mark own photo as inappropiate" do
      expect do
        post inappropriate_api_v1_comment_path(@comment2), nil, @token
      end.to_not change(Inappropriate, :count)
    end
    it "should mark photo as inappropiate" do
      expect do
        post inappropriate_api_v1_comment_path(@comment), nil, @token
      end.to change(Inappropriate, :count)
    end
  end
end