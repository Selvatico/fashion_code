require 'spec_helper.rb'

describe Api::V1::ContestsController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @contest = FactoryGirl.create(:contest_with_prizes)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  let(:params){ {page:1, per_page: 5}}

  describe '#index' do
    it "should return actvie contest" do
      get api_v1_contests_path, nil, @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['contests'].count.should be > 0)
    end
  end
  describe '#join' do
    it "should add user to the contest" do
      expect do
        post join_api_v1_contest_path(@contest), nil, @token
      end.to change(Participant, :count)
    end

    it "should not add user to the contest if already registered" do
      participant = Participant.new
      participant.user = @user
      participant.contest = @contest
      participant.save
      expect do
        post join_api_v1_contest_path(@contest), nil, @token
      end.to_not change(Participant, :count)
    end
  end



  describe '#leave' do
    it "should remove user from the contest" do
      participant = Participant.new
      participant.user = @user
      participant.contest = @contest
      participant.save

      expect do
        post leave_api_v1_contest_path(@contest), nil, @token
      end.to change(Participant, :count)
    end
    it "should say error if you are not taking part in the contest" do
      post leave_api_v1_contest_path(@contest), nil, @token
      (JSON.parse(response.body)['status']['code'].should == 500)
    end
  end

  describe '#inappropriate' do
    it "should mark contest as inappropiate" do
      expect do
        post inappropriate_api_v1_contest_path(@contest), nil, @token
      end.to change(Inappropriate, :count)
    end
  end
end