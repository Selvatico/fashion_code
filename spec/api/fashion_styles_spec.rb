require 'spec_helper.rb'

describe Api::V1::FashionStylesController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user, fashion_style: nil)
    @fashion_style = FactoryGirl.create(:fashion_style)
    @fashion_style2 = FactoryGirl.create(:fashion_style)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  let(:params){ {page:1, per_page: 5}}

  describe '#index' do
    it "should return all fashion_styles" do
      get api_v1_fashion_styles_path(), nil, @token
      body = JSON.parse(response.body)
      body['status']['code'].should == 200
      body['fashion_styles'].count.should be > 0
    end
  end

  describe '#choose' do
    it "should allow user to choose fashion style" do
      post choose_api_v1_fashion_style_path(@fashion_style), nil, @token
      @user.reload
      @user.fashion_style.should == @fashion_style
    end

    it "should not allow user to change fashion style" do
      @user.fashion_style = @fashion_style
      @user.save
      post choose_api_v1_fashion_style_path(@fashion_style2), nil, @token
      @user.fashion_style.should_not eq(@fashion_style2)
      body = JSON.parse(response.body)
      body['status']['code'].should == 403
      body['status']['message'].should be_present
    end
  end
end