require 'spec_helper.rb'

describe Api::V1::NotificationsController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @user2 = FactoryGirl.create(:user, username: "user2", email: 'user2@gmail.com')
    @user3 = FactoryGirl.create(:another_user)
    @user2.follow!(@user)
    @user3.follow!(@user)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  describe '#index' do
    let(:params){ {page:1, per_page: 5}}
    it "should show notifications" do
      relationships = Relationship.where(followed_id: @user.id)
      relationships.each { |relationship| Notifications::FollowingsWorker.perform(relationship.id) }

      get api_v1_notifications_path, params, @token
      body = JSON.parse(response.body)
      body['status']['code'].should == 200
      body['notifications'].count.should be > 0
    end
  end
end