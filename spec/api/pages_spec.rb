require 'spec_helper.rb'

describe Api::V1::PagesController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  describe '#index' do
    it "should return pages" do
      FactoryGirl.create(:page)
      get api_v1_pages_path(), nil, @token
      body = JSON.parse(response.body)
      body['status']['code'].should eq(200)
      body['pages'].should_not be_empty
    end

    it "should not return pages" do
      get api_v1_pages_path(), nil, @token
      body = JSON.parse(response.body)
      body['status']['code'].should eq(200)
      body['pages'].should be_empty
    end

  end
end