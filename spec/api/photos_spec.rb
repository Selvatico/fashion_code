require 'spec_helper.rb'

describe Api::V1::PhotosController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user_with_photos)
    @user2 = FactoryGirl.create(:user_with_photos, username: "user2", email: 'user2@gmail.com')
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  let(:params){ {page:1, per_page: 5}}

  describe '#index' do
    it "should return photos of the user" do
      get api_v1_profile_photos_path(@user), params, @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['photos_count'].should be > 0)
    end
  end
  describe '#destroy' do
    it "should remove photo of the user" do
      photo = @user.photos.first
      delete api_v1_photo_path(photo), nil, @token
      body = JSON.parse(response.body)
      body['status']['code'].should eq(200)
      photo.reload
      photo.deleted.should be_true
      photo.image.should be_blank
    end
  end

  describe '#inappropriate' do
    it "should not mark own photo as inappropiate" do
      photo = @user.photos.first
      expect do
        post inappropriate_api_v1_photo_path(photo), nil, @token
      end.to_not change(Inappropriate, :count)
    end
    it "should mark photo as inappropiate" do
      photo = @user2.photos.first
      expect do
        post inappropriate_api_v1_photo_path(photo), nil, @token
      end.to change(Inappropriate, :count)
    end
  end

  describe '#by_tag' do
    it 'get phtotos by tag' do
      create :photo, tag_list: "tag1, tag2, tag3"
      get 'api/v1/photos/by_tag/tag1',
              params, @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['photos'].count.should be > 0)
    end
  end
end