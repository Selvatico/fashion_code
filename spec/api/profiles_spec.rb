require 'spec_helper.rb'

describe Api::V1::ProfilesController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @user2 = FactoryGirl.create(:user, username: "user2", email: 'user2@gmail.com')
    @user3 = FactoryGirl.create(:another_user)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end

  let(:params){ {page:1, per_page: 5}}

  describe '#show' do
    it "should show user info" do
      get api_v1_profile_path(@user2.username), nil, @token
      JSON.parse(response.body)['status']['code'].should == 200
    end
    it "should return 404 if no user" do
      get api_v1_profile_path(User.last.id+1), params, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
  end
  describe '#follow' do
    it "should make current user follow the second user" do
        post follow_api_v1_profile_path(@user2.username), nil, @token
        (JSON.parse(response.body)['status']['code'].should == 200) &&
          (@user.following.should include(@user2))
    end
    it "should return 404 if no user" do
      post follow_api_v1_profile_path(User.last.id+1), nil, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
    it "should not allow follow twice" do
      @user.follow!(@user2)
      post follow_api_v1_profile_path(@user2.username), nil, @token
      (JSON.parse(response.body)['status']['code'].should == 500)
    end
  end

  describe '#unfollow' do
    it "should make current user unfollow the second user" do
      @user.follow!(@user2)
      post unfollow_api_v1_profile_path(@user2.username), nil, @token
      (JSON.parse(response.body)['status']['code'].should == 200) &&
        (@user.following.should_not include(@user2))
    end
    it "should return 404 if no user" do
      post unfollow_api_v1_profile_path(User.last.id+1), nil, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
    it "should say not following if not folloing" do
      post unfollow_api_v1_profile_path(@user2.username), nil, @token
      (JSON.parse(response.body)['status']['code'].should == 500)
    end
  end

  describe '#followers' do
    it "should list users followers" do
      @user2.follow!(@user)
      get followers_api_v1_profile_path(@user.username), nil, @token
      JSON.parse(response.body)['status']['code'].should == 200
      (JSON.parse(response.body)['followers_count'].should be > 0)
    end
    it "should return 404 if no user" do
      get followers_api_v1_profile_path(User.last.id+10), params, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
  end
  describe '#following' do
    it "should list users followers" do
      @user.follow!(@user2)
      get following_api_v1_profile_path(@user.username), nil, @token
      JSON.parse(response.body)['status']['code'].should == 200
      (JSON.parse(response.body)['users'].count.should be > 0)
    end
    it "should return 404 if no user" do
      get following_api_v1_profile_path(User.last.id+1), params, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
  end

  describe '#upload_avatar' do
    before :each do
      @file = generate :image
    end
    it "should do avatar upload" do
      post upload_avatar_api_v1_profile_path(@user.username), {:avatar => @file}, @token
      JSON.parse(response.body)['status']['code'].should == 200
    end
    it "should not allow update not own avatar" do
      post upload_avatar_api_v1_profile_path(@user2.username), {:avatar => @file}, @token
      JSON.parse(response.body)['status']['code'].should == 403
    end
  end

  describe '#upload_photo' do
    before :each do
      @file = generate :image
    end
    it "should do photo upload" do
      @user2.follow!(@user)
      @user3.follow!(@user)
        expect do
          post photos_api_v1_profile_path(@user.username), {:image => @file}, @token
          JSON.parse(response.body)['status']['code'].should == 200
        end.to change(Photo, :count)

    end
    it "should not allow upload photo" do
      expect do
        post photos_api_v1_profile_path(@user2.username), {:photo => @file}, @token
        JSON.parse(response.body)['status']['code'].should == 403
      end.to_not change(Photo, :count)
    end

  end

  describe '#follow_all' do
    it 'should follow all followers' do
      @user2.follow!(@user)
      @user3.follow!(@user)
      expect do
        post api_v1_follow_all_path(), {}, @token
          (JSON.parse(response.body)['status']['code'].should == 200) &&
          @user.following.count.should == @user.followers.count
      end.to change(Relationship, :count)
    end
  end


  describe '#unfollow_all' do
    it  'should unfollow all followings' do
      @user.follow!(@user2)
      @user.follow!(@user3)
      expect do
        post api_v1_unfollow_all_path(), {}, @token
        JSON.parse(response.body)['status']['code'].should == 200
      end.to change(Relationship, :count)
    end

    it 'should return 500 if no followers' do
      post api_v1_unfollow_all_path(), {}, @token
      JSON.parse(response.body)['status']['code'].should == 500
    end
  end
end