require 'spec_helper.rb'

describe Api::V1::SearchController, type: :controller do
  before(:each) do
    @user2 = FactoryGirl.create(:user_with_photos)
    @user = FactoryGirl.create(:another_user)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end
  let(:params){ {page:1, per_page: 5}}
  describe '#users' do
    it "should search for users" do
      get users_api_v1_search_index_path(), params.merge({q: 'user'}), @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['users'].count.should be > 0)
    end
    it "should return empty set for not existing user" do
      get users_api_v1_search_index_path(), params.merge({q: 'user111111'}), @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['users'].count.should == 0)
    end
  end
  describe '#tags' do
    it "should search by tags" do
      create(:photo, tag_list: 'won')
      get tags_api_v1_search_index_path(), params.merge({q: 'won'}), @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['tags'].count.should be > 0)
    end
    it "should return empty set on nonexistent tags" do
      get tags_api_v1_search_index_path(), params.merge({q: 'tag1000'}), @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['tags'].count.should == 0)
    end

  end
end