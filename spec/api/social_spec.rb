# info_api_v1_social GET    /v1/social/:id/info(.:format)               v1/social#info
# sign_in_api_v1_social POST   /v1/social/:id/sign_in(.:format)            v1/social#login
# facebook_friends_api_v1_social_index GET    /v1/social/facebook/friends(.:format)       v1/social#fb_friends


# NOt sure in this hole thing probably need to stub social responses.
require 'spec_helper.rb'

# TODO: rework this tests after redmine issue #39
describe Api::V1::SocialController, type: :controller, broken: true do
  before(:each) do
    @user2 = FactoryGirl.create(:user_with_photos)
    @user = FactoryGirl.create(:another_user)
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
  end
  let(:params){ {page:1, per_page: 5}}

  describe '#info' do
    let(:valid_params) do
      {oauth_token: "dsfsf432"}
    end
    let(:invalid_params) do
      {oauth_token: "dsfsf4321111"}
    end
    it "should return user info" do
      get info_api_v1_social_path('facebook'), params, @token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['user'].should_not be_nil)
    end
    it "should return 404 for unregistered provider" do
      get info_api_v1_social_path('facebook'), invalid_params, @token
      JSON.parse(response.body)['status']['code'].should == 404
    end
  end
  
  describe '#login' do
    let(:valid_params) do
      {oauth_token: "dsfsf432"}
    end
    let(:invalid_params) do
      {oauth_token: "dsfsf4321111"}
    end

    it "should login with valid params", valid: true do
      post sign_in_api_v1_social_path('facebook'), valid_params, @token
      JSON.parse(response.body)['status']['code'].should == 200
    end

    it "should not login with invalid params", invalid: true do
      post sign_in_api_v1_social_path('facebook'), invalid_params, @token
      body = JSON.parse(response.body)
      body['status']['code'].should eq(401)
      body['status']['message'].should be_present
    end
  end

  describe '#fb_friends' do
    it 'should show current users friends' do
      get facebook_friends_api_v1_social_index_path(), nil, @token
      JSON.parse(response.body)['status']['code'].should == 200
    end
  end
end