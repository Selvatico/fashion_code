require 'spec_helper.rb'

describe Api::V1::UsersController, type: :controller do
  before(:each){request.env["HTTP_ACCEPT"] = "application/json"}
  let(:valid_params){{username: "user", email: "user@gmail.com", password: "test12345"}}
  let(:invalid_params){{username: "user", email: nil, password: "test12345"}}

  describe '#create' do
    it 'should register a new user with valid params' do
      valid_params = {username: "user",email: "user@gmail.com",password: "test1234"}
      post sign_up_api_v1_users_path(:format => :json), valid_params
      JSON.parse(response.body)['status']['code'].should == 200
    end
    it 'should not register a new user with invalid params' do
      invalid_params = {username: "us",email: nil,password: "test1234"}
      post sign_up_api_v1_users_path(:format => :json), invalid_params
      JSON.parse(response.body)['status']['code'].should == 500
    end
  end
  describe do
    before(:each) do
      @user = FactoryGirl.create(:user, valid_params)
      @user.ensure_authentication_token!
      @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
    end

    describe '#login' do
      it 'should login with valid credentials' do
        post sign_in_api_v1_users_path(:format => :json), valid_params
        JSON.parse(response.body)['status']['code'].should == 200
      end

      it 'should not login with invalid credentials' do
        post sign_in_api_v1_users_path(:format => :json), invalid_params
        JSON.parse(response.body)['status']['code'].should == 401
      end
    end
    describe '#password' do
      it 'should send password reset email' do
        post password_api_v1_users_path(:format => :json), {email: @user.email}
        JSON.parse(response.body)['status']['code'].should == 200
      end
    end


    describe '#update' do
      it 'should update current user settings' do
        @params = { "settings" => {fullname: "test", website: "http://test.com", about: "test", phone: "123321", sex: 1, birth: "2013-01-15T20:10:27+05:00", photos_private: "false",
                                    store_filtered_photos: "false",
                                    store_original_photos: "false",
                                    allow_push: "true",
                                    facebook: "http://facebook.com",
                                    twitter: "http://twitter.com",
                                    instagram: "http://instagram.com",
                                    pinterest: "http://pinterest.com"}}
        
        put api_v1_users_path(), @params, @token
        @user.reload
        @user.facebook.should eq(@params["settings"][:facebook])
      end

    end
    describe '#settings' do
      it 'should return current user settings' do
        get settings_api_v1_users_path(@user), nil, @token
        (JSON.parse(response.body)['status']['code'].should == 200) &&
          (JSON.parse(response.body)['settings'].should_not be_nil) &&
          (JSON.parse(response.body)['social'].should_not be_nil)
      end
    end
  end


end
