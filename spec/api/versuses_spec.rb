require 'spec_helper.rb'

describe Api::V1::VersusesController, type: :controller do
  before(:each) do
    @user = FactoryGirl.create(:user_with_photos)
    @user2 = FactoryGirl.create(:user_with_photos, username: "user2", email: 'user2@gmail.com')
    @user.ensure_authentication_token!
    @token = {'HTTP_ACCESS_TOKEN' => @user.authentication_token, "HTTP_ACCEPT" => "application/json"}
    @challenge = FactoryGirl.build(:challenge)
    @challenge.user = @user
    @challenge.photo = @user.photos.last
    @challenge.save
    @vote = FactoryGirl.build(:vote)
    @vote.voter = @user2
    @vote.voted_photo = @user.photos.last
    @vote.unvoted_photo = @user.photos.first
    @vote.challenge = @challenge
    @vote.save
    4.times do |i|
      @vs_top_photo = FactoryGirl.create(:vs_top_photo)
      @vs_top_photo.photo = Photo.all[i-1]
      @vs_top_photo.save!
    end
    @user.follow!(@user2)
  end

  let(:params){ { count: 2 }}

  describe '#index' do
    it 'should return own versuses for current users' do
      get api_v1_profile_versuses_path(@user.username), nil, @token
      (JSON.parse(response.body)['status']['code'].should == 200) &&
        (JSON.parse(response.body)['versuses'].count.should be > 0)
    end

    it 'should return versuses for users' do
      get api_v1_profile_versuses_path(@user2.username), nil, @token
      (JSON.parse(response.body)['status']['code'].should == 200) &&
        (JSON.parse(response.body)['versuses'].count.should_not be_nil)
    end
  end

  describe '#vote' do
    let(:valid_params){ { challenge_id: @challenge.id,
                          voted_photo_id: @user2.photos.last.id,
                          unvoted_photo_id: @user2.photos.first.id} }
    let(:own_params){ { challenge_id: @challenge.id,
                          voted_photo_id: @user.photos.last.id,
                          unvoted_photo_id: @user.photos.first.id} }

    it 'should create a vote for a photo' do
      pending("Uses Challenge, don't need in the first release")
      expect { post vote_api_v1_versuses_path(), valid_params, @token }.to change(Vote, :count)
      JSON.parse(response.body)['status']['code'].should eq(200)
    end

    it 'should return 500 if cant vote e.g. invalid params' do
      expect do
        post vote_api_v1_versuses_path(),{}, @token
        JSON.parse(response.body)['status']['code'].should == 500
      end.to_not change(Vote, :count)
    end

  end


  describe '#style' do
    it 'should list versuses' do
      # get style_api_v1_versuses_path(), params ,@token
      # JSON.parse(response.body)['status']['code'].should == 200 &&
      #   (JSON.parse(response.body)['versuses'].count.should be > 0)
    end
  end

  #poorly tested
  describe '#friends' do
    it 'should list versuses' do
      get friends_api_v1_versuses_path(), params ,@token
      JSON.parse(response.body)['status']['code'].should == 200 &&
        (JSON.parse(response.body)['versuses'].count.should_not be_nil)
    end
  end
end