#require 'spec_helper'
#
#describe ChallengesController do
#  before(:each) do
#    @user = FactoryGirl.create(:user)
#    controller.stub!(:authenticate_user!).and_return(true)
#    controller.stub!(:current_user).and_return(@user)
#    controller.stub!(:style_choosen?).and_return(true)
#  end
#
#  describe '#list' do
#    render_views
#    it 'should list current user`s versuses' do
#      photos = FactoryGirl.create_list(:photo, 3, user: @user)
#      versuses = photos.map{|photo| FactoryGirl.create(:vote, voted_photo: photo)}
#      get :list, :format => :json
#      assigns(:current_user_photo_ids).should match_array photos.map &:id
#      assigns(:versuses).should match_array versuses
#      JSON.parse response.body
#    end
#  end
#
#  describe '#new' do
#    it 'should allow user to create new challenge' do
#      get :new
#      assert_template 'new'
#    end
#    it 'should not allow user to create new contest' do
#      FactoryGirl.create :challenge, user: @user
#      get :new
#      assert_redirected_to home_path
#    end
#  end
#
#  describe '#create', focus: true do
#    it 'should create new challenge by user' do
#      photo = FactoryGirl.create :photo, user: @user
#      challenge_type = FactoryGirl.create :challenge_type
#      post :create, challenge_type_id: challenge_type.id, level: 'easy', photo_id: photo.id
#      challenge = @user.challenges.first
#      challenge.challenge_type.should == challenge_type
#      challenge.photo.should == photo
#      challenge.level.should == 'easy'
#    end
#  end
#
#end
