require 'spec_helper'

describe CommentsController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@user)
    controller.stub!(:style_choosen?).and_return(true)
  end
  describe '#index' do
    it 'should show all comments ordered DESC' do
      photo = FactoryGirl.create :photo, user: @user
      last_comment = FactoryGirl.create :comment, photo: photo, user: @user
      comments = FactoryGirl.create_list :comment, 3, photo: photo, user: @user
      first_comment = FactoryGirl.create :comment, photo: photo, user: @user
      comments << last_comment
      comments << first_comment

      get :index, :format => :json, photo_id: photo

      response.should be_success
      assigns(:comments).should match_array(comments)
      json = JSON.parse(response.body)
      puts json
      json['comments'].first['id'].should == first_comment.id
      json['comments'].last['id'].should == last_comment.id
    end
  end
end