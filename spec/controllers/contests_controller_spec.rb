require 'spec_helper'

describe ContestsController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@user)
    controller.stub!(:style_choosen?).and_return(true)
    @another_user = FactoryGirl.create :user
  end

  describe 'list' do
    render_views

    it 'should be filled with started contests' do
      FactoryGirl.create :contest, finished: true
      FactoryGirl.create :contest, started_at: 1.hour.from_now, expired_at: 2.days.from_now
      contests = FactoryGirl.create_list :joined_contest, 3, user: @another_user, started_at: 1.day.ago
      get :list, type: 'started', :format => :json
      assigns(:contests).should match_array(contests)
      body = JSON.parse(response.body)
      body['contests'].should have(3).items
    end

    it 'should list new contests' do
      FactoryGirl.create :contest, started_at: 1.day.ago
      FactoryGirl.create :contest, finished: true
      FactoryGirl.create :joined_contest, user: @user, started_at: 1.day.from_now, expired_at: 2.days.from_now
      contests = FactoryGirl.create_list :joined_contest, 3, user: @another_user, started_at: 1.day.from_now, expired_at: 2.days.from_now
      get :list, type: 'new', :format => :json
      assigns(:contests).should match_array(contests)
      body = JSON.parse(response.body)
      body['contests'].should have(3).items
    end

    it 'should list user`s contests' do
      FactoryGirl.create :contest
      FactoryGirl.create :contest, finished: true
      FactoryGirl.create :contest, started_at: 1.day.from_now, expired_at: 2.days.from_now
      contests = FactoryGirl.create_list :joined_contest, 3, user: @user
      get :list, type: 'joined', :format => :json
      assigns(:contests).should match_array(contests)
      body = JSON.parse(response.body)
      body['contests'].should have(3).items
    end

    it 'should raise exception for undefined type of contest list' do
      expect { get :list, type: 'undefined' }.to raise_error
    end
  end

  describe '#index' do

    render_views

    it 'should show all contests' do
      contests = [FactoryGirl.create(:contest),
                  FactoryGirl.create(:contest),
                  FactoryGirl.create(:contest, finished: true)]
      get :index, :format => :json
      response.should be_success
      JSON.parse(response.body)
      expect(assigns(:contests)).to match_array(contests)
    end
  end

  describe '#hall_of_fame' do
    it 'should show hall_of_fame by contests' do
      contest = FactoryGirl.create(:contest)
      users_count = 3
      users = FactoryGirl.create_list :user, users_count
      users.each_with_index { |u, i| FactoryGirl.create(:participant, user: u, contest: contest, votes_score: users_count + 1 - i) }
      users << @user
      FactoryGirl.create(:participant, user: @user, contest: contest, votes_score: 0)
      get :hall_of_fame, :format => :json, contest_id: contest.id
      response.should be_success
      results = JSON.parse response.body
      #puts results
      expect(assigns(:users)).to match_array(users)
      results.first['place'].should == '1'
      results.first['username'].should == users[0].username
      results.last['place'].should == '4'
      results.last['username'].should == @user.username
    end

    it 'should show hall_of_fame by global contests' do
      get :hall_of_fame, :format => :json
      response.should be_success
      expect(assigns(:users)).to match_array([@user, @another_user])
      JSON.parse(response.body).should have(2).items
    end
  end

  describe '#user_place' do
    it 'should show user`s place in contest' do
      get :user_place, :format => :json, username: @user.username
      response.should be_success
      assigns(:user).should == @user
    end
  end

  describe '#join' do
    render_views
    it 'should join user to contest' do
      photo = FactoryGirl.create :photo, user: @user
      contest = FactoryGirl.create(:contest)
      get :join, :format => :json, id: contest.id
      response.should be_success
      @user.participants.count == 2
      @user.contests.count.should == 1
      @user.contests.first.should == contest
      assigns(:photo).should == photo
      JSON.parse response.body
    end
    it 'should not join user to contest if user have one' do
      photo = FactoryGirl.create :photo, user: @user
      contest = FactoryGirl.create(:contest)
      FactoryGirl.create(:participant, user: @user, contest: contest)
      get :join, :format => :json, id: contest.id
      response.should be_success
      @user.participants.count == 2
      @user.contests.count.should == 1
      @user.contests.first.should == contest
      assigns(:photo).should == photo
      JSON.parse response.body
    end
    it 'should not join user to contest if there are no photos' do
      contest = FactoryGirl.create(:contest)
      get :join, id: contest.id
      response.status.should == 500
    end
  end

  describe '#leave' do
    it 'should leave contest' do
      contest = FactoryGirl.create(:contest)
      FactoryGirl.create(:participant, user: @user, contest: contest)
      get :leave, id: contest.id
      response.should be_success
      @user.contests.count.should == 0
      @user.participants.count == 1
    end
    it 'should not leave contest if user has not one' do
      contest = FactoryGirl.create(:contest)
      get :leave, id: contest.id
      response.should be_success
      @user.contests.count.should == 0
      @user.participants.count == 1
    end
  end

  describe '#inappropriate' do
    it 'should report about inappropriate contest' do
      contest = FactoryGirl.create(:contest)
      post :inappropriate, id: contest.id
      response.should be_success
      post :inappropriate, id: contest.id
      response.should be_success
      contest.inappropriates.length.should == 1
    end
  end
end