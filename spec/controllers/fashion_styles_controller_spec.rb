require 'spec_helper'

describe FashionStylesController do
  render_views
  it 'should show styles list' do
    user = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(user)
    controller.stub!(:style_choosen?).and_return(true)
    styles = [FactoryGirl.create(:fashion_style),
              FactoryGirl.create(:fashion_style),
              FactoryGirl.create(:fashion_style)] << user.fashion_style
    get :index, :format => :json
    response.status.should == 200
    expect(assigns(:fashion_styles)).to match_array(styles)
    JSON.parse(response.body)
  end
end