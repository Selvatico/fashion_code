require 'spec_helper'

describe HashtagContestsController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@user)
    controller.stub!(:style_choosen?).and_return(true)
  end

  describe '#trending' do
    it 'should return top 7 HashtagContests' do
      contests = []
      10.times { contests << FactoryGirl.create(:hashtag_contest) }
      contests.slice!(7, 9)
      # Чтобы тест не зависел, от способа сортировки postgres'ом
      # эквивалентных (по выражению в части order by в sql запросе) записей
      # устанавливаем *уникальные* значения кол-ва hashtag_participant для 7-ми конкурсов (contests)
      (1..7).each do |i|
        FactoryGirl.create_list(:hashtag_participant, i, contest: contests[7 - i])
      end
      get :trending, :format => :json
      response.body.should == {tags: contests.to_json(:only => :name)}.to_json
    end
  end

end