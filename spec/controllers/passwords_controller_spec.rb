require 'spec_helper'

describe PasswordsController do
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @user = FactoryGirl.create(:user)
  end

  describe '#create' do
    it 'should reset user`s password' do
      post :create, :format => :json, user: {email: @user.email}
      response.should be_success
      User.find(@user.id).reset_password_token.should be_present
    end

    it 'should not reset user`s password' do
      post :create, :format => :json, user: {email: 'fail@mail.com'}
      response.status.should == 404
      r = JSON.parse(response.body)
      r['message'].should be_present
    end
  end
end