require 'spec_helper'

describe PhotosController do
  #render_views
  before(:each) do
    @user = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@user)
    controller.stub!(:style_choosen?).and_return(true)
    @file_path = File.expand_path '../../fixtures', __FILE__
  end

  describe '#index', focus: true do
    render_views
    it 'should show all active user`s photos' do
      photo1 = FactoryGirl.create(:photo, user: @user)
      photo2 = FactoryGirl.create(:photo, user: @user)
      FactoryGirl.create(:photo, user: @user, deleted: true)
      view = 'list'
      get :index, :format => :json, profile_id: @user.username, view: view
      JSON.parse(response.body)['photos_count'].should == 2
      expect(assigns(:photos)).to match_array([photo1, photo2])
      expect(assigns(:view)).to match(view)
    end
  end

  describe '#show' do
    it 'should show active user`s photo' do
      photo = FactoryGirl.create(:photo, user: @user)
      get :show, profile_id: @user.username, id: photo.id
      assert_template 'profiles/show'
      assigns(:shared_photo).should be_true
      assigns(:profile).should eq @user
      assigns(:photo_id).should == photo.id.to_s
    end
  end

  describe '#create' do
    it 'should add new photo to current user, photo_crop_h != photo_crop_w' do
      params = {photo_crop_h: 0, photo_crop_w: 1, photo_min_size: 2, photo_crop_default: 3, photo_image_cache: "#{@file_path}/image.jpg"}
      post :create, params
      photo = Photo.find_by_user_id(@user.id)
      photo.should_not be_nil
      @user.photos.length.should be 1
      assert_redirected_to share_photo_path(photo, caption: true)

      assigns(:photo).crop_x.should == 0
      assigns(:photo).crop_y.should == 0
      assigns(:photo).crop_h.should == params[:photo_crop_default].to_s
      assigns(:photo).crop_w.should == params[:photo_crop_default].to_s
    end

    it 'should add new photo to current user, photo_crop_h and photo_crop_w are 0' do
      params = {photo_crop_h: 0, photo_crop_w: 0, photo_min_size: 2, photo_crop_default: 3, photo_image_cache: "#{@file_path}/image.jpg"}
      post :create, params
      photo = Photo.find_by_user_id(@user.id)
      photo.should_not be_nil
      @user.photos.length.should be 1
      assert_redirected_to share_photo_path(photo, caption: true)

      assigns(:photo).crop_x.should == 0
      assigns(:photo).crop_y.should == 0
      assigns(:photo).crop_h.should == params[:photo_crop_default].to_s
      assigns(:photo).crop_w.should == params[:photo_crop_default].to_s
    end

    it 'should add new photo to current user, photo_crop_h < photo_min_size' do
      params = {photo_crop_h: 1, photo_crop_w: 1, photo_min_size: 2, photo_crop_default: 3, photo_image_cache: "#{@file_path}/image.jpg"}
      post :create, params
      photo = Photo.find_by_user_id(@user.id)
      photo.should_not be_nil
      @user.photos.length.should be 1
      assert_redirected_to share_photo_path(photo, caption: true)

      assigns(:photo).crop_x.should == 0
      assigns(:photo).crop_y.should == 0
      assigns(:photo).crop_h.should == params[:photo_crop_default].to_s
      assigns(:photo).crop_w.should == params[:photo_crop_default].to_s
    end

    it 'should add new photo to current user, others crops' do
      params = {photo_crop_h: 2, photo_crop_w: 2, photo_min_size: 2, photo_crop_default: 2, photo_image_cache: "#{@file_path}/image.jpg", photo_crop_x: 321, photo_crop_y: 123}
      post :create, params
      photo = Photo.find_by_user_id(@user.id)
      photo.should_not be_nil
      @user.photos.length.should be 1
      assert_redirected_to share_photo_path(photo, caption: true)

      assigns(:photo).crop_x.should == params[:photo_crop_x].to_s
      assigns(:photo).crop_y.should == params[:photo_crop_y].to_s
      assigns(:photo).crop_h.should == params[:photo_crop_h].to_s
      assigns(:photo).crop_w.should == params[:photo_crop_w].to_s
    end
  end

  describe '#vote' do
    before(:each) do
      @photo = FactoryGirl.create(:photo)
    end

    it 'should vote for photo' do
      post :vote, id: @photo.id
      vote = @user.votes.first
      vote.voted_photo_id.should == @photo.id
      vote.vote_score.should == 1
      vote.voted_photo_score.should == 1

      user = FactoryGirl.create(:user)
      controller.stub!(:current_user).and_return(user)
      post :vote, id: @photo.id
      @photo.voters_count.should == 2
      @photo.up_votes.map(&:vote_score).sum.should == 2
    end

    it 'should not vote for voted photo' do
      post :vote, id: @photo.id
      @user.votes.length.should == 1
      vote = @user.votes.first
      vote.voted_photo_id.should == @photo.id
      vote.vote_score.should == 1
      vote.voted_photo_score.should == 1
      post :vote, id: @photo.id
      @user.votes.length.should == 1
      vote = @user.votes.first
      vote.voted_photo_id.should == @photo.id
      vote.vote_score.should == 1
      vote.voted_photo_score.should == 1
    end
  end

  describe '#inappropriate' do
    it 'should report about inappropriate photo' do
      photo = FactoryGirl.create(:photo)
      post :inappropriate, id: photo.id
      response.status.should == 200
      post :inappropriate, id: photo.id
      response.status.should == 200
      photo.inappropriates.length.should == 1
    end
    it 'should not report about own inappropriate photo' do
      photo = FactoryGirl.create(:photo, user: @user)
      post :inappropriate, id: photo.id
      response.status.should == 401
    end
  end
  describe '#upload' do
    it 'should upload photo' do
      file = generate :image
      post :upload_photo, qqfile: file
      photo = @user.photos.first
      json = JSON.parse(response.body)
      json['success'].should be_true
      photo.image_cache.should == json['image_cache']
      photo.image_url.should == json['image_url']
    end
    it 'should not upload empty photo' do
      post :upload_photo
      json = JSON.parse(response.body)
      json['success'].should be_false
    end
  end

  describe '#by_hashtag' do
    render_views
    it 'should return photos bya tag' do
      photo1 = FactoryGirl.create :photo, tag_list: 'test1, test2'
      photo2 = FactoryGirl.create :photo, tag_list: 'test1'
      FactoryGirl.create :photo, tag_list: 'test2'
      get :by_hashtag, :format => :json, tag: 'test1'
      expect(assigns(:photos)).to match_array([photo1, photo2])
      JSON.parse(response.body)["total_photos"].should == 2
    end
  end

  describe '#voters' do
    render_views
    it 'should show all voted users', focus: true do
      photo = FactoryGirl.create :photo
      user1 = FactoryGirl.create :user
      user2 = FactoryGirl.create :user
      photo.vote_for user1
      photo.vote_for user2
      @user.stub!(:following_ids).and_return([123321,user2.id])
      get :voters, :format => :json, id: photo.id
      response.status.should == 200
      expect(assigns(:voters)).to match_array([user1, user2])
      json = JSON.parse(response.body)
      json['voters'][0]['is_following'].should be_false
      json['voters'][1]['is_following'].should be_true
    end
  end

  describe '#destroy' do
    it 'should destroy user`s photo' do
      photo = FactoryGirl.create :photo, user: @user
      delete :destroy, id: photo.id
      response.status.should == 200
      photo = Photo.find(photo.id)
      photo.deleted.should be_true
      photo.image.should be_blank
    end
    it 'should destroy user`s photo and redirect to new_photo_path' do
      photo = FactoryGirl.create :photo, user: @user
      delete :destroy, id: photo.id, redirect: true
      photo = Photo.find(photo.id)
      photo.deleted.should be_true
      photo.image.should be_blank
      assert_redirected_to new_photo_path
    end
    it 'should not delete photo of another user' do
      photo = FactoryGirl.create :photo
      delete :destroy, id: photo.id
      response.status.should == 500
      photo = Photo.find(photo.id)
      photo.deleted.should be_false
      photo.image.should be_present
    end
    it 'should not delete photo if there is active challenge' do
      photo = FactoryGirl.create :photo, user: @user
      FactoryGirl.create :challenge, user: @user, photo: photo
      delete :destroy, id: photo.id
      response.status.should == 500
      photo = Photo.find(photo.id)
      photo.deleted.should be_false
      photo.image.should be_present
    end
  end

  #describe '#load_cache' do
  #  it 'should set photo' do
  #    photo = @user.photos.create!
  #    photo.image = fixture_file_upload("#{Rails.root}/public/style.png")
  #    puts photo.image_cache.inspect
  #    put :load_cache, id: photo.id, photo: {image_cache: photo.image_cache}
  #    puts Photo.find(photo.id).inspect
  #  end
  #end

end