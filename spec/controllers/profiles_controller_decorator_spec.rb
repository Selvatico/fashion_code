require 'spec_helper'

describe ProfilesController do
  render_views

  before(:each) do
    @follower = FactoryGirl.create(:user)
    @followed = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@follower)
    controller.stub!(:style_choosen?).and_return(true)
  end

  it 'should follow user' do
    post :follow, id: @followed.username
    Relationship.find_by_follower_id(@follower.id).followed_id.should == @followed.id
  end

  it 'should unfollow user' do
    Relationship.create!({follower_id: @follower.id, followed_id: @followed.id}, without_protection: true)
    post :unfollow, id: @followed.username
    Relationship.find_by_follower_id(@follower.id).should be_nil
  end

  it 'should show all followers' do
    follower1 = FactoryGirl.create(:user)
    follower2 = FactoryGirl.create(:user)
    @follower.follow!(@followed)
    follower1.follow!(@followed)
    follower2.follow!(@followed)
    get :followers, :format => :json, id: @followed.username, page: 1
    expect(assigns(:users)).to match_array([@follower, follower1, follower2])
    JSON.parse(response.body)['count'].should == 3
  end

  it 'should show all following' do
    followed1 = FactoryGirl.create(:user)
    followed2 = FactoryGirl.create(:user)
    @follower.follow!(@followed)
    @follower.follow!(followed1)
    @follower.follow!(followed2)
    get :following, :format => :json, id: @follower.username, page: 1
    expect(assigns(:users)).to match_array([@followed, followed1, followed2])
    JSON.parse(response.body)['count'].should == 3

  end
end