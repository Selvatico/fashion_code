require 'spec_helper'

#TODO: rewrite me. please!
describe ProfilesController do
  render_views
  before(:each) do
    @user = FactoryGirl.create(:user, avatar: nil)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@user)
    controller.stub!(:style_choosen?).and_return(true)
  end
  describe '#friends' do
    it 'should show all 2 user`s friends' do
      Friend = Struct.new :id, :provider, :fullname, :avatar
      Auth = Struct.new :uid, :user_id
      friends = [Friend.new(1, 'facebook', 'facebook user', '/avatar1'),
                 Friend.new(22, 'facebook', 'facebook user', '/avatar2')]
      @user.stub!(:friends).and_return(friends)
      following = [FactoryGirl.create(:user),FactoryGirl.create(:user),
                   FactoryGirl.create(:user),FactoryGirl.create(:user)]
      auths = [Auth.new(1, following[0].id),
               Auth.new(2, following[1].id)]

      UserAuthentication.stub!(:where).and_return(auths)
      @user.stub!(:following).and_return(following)
      get :friends, id: @user.username, :format => :json
      response.status.should == 200
      json = JSON.parse(response.body)
      json['friends_count'].should == 2
      json['friends'][0]['is_follow'].should be_true
      json['friends'][1]['is_follow'].should be_false
    end

    it 'should show 0 user`s friends' do
      @user.stub!(:friends)#.and_return([FactoryGirl.create(:user),FactoryGirl.create(:user)])
      @user.stub!(:following).and_return([])
      UserAuthentication.stub!(:where).and_return([])
      get :friends, id: @user.username, :format => :json
      response.status.should == 200
      JSON.parse(response.body)['friends_count'].should == 0
    end
  end
  describe '#versuses' do
    it 'should show all versuses of current user' do
      photo1= FactoryGirl.create :photo, user: @user
      photo2= FactoryGirl.create :photo, user: @user
      photo3 = FactoryGirl.create :photo, user: @user
      user = FactoryGirl.create(:user)
      user.vote_photo photo1, FactoryGirl.create(:photo)
      user1 = FactoryGirl.create(:user)
      user1.vote_photo photo1, FactoryGirl.create(:photo)
      get :versuses, :format => :json, id: @user.username
      response.status.should == 200
      JSON.parse(response.body)
      expect(assigns(:current_user_photo_ids)).to match_array([photo1.id, photo2.id, photo3.id])
      expect(assigns(:versuses)).to match_array([user.votes.first, user1.votes.first])
    end
    it 'should show all versuses of another user' do
      user = FactoryGirl.create(:user)
      photo1= FactoryGirl.create :photo, user: user
      photo2= FactoryGirl.create :photo, user: user
      photo3 = FactoryGirl.create :photo, user: user
      @user.vote_photo photo1, FactoryGirl.create(:photo)
      user1 = FactoryGirl.create(:user)
      user1.vote_photo photo1, FactoryGirl.create(:photo)
      get :versuses, :format => :json, id: user.username
      response.status.should == 200
      JSON.parse(response.body)
      expect(assigns(:current_user_photo_ids)).to match_array([photo1.id, photo2.id, photo3.id])
      expect(assigns(:versuses)).to match_array([@user.votes.first, user1.votes.first])
    end
  end

  describe '#upload_avatar' do
    it 'should upload avatar' do
      file = generate :image
      post :upload_avatar, id: @user.username, user: { avatar: file }
      JSON.parse(response.body)['avatar'].should be
      response.status.should == 200
    end
  end


end