require 'spec_helper'

describe RegistrationsController do

  before(:each) { @request.env["devise.mapping"] = Devise.mappings[:user] }

  #describe '#new' do
  #  render_views
  #  it 'should prepare new user' do
  #    get :new, :format => :json
  #    response.status.should == 200
  #    JSON.parse response.body
  #  end
  #end

  describe '#create' do
    it 'should register a new user' do
      email = 'user@gmail.com'
      valid_params ={user:  {username: 'user', email: email, password: 'test1234'}}
      post :create, valid_params
      #puts response.body
      user = User.find_by_email(email)
      user.should_not be_nil
      user.email.should == email
      r = JSON.parse(response.body)
      r['redirect_uri'].should == '/home/'
      r['csrf_token'].should be_present
      response.status.should == 302
    end
    it 'should register a new user with provider' do
      email = 'user@gmail.com'
      fullname = 'facebook_name'
      username = 'facebook_nickname'
      session[:omniauth] = {
          'provider' => 'facebook',
          'uid' => '123',
          'credentials' => {
            'token' =>  'token',
            'secret_token' => 'secret_token'
          },
          'info' => {
              'email' => 'test@facebook.com',
              'nickname' => username,
              'name' => fullname
          }
      }
      post :create, user:  {email: email, password: 'test1234'}
      #puts response.body
      response.status.should == 302
      user = User.find_by_email(email)
      user.user_authentications.length.should == 1
      user.username.should == username
      user.fullname.should == fullname
    end

    #it 'should register a new user and redirect to choose_style' do
    #  email = "user@gmail.com"
    #  valid_params ={user:  {username: "user", email: email, password: "test1234"}}
    #  session[:origin_path] = '/origin_path'
    #  post :create, valid_params
    #  #puts response.body
    #  response.status.should == 200
    #  User.find_by_email(email).email.should == email
    #  r = JSON.parse(response.body)
    #  r["redirect_uri"].should == "/choose_style"
    #end

    #it 'should not register a new user without password_conformation' do
    #  email = "user@gmail.com"
    #  valid_params ={user:  {username: "user", email: email, password: "test1234"}}
    #  post :create, valid_params
    #  response.status.should == 500
    #  r = JSON.parse(response.body)
    #  r["password"].should == "Password doesn't match confirmation"
    #end

    it 'should not register user with the same username and email' do
      user = FactoryGirl.create(:user)
      params ={user:  {username: user.username, email: user.email, password: "test1234"}}
      post :create, params
      response.status.should == 500
      r = JSON.parse(response.body)
      r['username'].should be_present
      r['email'].should be_present
    end

    it 'should not register a new user with invalid params' do
      post :create
      response.status.should == 500
      r = JSON.parse(response.body)
      r['username'].should be_present
      r['email'].should be_present
      r['password'].should be_present
    end

    it 'should register a new user with invitation' do
      inv = FactoryGirl.create(:invitation)
      email = inv.email
      valid_params ={user:  {username: 'user', email: email, password: 'test1234'}}
      session["invitation_token"]  =  inv.invitation_token
      post :create, valid_params
      response.status.should == 302
      Invitation.find_by_invitation_token(inv.invitation_token).should be_nil
      User.find_by_email(email).invited_by.should == inv.user.id
    end
  end

  describe '#change_password' do
    it 'should change password' do
      user = FactoryGirl.create :user
      sign_in user
      params = {user: {current_password: user.password, password: 'newpassword'}}
      put(:change_password, params)
      response.should be_success
    end
  end

  describe '#upload_avatar' do
    it 'should upload avatar' do
      image = generate :image
      post :upload_avatar, file: image
      response.should be_success
      #puts response.body
      json = JSON.parse response.body
      json['avatar_cache'].should be_present
      json['avatar'].should be_present
    end
  end
end