require 'spec_helper'

describe SessionsController do
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @user = FactoryGirl.create(:user)
  end

  describe '#create' do
    it "should signin user by email" do
      post :create, :format => :json, user: {login: @user.email, password: @user.password}
      subject.current_user.id.should == @user.id
      expect(response).to be_success
      JSON.parse(response.body)['redirect_uri'].should be_present
    end

    it "should signin user by username" do
      post :create, :format => :json, user: {login: @user.username, password: @user.password}
      subject.current_user.id.should == @user.id
      #flash[:notice].should be_present
      expect(response).to be_success
      JSON.parse(response.body)['redirect_uri'].should be_present
    end

    it "should redirect user to /oreign_path" do
      style = FactoryGirl.create(:fashion_style)
      @user.fashion_style_id = style.id
      @user.save!
      origin_path = '/origin_path'
      session[:origin_path] = origin_path
      post :create, :format => :json, user: {login: @user.email, password: @user.password}
      expect(response).to be_success
      JSON.parse(response.body)['redirect_uri'].should == origin_path
    end

    it "should not signin user" do
      post :create, :format => :json, user: {login: @user.email, password: 'some_password'}
      response.status.should == 401
      JSON.parse(response.body)['error'].should be_present
    end
  end

  describe '#create_by_provider' do
    it "should signin user by provider" do
      auth = FactoryGirl.create :user_authentication, user: @user
      new_secret_token = 'new_secret_token'
      new_token = 'new_token'
      post :create_by_provider, provider: auth.provider, uid: auth.uid, token: new_token, secret_token: new_secret_token
      response.should be_success
      subject.current_user.id.should == @user.id
      auth = UserAuthentication.find_by_provider_and_uid(auth.provider, auth.uid)
      auth.secret_token.should == new_secret_token
      auth.token.should == new_token
      JSON.parse(response.body)['redirect_uri'].should be_present
    end
    it "should not signin user by provider" do
      auth = FactoryGirl.build :user_authentication, user: @user
      post :create_by_provider, provider: auth.provider, uid: auth.uid, token: auth.token, secret_token: auth.secret_token
      response.status.should == 401
      subject.current_user.should be_nil
      JSON.parse(response.body)['error'].should be_present
    end
  end
end