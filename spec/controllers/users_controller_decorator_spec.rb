require 'spec_helper'

describe UsersController do

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    # user without fashion style
    @user = FactoryGirl.create(:user, fashion_style: nil)
    sign_in @user
  end

  describe '#choose_style' do
    it 'should choose style after new user was registered and redirect to invitation' do
      style = FactoryGirl.create(:fashion_style)
      post :choose_style, fashion_style_id: style.id
      User.find(@user.id).fashion_style_id.should == style.id
      body = JSON.parse(response.body)
      body['redirect_uri'].should match('/invitation')
    end

    it 'should choose style after new user was registered and redirect to origin_path' do
      style = FactoryGirl.create(:fashion_style)
      origin_path = '/origin_path'
      sign_in @user
      session[:origin_path] = origin_path
      post :choose_style, fashion_style_id: style.id
      User.find(@user.id).fashion_style_id.should == style.id
      JSON.parse(response.body)['redirect_uri'].should == origin_path
    end

    #it 'should shows all styles' do
    #  style1 = FactoryGirl.create(:fashion_style)
    #  style2 = FactoryGirl.create(:fashion_style)
    #  get :choose_style
    #  expect(assigns(:fashion_styles)).to match_array([style1, style2])
    #  assert_template 'modera/layouts/welcome'
    #end
  end

end