require 'spec_helper'

describe UsersController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    controller.stub!(:authenticate_user!).and_return(true)
    controller.stub!(:current_user).and_return(@user)
    controller.stub!(:style_choosen?).and_return(true)
  end

  it 'should update user' do
    new_fullname = 'new fullname'
    put :update, id: @user.username, user: { fullname: new_fullname}
    response.status.should == 200
    User.find(@user.id).fullname.should == new_fullname
  end
  it 'should update user`s birthday' do
    put :update, id: @user.username, date: {year: 1986, month: 1, day: 18}
    response.status.should == 200
    User.find(@user.id).birth.to_date.should == Date.new(1986, 1, 18)
  end
  it 'should not update username' do
    put :update, id: @user.username, user: { username: nil}
    response.status.should == 500
    JSON.parse(response.body)
  end

end