require 'spec_helper'

describe Web::Admin::ChallengeTypesController do
  before do
    @challenge_type = create :challenge_type
    @attrs = attributes_for :challenge_type
    @params = { id: @challenge_type.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create challenge_type" do
    post :create, @params.merge(challenge_type: @attrs)
    expect(response).to be_redirect
    expect(ChallengeType.find_by_easy_score(@attrs[:easy_score])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update challenge_type" do
    pending "redmine issue #19"

    put :update, @params.merge(challenge_type: @attrs)
    expect(response).to be_redirect
    expect(ChallengeType.find_by_easy_score(@attrs[:easy_score])).to be
  end

  it "destroy challenge_type" do
    delete :destroy, @params
    expect(response).to be_redirect
    expect(ChallengeType.find_by_id(@challenge_type.id)).to be_nil
  end
end
