require 'spec_helper'

describe Web::Admin::ContestsController do
  before do
    @contest = create :contest
    @attrs = attributes_for :contest
    @params = { id: @contest.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create contest" do
    post :create, @params.merge(contest: @attrs)
    expect(response).to be_redirect
    expect(Contest.find_by_name(@attrs[:name])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update contest" do
    put :update, @params.merge(contest: @attrs)
    expect(response).to be_redirect
    expect(Contest.find_by_name(@attrs[:name])).to be
  end

  it "destroy contest" do
    delete :destroy, @params
    expect(response).to be_redirect
    expect(Contest.find_by_id(@contest.id)).to be_nil
  end
end
