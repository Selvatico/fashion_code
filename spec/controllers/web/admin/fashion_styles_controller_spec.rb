require 'spec_helper'

describe Web::Admin::FashionStylesController do
  before do
    @fashion_style = create :fashion_style
    @attrs = attributes_for :fashion_style
    @params = { id: @fashion_style.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create fashion_style" do
    post :create, @params.merge(fashion_style: @attrs)
    expect(response).to be_redirect
    expect(FashionStyle.find_by_name(@attrs[:name])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update fashion_style" do
    put :update, @params.merge(fashion_style: @attrs)
    expect(response).to be_redirect
    expect(FashionStyle.find_by_name(@attrs[:name])).to be
  end

  it "destroy fashion_style" do
    delete :destroy, @params
    expect(response).to be_redirect
    expect(FashionStyle.find_by_id(@fashion_style.id)).to be_nil
  end
end
