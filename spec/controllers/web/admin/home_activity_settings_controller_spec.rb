require 'spec_helper'

describe Web::Admin::HomeActivitySettingsController do
  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get edit" do
    get :edit
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end
end
