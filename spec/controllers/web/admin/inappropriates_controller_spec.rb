require 'spec_helper'

describe Web::Admin::InappropriatesController do

  describe 'shows' do
    before do
      @inappropriate = create :inappropriate
      @params = { id: @inappropriate.id }
    end

    it "get index" do
      get :index
      expect(response).to be_success
    end

    it "get show" do
      get :show, @params
      expect(response).to be_success
    end
  end

  it "delete photo" do
    photo = create :photo
    inappropriate = create :inappropriate, inappropriateable: photo
    delete :destroy_photo, id: inappropriate.id
    expect(response).to be_redirect
  end

  it "delete comment" do
    comment = create :comment
    inappropriate = create :inappropriate, inappropriateable: comment
    delete :destroy_comment, id: inappropriate.id
    expect(response).to be_redirect
  end

end
