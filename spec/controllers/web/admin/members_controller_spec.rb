require 'spec_helper'

describe Web::Admin::MembersController do
  before do
    @member = create :member
    @attrs = attributes_for :member
    @params = { id: @member.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create member" do
    post :create, @params.merge(member: @attrs)
    expect(response).to be_redirect
    expect(Member.find_by_fullname(@attrs[:fullname])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update member" do
    put :update, @params.merge(member: @attrs)
    expect(response).to be_redirect
    expect(Member.find_by_fullname(@attrs[:fullname])).to be
  end

  #it "destroy member" do
  #  delete :destroy, @params
  #  expect(response).to be_redirect
  #  expect(Member.find_by_id(@member.id)).to be_nil
  #end
end
