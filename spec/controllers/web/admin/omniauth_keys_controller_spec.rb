require 'spec_helper'

describe Web::Admin::OmniauthKeysController do
  it "get index" do
    get :index
    expect(response).to be_success
  end

  # FIXME: edit action on collection of omniauth_keys don't exist in routes
  it "get edit" do
    get :edit, id: "Facebook"
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end
end
