require 'spec_helper'

describe Web::Admin::PagesController do
  before do
    @page = create :page
    @attrs = attributes_for :page
    @params = { id: @page.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create page" do
    post :create, @params.merge(page: @attrs)
    expect(response).to be_redirect
    expect(Page.find_by_title(@attrs[:title])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update page" do
    put :update, @params.merge(page: @attrs)
    expect(response).to be_redirect
    expect(Page.find_by_title(@attrs[:title])).to be
  end

  it "destroy page" do
    delete :destroy, @params
    expect(response).to be_redirect
    expect(Page.find_by_id(@page.id)).to be_nil
  end
end
