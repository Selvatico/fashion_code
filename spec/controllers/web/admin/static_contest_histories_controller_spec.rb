require 'spec_helper'

describe Web::Admin::StaticContestHistoriesController do
  before do
    @static_contest_history = create :static_contest_history
    @attrs = attributes_for :static_contest_history
    @params = { id: @static_contest_history.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create static_contest_history" do
    post :create, @params.merge(static_contest_history: @attrs)
    expect(response).to be_redirect
    expect(StaticContestHistory.find_by_title(@attrs[:title])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update static_contest_history" do
    put :update, @params.merge(static_contest_history: @attrs)
    expect(response).to be_redirect
    expect(StaticContestHistory.find_by_title(@attrs[:title])).to be
  end

  it "destroy static_contest_history" do
    delete :destroy, @params
    expect(response).to be_redirect
    expect(StaticContestHistory.find_by_id(@static_contest_history.id)).to be_nil
  end
end
