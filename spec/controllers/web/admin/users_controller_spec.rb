require 'spec_helper'

describe Web::Admin::UsersController do
  before do
    @user = create :user
    @attrs = attributes_for :user
    @params = { id: @user.id }
  end

  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get show" do
    get :show, @params
    expect(response).to be_success
  end

  it "get new" do
    get :new
    expect(response).to be_success
  end

  it "create user" do
    post :create, @params.merge(user: @attrs)
    expect(response).to be_redirect
    expect(User.find_by_email(@attrs[:email])).to be
  end

  it "get edit" do
    get :edit, @params
    expect(response).to be_success
  end

  it "update user" do
    put :update, @params.merge(user: @attrs)
    expect(response).to be_redirect
    expect(User.find_by_email(@attrs[:email])).to be
  end

  it "destroy user" do
    delete :destroy, @params
    expect(response).to be_redirect
    expect(User.find_by_id(@user.id)).to be_nil
  end
end
