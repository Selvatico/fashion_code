require 'spec_helper'

describe Web::Admin::VsTopSettingsController do
  it "get index" do
    get :index
    expect(response).to be_success
  end

  it "get edit" do
    get :edit
    expect(response).to be_success
  end
end
