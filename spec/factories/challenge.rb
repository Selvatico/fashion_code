FactoryGirl.define do
  factory :challenge do
    user
    photo
    challenge_type
    name
    finished false
  end
end
