FactoryGirl.define do
  factory :challenge_type do
    fashion_style
    association :opponent_fashion_style, factory: :fashion_style

    easy_score { generate :n }
    medium_score { generate :n }
    hard_score { generate :n }
  end
end
