FactoryGirl.define do
  factory :comment do
    user
    photo
    comment { generate :text }
  end
end
