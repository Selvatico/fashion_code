FactoryGirl.define do
  factory :contest do
    name
    started_at { DateTime.now }
    expired_at { 1.day.from_now }

    after(:create) do |contest|
      FactoryGirl.create :prize, contest: contest
    end

    factory :joined_contest do
      ignore {user nil}
      after(:create) do |contest, evaluator|
        FactoryGirl.create :participant, contest: contest, user: evaluator.user
      end
    end

    factory :contest_with_prizes do
      ignore do
        prizes_count 2
      end
      after(:create) do |contest, evaluator|
        FactoryGirl.create_list(:prize, evaluator.prizes_count, contest: contest)
      end
    end
  end
end
