FactoryGirl.define do
  factory :fashion_style do
    name
    presentation { generate :string }
    image
  end
end
