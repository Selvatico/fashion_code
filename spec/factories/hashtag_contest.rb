FactoryGirl.define do
  factory :hashtag_contest, class: HashtagContest do
    association :owner, :factory => :user
    sequence(:name) { |n| "name_#{n}"}

    after(:create) do |contest|
      FactoryGirl.create :hashtag_participant, contest: contest
    end

  end
end
