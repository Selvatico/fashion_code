FactoryGirl.define do
  factory :hashtag_participant, class: HashtagParticipant do
    user
    association :contest, :factory => :hashtag_contest
  end
end
