FactoryGirl.define do
  factory :inappropriate do
    user
    association :inappropriateable, factory: :photo
  end
end
