FactoryGirl.define do
  factory :invitation, class: Invitation do
    user
    sequence(:email) { |n| "invited_user#{n}@modera.co"}
  end
end
