FactoryGirl.define do
  factory :member do
    avatar
    background
    fullname
    position { generate :string }
    ord { generate :n }
  end
end
