FactoryGirl.define do
  factory :page do
    link "about"
    title
    content
  end
end
