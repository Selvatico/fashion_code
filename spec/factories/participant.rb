FactoryGirl.define do
  factory :participant, class: Participant do
    user
    contest
    votes_score {Random.rand(1000)}
  end
end
