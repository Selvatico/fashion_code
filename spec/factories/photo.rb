FactoryGirl.define do
  factory :photo do
    user
    image
    deleted false
  end
end
