FactoryGirl.define do
  factory :prize, class: Prize do
    sequence(:name) { |n| "Prize#{n}"}
  end
end
