FactoryGirl.define do
  sequence :integer, aliases: [:n] do |n|
    n
  end

  sequence :date do |n|
    Date.today - 1000 - n
  end

  sequence :name, aliases: [:title, :fullname] do |n|
    "name-#{n}"
  end

  sequence :slug do |n|
    "slug#{n}"
  end

  sequence :string do |n|
    "string-#{n}"
  end

  sequence :text, aliases: [:content] do |n|
    FactoryGirl.generate(:string) * 100
  end

  sequence :email do |n|
    "email-#{n}@example.com"
  end

  sequence :url do |n|
    "http://url-#{n}.com/url#{n}"
  end

  sequence :image, aliases: [:photo, :logo, :avatar, :background] do
    file_name = 'image.jpg'
    fixture_file_upload("#{Rails.root}/spec/fixtures/#{file_name}")
  end
end
