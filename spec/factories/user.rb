FactoryGirl.define do
  factory :user do
    fashion_style
    email
    password "password"
    sequence(:fullname) { |n| "Modera Test User#{n}"}
    #avatar {fixture_file_upload 'spec/support/fashion_styles/image.jpg'}
    sequence(:username) { |n| "test_user#{n}"}

    #TODO: fix me
    factory :another_user do
      username "user3"
      email "user3@gmail.com"
      password  "test12345"
    end

    factory :user_with_photos do
      ignore do
        photos_count 3
      end
      after(:create) do |user, evaluator|
        FactoryGirl.create_list(:photo, evaluator.photos_count, user: user)
      end
    end
  end
end
