FactoryGirl.define do
  factory :user_authentication, class: UserAuthentication do
    association :user, :factory => :user
    provider 'facebook'
    sequence(:uid) { |n| "uid#{n}"}
    sequence(:token) { |n| "token#{n}"}
    sequence(:secret_token) { |n| "secret_token#{n}"}
  end
end
