FactoryGirl.define do
  factory :vote, class: Vote do
    association :voter, :factory => :user
    association :voted_photo, :factory => :photo
    association :unvoted_photo, :factory => :photo
    challenge
    #hashtag_contest
    vote_score 1
    unvote_score 1
    voted_photo_score 1
    unvoted_photo_score 1
    challenge_votes_count 1
  end
end
