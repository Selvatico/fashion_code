require "spec_helper"

describe Photo do
  describe 'for_versus_list' do
    it 'get empty array if photos count less then limit' do
      limit = 10
      photos_count = 3

      create_list :photo, photos_count
      photos = Photo.for_versus_list limit: limit

      expect(photos).to be_empty
    end

    it 'limit photos count' do
      limit = 2
      photos_count = 3

      create_list :photo, photos_count
      photos = Photo.for_versus_list limit: limit

      expect(photos.count).to eq(limit)
    end

    it 'get photos for users' do
      user_photos_count = 3
      user = create :user
      create_list :photo, user_photos_count, user: user
      another_photo = create :photo

      photos = Photo.for_versus_list users: [user.id], limit: user_photos_count

      expect(photos).not_to include(another_photo)
    end

    it 'exclude photos' do
      photos_count = 3

      create_list :photo, photos_count
      excluded_photo = create :photo

      photos = Photo.for_versus_list excluded_photos: [excluded_photo.id], limit: photos_count

      expect(photos).not_to include(excluded_photo)
    end

    it 'get photos except users' do
      user_photos_count = 3
      user = create :user
      create_list :photo, user_photos_count, user: user
      another_photo = create :photo

      photos = Photo.for_versus_list excluded_users: [another_photo.user_id], limit: user_photos_count

      expect(photos).not_to include(another_photo)
    end

    it 'get photos for one users and except another users' do
      user_photos_count = 3

      user = create :user
      create_list :photo, user_photos_count, user: user

      excluded_user = create :user
      excluded_photo = create :photo, user: excluded_user

      another_photo = create :photo

      photos = Photo.for_versus_list users: [user.id],
                                     excluded_users: [another_photo.user_id],
                                     limit: user_photos_count

      expect(photos).not_to include(another_photo)
      expect(photos).not_to include(excluded_photo)
    end

    it 'get photos by tags' do
      photos_count = 3
      tags = %w(one two three)
      create_list :photo, photos_count, tag_list: tags
      excluded_photo = create :photo

      photos = Photo.for_versus_list tags: tags, limit: photos_count

      expect(photos).not_to include(excluded_photo)
    end
  end
end
