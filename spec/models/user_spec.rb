require "spec_helper"

describe User do
  describe 'versus list' do
    before do
      @list_size = 3
      @user = create :user
      tags = 'one, two, three'
      @user_photo = create :photo, user: @user, tag_list: tags


      #first page
      @following_photos = create_list :photo, @list_size * 2
      @following_photos.each{ |p| @user.following << p.user }
      #second page
      @tagged_photos = create_list :photo, @list_size * 2, tag_list: tags
      #third page
      @photos = create_list :photo, @list_size * 2

      @voted_photo = create :photo
      create :vote, voter: @user, voted_photo: @voted_photo
    end

    it "contains the correct number of photos" do
      list = @user.photo_versus_list(0, @list_size)
      expect(list.size).to eq(@list_size)
    end

    it "contains uniq photos" do
      list = @user.photo_versus_list(0, @list_size)
      uniq_photos = list.flatten.uniq

      expect(uniq_photos.size).to eq(@list_size * 2)
    end

    it "contains correct photos" do
      list = @user.photo_versus_list(0, @list_size * 3)
      left_photos = list.map(&:first)
      photos = list.flatten

      expect(photos).not_to include(@user_photo)
      expect(left_photos).not_to include(@voted_photo)
    end

    it "contains photos of friends on the first page" do
      list = @user.photo_versus_list(0, @list_size)
      photos = list.flatten

      expect(photos).to match_array(@following_photos)
    end
  end

  describe 'versus list for user without photos' do
    before do
      @list_size = 3
      @user = create :user
      @photos = create_list :photo, @list_size * 2

      @list = @user.photo_versus_list(0, @list_size)
    end

    it "contains the correct number of photos" do
      expect(@list.size).to eq(@list_size)
    end
  end
end
