require([
	'loader',
	'backbone',
	'layoutmanager',
	'routemanager',
	'validation',
    'backcookie'
], function( loader ) {

	'use strict';

	console.log( 'All the Backbone plugins are ready.' );
	loader.loaded( 'backbonePlugins' );

});