require([
	'loader',
	'jquery',
	'arcticmodal'
], function( loader ) {

	'use strict';

	console.log( 'All the jQuery plugins are ready.' );
	loader.loaded( 'jQueryPlugins' );

});