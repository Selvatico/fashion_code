/**
 * Small service plugin to controll cookies via javascript
 * @author dseredenko
 * @exports Backbone.Cookie
 * @namespace Backbone
 * @requires Backbone, Underscore
 */
define([
    'backbone',
    'underscore'
], function ( Backbone ) {
    return _.extend(Backbone, {
        Cookie : {
            getCookie: function ( name ) {
                var matches = document.cookie.match(new RegExp(
                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ));
                return matches ? decodeURIComponent( matches[1] ) : false ;
            },
            /**
             * Set cookie one by one
             * @param {String} name Cookie key name
             * @param {String} value Cookie value
             * @param {Object} props Additional cookie params
             */
            setCookie: function ( name, value, props) {
                props = props || {};
                var exp = props.expires;
                if (typeof exp == "number" && exp) {
                    var d = new Date();
                    d.setTime( d.getTime() + exp*1000 );
                    exp = props.expires = d;
                }
                if( exp && exp.toUTCString ) {
                    props.expires = exp.toUTCString();
                }

                value = encodeURIComponent( value );
                var updatedCookie = name + "=" + value;
                for(var propName in props){
                    updatedCookie += "; " + propName;
                    var propValue = props[propName];
                    if(propValue !== true){
                        updatedCookie += "=" + propValue;
                    }
                }
                document.cookie   = updatedCookie;
                window.userCookie = updatedCookie;
            },
            /**
             * Set special generated list of cookies
             * @param {Object} cookieObject
             */
            setCookieList: function ( cookieObject ) {
                for ( var cookie in cookieObject ) {
                    if (cookieObject.hasOwnProperty( cookie )) {
                        Backbone.Cookie.setCookie(
                            cookie, cookieObject[cookie].value, {path: cookieObject[cookie].path }
                        );
                    }
                }
            },
            /**
             * Delete cookie from the document object
             * @param name
             */
            deleteCookie: function ( name ) {
                this.setCookie(name, null, { expires: -1 })
            }
        }
    });
});